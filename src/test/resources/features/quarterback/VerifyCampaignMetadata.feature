Feature: Verify smoke scenarios of gemerated xml 


Scenario: Verify gemerated xml against the RNG schema
	Given generated XML available in the local path
	When the generated XML is validated against the RNG schema

@First	
Scenario: Verify the generated XML contains only required Agencies
	Given generated XML available in the local path
	When XML should not be generated without agency details
	Then expected agency details should not be empty
	And XML should contain only required Agency Ids
	
Scenario: Verify the generated XML should not contain additional Agencies
	Given generated XML available in the local path
	When XML should not be generated without agency details
	Then expected agency details should not be empty
	And XML should not contain additional Agencies
	
Scenario: Verify the generated XML contains only required Campaigns
	Given generated XML available in the local path
	When XML should not be generated without Campaign details
	Then expected Campaign details should not be empty
	And XML should contain only required Campaign Ids
	
Scenario: Verify the generated XML should not contain additional Campaigns
	Given generated XML available in the local path
	When XML should not be generated without Campaign details
	Then expected Campaign details should not be empty
	And XML should not contain additional Campaigns

Scenario: Verify the generated XML contains only required Placements
	Given generated XML available in the local path
	When XML should not be generated without Placement details
	Then expected Placement details should not be empty
	And XML should contain only required Placement Ids
	
Scenario: Verify the generated XML should not contain additional Placements
	Given generated XML available in the local path
	When XML should not be generated without Placement details
	Then expected Placement details should not be empty
	And XML should not contain additional Placements
	
Scenario: Verify the generated XML contains only required Placements
	Given generated XML available in the local path
	When XML should not be generated without Placement details
	Then expected Placement details should not be empty
	And XML should contain only required Placement Ids

@Last	
Scenario: Verify the generated XML should not contain additional Ads
	Given generated XML available in the local path
	When XML should not be generated without Ads details
	Then expected Ads details should not be empty
	And XML should not contain additional Ads
	