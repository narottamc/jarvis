Feature: Verify Generated Campaign XML For Different Business Models for ProductType Survey

  Scenario: Verify XML File With RNG Schema
    Given A Campaign XML is available at local file path
    When XML is validated with latest RNG Schema file
    Then Campaign XML schema should be as per the supplied RNG Schema file.

   @Scenario31
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario31
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
     And User validates the SSP count for placement in XML
     And USer validates the SSP Details for placement in XML
     And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario32
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario32
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario33
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario33
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.