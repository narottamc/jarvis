Feature: Verify Generated Campaign XML For Different Business Models for ProductType Alchemy

  Scenario: Verify XML File With RNG Schema
    Given A Campaign XML is available at local file path
    When XML is validated with latest RNG Schema file
    Then Campaign XML schema should be as per the supplied RNG Schema file.

  @Scenario16
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario16
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario17
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario17
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario18
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario18
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario19
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario19
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario20
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario20
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario21
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario21
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario22
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario22
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario23
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario23
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario24
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario24
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario25
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario25
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario26
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario26
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario27
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario27
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario28
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario28
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario29
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario29
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario30
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario30
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the SSP count for placement in XML
    And USer validates the SSP Details for placement in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.
