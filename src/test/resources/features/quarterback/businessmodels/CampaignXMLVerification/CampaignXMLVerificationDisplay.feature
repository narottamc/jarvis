Feature: Verify Generated Campaign XML For Different Business Models for ProductType Display

  Scenario: Verify XML File With RNG Schema
    Given A Campaign XML is available at local file path
    When XML is validated with latest RNG Schema file
    Then Campaign XML schema should be as per the supplied RNG Schema file.


  @Scenario34
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario34
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario35
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario35
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario36
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario36
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario37
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario37
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario38
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario38
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario39
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario39
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario40
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario40
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario41
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario41
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario42
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario42
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario43
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario43
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario44
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario44
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario45
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario45
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario46
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario46
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario47
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario47
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario48
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario48
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario49
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario49
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario50
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario50
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.

  @Scenario51
  Scenario: Verify Campaign Details in XML for Campaign Created with OSAPI in Scenario51
    Given User is able to extract the Campaign Details From XML
    When User validates the advertiser information in Campaign XML
    Then User validates the placement and advertiser information in XML
    And User validates campaign tag details for placements in XML
    And User validates the Targeting Details for placement in XML
    And User validates the total LineItems count of a placement in XML
    And User validates the LineItems details of a placement in XML
    And User validates the total Creatives count of a placement in XML
    And User validates the Creatives details of a placement in XML
    And All validations of Campaign in XML should pass successfully.