Feature: Launch Created Campaigns For ProductType Video

  @Scenario3
  Scenario: Verify User launches A Valid Campaign created for Scenario1.
    Given user has valid login credential as "LAUNCH" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
#    Then user request the Launch Campaign API with POST method
    Then user request the Launch Campaign API with POST method to launch campaign
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

