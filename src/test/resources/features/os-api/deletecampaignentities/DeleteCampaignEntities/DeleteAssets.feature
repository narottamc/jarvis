Feature: Verify Delete Method for APIs of the different Types of Assets

   ##Delete Campaign Assets with Campaign in Draft State
  @Scenario2
  Scenario: Verify Delete Method for API of Assets of ClearStream Type for Campaign in Draft State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "CLEARSTREAMADTAG" type Video Asset
    And user request the Asset API with DELETE method to delete 1 video assets
    And response should indicate that "CLEARSTREAMADTAG" Type video assets deleted

  @Scenario4
  Scenario: Verify Delete Method for API of Assets of ThirdPartyAdTag Type for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "THIRDPARTYADTAG" type Video Asset
    And user request the Asset API with DELETE method to delete 1 video assets
    And response should indicate that "THIRDPARTYADTAG" Type video assets deleted

  @Scenario8
  Scenario: Verify Delete Method for API of Display Assets of Script Type for Campaign in Draft State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "SCRIPTTAG" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "SCRIPTTAG" Type display assets deleted

  @Scenario8
  Scenario: Verify Delete Method for API of Display Assets of Image Type for Campaign in Draft State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "IMAGE" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "IMAGE" Type display assets deleted

  @Scenario8
  Scenario: Verify Delete Method for API of Display Assets of ZIPFile Type for Campaign in Draft State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "ZIPFILE" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "ZIPFILE" Type display assets deleted

  @Scenario6
  Scenario: Verify Delete Method for API of Survey Assets for Campaign in Draft State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having Survey Assets
    And user request the Asset API with DELETE method to delete 2 survey assets
    And response should indicate that survey assets deleted


  ##Delete Campaign Assets with Campaign in Active State
  @Scenario1
  Scenario: Verify Delete Method for API of Assets of ClearStream Type for Campaign in Active State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "CLEARSTREAMADTAG" type Video Asset
    And user request the Asset API with DELETE method to delete 1 video assets
    And response should indicate that "CLEARSTREAMADTAG" Type video assets deleted

  @Scenario3
  Scenario: Verify Delete Method for API of Assets of ThirdPartyAdTag Type for Campaign in Active State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "THIRDPARTYADTAG" type Video Asset
    And user request the Asset API with DELETE method to delete 1 video assets
    And response should indicate that "THIRDPARTYADTAG" Type video assets deleted

  @Scenario7
  Scenario: Verify Delete Method for API of Display Assets of Script Type for Campaign in Active State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "SCRIPTTAG" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "SCRIPTTAG" Type display assets deleted

  @Scenario7
  Scenario: Verify Delete Method for API of Display Assets of Image Type for Campaign in Active State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "IMAGE" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "IMAGE" Type display assets deleted

  @Scenario7
  Scenario: Verify Delete Method for API of Display Assets of ZIPFile Type for Campaign in Active State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having a "ZIPFILE" type Display Asset
    And user request the Asset API with DELETE method to delete 1 display assets
    And response should indicate that "ZIPFILE" Type display assets deleted

  @Scenario5
  Scenario: Verify Delete Method for API of Survey Assets for Campaign in Active State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having Survey Assets
    And user request the Asset API with DELETE method to delete 2 survey assets
    And response should indicate that survey assets deleted
