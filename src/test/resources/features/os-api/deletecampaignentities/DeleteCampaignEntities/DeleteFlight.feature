Feature: Verify Delete Method for APIs of the different Types of Flights

  ##Delete Flights of A Placement With Campaign in Draft State
  @Scenario4
  Scenario: Verify Delete Method for Flights API of Placement With ProductType VIDEO for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "VIDEO" with more than one flight
    And user request the Flights API with DELETE method to delete 1 flights of "Placement2"
    And response should indicate that flight deleted from the placement

  @Scenario8
  Scenario: Verify Delete Method for Flights API of Placement With ProductType DISPLAY for Campaign in Draft State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "DISPLAY" with more than one flight
    And user request the Flights API with DELETE method to delete flights as following
      | Placement1           | Flight1        |
    And response should indicate that flight deleted from the placement

  @Scenario6
  Scenario: Verify Delete Method for Flights API of Placement With ProductType SURVEY for Campaign in Draft State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "SURVEY" with more than one flight
    And user request the Flights API with DELETE method to delete 1 flights of "Placement2"
    And response should indicate that flight deleted from the placement

  @Scenario2
  Scenario: Verify Delete Method for Flights API of Placement With ProductType ALCHEMY for Campaign in Draft State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "ALCHEMY" with more than one flight
    And user request the Flights API with DELETE method to delete flights as following
      | Placement2           | Flight2        |
    And response should indicate that flight deleted from the placement


    ##Delete Flights of A Placement With Campaign in Active State
  @Scenario3
  Scenario: Verify Delete Method for Flights API of Placement With ProductType VIDEO for Campaign in Active State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "VIDEO" with more than one flight
    And user request the Flights API with DELETE method to delete 1 flights of "Placement1"
    And response should indicate that flight deleted from the placement

  @Scenario7
  Scenario: Verify Delete Method for Flights API of Placement With ProductType DISPLAY for Campaign in Active State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "DISPLAY" with more than one flight
    And user request the Flights API with DELETE method to delete flights as following
      | Placement1           | Flight1        |
    And response should indicate that flight deleted from the placement

  @Scenario5
  Scenario: Verify Delete Method for Flights API of Placement With ProductType SURVEY for Campaign in Active State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "SURVEY" with more than one flight
    And user request the Flights API with DELETE method to delete 1 flights of "Placement2"
    And response should indicate that flight deleted from the placement

  @Scenario1
  Scenario: Verify Delete Method for Flights API of Placement With ProductType ALCHEMY for Campaign in Active State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placement of product type "ALCHEMY" with more than one flight
    And user request the Flights API with DELETE method to delete flights as following
      | Placement2           | Flight2        |
    And response should indicate that flight deleted from the placement
