Feature: Verify Delete Method for APIs of the Campaign

  @Scenario4
  Scenario: Verify Delete Method for API of Campaign having placement of Product Type VIDEO for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placements of product type "VIDEO" in draft state
    And user request the Campaign API with DELETE method to delete campaign
    And response should indicate that campaign deleted

  @Scenario8
  Scenario: Verify Delete Method for API of Campaign having placement of Product Type DISPLAY for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placements of product type "DISPLAY" in draft state
    And user request the Campaign API with DELETE method to delete campaign
    And response should indicate that campaign deleted

  @Scenario6
  Scenario: Verify Delete Method for API of Campaign having placement of Product Type SURVEY for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placements of product type "SURVEY" in draft state
    And user request the Campaign API with DELETE method to delete campaign
    And response should indicate that campaign deleted

  @Scenario2
  Scenario: Verify Delete Method for API of Campaign having placement of Product Type ALCHEMY for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having placements of product type "ALCHEMY" in draft state
    And user request the Campaign API with DELETE method to delete campaign
    And response should indicate that campaign deleted
