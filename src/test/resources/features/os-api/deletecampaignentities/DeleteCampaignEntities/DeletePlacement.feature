Feature: Verify Delete Method for APIs of the different Types of Placements

  ##Delete Placements For Campaign in Draft State
  @Scenario4
  Scenario: Verify Delete Method for API of Placement of Product Type Video for Campaign in Draft State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "VIDEO"
    And user request the Placement API with DELETE method to delete 1 placement
    And response should indicate that placements of product type "VIDEO" deleted

  @Scenario8
  Scenario: Verify Delete Method for API of Placement of Product Type Display for Campaign in Draft State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "DISPLAY"
    And user request the Placement API to delete below placements of above product type
      | Placement1 |
    And response should indicate that placements of product type "DISPLAY" deleted

  @Scenario6
  Scenario: Verify Delete Method for API of Placement of Product Type Survey for Campaign in Draft State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "SURVEY"
    And user request the Placement API with DELETE method to delete 1 placement
    And response should indicate that placements of product type "SURVEY" deleted

  @Scenario2
  Scenario: Verify Delete Method for API of Placement of Product Type Alchemy for Campaign in Draft State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "ALCHEMY"
    And user request the Placement API to delete below placements of above product type
      | Placement2 |
    And response should indicate that placements of product type "ALCHEMY" deleted

    ##Delete Placements For Campaign in Active State
  @Scenario3
  Scenario: Verify Delete Method for API of Placement of Product Type Video for Campaign in Active State
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "VIDEO"
    And user request the Placement API with DELETE method to delete 1 placement
    And response should indicate that placements of product type "VIDEO" deleted

  @Scenario7
  Scenario: Verify Delete Method for API of Placement of Product Type Display for Campaign in Active State
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "DISPLAY"
    And user request the Placement API to delete below placements of above product type
      | Placement2 |
    And response should indicate that placements of product type "DISPLAY" deleted

  @Scenario5
  Scenario: Verify Delete Method for API of Placement of Product Type Survey for Campaign in Active State
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "SURVEY"
    And user request the Placement API with DELETE method to delete 1 placement
    And response should indicate that placements of product type "SURVEY" deleted

  @Scenario1
  Scenario: Verify Delete Method for API of Placement of Product Type Alchemy for Campaign in Active State
    Given user has valid login credential as "ALCHEMY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user has a campaign created having more than one placements of product type "ALCHEMY"
    And user request the Placement API to delete below placements of above product type
      | Placement2 |
    And response should indicate that placements of product type "ALCHEMY" deleted