Feature: Create Users for Various Product Types And Generate Master Data Sources for Targeting Attributes.

  Scenario: Create Users for Product Type Video,Survey,Display & Alchemy

    Given user has valid login credential as "GUEST" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user validates if credentials with access to different product types should exist
    And user create credentials with access according to product types


  ##This Scenario Outline is used to get the informartion of various Targeting Entities from their MASTER APIs , the data is stored
  ## at the "src/test/resources/targetingdatasources" , It will be useful while validating the information in XML.
  Scenario Outline: Generate data for Targeting Attributes from Master APIs
    Given user has valid login credential as "GUEST" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the targeting master APIs for "<entityName>" with POST method
    And returned response should be extracted to a JSON file

    Examples:
      | entityName      |
      | LANGUAGES       |
      | REGIONS         |
      | VISIBILITIES    |
      | PLAYERSIZEGROUP |
      | COUNTRIES       |
      | PCATEGORIES     |
      | DMAS            |
      | INVENTORYTYPES  |
      | GEOLISTS        |


  Scenario: Generate data for DMP Segements Attributes from Master APIs

    Given user has valid login credential as "GUEST" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the dmp provider API with GET method
    And returned response of list of dmp providers should be extracted to a JSON File
    Then user request each dmp client in every dmp provider with GET method
    And returned response of segments for each dmp client should be extracted to a JSOON File


