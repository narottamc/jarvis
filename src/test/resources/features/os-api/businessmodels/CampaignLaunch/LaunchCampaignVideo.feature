Feature: Launch Created Campaigns For ProductType Video

  @Scenario1
  Scenario: Verify User launches A Valid Campaign created for Scenario1.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario2
  Scenario: Verify User launches A Valid Campaign created for Scenario2.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario3
  Scenario: Verify User launches A Valid Campaign created for Scenario3.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario4
  Scenario: Verify User launches A Valid Campaign created for Scenario4.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario5
  Scenario: Verify User launches A Valid Campaign created for Scenario5.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario6
  Scenario: Verify User launches A Valid Campaign created for Scenario6.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario7
  Scenario: Verify User launches A Valid Campaign created for Scenario7.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario8
  Scenario: Verify User launches A Valid Campaign created for Scenario8.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario9
  Scenario: Verify User launches A Valid Campaign created for Scenario9.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario10
  Scenario: Verify User launches A Valid Campaign created for Scenario10.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario11
  Scenario: Verify User launches A Valid Campaign created for Scenario11.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario12
  Scenario: Verify User launches A Valid Campaign created for Scenario12.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario13
  Scenario: Verify User launches A Valid Campaign created for Scenario13.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario14
  Scenario: Verify User launches A Valid Campaign created for Scenario14.
    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario15
  Scenario: Verify User launches A Valid Campaign created for Scenario15.
    Given user has valid login credential as "LAUNCH" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method to launch campaign
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully