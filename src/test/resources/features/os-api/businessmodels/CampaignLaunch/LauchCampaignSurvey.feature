Feature: Launch Created Campaigns For ProductType Survey

  @Scenario31
  Scenario: Verify User launches A Valid Campaign created for Scenario31.
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario32
  Scenario: Verify User launches A Valid Campaign created for Scenario32.
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario33
  Scenario: Verify User launches A Valid Campaign created for Scenario33.
    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully