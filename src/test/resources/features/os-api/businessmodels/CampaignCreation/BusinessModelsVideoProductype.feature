Feature: Create Campaigns with different Business Models For Video Product Type.

  ## Business Model Simple CPM with Margin Maximization
  @Scenario1
  Scenario: Campaign With Business Model "Simple CPM with Margin Maximization" with 1 Placement of ProductType VIDEO and have 1 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "Simple CPM with Margin Maximization" with strategy ID 1 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 1 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       1 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | VAST      |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  ##Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario2
  Scenario: Campaign with Business Model "Simple CPM with Margin Maximization" 1 Placement of ProductType VIDEO & 2 Flights
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "Simple CPM with Margin Maximization" with strategy ID 1 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 1 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario3
  Scenario: Campaign With Business Model "Simple CPM with Margin Maximization" with 2 Placement of ProductType VIDEO and have 2 Flight Each
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "Simple CPM with Margin Maximization" with strategy ID 1 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 1 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
      | Placement2      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
      | Placement2      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VPAIDNonLinear |          1 |
      | Flight2      | VPAIDLinear    |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   |       96700 |    17062117 |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  # Business Model CPM WITH COMPLETED VIEW RATE
  @Scenario4
  Scenario: Campaign With Business Model "CPM WITH COMPLETED VIEW RATE" with 1 Placement of ProductType VIDEO and have 1 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM WITH COMPLETED VIEW RATE" with strategy ID 2 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 2 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       1 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | VAST      |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario5
  Scenario: Campaign With Business Model "CPM WITH COMPLETED VIEW RATE" with 1 Placement of ProductType VIDEO and have 2 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM WITH COMPLETED VIEW RATE" with strategy ID 2 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 2 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario6
  Scenario: Campaign With Business Model "CPM WITH COMPLETED VIEW RATE" with 2 Placement of ProductType VIDEO and have 2 Flight Each
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM WITH COMPLETED VIEW RATE" with strategy ID 2 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 2 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
      | Placement2      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
      | Placement2      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VPAIDNonLinear |          1 |
      | Flight2      | VPAIDLinear    |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   |       96700 |    17062117 |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  # Business Model : CPM with CTR
  @Scenario7
  Scenario: Campaign With Business Model "CPM with CTR" with 1 Placement of ProductType VIDEO and have 1 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR" with strategy ID 3 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 3 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       1 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | VAST      |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario8
  Scenario: Campaign With Business Model "CPM with CTR" with 1 Placement of ProductType VIDEO and have 2 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR" with strategy ID 3 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 3 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario9
  Scenario: Campaign With Business Model "CPM with CTR" with 2 Placement of ProductType VIDEO and have 2 Flight Each
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR" with strategy ID 3 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 3 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
      | Placement2      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
      | Placement2      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VPAIDNonLinear |          1 |
      | Flight2      | VPAIDLinear    |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   |       96700 |    17062117 |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  #Business Model : CPM with CTR and Completed View Rate
  @Scenario10
  Scenario: Campaign With Business Model "CPM with CTR and Completed View Rate" with 1 Placement of ProductType VIDEO and have 1 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR and Completed View Rate" with strategy ID 4 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 4 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       1 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | VAST      |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario11
  Scenario: Campaign With Business Model "CPM with CTR and Completed View Rate" with 1 Placement of ProductType VIDEO and have 2 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR and Completed View Rate" with strategy ID 4 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 4 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario12
  Scenario: Campaign With Business Model "CPM with CTR and Completed View Rate" with 2 Placement of ProductType VIDEO and have 2 Flight Each
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR and Completed View Rate" with strategy ID 4 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 4 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
      | Placement2      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
      | Placement2      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VPAIDNonLinear |          1 |
      | Flight2      | VPAIDLinear    |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   |       96700 |    17062117 |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200

  #Business Model : CPCV with Margin Maximization
  @Scenario13
  Scenario: Campaign With Business Model "CPCV with Margin Maximization" with 1 Placement of ProductType VIDEO and have 1 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPCV with Margin Maximization" with strategy ID 5 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 5 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       1 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | VAST      |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario14
  Scenario: Campaign With Business Model "CPCV with Margin Maximization" with 1 Placement of ProductType VIDEO and have 2 Flight
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPCV with Margin Maximization" with strategy ID 5 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 5 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
  #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
  @Scenario15
  Scenario: Campaign With Business Model "CPCV with Margin Maximization" with 2 Placement of ProductType VIDEO and have 2 Flight Each
    1. ClearStream Asset (One) with Mp4 Creative of Size 300X250
    2. Third party Asset (Two) Size : 160x600 And 250x360
    3. SSP (Bidswitch)

    Given user has valid login credential as "VIDEO" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPCV with Margin Maximization" with strategy ID 5 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 5 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDNonLinear | ThirdPartyAdTag  | Size2     |
      | Asset3      | VPAIDLinear    | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       |          1 |           2 |            |
      | Placement2      | VIDEO       |          1 |           2 |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      |       2 |
      | Placement2      |       2 |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        |          1 |
      | Flight2      | VPAIDLinear |          1 |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VPAIDNonLinear |          1 |
      | Flight2      | VPAIDLinear    |          1 |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   |       96700 |    17062117 |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
