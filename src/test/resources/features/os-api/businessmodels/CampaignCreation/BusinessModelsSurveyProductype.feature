Feature: Create Campaigns with different Business Models For Survey Product Type.

  ### Business Model CPM with Margin Maximization
  @Scenario31
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 1 Placement of ProductType SURVEY and have 1 Flight
  1. Survey With Single Answers (Two) with Size : 300x250 And 250x360
  2. Survey With Multiple Answers (Two) with Size : 160x600 And 300x1050
  3. SSP (Bidswitch)

    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user posts a POST request to Survey EndPoint URL to add surveys as follows:
      | SurveyNumber | AnswerType | CustomResponse | AssetSize |
      | Survey1      | Single     | No             | Size1     |
      | Survey2      | Multiple   | No             | Size2     |
      | Survey3      | Single     | Yes            | Size3     |
      | Survey4      | Multiple   | Yes            | Size4     |
    And Survey response should have status code as 200 and content-type as JSON.
    And returned response should indicate surveys created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | SURVEY      |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | SURVEY      |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario32
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 1 Placement of ProductType SURVEY and have 2 Flight
  1. Survey With Single Answers (Two) with Size : 300x250 And 250x360
  2. Survey With Multiple Answers (Two) with Size : 160x600 And 300x1050
  3. SSP (Bidswitch)

    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user posts a POST request to Survey EndPoint URL to add surveys as follows:
      | SurveyNumber | AnswerType | CustomResponse | AssetSize |
      | Survey1      | Single     | No             | Size1     |
      | Survey2      | Multiple   | No             | Size2     |
      | Survey3      | Single     | Yes            | Size3     |
      | Survey4      | Multiple   | Yes            | Size4     |
    And Survey response should have status code as 200 and content-type as JSON.
    And returned response should indicate surveys created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | SURVEY      |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | SURVEY      |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
      | Flight2      | SURVEY    | 1          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario33
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 2 Placement of ProductType SURVEY and have 2 Flight Each
  1. Survey With Single Answers (Two) with Size : 300x250 And 250x360
  2. Survey With Multiple Answers (Two) with Size : 160x600 And 300x1050
  3. SSP (Bidswitch)

    Given user has valid login credential as "SURVEY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user posts a POST request to Survey EndPoint URL to add surveys as follows:
      | SurveyNumber | AnswerType | CustomResponse | AssetSize |
      | Survey1      | Single     | No             | Size1     |
      | Survey2      | Multiple   | No             | Size2     |
      | Survey3      | Single     | Yes            | Size3     |
      | Survey4      | Multiple   | Yes            | Size4     |
    And Survey response should have status code as 200 and content-type as JSON.
    And returned response should indicate surveys created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | SURVEY      |
      | Placement2      | SURVEY      |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | SURVEY      |            |             | 2          |
      | Placement2      | SURVEY      |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
      | Flight2      | SURVEY    | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
      | Flight2      | SURVEY    | 1          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
