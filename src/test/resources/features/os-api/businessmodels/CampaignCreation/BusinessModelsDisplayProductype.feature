Feature: Create Campaigns with different Business Models For Display Product Type.

  ### Business Model CPM With Margin Maximization
  @Scenario34
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario35
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario36
  Scenario: Campaign With Business Model "CPM with Margin Maximization" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  ### Business Model CPM with CTR Maximization
  @Scenario37
  Scenario: Campaign With Business Model "CPM with CTR Maximization" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR Maximization" with strategy ID 8 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 8 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario38
  Scenario: Campaign With Business Model "CPM with CTR Maximization" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR Maximization" with strategy ID 8 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 8 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario39
  Scenario: Campaign With Business Model "CPM with CTR Maximization" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with CTR Maximization" with strategy ID 8 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 8 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  ### Business Model  CPC with Margin Maximization
  @Scenario40
  Scenario: Campaign With Business Model "CPC with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPC with Margin Maximization" with strategy ID 9 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 9 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario41
  Scenario: Campaign With Business Model "CPC with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPC with Margin Maximization" with strategy ID 9 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 9 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario42
  Scenario: Campaign With Business Model "CPC with Margin Maximization" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPC with Margin Maximization" with strategy ID 9 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 9 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  ### Business Mode CPM with Action Maximization
  @Scenario43
  Scenario: Campaign With Business Model "CPM with Action Maximization" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Action Maximization" with strategy ID 10 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 10 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario44
  Scenario: Campaign With Business Model "CPM with Action Maximization" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Action Maximization" with strategy ID 10 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 10 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario45
  Scenario: Campaign With Business Model "CPM with Action Maximization" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Action Maximization" with strategy ID 10 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 10 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  ### Business Model CPA with Margin Maximization
  @Scenario46
  Scenario: Campaign With Business Model "CPA with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA with Margin Maximization" with strategy ID 11 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 11 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario47
  Scenario: Campaign With Business Model "CPA with Margin Maximization" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA with Margin Maximization" with strategy ID 11 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 11 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario48
  Scenario: Campaign With Business Model "CPA with Margin Maximization" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA with Margin Maximization" with strategy ID 11 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 11 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  ### Business Model CPA Learning
  @Scenario49
  Scenario: Campaign With Business Model "CPA Learning" with 1 Placement of ProductType DISPLAY and have 1 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA Learning" with strategy ID 12 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 12 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario50
  Scenario: Campaign With Business Model "CPA Learning" with 1 Placement of ProductType DISPLAY and have 2 Flight
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA Learning" with strategy ID 12 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 12 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario51
  Scenario: Campaign With Business Model "CPA Learning" with 2 Placement of ProductType DISPLAY and have 2 Flight Each
  1. Display With Script Tag Asset (One) Size : 300x250
  2. Display With Image Type Asset (One) Size : 160x600
  3. Display With ZIP Type Asset (One) Size : 250x360
  4. SSP (Bidswitch)

    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPA Learning" with strategy ID 12 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 12 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 10 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 5 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | Yes       | No        |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
