Feature: Verify Create campaign Feature with valid data & multiple placement/flight for ClearStream-OS


  @Scenario1
  Scenario: Create Campaign With Product Type VIDEO
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "Simple CPM with Margin Maximization" with strategy ID 1 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 1 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag |           |
      | Asset2      | VPAIDLinear    | ThirdPartyAdTag  |           |
      | Asset3      | VPAIDNonLinear | ThirdPartyAdTag  |           |
      | Asset4      | VAST           | ClearStreamAdTag |           |
      | Asset5      | VPAIDLinear    | ThirdPartyAdTag  |           |
      | Asset6      | VPAIDNonLinear | ThirdPartyAdTag  |           |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | VIDEO       |
      | Placement2      | VIDEO       |
      | Placement3      | VIDEO       |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | VIDEO       | 1          | 2           |            |
      | Placement2      | VIDEO       | 1          | 1           |            |
      | Placement3      | VIDEO       | 2          | 4           |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 3       |
      | Placement2      | 2       |
      | Placement3      | 3       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VAST           | 1          |
      | Flight2      | VPAIDLinear    | 1          |
      | Flight3      | VPAIDNonLinear | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        | 1          |
      | Flight2      | VPAIDLinear | 1          |
    Then user post a PUT request to associate flights of "Placement3" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VAST           | 2          |
      | Flight2      | VPAIDLinear    | 2          |
      | Flight3      | VPAIDNonLinear | 2          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | No        | Yes       |
      | Placement3      | Yes       | Yes       |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
      | Placement3      | true          | true(true)      | true  | true(true)   | 99099       | 17062173    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | Yes               |
      | Placement3      | Yes          | Yes  | Yes        | Yes      | Yes               |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
    #Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200

  @Scenario2
  Scenario: Create Campaign With Product Type SURVEY
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user posts a POST request to Survey EndPoint URL to add surveys as follows:
      | SurveyNumber | AnswerType | CustomResponse | AssetSize |
      | Survey1      | Single     | No             | Size1     |
      | Survey2      | Multiple   | No             | Size2     |
      | Survey3      | Single     | Yes            | Size3     |
      | Survey4      | Multiple   | Yes            | Size4     |
    And Survey response should have status code as 200 and content-type as JSON.
    And returned response should indicate surveys created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | SURVEY      |
      | Placement2      | SURVEY      |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | SURVEY      |            |             | 2          |
      | Placement2      | SURVEY      |            |             | 2          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 2       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
      | Flight2      | SURVEY    | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "SURVEY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | SURVEY    | 1          |
      | Flight2      | SURVEY    | 1          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | No        | Yes       |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario3
  Scenario: Create Campaign With Product Type DISPLAY
    Given user has valid login credential as "DISPLAY" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "CPM with Margin Maximization" with strategy ID 7 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 7 assigned to campaign
    Then user POST a POST request to Display EndPoint URL to add display assets as follows:
      | DisplayNumber | Type      | AssetSize |
      | Display1      | ScriptTag | Size1     |
      | Display2      | Image     | Size2     |
      | Display3      | ZIP       | Size3     |
    And Display response should have status code as 200 and content-type as JSON.
    And returned response should indicate display assets created.
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | DISPLAY     |
      | Placement2      | DISPLAY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | DISPLAY     |            |             | 2          |
      | Placement2      | DISPLAY     |            |             | 1          |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the activity pixel API with POST Method to create 5 activity pixels of "RETARGETING" type
    And returned response should indicate activity pixels created
    Then user request the activity pixel API with PUT Method to update created pixels with additional details
    And returned response should indicate activity pixels updated with additional information
    Then user request conversion pixel API with PUT method to assign 3 activity pixels to all placements
    And returned response should indicate conversion pixels assigned to all placements
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 2       |
      | Placement2      | 1       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
      | Flight2      | DISPLAY   | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "DISPLAY" assets as follows
      | FlightNumber | AssetType | AssetCount |
      | Flight1      | DISPLAY   | 1          |
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | Yes       |
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96700       | 17062117    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements

  @Scenario4
  Scenario: Create Campaign With Product Type ALCHEMY
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user post a POST request to create Agency
    And Agency response should have status code as 200 and content-type as JSON
    And returned response should indicate a agency created
    Then user post a POST request to create Brand
    And Brand response should have status code as 200 and content-type as JSON
    And returned response should indicate a brand created
    Then user post a POST request to create Campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign created
    Then user request the Campaign API with PUT method to assign business model "Simple CPM with Margin Maximization" with strategy ID 1 to campaign
    And Campaign response should have status code as 200 and content-type as JSON
    And response should indicate business model with strategy ID 1 assigned to campaign
    Then user post a POST request to Asset Endpoint URL to create video assets of following types:
      | AssetNumber | AssetType      | AdTagType        | AssetSize |
      | Asset1      | VAST           | ClearStreamAdTag | Size1     |
      | Asset2      | VPAIDLinear    | ClearStreamAdTag | Size2     |
      | Asset3      | VPAIDNonLinear | ClearStreamAdTag | Size3     |
      | Asset4      | VAST           | ThirdPartyAdTag  | Size1     |
      | Asset5      | VPAIDLinear    | ThirdPartyAdTag  | Size2     |
      | Asset6      | VPAIDNonLinear | ThirdPartyAdTag  | Size3     |
      | Asset7      | VAST           | ThirdPartyAdTag  | Size1     |
      | Asset8      | VPAIDLinear    | ThirdPartyAdTag  | Size2     |
      | Asset9      | VPAIDNonLinear | ThirdPartyAdTag  | Size3     |
    And Asset response should have status code as 200 and content-type as JSON
    And returned response should indicate assets created
    Then User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets
    And returned response should indicate assets created
    Then user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:
      | PlacementNumber | ProductType |
      | Placement1      | ALCHEMY     |
      | Placement2      | ALCHEMY     |
      | Placement3      | ALCHEMY     |
    Then user post a PUT request to Placement EndPoint URL to update placement with asset details as:
      | PlacementNumber | ProductType | VASTAssets | VPAIDAssets | AssetCount |
      | Placement1      | ALCHEMY     | 2          | 2           |            |
      | Placement2      | ALCHEMY     | 2          | 4           |            |
      | Placement3      | ALCHEMY     | 3          | 6           |            |
    And Placement response should have status code as 200 and content-type as JSON
    And returned response should indicate placements created
    Then user request the Deals & Seats API with PUT Method to assign deals and seats to placements
    And returend resposne should indicate deals and seats assigned to placement
    Then user post a POST Request to Flight Endpoint URL to create flights as following:
      | PlacementNumber | Flights |
      | Placement1      | 3       |
      | Placement2      | 3       |
      | Placement3      | 3       |
    And Flight response should have status code as 200 and content-type as JSON
    And returned response should indicate flights created
    Then user post a PUT request to associate flights of "Placement1" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VAST           | 1          |
      | Flight2      | VPAIDLinear    | 1          |
      | Flight3      | VPAIDNonLinear | 1          |
    Then user post a PUT request to associate flights of "Placement2" with "VIDEO" assets as follows
      | FlightNumber | AssetType   | AssetCount |
      | Flight1      | VAST        | 1          |
      | Flight2      | VAST        | 1          |
      | Flight3      | VPAIDLinear | 2          |
    Then user post a PUT request to associate flights of "Placement3" with "VIDEO" assets as follows
      | FlightNumber | AssetType      | AssetCount |
      | Flight1      | VAST           | 3          |
      | Flight2      | VPAIDLinear    | 3          |
      | Flight3      | VPAIDNonLinear | 3          |
    And Flight Update response should have status code as 200 and content-type as JSON
    And returned response should indicate flights associated with assets
    Then user POST a PUT request to associate Media Inventory details for placements as follows:
      | PlacementNumber | MediaList | SmartList |
      | Placement1      | Yes       | No        |
      | Placement2      | No        | Yes       |
      | Placement3      | Yes       | Yes       |
    And Media Inventory response should have status code as 200 and content-type as JSON
    Then user request the Frauds API with PUT method to add Fraud Details to placements as follows:
      | PlacementNumber | DVVerify(Bot) | DVVerify(Fraud) | IAB   | IAS          | AdvEntityId | PubEntityId |
      | Placement1      | true          | true(true)      | false | false(false) |             |             |
      | Placement2      | false         | false(false)    | true  | true(true)   | 96701       | 17062178    |
      | Placement3      | true          | true(true)      | true  | true(true)   | 96696       | 17067313    |
    And returned response should indicate Media Inventory details associated with placements
    Then user POST a PUT request to associate Targeting details for placements as follows:
      | PlacementNumber | Technologies | Geos | Categories | DayParts | AdditionalOptions |
      | Placement1      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement2      | Yes          | Yes  | Yes        | Yes      | No                |
      | Placement3      | Yes          | Yes  | Yes        | Yes      | No                |
    And Targeting response should have status code as 200 and content-typs as JSON
    And returned response should indicate Targeting details associated with placements
   # Then user validates generated VAST TAG URLs for all Assets with GET Method to have status code as 200
