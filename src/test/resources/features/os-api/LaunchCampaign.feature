Feature: Launch Created Campaigns

  @Scenario1
  Scenario: Verify User launches A Valid Campaign created for Scenario1.
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
#    Then user request the Launch Campaign API with POST method to launch campaign
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario2
  Scenario: Verify User launches A Valid Campaign created for Scenario2.
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario3
  Scenario: Verify User launches A Valid Campaign created for Scenario3.
    Given user has valid login credential as "ADMIN" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

  @Scenario4
  Scenario: Verify User launches A Valid Campaign created for Scenario4.
    Given user has valid login credential as "LAUNCH" user to ClearStream-OS
    When user posts a POST request to LogIn API
    Then LogIn response should have status code as 200 and content-type as JSON
    And user is authenticated successfully with valid login credentials.
    Then user request the Launch Campaign API with POST method to launch campaign
    And Launch Campaign response should have status code as 200 and content-type as JSON
    And returned response should indicate a campaign launched successfully

