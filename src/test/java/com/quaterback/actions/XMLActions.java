/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quaterback.actions;

import com.osapi.factory.DependencyManager;
import com.quarterback.factory.CampaignXMLParser;
import com.quarterback.factory.SessionVariables;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import net.thucydides.core.annotations.Step;

import java.io.File;

/**
 * @author Clearstream.
 *
 */
public class XMLActions
{
	private static File rngSchemaFile = new File(Constants.RNG_SCHEMA_FILEPATH);
	
	@Step
	public File getXMLFileFromLocal() {
	File xmlFile = new File(CommonUtils.getCampaignXMLFileName());
	//File xmlFile = new File("C:\\Users\\gauravsax\\IdeaProjects\\quarterback-automation\\TestDataQuarterback\\w2DEgqV4w_campaign.xml");
		return xmlFile;
	}
	
	@Step
	public File getRNGSchemaFile() {
		return rngSchemaFile;
	}
	
	@Step
	public void storeXMLFileForParsing(File xmlFile) {
		 DependencyManager.getInjector().getInstance(SessionVariables.class).setXmlFile(xmlFile);
	}
	
	@Step
	public void buildCampaignXMLParser(File xmlFile) {
		DependencyManager.getInjector().getInstance(SessionVariables.class).setConfigData(CampaignXMLParser.getInstance(xmlFile).getCamapignParser());
	}
}
