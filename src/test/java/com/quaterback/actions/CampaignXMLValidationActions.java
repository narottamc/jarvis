/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quaterback.actions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table.Cell;
import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.targetingresponse.*;
import com.quarterback.comparatorfactory.*;
import com.quarterback.factory.SessionVariables;
import com.quarterback.factory.XMLCampaignSection;
import com.quarterback.mappers.ConfigData;
import com.quarterback.validators.*;
import com.quarterback.validators.AdvertiserPlacementValidator.AdvertiserPlacementXML;
import com.quarterback.validators.CampaignValidator.PlacementDataXML;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author Clearstream.
 */
public class CampaignXMLValidationActions {
    private static final Logger LOG = Logger.getLogger(CampaignXMLValidationActions.class);

    private String scenarioTestDataPath =
            DependencyManager.getInjector().getInstance(SessionVariables.class).getScenarioTestDataPath();
    private RootAgencyResponse agencyResponse =
            DependencyManager.getInjector().getInstance(SessionVariables.class).getAgencyDetails(scenarioTestDataPath);
    private List<RootPlacementResponse> placementResponseList =
            DependencyManager.getInjector().getInstance(SessionVariables.class).getPlacementDetails(scenarioTestDataPath);
    private List<RootActivityPixelResponse> activityPixelResponseList =
            DependencyManager.getInjector().getInstance(SessionVariables.class).getActivityPixelDeatails(scenarioTestDataPath);
    private Map<Integer,RootMediaInventoryResponse> mediaInventoryResponseSmartList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getSmartListDetailsForAPlacement(scenarioTestDataPath);

    @Step
    public RootAgencyResponse getAgencyResponse() {
        return agencyResponse;
    }

    @Step
    public HashMap<Integer, String> getAgencyDetailFromCampaignXML() {
        ConfigData configData = DependencyManager.getInjector().getInstance(SessionVariables.class).getConfigData();
        HashMap<Integer, String> agencyXMLMap = new HashMap<>();
        List<ConfigData.Advertiser> advertiserList = configData.advertisers;
        LOG.info("Validating if Advertiser with Agency Id " + agencyResponse.getId() + " and Name " + agencyResponse.getName() + " is present in Campaign XML For Campaign With Active Flights");
        if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldAdvertiserBePresentInXML(scenarioTestDataPath)) {
            for (ConfigData.Advertiser advertiser : advertiserList) {
                if (advertiser.id == agencyResponse.getId() && advertiser.name.contentEquals(agencyResponse.getName())) {
                    agencyXMLMap.put(advertiser.id, advertiser.name);
                    return agencyXMLMap;
                }
            }
            if (agencyXMLMap.isEmpty()) {
                return null;
            }
        }
        return agencyXMLMap;
    }


    @Step
    public Set<Integer> verifyActivityPixelDetailsFromXML() {
        Set<Integer> failedActivityPixelIds = new HashSet<>();
        if (activityPixelResponseList != null) {
            for (int i = 0; i < activityPixelResponseList.size(); i++) {
                boolean isActivityPixelInformationCorrect = false;
                RootActivityPixelResponse activityPixelResponse = activityPixelResponseList.get(i);
                ActivityPixelComparator activityPixelOS = new ActivityPixelValidator.ActivityPixelOS(activityPixelResponse);
                isActivityPixelInformationCorrect = activityPixelOS.compareActivityPixelDetails
                        (new ActivityPixelValidator.ActivityPixelXML(activityPixelResponse)) == 0;
                if (!isActivityPixelInformationCorrect) {
                    failedActivityPixelIds.add(activityPixelResponse.getId());
                    LOG.error("XML Validation(ACTIVITY PIXEL INFORMATION) Failed For Pixel Id " + activityPixelResponse.getId());
                    LOG.error(" XML Validation : Validating if XML Contains a activityPixel ; For Pixel Id " + activityPixelResponse.getId() +
                            " Expecting A ActivityPixel with details " + activityPixelOS.toString() + " but found Activity Pixel In XML as "
                            + new ActivityPixelValidator.ActivityPixelXML(activityPixelResponse).toString());
                }
            }
            return failedActivityPixelIds;
        }
        return failedActivityPixelIds;
    }

    @Step
    public Map<Integer, Boolean> verifyPlacementsMetaInfoInXML() {
        Map<Integer, Boolean> failedPlacementMap = new HashMap<>();
        List<Integer> placementIdOSList = DependencyManager.getInjector().getInstance(SessionVariables.class).getPlacementIdsFromOS(placementResponseList);
        for (Integer placementId : placementIdOSList) {
            boolean isPlacementPresentInXML = false;
            LOG.info("XML Validation : Validating Campaign In XML With Correct Advertiser ; For Placement Id " + placementId);
            AdvertiserPlacementComparator campaignOS = new AdvertiserPlacementValidator.AdvertiserPlacementOS(placementId, scenarioTestDataPath);
            isPlacementPresentInXML = campaignOS.compareTo(new AdvertiserPlacementXML(placementId, scenarioTestDataPath)) == 0;
            if (!isPlacementPresentInXML) {
                failedPlacementMap.put(placementId, isPlacementPresentInXML);
                LOG.error("XML Validation(PLACEMENT TOTATL COUNT) FAILED for Placement " + placementId);
                LOG.error("XML Validation : Validating if A Campaign In XML Is Present with For Advertiser ; For Placement Id " + placementId
                        + " Expecting A Camapign in XML with Attributes " + campaignOS.toString() + " but found Campaign with attributes "
                        + new AdvertiserPlacementXML(placementId, scenarioTestDataPath).toString());
            }
        }

        return failedPlacementMap;
    }

    @Step
    public Map<Integer, Boolean> verifySSPMetaInfoInXML() {
        Map<Integer, Boolean> failedPlacementMap = new HashMap<>();
        for (int i = 0; i < placementResponseList.size(); i++) {
            if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponseList.get(i).getId())) {
                boolean isSSPMetaInfoCorrect = false;
                LOG.info("XML Validation : Validating SSP Meta Info In XML For Placement Id " + placementResponseList.get(i).getId());
                SSPComparator sspOS = new SSPValidator.SellerNetworksOS(scenarioTestDataPath, i);
                isSSPMetaInfoCorrect = sspOS.compareSSPMetaInfo(new SSPValidator.SellerNetworkXML(placementResponseList.get(i))) == 0;
                if (!isSSPMetaInfoCorrect) {
                    failedPlacementMap.put(placementResponseList.get(i).getId(), isSSPMetaInfoCorrect);
                    LOG.error("XML Validation(SSP TOTAL COUNT FAILED) For Placement " + placementResponseList.get(i).getId());
                    LOG.error("XML Validation : Validating if A Campaign In XML Is Present with Correct SSP Count ; For Placement Id "
                            + placementResponseList.get(i).getId()
                            + " Expecting A Camapign in XML with SSP " + sspOS.toString() + " but found Campaign with attributes "
                            + new SSPValidator.SellerNetworkXML(placementResponseList.get(i)).toString());
                }
            }
        }

        return failedPlacementMap;
    }


    @Step
    public Map<Integer, Boolean> verifyTargetingInfoInXML() {
        Map<Integer, Boolean> failedPlacementMap = new HashMap<>();

        for (int i = 0; i < placementResponseList.size(); i++) {
            if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponseList.get(i).getId())) {
                boolean isTargetingInfoCorrect;
                RootPlacementResponse placementResponse = placementResponseList.get(i);
                LOG.info("XML Validation : Validating Targeting Section Info In XML For Placement Id " + placementResponseList.get(i).getId());
                TargetingComparator targetingOS = new TargetingValidator.OSTargeting(placementResponse,i,scenarioTestDataPath);
                isTargetingInfoCorrect = targetingOS.compareTargetingInfo(new TargetingValidator.XMLTargeting(placementResponseList.get(i))) == 0;
                if (!isTargetingInfoCorrect) {
                    failedPlacementMap.put(placementResponseList.get(i).getId(), isTargetingInfoCorrect);
                    LOG.error("XML Validation(TARGETING INFORMATION FAILED) For Placement " + placementResponseList.get(i).getId());
                    LOG.error("XML Validation : Validating if A Campaign In XML Is Present with Correct Targeting Details ; For Placement Id "
                            + placementResponseList.get(i).getId()
                            + " Expecting A Camapign in XML with Targeting Details " + targetingOS.toString() + " but found Campaign with attributes "
                            + new TargetingValidator.XMLTargeting(placementResponseList.get(i)).toString());
                }
                DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .setXmlCampaignVerifiactionStatus(placementResponseList.get(i).getId(), XMLCampaignSection.TARGETING, isTargetingInfoCorrect);
            }
        }
        return failedPlacementMap;
    }

    @Step
    public Map<Integer, Boolean> verifyLineItemsMetaInfoPresentInXML() {
        Map<Integer, Boolean> failedPlacementMap = new HashMap<>();
        for (RootPlacementResponse placementResponse : placementResponseList) {
            if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponse.getId())) {
                LOG.info("XML Validation : Validating LineItems Meta Info In XML With Correct Advertiser ; For Placement Id " + placementResponse.getId());
                List<RootFlightsResponse> flightResponseList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .getFlightsFromOSForAPlacement(scenarioTestDataPath, placementResponse.getId());
                LineItemsComparator lineItemCompare = new LineItemsValidator.LineItemsOS(flightResponseList);
                boolean isIndividualLineItemInfoCorrect =
                        lineItemCompare.compareAllLineItemInfo(new LineItemsValidator.LineItemsXML(placementResponse.getId())) == 0;
                if (!isIndividualLineItemInfoCorrect) {
                    failedPlacementMap.put(placementResponse.getId(), isIndividualLineItemInfoCorrect);
                    LOG.error("XML Validation(LINEITEMS TOTATL COUNT) FAILED for Placement " + placementResponse.getId());
                    LOG.error("XML Validation : Validating if LineItems In XML are Present with correct Placement ; For Placement Id " + placementResponse.getId()
                            + " Expecting A Camapign With Total Line Items " + lineItemCompare.getTotalLineItemsCount() + " but found Campaign with Total Line Items "
                            + new LineItemsValidator.LineItemsXML(placementResponse.getId()).getTotalLineItemsCount());
                }
            }
        }
        return failedPlacementMap;
    }

    @Step
    public Map<Integer, Boolean> verifyCreativesMetaInfoInXML() {
        Map<Integer, Boolean> failedPlacementMap = new HashMap<>();
        for (RootPlacementResponse placementResponse : placementResponseList) {
            {
                if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponse.getId())) {
                    LOG.info("XML Validation : Validating Creatives Meta Info In XML With Correct Advertiser ; For Placement Id " + placementResponse.getId());
                    List<RootFlightsResponse> flightResponseList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                            .getFlightsFromOSForAPlacement(scenarioTestDataPath, placementResponse.getId());
                    CreativeComparator creativeComparator = new CreativeComparator(flightResponseList, placementResponse.getId());
                    boolean isIndividualCreativeInfoCorrect = creativeComparator.compareCreativesMetaInfo() == 0;
                    if (!isIndividualCreativeInfoCorrect) {
                        failedPlacementMap.put(placementResponse.getId(), isIndividualCreativeInfoCorrect);
                        LOG.error("XML Validation(CREATIVES TOTATL COUNT) FAILED for Placement " + placementResponse.getId());
                        LOG.error("XML Validation : Validating if CREATIVES In XML are Present with correct Placement ; For Placement Id " + placementResponse.getId()
                                + " Expecting A Camapign With Total CREATIVES Items " + creativeComparator.getTotalCreativesCountOS() + " but found Campaign with Total CREATIVES Items "
                                + creativeComparator.getTotalCreativeCountXML());
                    }
                }
            }

        }
        return failedPlacementMap;
    }

    @Step
    public void verifyCampaignLevelInfoInXML() {
        for (int i = 0; i < placementResponseList.size(); i++) {
            if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponseList.get(i).getId())) {
                boolean isCamapignLevelInfoCorrect = false;
                LOG.info("XML Validation : Validating Campaign Details Information From XML ; For Placement Id " + placementResponseList.get(i).getId());
                CampaignParameterComparator campaign = new CampaignValidator.PlacementDataOS(placementResponseList.get(i), scenarioTestDataPath
                        , i, mediaInventoryResponseSmartList.get(placementResponseList.get(i).getId()));
                isCamapignLevelInfoCorrect = campaign.compareTo(new PlacementDataXML(placementResponseList.get(i))) == 0;
                if (!isCamapignLevelInfoCorrect) {
                    LOG.error("XML Validation(CAMPAIGNINFO) FAILED for Placement " + placementResponseList.get(i).getId());
                    LOG.error("XML Validation : Validating Campaign Level Information From XML ; For Placement Id " + placementResponseList.get(i).getId()
                            + " Expecting A Campaign In XML with Attributes " + campaign.toString() + " but found Campaign with attributes "
                            + new PlacementDataXML(placementResponseList.get(i)).toString());
                }
                DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .setXmlCampaignVerifiactionStatus(placementResponseList.get(i).getId(), XMLCampaignSection.CAMPAIGNS, isCamapignLevelInfoCorrect);
            }
        }
    }

    @Step
    public void verifySSPDetailsInfoInXML() {
        for (int i = 0; i < placementResponseList.size(); i++) {
            RootPlacementResponse placementResponse = placementResponseList.get(i);
            boolean isSSPDetailInfoCorrect = false;
            if (DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestDataPath, placementResponseList.get(i).getId())) {
                LOG.info("XML Validation : Validating SSP Details Info In XML For Placement Id " + placementResponseList.get(i).getId());
                SSPComparator sspOS = new SSPValidator.SellerNetworksOS(scenarioTestDataPath, i);
                isSSPDetailInfoCorrect = (sspOS.compareSSPDetailsInfo(new SSPValidator.SellerNetworkXML(placementResponseList.get(i))) == 0);
                if (!isSSPDetailInfoCorrect) {
                    LOG.error("XML Validation(SSP) FAILED for Placement " + placementResponse.getId());
                    LOG.error("XML Validation :Validating if A Campaign In XML Is Present with Correct SSP Details ; For Placement Id "
                            + placementResponseList.get(i).getId()
                            + " Expecting A Camapign in XML with SSP " + sspOS.toString() + " but found Campaign with attributes "
                            + new SSPValidator.SellerNetworkXML(placementResponseList.get(i)).toString());
                }
                DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .setXmlCampaignVerifiactionStatus(placementResponse.getId(), XMLCampaignSection.SSP, isSSPDetailInfoCorrect);
            }
        }
    }

    @Step
    public void verifyLineItemsInfoOfCampaignInXML() {
        for (int j = 0; j < placementResponseList.size(); j++) {
            RootPlacementResponse placementResponse = placementResponseList.get(j);
            RootConversionPixelResponse conversionPixelResponse = ProductType.fromProductTypeNumber(placementResponse.getProductType())
                    .getConversionPixelResponseListForProductType(placementResponse.getBusinessModel().getStrategyId(),
                            scenarioTestDataPath, placementResponseList.size()).get(j);
            List<Boolean> lineItemVerificationStatus = new ArrayList<Boolean>();
            boolean isAllLineItemsInfoCorrect = false;
            List<RootFlightsResponse> flightResponseList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getFlightsFromOSForAPlacement(scenarioTestDataPath, placementResponse.getId());
            LOG.info("XML Validation : Validating LineItems Details for Campaign in XML ; For Placement Id " + placementResponse.getId());
            for (int i = 0; i < flightResponseList.size(); i++) {
                if (flightResponseList.get(i).getActive()) {
                    boolean isLineItemInfoCorrect = false;
                    for (FlightResponseAd flightAd : flightResponseList.get(i).getAds()) {
                        LineItemsComparator lineItemCompare = new LineItemsValidator.LineItemsOS(flightResponseList.get(i), flightAd, placementResponse, conversionPixelResponse);
                        isLineItemInfoCorrect = lineItemCompare.compareLineItemDetails(
                                new LineItemsValidator.LineItemsXML(placementResponse, flightResponseList.get(i), flightAd)) == 0;
                        if (!isLineItemInfoCorrect) {
                            LOG.error("XML Validation(LINEITEMS) FAILED for Placement " + placementResponse.getId() + " With Flight " + flightResponseList.get(i).getId()
                                    + " With Ad " + flightAd.getId());
                            LOG.error("XML Validation : Validating LineItems Information From XML ; For Placement Id " + placementResponse.getId() + " With Flight Id "
                                    + flightResponseList.get(i).getId() + " Expecting A LineItem In XML with Attributes " + lineItemCompare.toString() + " but found LineItems with attributes "
                                    + new LineItemsValidator.LineItemsXML(placementResponse, flightResponseList.get(i), flightAd).toString());
                        }
                    }
                    lineItemVerificationStatus.add(isLineItemInfoCorrect);
                }
                isAllLineItemsInfoCorrect = !lineItemVerificationStatus.contains(false);
                DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .setXmlCampaignVerifiactionStatus(placementResponse.getId(), XMLCampaignSection.LINEITEMS, isAllLineItemsInfoCorrect);
            }
        }
    }

    @Step
    public void verifyCreativesInfoOfCampaignInXML() {
        for (RootPlacementResponse placementResponse : placementResponseList) {
            String productType = ProductType.fromProductTypeNumber(placementResponse.getProductType()).toString();
            List<Boolean> creativeItemVerficationStatus = new ArrayList<Boolean>();
            boolean isAllCreativeItemInfoCorrect = false;
            List<RootFlightsResponse> flightResponseList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getFlightsFromOSForAPlacement(scenarioTestDataPath, placementResponse.getId());
            LOG.info("XML Validation : Validating Creatives Details for Campaign in XML ; For Placement Id " + placementResponse.getId());
            for (int i = 0; i < flightResponseList.size(); i++) {
                if (flightResponseList.get(i).getActive()) {
                    boolean isCreativeItemInfoCorrect = false;
                    for (FlightResponseAd flightAd : flightResponseList.get(i).getAds()) {
                        CreativeComparator creativeComparatorOS = CreativeComparatorType.valueOf(productType)
                                .getCreativeCompratorOS(scenarioTestDataPath, flightAd, flightResponseList.get(i)
                                        , placementResponse.getProductType(), placementResponse);
                        CreativeComparator creativeComparatorXML = CreativeComparatorType.valueOf(productType)
                                .getCreativeCompratorXML(flightResponseList.get(i), flightAd);
                        isCreativeItemInfoCorrect = CreativeComparatorType.valueOf(productType)
                                .compareCreativeInfoProductType(creativeComparatorOS, creativeComparatorXML) == 0;
                        if (!isCreativeItemInfoCorrect) {
                            LOG.error("XML Validation(CREATIVES) FAILED for Placement " + placementResponse.getId() + " With Flight " + flightResponseList.get(i).getId()
                                    + " With Ad " + flightAd.getId());
                            LOG.error("XML Validation : Validating Creative Information From XML ; For Placement Id " + placementResponse.getId() + " With Flight Id "
                                    + flightResponseList.get(i).getId() + " Expecting A Creative In XML with Attributes " + creativeComparatorOS.toString()
                                    + " but found Creative with attributes "
                                    + creativeComparatorXML.toString());
                        }
                    }
                    creativeItemVerficationStatus.add(isCreativeItemInfoCorrect);
                }
                isAllCreativeItemInfoCorrect = !creativeItemVerficationStatus.contains(false);
                DependencyManager.getInjector().getInstance(SessionVariables.class)
                        .setXmlCampaignVerifiactionStatus(placementResponse.getId(), XMLCampaignSection.CREATIVES, isAllCreativeItemInfoCorrect);
            }

        }
    }

    @Step
    public Multimap<Integer, String> validateXMLVerificationStatusTable() {
        Multimap<Integer, String> failedPlacementMap = ArrayListMultimap.create();
        HashBasedTable<Integer, String, Boolean> campaignVerficationStatus =
                DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaignVerifiactionStatus();
        for (Cell<Integer, String, Boolean> cell : campaignVerficationStatus.cellSet()) {
            LOG.info("XML Validation : Placement Id(Campaign In XML) is " + cell.getRowKey() + " XML Campaign Section is "
                    + cell.getColumnKey() + " Verification Status is " + cell.getValue());
            if (!cell.getValue()) {
                LOG.error("XML Validation : FAILED ; Placement Id(Campaign In XML) " + cell.getRowKey() + " XML Campaign Section "
                        + cell.getColumnKey() + " Verification Status " + cell.getValue());
                failedPlacementMap.put(cell.getRowKey(), cell.getColumnKey());
            }
        }
        return failedPlacementMap;
    }
}
