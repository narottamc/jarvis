/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.deletecampaignentities.steps;

import com.osapi.actions.AssetsActions;
import com.osapi.actions.DeleteAssetsActions;
import com.osapi.actions.DisplayActions;
import com.osapi.actions.SurveyActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeleteAssetsSteps {

    @Steps
    DeleteAssetsActions deleteActions;
    @Steps
    AssetsActions assetsActions;
    @Steps
    DisplayActions displayActions;
    @Steps
    SurveyActions surveyActions;

    @Then("^user has a campaign created having a \"([^\"]*)\" type Video Asset$")
    public void userHasACampaignCreatedHavingATypeVideoAsset(String videoAssetType)  {
        assertThat("Verify A Campaign Exists With A Video Asset Of Type "+videoAssetType,
                deleteActions.campaignWithVideoAssets(videoAssetType), equalTo(true));
    }

    @And("^user request the Asset API with DELETE method to delete (\\d+) video assets$")
    public void userRequestTheAssetAPIWithDELETEMethodToDeleteVideoAssets(int noOfAssetToDelete)  {
        assertThat("Number of Video Assets of type"+deleteActions.getVideoAssetType()+" " +
                        "to delete is greater than the Total Number of that VideoAssets present in Campaign "+deleteActions.getCampaignResponse().getId(),
                deleteActions.deleteVideoAsset(deleteActions.getVideoAssetType(),noOfAssetToDelete), equalTo(noOfAssetToDelete));
    }

    @And("^response should indicate that \"([^\"]*)\" Type video assets deleted$")
    public void responseShouldIndicateThatTypeVideoAssetsDeleted(String videoAssetType)  {
        deleteActions.verifyDeletedVideoAsset(videoAssetType,deleteActions.getDeletedEntitiesIds());
        assetsActions.storeAssetsResponseToJsonFile();
    }

    @Then("^user has a campaign created having a \"([^\"]*)\" type Display Asset$")
    public void userHasACampaignCreatedHavingATypeDisplayAsset(String displayAssetType)  {
        assertThat("Verify A Campaign Exists With A Display Asset Of Type "+displayAssetType,
                deleteActions.verifyCampaignWithDisplayAsset(displayAssetType), equalTo(true));
    }

    @And("^user request the Asset API with DELETE method to delete (\\d+) display assets$")
    public void userRequestTheAssetAPIWithDELETEMethodToDeleteDisplayAssets(int noOfAssetToDelete)  {
        assertThat("Number of DisplayAssets of type"+deleteActions.getDisplayAssetType()+
                        " to delete is greater than the Total Number of that DisplayAsset present in Campaign "+deleteActions.getCampaignResponse().getId(),
                deleteActions.deleteDisplayAsset(deleteActions.getDisplayAssetType(),noOfAssetToDelete), equalTo(noOfAssetToDelete));
    }

    @And("^response should indicate that \"([^\"]*)\" Type display assets deleted$")
    public void responseShouldIndicateThatTypeDisplayAssetsDeleted(String displayAssetType)  {
       deleteActions.verifyDeletedDisplayAssets(displayAssetType,deleteActions.getDeletedEntitiesIds());
       displayActions.storeDisplayResponseToJsonFile();
    }

    @Then("^user has a campaign created having Survey Assets$")
    public void userHasACampaignCreatedHavingSurveyAssets()  {
        assertThat("Verify A Campaign Exists With A Survey Asset",
                deleteActions.verifyCampaignWithSurveyAsset(), equalTo(true));
    }

    @And("^user request the Asset API with DELETE method to delete (\\d+) survey assets$")
    public void userRequestTheAssetAPIWithDELETEMethodToDeleteSurveyAssets(int noOfAssetToDelete)  {
        assertThat("Number of SurveyAssets of type to delete are greater than the Total Number of that SurveyAsset present in Campaign "
                        +deleteActions.getCampaignResponse().getId(), deleteActions.deleteSurveyAssets(noOfAssetToDelete), equalTo(noOfAssetToDelete));
    }

    @And("^response should indicate that survey assets deleted$")
    public void responseShouldIndicateThatSurveyAssetsDeleted()  {
        deleteActions.verifyDeletedSurveyAssets(deleteActions.getDeletedEntitiesIds());
        surveyActions.storeAddSurveyResponseToJsonFile();
    }

}
