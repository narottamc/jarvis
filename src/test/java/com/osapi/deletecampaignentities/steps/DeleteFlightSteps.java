/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.deletecampaignentities.steps;

import com.osapi.actions.DeleteFlightActions;
import com.osapi.actions.FlightsActions;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.yecht.Data;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeleteFlightSteps {
    @Steps
    FlightsActions flightsActions;
    @Steps
    DeleteFlightActions deleteFlightActions;


    @Then("^user has a campaign created having placement of product type \"([^\"]*)\" with more than one flight$")
    public void userHasACampaignCreatedHavingPlacementOfProductTypeWithMoreThanOneFlight(String placementProductType) {
        assertThat("Number of Flights In A Placement Should be Greater Than One Atleast One Placement Should be Present Of Product Type " + placementProductType,
                deleteFlightActions.verifyTotalFlightsCountGreaterThanOne(placementProductType),equalTo(true));
        deleteFlightActions.setUpFlightInformation();

    }

    @And("^response should indicate that flight deleted from the placement$")
    public void responseShouldIndicateThatFlightDeletedFromThePlacement() {
        deleteFlightActions.verifyDeletedFlights(deleteFlightActions.getDeletedEntitiesIds());
        flightsActions.storeUpdatedFlightResponsToJsonFile();

    }

    @And("^user request the Flights API with DELETE method to delete (\\d+) flights of \"([^\"]*)\"$")
    public void userRequestTheFlightsAPIWithDELETEMethodToDeleteFlightsOf(int noOfFlightToDelete, String placementDetails) throws Throwable {
        deleteFlightActions.deleteFlightsInBulkForAPlacement(placementDetails,noOfFlightToDelete,deleteFlightActions.getProductType());

    }

    @And("^user request the Flights API with DELETE method to delete flights as following$")
    public void userRequestTheFlightsAPIWithDELETEMethodToDeleteFlightsAsFollowing(Map<String,String> placementFlightDetails)  {
        System.out.println(placementFlightDetails);
        deleteFlightActions.deleteSpecificFlightsForAPlacement(placementFlightDetails,deleteFlightActions.getProductType());

    }

    @And("^user request the Flights API with DELETE method to delete (\\d+) flights in following placements$")
    public void userRequestTheFlightsAPIWithDELETEMethodToDeleteFlightsInFollowingPlacements(int arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


}
