/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.deletecampaignentities.steps;

import com.osapi.actions.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeletePlacementSteps {
    @Steps
    DeletePlacementActions deletePlacementActions;
    @Steps
    PlacementActions placementActions;
    @Steps
    FlightsActions flightsActions;
    @Steps
    MediaInventoryActions mediaInventoryActions;
    @Steps
    TargetingActions targetingActions;

    @Then("^user request the Placement API with DELETE method to delete (\\d+) placement$")
    public void userRequestThePlacementAPIWithDELETEMethodToDeletePlacement(int noOfPlacementToDelete)  {
        assertThat("Number of Placements To delete is equal to or greater than the Total Number of Placements in Campaign "
                        +deletePlacementActions.getCampaignResponse().getId() +" AtLeast One Placement Should Exist In A Campaign",
                deletePlacementActions.verifyPlacementsCountAfterDelete(noOfPlacementToDelete), equalTo(true));
        assertThat("All the Placements Requested " + " Of Product Type "+deletePlacementActions.getProductType() +
                        " in Campaign "+deletePlacementActions.getCampaignResponse().getId() + "Are Deleted;No More Placement Of Product Type "
                        + deletePlacementActions.getProductType()+ " Exists"
                , deletePlacementActions.deleteBulkPlacementsForProductType(noOfPlacementToDelete,deletePlacementActions.getProductType())
                ,equalTo(noOfPlacementToDelete));
    }

    @Then("^user has a campaign created having more than one placements of product type \"([^\"]*)\"$")
    public void userHasACampaignCreatedHavingMoreThanOnePlacementsOfProductType(String productType)  {
        assertThat("Number of Placements In A Campaign Should be Greater Than One Or Atleast One Placement Should be Present Of Product Type "+ productType,
                deletePlacementActions.verifyTotalPlacementCountGreaterThanOne(productType),equalTo(true));
        deletePlacementActions.setUpPlacementInformation();
    }

    @And("^response should indicate that placements of product type \"([^\"]*)\" deleted$")
    public void responseShouldIndicateThatPlacementsOfProductTypeDeleted(String productType)  {
        deletePlacementActions.verifyDeletedPlacements(deletePlacementActions.getDeletedEntitiesIds(),productType);
        placementActions.storePlacementResponseToJsonFile();
        flightsActions.storeUpdatedFlightResponsToJsonFile();
        mediaInventoryActions.storeMediaInventoryResponseToJsonFile();
        targetingActions.storeTargetinResponseToJsonFile();
    }


    @And("^user request the Placement API to delete below placements of above product type$")
    public void userRequestThePlacementAPIToDeleteBelowPlacementsOfAboveProductType(DataTable placementDetails) throws Throwable {
        List<String> placementsToDeleteList = placementDetails.asList(String.class);
        assertThat("Number of Placements To delete is equal to or greater than the Total Number of Placements in Campaign "
                        +deletePlacementActions.getCampaignResponse().getId() +" AtLeast One Placement Should Exist In A Campaign",
                deletePlacementActions.verifyPlacementsCountAfterDelete(placementsToDeleteList.size()), equalTo(true));
        assertThat("All the Placements Requested " + " Of Product Type "+deletePlacementActions.getProductType() +
                " in Campaign "+deletePlacementActions.getCampaignResponse().getId() + "Are Deleted;No More Placement Of Product Type "
                        + deletePlacementActions.getProductType()+ " Exists" + " Or Placement is Not of Product Type " + deletePlacementActions.getProductType()
                , deletePlacementActions.deleteSpecificPlacement(placementsToDeleteList,deletePlacementActions.getProductType())
                ,equalTo(placementsToDeleteList.size()));
    }
}
