/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.deletecampaignentities.steps;

import com.osapi.actions.DeleteCampaignActions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeleteCampaignSteps {
    @Steps
    DeleteCampaignActions deleteCampaignActions;

    @Then("^user has a campaign created having placements of product type \"([^\"]*)\" in draft state$")
    public void userHasACampaignCreatedHavingPlacementsOfProductTypeInDraftState(String arg0)  {
        assertThat(" Campaign With Id" +  deleteCampaignActions.getCampaignResponse().getId() +" Is Not in Draft State",
                deleteCampaignActions.verifyCampaignExistInDraftState(), equalTo(true));
    }

    @And("^user request the Campaign API with DELETE method to delete campaign$")
    public void userRequestTheCampaignAPIWithDELETEMethodToDeleteCampaign()  {
        deleteCampaignActions.deleteACampaign();
    }

    @And("^response should indicate that campaign deleted$")
    public void responseShouldIndicateThatCampaignDeleted()  {
        deleteCampaignActions.verifyDeleteCampaign();
    }
}
