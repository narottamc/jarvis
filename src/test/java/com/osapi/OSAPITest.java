/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * @author Clearstream
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features =
{ "src/test/resources/features/os-api/CampaignMultiple.feature" }, glue =
{ "com.osapi.createcampaign.steps", "com.osapi.deletecampaignentities.steps"})
public class OSAPITest
{
}