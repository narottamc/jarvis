/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.launchcampaign.steps;

import com.osapi.actions.LaunchCampaignActions;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Clearstream
 */
public class LaunchCampaignStep
{
	@Steps
	LaunchCampaignActions launchCampaignAction;

	@Then("^user request the Launch Campaign API with POST method to launch campaign$")
	public void userRequestTheLaunchCampaignAPIWithPOSTMethodToLaunchCampaign() throws Throwable
	{
		launchCampaignAction.userPostAPOSTRequestToLaunchCampaignUrlToLaunchCampaign();
	}

	@Then("^user request the Launch Campaign API with POST method$")
	public void userRequestTheLaunchCampaignAPIWithPOSTMethod() throws Throwable {
		launchCampaignAction.userPostAPOSTRequestToLaunchCampaignUrl();
	}

	@Then("^Launch Campaign response should have status code as (\\d+) and content-type as JSON$")
	public void launch_Campaign_response_should_have_status_code_as_and_content_type_as_JSON(int statusCode)
			throws Exception
	{
		assertThat("Verify Content Type for Active Campaign Api ",
				launchCampaignAction.getContentTypeforActiveCampaign(), equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Active Campaign Api ", launchCampaignAction.getStatusCodeforActiveCampaign(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate a campaign launched successfully$")
	public void returned_response_should_indicate_a_campaign_launched_successfully() throws Exception
	{
		launchCampaignAction.storeActiveCampaignResponseToJsonFile();
	}

	


}
