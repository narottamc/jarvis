/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.SurveyActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class SurveySteps
{

	@Steps
	SurveyActions surveyActions;

	@Then("^user posts a POST request to Survey EndPoint URL to add surveys as follows:$")
	public void userPostsAPOSTRequestToSurveyEndPointURLToAddSurveysAsFollows(DataTable surveyDataTable)
			throws Throwable
	{
		List<Map<String, String>> surveyMap = surveyDataTable.asMaps(String.class, String.class);
		surveyActions.createNewSurvey(surveyMap);
	}

	@And("^Survey response should have status code as (\\d+) and content-type as JSON\\.$")
	public void surveyResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode) throws Throwable
	{
		assertThat("Verify Content Type for Survey Api ", surveyActions.getContentTypeForSurveyRequest(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Survey Api ", surveyActions.getStatusCodeForSurveyRequest(),
				equalTo(statusCode));
	}

	@And("^returned response should indicate surveys created\\.$")
	public void returnedResponseShouldIndicateSurveysCreated() throws Throwable
	{
		surveyActions.storeAddSurveyResponseToJsonFile();
	}

}
