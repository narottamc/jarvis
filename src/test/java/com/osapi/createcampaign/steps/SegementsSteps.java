/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.SegementsActions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

/**
 * @author Clearstream.
 */
public class SegementsSteps {

    @Steps
    SegementsActions segementsActions;

    @Then("^user request the dmp provider API with GET method$")
    public void userRequestTheDmpProviderAPIWithGETMethod() throws Throwable {
        segementsActions.userRequestTheDmpProviderAPIWithPOSTMethod();
    }

    @Then("^user request each dmp client in every dmp provider with GET method$")
    public void userRequestEachDmpClientInEveryDmpProviderWithGETMethod(){
        segementsActions.userRequestEachDmpClientInEveryDmpProviderWithGETMethod();
    }

    @And("^returned response of list of dmp providers should be extracted to a JSON File$")
    public void returnedResponseOfListOfDmpProvidersShouldBeExtractedToAJSONFile()  {
        segementsActions.storeDmpProviderMasterDataToJsonFile();
    }

    @And("^returned response of segments for each dmp client should be extracted to a JSOON File$")
    public void returnedResponseOfSegmentsForEachDmpClientShouldBeExtractedToAJSOONFile()  {
        segementsActions.storeDataSegmentMasterDataToJsonFile(segementsActions.getHashMapConvertedResponseForDataSegments());

    }
}
