/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.PlacementActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class PlacementSteps
{

	@Steps
	PlacementActions placementActions;

	@Then("^user POST a POST Request to Placement EndPoint URL to add placement with product type as follows:$")
	public void userPOSTAPOSTRequestToPlacementEndPointURLToAddPlacementWithProductTypeAsFollows(
			DataTable addNewPlacementTable) throws Throwable
	{
		List<Map<String, String>> newplacementDetailsMap = addNewPlacementTable.asMaps(String.class, String.class);
		placementActions.addNewPlacementWithProductType(newplacementDetailsMap);
	}

	@Then("^user post a PUT request to Placement EndPoint URL to update placement with asset details as:$")
	public void userPostAPUTRequestToPlacementEndPointURLToUpdatePlacementWithAssetDetailsAs(DataTable placementDataTable)
			throws Throwable
	{
		List<Map<String, String>> placementDetailsMap = placementDataTable.asMaps(String.class, String.class);
		placementActions.addAssetsToPlacements(placementDetailsMap);
	}

	@Then("^Placement response should have status code as (\\d+) and content-type as JSON$")
	public void placementResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode) throws Exception
	{
		assertThat("Verify Content Type for Log-In Api ", placementActions.getContentType(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Log-In Api ", placementActions.getStatusCode(), equalTo(statusCode));
	}

	
	@Then("^returned response should indicate placements created$")
	public void returned_response_should_indicate_placements_created() throws Exception
	{
		placementActions.storePlacementResponseToJsonFile();
	}

	@Then("^user request the Deals & Seats API with PUT Method to assign deals and seats to placements$")
	public void userRequestTheDealsSeatsAPIWithPUTMethodToAssignDealsAndSeatsToPlacements() throws Throwable {
		placementActions.assignDealsAndSeatsToPlacements();
		placementActions.storeAddNewSeatDealResponse();
	}

	@And("^returend resposne should indicate deals and seats assigned to placement$")
	public void returendResposneShouldIndicateDealsAndSeatsAssignedToPlacement() throws Throwable {
		placementActions.storeDealsAndSeatsResponse();
	}

    @Then("^user request the activity pixel API with POST Method to create (\\d+) activity pixels of \"([^\"]*)\" type$")
    public void userRequestTheActivityPixelAPIWithPOSTMethodToCreateActivityPixelsOfType(int noOfActivityPixels, String activityPixelType) throws Throwable {
        placementActions.createNewActivityPixels(noOfActivityPixels,activityPixelType);

    }

    @And("^returned response should indicate activity pixels created$")
    public void returnedResponseShouldIndicateActivityPixelsCreated() throws Throwable {
        placementActions.storeNewCreatedActivityPixelResposne();
    }

	@Then("^user request the activity pixel API with PUT Method to update created pixels with additional details$")
	public void userRequestTheActivityPixelAPIWithPUTMethodToUpdateCreatedPixelsWithAdditionalDetails() throws Throwable {
		placementActions.buildActivityPixelInfoWithAddtionalDetails();
	}

	@And("^returned response should indicate activity pixels updated with additional information$")
	public void returnedResponseShouldIndicateActivityPixelsUpdatedWithAdditionalInformation() throws Throwable {
		placementActions.storeActivityPixelResponseData();
	}

	@Then("^user request conversion pixel API with PUT method to assign (\\d+) activity pixels to all placements$")
	public void userRequestConversionPixelAPIWithPUTMethodToAssignActivityPixelsToAllPlacements(int countOfPixel) throws Throwable {
		placementActions.assignConversionPixelToPlacements(countOfPixel);
	}

    @And("^returned response should indicate conversion pixels assigned to all placements$")
    public void returnedResponseShouldIndicateConversionPixelsAssignedToAllPlacements() throws Throwable {
        placementActions.storeConversionPixelResponse();
    }


}
