/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.TargetingActions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

/**
 * @author Clearstream.
 */
public class TargetingMasterDataSteps {

    @Steps
    TargetingActions targetingActions;

//    @When("^user request the targeting master APIs for following entities with POST method$")
//    public void userRequestTheTargetingMasterAPIsForFollowingEntitiesWithPOSTMethod(DataTable targetingDetails)  {
//        List<String> targetingAttributeTypes = targetingDetails.asList(String.class);
//        targetingActions.fetchTargetingMasterDataDetailsFromAPI(targetingAttributeTypes);
//    }

    @And("^returned response should be extracted to a JSON file$")
    public void returnedResponseShouldBeExtractedToAJSONFile() {
        targetingActions.storeTargetingMasterDataJsonResponseToJsonFile();
    }


    @Then("^user request the targeting master APIs for \"([^\"]*)\" with POST method$")
    public void userRequestTheTargetingMasterAPIsForWithPOSTMethod(String entityName) throws Throwable {
        targetingActions.fetchTargetingMasterDataDetailsFromAPI(entityName);
    }


}
