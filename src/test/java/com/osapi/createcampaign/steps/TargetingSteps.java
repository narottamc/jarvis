/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.TargetingActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class TargetingSteps
{

	@Steps
	TargetingActions targetingActions;

	@Then("^user POST a PUT request to associate Targeting details for placements as follows:$")
	public void user_POST_a_PUT_request_to_associate_Targeting_details_for_placements_as_follows(
			DataTable targetingDetails) throws Exception
	{
		List<Map<String, String>> targetingDetailsMap = targetingDetails.asMaps(String.class, String.class);
		targetingActions
				.user_POST_a_PUT_request_to_associate_Targeting_details_for_placements_as_follows(targetingDetailsMap);
	}

	@Then("^Targeting response should have status code as (\\d+) and content-typs as JSON$")
	public void targeting_response_should_have_status_code_as_and_content_typs_as_JSON(int statusCode) throws Exception
	{
		assertThat("Verify Content Type for Targeting Api ", targetingActions.getContentType(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Targeting Api ", targetingActions.getStatusCode(), equalTo(statusCode));
	}

    @And("^returned response should indicate Targeting details associated with placements$")
    public void returnedResponseShouldIndicateTargteingDetailsAssociatedWithPlacements() throws Throwable {
		targetingActions.storeTargetinResponseToJsonFile();
    }


}
