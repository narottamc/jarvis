/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.DisplayActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class DisplaySteps
{
	@Steps
	DisplayActions displayActions;

	@Then("^user POST a POST request to Display EndPoint URL to add display assets as follows:$")
	public void userPOSTAPOSTRequestToDisplayEndPointURLToAddDisplayAssetsAsFollows(DataTable displayDataTable)
			throws Throwable
	{
		List<Map<String, String>> displayMap = displayDataTable.asMaps(String.class, String.class);
		displayActions.createDisplayAssets(displayMap);
	}

	@And("^Display response should have status code as (\\d+) and content-type as JSON\\.$")
	public void displayResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode) throws Throwable
	{
		assertThat("Verify Content Type for Display Api ", displayActions.getContentTypeForDisplayRequest(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Display Api ", displayActions.getStatusCodeForDisplayRequest(),
				equalTo(statusCode));
	}

	@And("^returned response should indicate display assets created\\.$")
	public void returnedResponseShouldIndicateDisplayAssetsCreated() throws Throwable
	{
		displayActions.storeDisplayResponseToJsonFile();
	}

}
