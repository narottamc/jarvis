/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.AuthenticationActions;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.IsNull;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class AuthenticationSteps
{

	@Steps
	private AuthenticationActions authenticationActions;


	@Given("^user has valid login credential as \"([^\"]*)\" user to ClearStream-OS$")
	public void userHasValidLoginCredentialAsUserToClearStreamOS(String userType)
	{
		authenticationActions.getJSONRequestWithValidLogInData(userType);
	}

	@When("^user posts a POST request to LogIn API$")
	public void userPostsAPOSTRequestToLogInAPI() throws Throwable
	{
		authenticationActions.requestLogInAPIWithPostMethod();
	}

	
	@Then("^LogIn response should have status code as (\\d+) and content-type as JSON$")
	public void loginResponseShouldHaveStatusCodeAsandContentTypeAsJSON(int statusCode) 
	{
		assertThat("Verify Content Type for Log-In Api ", authenticationActions.getContentTypeOfResponse(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Log-In Api ", authenticationActions.getStatusCodeOfResponse(), equalTo(statusCode));

	}

	@And("^user is authenticated successfully with valid login credentials\\.$")
	public void userIsAuthenticatedSuccessfullyWithValidLoginCredentials()
	{
		authenticationActions.setUpAccessTokenAndFilterObject();
		assertThat("Verify Access-Token from Log-In API Response",
				authenticationActions.getValueFromResponseOfHeader("Access-Token"), IsNull.notNullValue());
		assertThat("Verify Client from Log-In API Response",
				authenticationActions.getValueFromResponseOfHeader("Client"), IsNull.notNullValue());
		assertThat("Verify TokenType from Log-In API Response",
				authenticationActions.getValueFromResponseOfHeader("Token-Type"), equalTo("Bearer"));
		//add verificaion for expiry
		assertThat("Verify UID from Log-In API Response",
				authenticationActions.getValueFromResponseOfHeader("Uid"), equalTo(authenticationActions.getContentBodyFromRequest().getEmail()));
	}


	@Then("^user validates if credentials with access to different product types should exist$")
	public void userValidatesIfCredentialsWithAccessToDifferentProductTypesShouldExist() throws Throwable {
		authenticationActions.verifyUserExists(authenticationActions.getUserListResponse());
	}

	@And("^user create credentials with access according to product types$")
	public void userCreateCredentialsWithAccessAccordingToProductTypes() throws Throwable {
		authenticationActions.createUsersAndVerify();
	}


}
