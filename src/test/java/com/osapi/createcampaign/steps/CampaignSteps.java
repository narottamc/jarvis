/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.CampaignActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.IsNull;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class CampaignSteps
{
	@Steps
	CampaignActions campaignActions;

	@Then("^user post a POST request to create Campaign$")
	public void userpostaPOSTRequestToCreateCampaign() throws Exception
	{
		campaignActions.createCampaign();
	}

	@Then("^Campaign response should have status code as (\\d+) and content-type as JSON$")
	public void campaignResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode) throws Exception
	{
		assertThat("Verify Content Type for Create Campaign Api ", campaignActions.getContentType(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Create Campaign Api ", campaignActions.getStatusCode(), equalTo(statusCode));
	}

	@Then("^returned response should indicate a campaign created$")
	public void returnedResponseShouldIndicateaCampaignCreated() throws Exception
	{
		assertThat("Verify Campaign Id is Not Null ", campaignActions.getResponseCreateCampaign().getId(), IsNull.notNullValue());
	}

	@Then("^user request the Campaign API with PUT method to assign business model \"([^\"]*)\" with strategy ID (\\d+) to campaign$")
	public void userRequestTheCampaignAPIWithPUTMethodToAssignBusinessModelWithStrategyIDToCampaign(String businessModelName,
			int strategyId) throws Throwable
	{ 
		campaignActions.assignBusinessModelToCampaign(businessModelName,strategyId);
	}

	@And("^response should indicate business model with strategy ID (\\d+) assigned to campaign$")
	public void responseShouldIndicateBusinessModelWithStrategyIDAssignedToCampaign(int strategyID) throws Throwable
	{
		assertThat("Verify Strategy Id of the Business Model for Campaign", campaignActions.getResponseUpdateCampaign().getBusinessModel().getStrategyId()
				, equalTo(strategyID));
	}

}
