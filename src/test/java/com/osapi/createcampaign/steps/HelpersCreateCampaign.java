/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.osapi.builders.*;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.quarterback.factory.SessionVariables;
import com.quarterback.utils.Constants;
import com.quarterback.utils.PropertiesUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.FilterContext;
import io.restassured.http.ContentType;
import io.restassured.mapper.factory.GsonObjectMapperFactory;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import org.apache.log4j.Logger;
import org.ini4j.Ini;

import javax.annotation.concurrent.NotThreadSafe;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Clearstream
 */
@NotThreadSafe
public class HelpersCreateCampaign {
    private static final Logger LOG = Logger.getLogger(HelpersCreateCampaign.class);

    private static Ini testDataIni = new Ini();
    private static File fileBasePath = new File(Constants.CAMPAIGNDATA_BASEPATH);
    protected static AuthFilter filterAuth;

    @After(order = 2)
    public void updateFlightResponse(Scenario scenario) {
        List<RootFlightsResponse> flightResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFlightsResponseList();
        String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
        String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class).getScenarioTestDatPath();
        AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

        if (!flightResponseList.isEmpty()) {
            LOG.info("Updating Flight Responses with Latest Generated ADTag URLs");
            for (int i = 0; i < flightResponseList.size(); i++) {
                Response flightResponseGet = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .get(FlightBuilder.getEndPointURLFlightForGETRequest(flightResponseList.get(i).getId()));
                String reponseFlightJSONString = flightResponseGet.then().contentType(ContentType.JSON).extract().response()
                        .asString();

                assertThat("Verify Content Type for FLight GET API", flightResponseGet.then().extract().contentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status Code for FLight GET API", flightResponseGet.then().extract().statusCode(),
                        equalTo(200));

                RootFlightsResponse flightResponse = MapBuilderfromJsonString.build(RootFlightsResponse.class, reponseFlightJSONString, false).get();
                for (int j = 0; j < flightResponseList.get(i).getAds().size(); j++) {
                    flightResponseList.get(i).getAds().get(j).setAdTag(flightResponse.getAds().get(j).getAdTag());
                    flightResponseList.get(i).getAds().get(j).setAdTagSecure(flightResponse.getAds().get(j).getAdTagSecure());
                }

            }
            try {
                fileWriter.writeToJsonFile(ObjectType.FLIGHTS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
                        RootFlightsResponse.class,
                        DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFlightsResponseList());
            } catch (JsonIOException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }


    }

    @After(order = 1)
    public void updateCampaignResponse(){
        String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
        String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class).getScenarioTestDatPath();
        AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        if(scenarioTestData!=null && accessToken!=null){
            if(new File(scenarioTestData).exists()){
                LOG.info("Updating Campaign Response in Json File");
                CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
                Response campaignResponseGET = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .get(campaignBuilder.getURLForUpdateCampaign());
                String responseCampaignJsonString = campaignResponseGET.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                assertThat("Verify Content Type for Campaign GET API", campaignResponseGET.then().extract().contentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status Code for Campaign GET API", campaignResponseGET.then().extract().statusCode(),
                        equalTo(200));
                RootCampaignResponse campaignResponse = MapBuilderfromJsonString.build(RootCampaignResponse.class,responseCampaignJsonString,false).get();
                fileWriter.writeToJsonFile(ObjectType.CAMPAIGNS,QueryMethod.RESPONSE).writeRequestSingleJsonObjectToJsonFile(campaignResponse);
            }
        }
    }

    @After(order = 0)
    public void afterFinal(Scenario scenario) {
        DependencyManager.closeInjector();
    }

    @After("@DBConnectionEnd")
    public void endDatabaseConnection(){
        LOG.info("DB Connection Closed for SessionFactory: "+ DBSessionBuilder.sessionFactory.toString());
        Injector injector = Guice.createInjector(new BuilderInjector());
        DBSessionBuilder dbSession = injector.getInstance(DBSessionBuilder.class);
        dbSession.closeSessionFactory();
    }


    @Before(order=1)
    public void getScenarioTags(Scenario scenario) {
        String scenarioTag = null;
        String scenarioTestDataPath = null;
        for (String tag : scenario.getSourceTagNames()) {
            LOG.info("Tag: " + tag);
            tag = tag.substring(1);
            if (tag.toLowerCase().contains("scenario")) {
                scenarioTag = tag;
            }
        }
        if(scenarioTag!=null){
            scenarioTestDataPath = HelpersCreateCampaign.setupTestDataFolderForScenario(scenarioTag);
            HelpersCreateCampaign.setupTestDataForRequestQueryMethod(scenarioTestDataPath);
            HelpersCreateCampaign.setupTestDataForResponseQueryMethod(scenarioTestDataPath);
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setScenarioTestDatPath(scenarioTestDataPath);
            DependencyManager.getInjector().getInstance(SessionVariables.class).setScenarioTestDataPath(scenarioTestDataPath);
            LOG.info("Scenario Test Data path is" + " " + DependencyManager.getInjector().getInstance(RequestBuilder.class).getScenarioTestDatPath());
        }
        long threadId = Thread.currentThread().getId();
        String processName = ManagementFactory.getRuntimeMXBean().getName();
        LOG.info("Started in thread: " + threadId + ", in JVM: " + processName);
    }

    @Before(order=0)
    public void setupConfigurationAndFolders(){
        LOG.info("Configuring Serenity Rest Object Mappeer GSON");
        ObjectMapperConfig objectMapperConfig = new ObjectMapperConfig();
        SerenityRest.setDefaultConfig(RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig.gsonObjectMapperFactory(
                new GsonObjectMapperFactory() {
                    public Gson create(Class cls, String charset) {
                        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
                    }
                }
        )));
        if (!fileBasePath.exists()) {
            new File(Constants.CAMPAIGNDATA_BASEPATH).mkdirs();
            LOG.info("Creating Directory for TestData for OSAPI" + " " + fileBasePath.getName());
        }
        LOG.info("TestData Folder Exists for OSAPI");
        PropertiesUtils.loadAWSProperties();
        DependencyManager.initializeWith(new BuilderInjector());
        HelpersCreateCampaign.setUpTestDataForSegmentsDataSource();
        HelpersCreateCampaign.setUpTestDataForTargetingDataSource();
    }

    @Before("@DBConnectionStart")
    public void initializedDatabaseConnection(){
        LOG.info("Intializing Database Connection Session Factory");
        Injector injector = Guice.createInjector(new BuilderInjector());
        DBSessionBuilder dbSession = injector.getInstance(DBSessionBuilder.class);
        dbSession.initializeDBSession();
        DBSessionBuilder.sessionFactory.openSession();
        LOG.info("Database Connection Initialized with SessionFactory: "+ DBSessionBuilder.sessionFactory.toString());
    }

    public static void authenticate(Response respLogin) {
        filterAuth = new AuthFilter() {

            @Override
            public Response filter(FilterableRequestSpecification requestSpec,
                                   FilterableResponseSpecification responseSpec, FilterContext ctx) {
                requestSpec.header("Access-Token", respLogin.getHeaders().getValue("Access-Token"));
                requestSpec.header("Client", respLogin.getHeaders().getValue("Client"));
                requestSpec.header("Expiry", respLogin.getHeaders().getValue("Expiry"));
                requestSpec.header("Token-Type", respLogin.getHeaders().getValue("Token-Type"));
                requestSpec.header("Uid", respLogin.getHeaders().getValue("Uid"));
                return ctx.next(requestSpec, responseSpec);
            }
        };
        DependencyManager.getInjector().getInstance(RequestBuilder.class).setFilterAuth(filterAuth);
        DependencyManager.getInjector().getInstance(RequestBuilder.class)
                .setAccessToken(respLogin.getHeaders().getValue("Access-Token"));

    }

    public static String setupTestDataFolderForScenario(String scenarioTag) {
        String scenarioTestDataPath = null;
        File scenarioTestDataFolder = new File(Constants.CAMPAIGNDATA_BASEPATH + "\\" + scenarioTag);
        if (fileBasePath.exists()) {
            if (!scenarioTestDataFolder.exists()) {
                new File(Constants.CAMPAIGNDATA_BASEPATH + "\\" + scenarioTag).mkdirs();
                LOG.info("Creating Directory for Scenario" + " " + scenarioTag);
                scenarioTestDataPath = scenarioTestDataFolder.getPath();
            } else {
                scenarioTestDataPath = scenarioTestDataFolder.getPath();
            }
        }

        return scenarioTestDataPath;
    }

    public static void setUpTestDataForTargetingDataSource(){
        try {
            testDataIni.load(Constants.TESTDATACONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ini.Section section = testDataIni.get(QueryMethod.TARGETINGDATA.toString());
        for (ObjectType type : ObjectType.values()) {
            String filePath = section.get(type.toString());
            if (!(filePath == null)) {
                File targetingDataSourceFolder = new File(filePath);
                if (!targetingDataSourceFolder.getParentFile().exists()) {
                    targetingDataSourceFolder.getParentFile().mkdirs();
                    LOG.info("Creating Directory for Entity" + " " + "for Method" + " "
                            + QueryMethod.TARGETINGDATA.toString() + " " + "with Name:" + " "
                            + targetingDataSourceFolder.getParentFile().getName());
                }
                if (!targetingDataSourceFolder.exists()) {
                    try {
                        targetingDataSourceFolder.createNewFile();
                        LOG.info("Creating Json File for Entity" + " " + type + " " + "for Method" + " "
                                + QueryMethod.TARGETINGDATA.toString() + " " + "with Name:" + " "
                                + targetingDataSourceFolder.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public static void setUpTestDataForSegmentsDataSource(){
        try {
            testDataIni.load(Constants.TESTDATACONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ini.Section section = testDataIni.get(QueryMethod.SEGMENTSDATA.toString());
        for (ObjectType type : ObjectType.values()) {
            String filePath = section.get(type.toString());
            if (!(filePath == null)) {
                File targetingDataSourceFolder = new File(filePath);
                if (!targetingDataSourceFolder.getParentFile().exists()) {
                    targetingDataSourceFolder.getParentFile().mkdirs();
                    LOG.info("Creating Directory for Entity" + " " + "for Method" + " "
                            + QueryMethod.SEGMENTSDATA.toString() + " " + "with Name:" + " "
                            + targetingDataSourceFolder.getParentFile().getName());
                }
                if (!targetingDataSourceFolder.exists()) {
                    try {
                        targetingDataSourceFolder.createNewFile();
                        LOG.info("Creating Json File for Entity" + " " + type + " " + "for Method" + " "
                                + QueryMethod.SEGMENTSDATA.toString() + " " + "with Name:" + " "
                                + targetingDataSourceFolder.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public static void setupTestDataForRequestQueryMethod(String scenarioTestDataPath) {
        try {
            testDataIni.load(Constants.TESTDATACONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ini.Section section = testDataIni.get(QueryMethod.REQUEST.toString());
        for (ObjectType type : ObjectType.values()) {
            String filePath = section.get(type.toString());
            if (!(filePath == null)) {
                File scenarioTestDataFolder = new File(scenarioTestDataPath + filePath);
                if (!scenarioTestDataFolder.getParentFile().exists()) {
                    scenarioTestDataFolder.getParentFile().mkdirs();
                    LOG.info("Creating Directory for Entity" + " " + type + " " + "for Method" + " "
                            + QueryMethod.REQUEST.toString() + " " + "with Name:" + " "
                            + scenarioTestDataFolder.getParentFile().getName());
                }
                if (!scenarioTestDataFolder.exists()) {
                    try {
                        scenarioTestDataFolder.createNewFile();
                        LOG.info("Creating Json File for Entity" + " " + type + " " + "for Method" + " "
                                + QueryMethod.REQUEST.toString() + " " + "with Name:" + " "
                                + scenarioTestDataFolder.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }

    }

    public static void setupTestDataForResponseQueryMethod(String scenarioTestDataPath) {
        try {
            testDataIni.load(Constants.TESTDATACONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ini.Section section = testDataIni.get(QueryMethod.RESPONSE.toString());
        for (ObjectType type : ObjectType.values()) {
            String filePath = section.get(type.toString());
            if (!(filePath == null)) {
                File scenarioTestDataFolder = new File(scenarioTestDataPath + filePath);
                if (!scenarioTestDataFolder.getParentFile().exists()) {
                    scenarioTestDataFolder.getParentFile().mkdirs();
                    LOG.info("Creating Directory for Entity" + " " + type + " " + "for Method" + " "
                            + QueryMethod.RESPONSE.toString() + " " + "with Name:" + " "
                            + scenarioTestDataFolder.getParentFile().getName());
                }
                if (!scenarioTestDataFolder.exists()) {
                    try {
                        scenarioTestDataFolder.createNewFile();
                        LOG.info("Creating Json File for Entity" + " " + type + " " + "for Method" + " "
                                + QueryMethod.RESPONSE.toString() + " " + "with Name:" + " "
                                + scenarioTestDataFolder.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}
