/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.AssetsActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class AssetsSteps
{
	@Steps
	private AssetsActions assetsActions;

	@Then("^user post a POST request to Asset Endpoint URL to create video assets of following types:$")
	public void userPostAPOSTRequestToAssetEndpointURLToCreateVideoAssetsOfFollowingTypes(DataTable assetsDetailsTable)

	{
		List<Map<String, String>> assetsDetailsMap = assetsDetailsTable.asMaps(String.class, String.class);
		assetsActions.userPostAPOSTRequestToAssetEndpointURLToCreateThirdyPartyAssetsOfFollowingTypes(assetsDetailsMap);
	}

	@Then("^Asset response should have status code as (\\d+) and content-type as JSON$")
	public void assetsResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode) 
	{
		assertThat("Verify Content Type for Assets Api ", assetsActions.getContentType(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Assets Api ", assetsActions.getStatusCode(), equalTo(statusCode));
	}
	
	@Then("^User request the Asset API with PUT method to Generate VAST for ClearStream Ad Tag Type assets$")
	public void userRequestTheAssetAPIWithPUTMethodToGenerateVASTForClearStreamAdTagTypeAssets()
	{
		assetsActions.generateAndValidateVASTForClearStreaAdTagType();
	}
	
	@Then("^user validates generated VAST TAG URLs for all Assets with GET Method to have status code as (\\d+)$")
	public void userValidatesGeneratedVASTTAGURLsForAllAssetsWithGETMethodToHaveStatusCodeAs(int arg1)
	{
		assetsActions.verifyGenerateVASTTag();
	}

	@Then("^returned response should indicate assets created$")
	public void returnedResponseShouldIndicateaAssetsCreated() 
	{
		assetsActions.storeAssetsResponseToJsonFile();
	}

	
}
