/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.MediaInventoryActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class MediaInventorySteps
{
	@Steps
	MediaInventoryActions mediaInventoryActions;

	@Then("^user POST a PUT request to associate Media Inventory details for placements as follows:$")
	public void user_POST_a_PUT_request_to_associate_Media_Inventory_details_for_placements_as_follows(
			DataTable mediaInventoryDetails) throws Exception
	{
		List<Map<String, String>> mediaInventoryDetailsMap = mediaInventoryDetails.asMaps(String.class, String.class);
		mediaInventoryActions.user_POST_a_PUT_request_to_associate_Media_Inventory_details_for_placements_as_follows(
				mediaInventoryDetailsMap);
	}

	@Then("^Media Inventory response should have status code as (\\d+) and content-type as JSON$")
	public void media_Inventory_response_should_have_status_code_as_and_content_type_as_JSON(int statusCode)
			throws Exception
	{
		assertThat("Verify Content Type for MediaInventory Api ", mediaInventoryActions.getContentType(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for MediaInventory Api ", mediaInventoryActions.getStatusCode(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate Media Inventory details associated with placements$")
	public void returned_response_should_indicate_Media_Inventory_details_associated_with_placements() throws Exception
	{
		mediaInventoryActions.storeMediaInventoryResponseToJsonFile();
	}

	@Then("^user request the Frauds API with PUT method to add Fraud Details to placements as follows:$")
	public void userRequestTheFraudsAPIWithPUTMethodToAddFraudDetailsToPlacementsAsFollows(DataTable fraudDetailsTable)
			throws Throwable
	{
		List<Map<String, String>> fraudDetails = fraudDetailsTable.asMaps(String.class, String.class);
		mediaInventoryActions.addFraudsDetailsToThePlacement(fraudDetails);
	}

}
