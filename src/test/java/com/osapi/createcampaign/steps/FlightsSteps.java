/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.FlightsActions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class FlightsSteps
{

	@Steps
	FlightsActions flightActions;

	@Then("^user post a POST Request to Flight Endpoint URL to create flights as following:$")
	public void userPostAPOSTRequestToCreateFlights(DataTable flightRequestData) 
	{
		List<Map<String, String>> flightDetailsMap = flightRequestData.asMaps(String.class, String.class);
		flightActions.userPostAPOSTRequestToFlightEndpointURLToCreateFlightsAsFollowing(flightDetailsMap);
	}

	@Then("^Flight response should have status code as (\\d+) and content-type as JSON$")
	public void flightsResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode)
	{
		assertThat("Verify Content Type for Log-In Api ", flightActions.getContentTypeForNewFlightRequest(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Log-In Api ", flightActions.getStatusCodeForNewFlightRequest(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate flights created$")
	public void returnedResponseShouldIndicateFlightsCreated() 
	{
		flightActions.storeAddFlightResponseToJsonFile();
	}

	@Then("^Flight Update response should have status code as (\\d+) and content-type as JSON$")
	public void flightUpdateResponseShouldHaveStatusCodeasAndContentTypeasJSON(int statusCode)
			
	{
		assertThat("Verify Content Type for Log-In Api ", flightActions.getContentTypeForUpdateFlightRequest(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Log-In Api ", flightActions.getStatusCodeForUpdateFlightRequest(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate flights associated with assets$")
	public void returnedResponseShouldIndicateFlightsassociatedWithAssets()
	{
		flightActions.storeUpdatedFlightResponsToJsonFile();
	}

	@Then("^user post a PUT request to associate flights of \"([^\"]*)\" with \"([^\"]*)\" assets as follows$")
	public void userPostAPUTRequestToAssociateFlightsOfWithAssetsAsFollows(String placement, String assetType,
			DataTable flightDetails) 
	{
		List<Map<String, String>> updateFlightDetailsMap = flightDetails.asMaps(String.class, String.class);
		flightActions.userPostAPUTRequestToAssociateFlightsOfWithAssetsAsFollows(placement, assetType,
				updateFlightDetailsMap);
	}

}
