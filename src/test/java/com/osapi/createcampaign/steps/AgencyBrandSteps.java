/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.createcampaign.steps;

import com.osapi.actions.AgencyBrandActions;
import com.quarterback.utils.CommonUtils;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.IsNull;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class AgencyBrandSteps
{

	@Steps
	AgencyBrandActions agencyBrandActions;
	Random randomNumber = new Random();
	private String agencyName = "Auto-Agency" + CommonUtils.getDateTimeStampAsString()+randomNumber.nextInt(100)+1;
	private String brandName = "Auto-Brand" + CommonUtils.getDateTimeStampAsString()+randomNumber.nextInt(100)+1;
	
	@Then("^user post a POST request to create Agency$")
	public void userPostAPOSTRequestToCreateAgency() 
	{
	   agencyBrandActions.userPostAPOSTRequestToCreateAgencyWithName(agencyName);
	}

	@Then("^Agency response should have status code as (\\d+) and content-type as JSON$")
	public void agencyResponseShouldHaveStatusCodeAsAndContentTypeAsJson(int statusCode) throws Exception
	{
		assertThat("Verify Content Type for Create Agency Api ", agencyBrandActions.getContentTypeforAgency(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Create Agency Api ", agencyBrandActions.getStatusCodeforAgency(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate a agency created$")
	public void returnedResponseShouldIndicateAgencyCreated() 
	{
		assertThat("Verify Agnecy ID is not Null", agencyBrandActions.validateAgencyResponseFromJson().getId(),
				IsNull.notNullValue());
		assertThat("Verify Agency Name", agencyBrandActions.validateAgencyResponseFromJson().getName(),
				equalTo(agencyName));
	}

	@Then("^user post a POST request to create Brand$")
	public void userPostRequestToCreateBrand() 
	{	
		agencyBrandActions.userPostAPOSTRequestToCreateBrandWithName(brandName);	
	}

	@Then("^Brand response should have status code as (\\d+) and content-type as JSON$")
	public void brandResponseShouldHaveStatusCodeAsAndContentTypeAsJson(int statusCode) throws Exception
	{
		assertThat("Verify Content Type for Create Brand Api ", agencyBrandActions.getContentTypeforBrand(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Create Brand Api ", agencyBrandActions.getStatusCodeforBrand(),
				equalTo(statusCode));
	}

	@Then("^returned response should indicate a brand created$")
	public void returnedResponseShouldIndicateBrandCreated() 
	{
		assertThat("Verify Brand ID is not Null", agencyBrandActions.validateBrandResponseFromJson().getId(),
				IsNull.notNullValue());
		assertThat("Verify Brand Name", agencyBrandActions.validateBrandResponseFromJson().getName(),
				equalTo(brandName));
	}
}
