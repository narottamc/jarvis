/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.builders.SurveyBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class SurveyActions
{

	private static final Logger LOG = Logger.getLogger(SurveyActions.class);
	private Response responseSurvey;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void createNewSurvey(List<Map<String, String>> surveyDetails)
	{
		Integer surveyCount = surveyDetails.size();

		String answerType;
		String customResponse;
		String assetSize = null;
		SurveyBuilder surveyBuilder = new SurveyBuilder(scenarioTestData);
		LOG.info("Number of Surveys to be added are" + " " + surveyCount);
		for (Map<String, String> surveyMap : surveyDetails)
		{
			List<String> keysList = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : surveyMap.entrySet())
			{
				keysList.add(keyEntries.getKey());
			}
			Integer surveyNumber = Integer
					.parseInt(CharMatcher.inRange('0', '9').retainFrom(surveyMap.get(keysList.get(0))));
			answerType = surveyMap.get(keysList.get(1));
			customResponse = surveyMap.get(keysList.get(2));
			assetSize =  surveyMap.get(keysList.get(3));
			surveyBuilder.buildSurveyRequest(surveyNumber, answerType, customResponse,assetSize);

			DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.setRootSurveyRequestList(surveyBuilder.getRootSurveyRequest());

			responseSurvey = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(surveyBuilder.getRootSurveyRequest(), ObjectMapperType.GSON)
					.post(surveyBuilder.getSurveyEndPointURL());
			assertThat("Verify Content Type for Survey # "+surveyNumber, this.getContentTypeForSurveyRequest(),
					equalTo("application/json; charset=utf-8"));
			assertThat("Verify Status code for Survey # "+surveyNumber, this.getStatusCodeForSurveyRequest(),
					equalTo(200));
			assertThat("Verify Id for Survey # "+surveyNumber + " should Not be Null", 
					responseSurvey.then().extract().as(RootSurveyResponse.class, ObjectMapperType.GSON).getId(),IsNull.notNullValue());
			assertThat("Verify Name for Survey # "+surveyNumber + " should Not be Null", 
					responseSurvey.then().extract().as(RootSurveyResponse.class, ObjectMapperType.GSON).getName(),
					equalTo(surveyBuilder.getRootSurveyRequest().getSurvey().getName()));
			assertThat("Verify 3rd Party Ad Tag for Survey # "+surveyNumber + " should Not be Null", 
					responseSurvey.then().extract().as(RootSurveyResponse.class, ObjectMapperType.GSON).getThirdPartyAdTag(),IsNull.notNullValue());
			String responseSurveyJSONString = responseSurvey.then().contentType(ContentType.JSON).extract().response()
					.asString();
			LOG.info("Survey Assets Added of Answer Type " + " " + answerType + " " + responseSurveyJSONString);
			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setRootSurveyResponseList(
					MapBuilderfromJsonString.build(RootSurveyResponse.class, responseSurveyJSONString, false).get());
		}
		surveyBuilder.writeSurveyRequestToJsonFile();
	}

	@Step
	public int getStatusCodeForSurveyRequest() 
	{
		return responseSurvey.then().extract().statusCode();
	}

	@Step
	public String getContentTypeForSurveyRequest() 
	{
		return responseSurvey.then().extract().contentType();

	}

	@Step
	public void storeAddSurveyResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.SURVEY, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
					RootSurveyResponse.class,
					DependencyManager.getInjector().getInstance(ResponseBuilder.class).getRootSurveyResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

}
