/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.AuthenticationBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.createcampaign.steps.HelpersCreateCampaign;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.appusersreponse.RootCreateUserResponse;
import com.osapi.models.appusersreponse.RootUsersResponse;
import com.osapi.models.authentication.RootLogInRequest;
import com.quarterback.utils.Constants;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;
import org.ini4j.Ini;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class AuthenticationActions {

    private static final Logger LOG = Logger.getLogger(AuthenticationActions.class);
    private Response requestLogin;
    private Response usersList;
    private List<String> nonExistUserList;
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    private FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
    private AuthenticationBuilder authenticationBuilder;

    @Step
    public void getJSONRequestWithValidLogInData(String userType) {
        authenticationBuilder = new AuthenticationBuilder();
        authenticationBuilder.buildLogInRequest(userType);
    }

    @Step
    public void requestLogInAPIWithPostMethod() {
        requestLogin = SerenityRest.given().contentType("application/json").body(authenticationBuilder.getRootLogInRequest(), ObjectMapperType.GSON).when()
                .post(Constants.SIGNIN_BASEENDPOINT_URL);

    }

    @Step
    public int getStatusCodeOfResponse() {
        return requestLogin.then().extract().statusCode();
    }

    @Step
    public String getContentTypeOfResponse() {
        return requestLogin.then().extract().contentType();

    }

    @Step
    public void setUpAccessTokenAndFilterObject() {
        HelpersCreateCampaign.authenticate(requestLogin);
        LOG.info("Access Token is : "
                + DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken());
    }

    @Step
    public String getValueFromResponseOfHeader(String headerName) {
        return requestLogin.getHeader(headerName);
    }

    @Step
    public RootLogInRequest getContentBodyFromRequest() {
        return authenticationBuilder.getRootLogInRequest();
    }

    @Step
    public RootUsersResponse getUserListResponse(){
         usersList = SerenityRest.given().auth().preemptive().oauth2(DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken()).filter(
                 DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth())
                .contentType("application/json;charset=UTF-8").when()
                .get(Constants.USERS_BASEENDPOINT_URL);
         RootUsersResponse rootUsersResponse = usersList.then().extract().as(RootUsersResponse.class, ObjectMapperType.GSON);
        return rootUsersResponse;
    }

    @Step
    public void verifyUserExists(RootUsersResponse usersList) {
        nonExistUserList = new ArrayList<>();
        Ini testDataIni = new Ini();
        try {
            testDataIni.load(Constants.TESTDATACONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ini.Section section = testDataIni.get(QueryMethod.AUTHENTICATION.toString());
        for (String key : section.keySet()) {
            boolean isExist = false;
            String userEmaild = section.get(key).split(";")[0];
            if (!(key.contentEquals("ADMIN") || key.contentEquals("GUEST"))) {
                for (int i = 0; i < usersList.getUsers().size(); i++) {
                    String emaildId = usersList.getUsers().get(i).getEmail();
                    isExist = userEmaild.contentEquals(emaildId);
                    if (isExist) {
                        break;
                    }
                }
                if (!isExist) {
                    nonExistUserList.add(userEmaild.split("@")[0].toUpperCase());
                }
            }
        }
        LOG.info("User Type that don't exist are " + nonExistUserList);

    }

    @Step
    public void createUsersAndVerify() {
        if (!nonExistUserList.isEmpty()) {
            for (int i = 0; i < nonExistUserList.size(); i++) {
                AuthenticationBuilder authenticationBuilder = new AuthenticationBuilder();
                authenticationBuilder.buildCreateUserRequest(nonExistUserList.get(i));
                LOG.info("Creating User of Type " + nonExistUserList.get(i));
                Response responseCreateUser = SerenityRest.given().given().auth().preemptive().oauth2(DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken()).
                        filter(DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth())
                        .contentType("application/json;charset=UTF-8")
                        .body(authenticationBuilder.getRootCreateUserRequest(), ObjectMapperType.GSON).when()
                        .post(Constants.USERS_BASEENDPOINT_URL);
                assertThat("Verify Content Type for Create User Api ",
                        responseCreateUser.then().extract().contentType(), equalTo("application/json; charset=utf-8"));
                assertThat("Verify Status code for Create User Api ", responseCreateUser.then().extract().statusCode(),
                        equalTo(200));
                assertThat("Verify Id of the New Create User of Type " + nonExistUserList.get(i),
                        responseCreateUser.then().extract().as(RootCreateUserResponse.class, ObjectMapperType.GSON).getId(), IsNull.notNullValue());
                assertThat("Verify Name of the New Create User of Type " + nonExistUserList.get(i),
                        responseCreateUser.then().extract().as(RootCreateUserResponse.class, ObjectMapperType.GSON)
                                .getFirstName(), equalTo(authenticationBuilder.getRootCreateUserRequest().getUser().getFirstName()));
            }
        }

    }

}
