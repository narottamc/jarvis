/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.osapi.builders.*;
import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.conversionpixelrequest.RootConversionPixelRequest;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.dealsandseatsrequest.RootDealsAndSeatsRequest;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.fraudsrequest.RootFraudRequest;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.mediainventoryrequest.RootMediaListRequest;
import com.osapi.models.mediainventoryrequest.RootSmartListRequest;
import com.osapi.models.placementrequest.RootPlacementRequest;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.targetingrequest.*;
import com.osapi.models.targetingresponse.*;
import com.quarterback.utils.Constants;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.TreeSet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeletePlacementActions {
    private static final Logger LOG = Logger.getLogger(DeletePlacementActions.class);
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    private RootCampaignResponse campaignResponse = MapBuilderfromJsonFile.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA
            , scenarioTestData, false).get();
    private String productType;
    private TreeSet<Integer> deletedEntitiesIds = new TreeSet<>();

    public TreeSet<Integer> getDeletedEntitiesIds() {
        return deletedEntitiesIds;
    }

    public String getProductType() {
        return productType;
    }

    public RootCampaignResponse getCampaignResponse() {
        return campaignResponse;
    }

    @Step
    public void setUpPlacementInformation() {
        //Placement's Info
        List<RootPlacementRequest> placementRequestList = MapBuilderfromJsonFile.build(RootPlacementRequest.class, Constants.PLACEMENT_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootPlacementResponse> placementResponseList = MapBuilderfromJsonFile.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        //Placement's Flight Info
        List<RootFlightRequest> flightRequestsList = MapBuilderfromJsonFile.build(RootFlightRequest.class, Constants.FLIGHTS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootFlightsResponse> flightsResponses = MapBuilderfromJsonFile.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        for (int i = 0; i < flightsResponses.size(); i++) {
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootFlightRequestListUpdated(flightRequestsList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setFlightsResponseList(flightsResponses.get(i));
        }

        //Placement's MediaInvenrtory Info
        List<RootMediaListRequest> mediaListRequests = MapBuilderfromJsonFile.build(RootMediaListRequest.class, Constants.MEDIALISTS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootSmartListRequest> smartListRequests = MapBuilderfromJsonFile.build(RootSmartListRequest.class, Constants.SMARTLISTS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootMediaInventoryResponse> mediaInventoryResponsesMediaList = MapBuilderfromJsonFile.build(RootMediaInventoryResponse.class, Constants.MEDIALIST_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootMediaInventoryResponse> mediaInventoryResponseSmartList = MapBuilderfromJsonFile.build(RootMediaInventoryResponse.class, Constants.SMARTLIST_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        //Placement's Deals And Seat Info
        List<RootDealsAndSeatsRequest> dealsAndSeatsRequests = MapBuilderfromJsonFile.build(RootDealsAndSeatsRequest.class, Constants.DEALSANDSEATS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootDealsAndSeatsResponse> dealsAndSeatsResponses = MapBuilderfromJsonFile.build(RootDealsAndSeatsResponse.class, Constants.DEALSANDSEATS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        //Placement's ConversionPixel Info
        List<RootConversionPixelRequest> conversionPixelRequests = MapBuilderfromJsonFile.build(RootConversionPixelRequest.class, Constants.CONVERSIONPIXELS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootConversionPixelResponse> conversionPixelResponses = MapBuilderfromJsonFile.build(RootConversionPixelResponse.class, Constants.CONVERSIONPIXELS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        //Placement's Frauds Info
        List<RootFraudRequest> fraudRequests = MapBuilderfromJsonFile.build(RootFraudRequest.class, Constants.FRAUDS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootFraudsResponse> fraudsResponses = MapBuilderfromJsonFile.build(RootFraudsResponse.class, Constants.FRAUDS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        //Placement's Targeting Info
        List<RootAditionalOptionsRequest> aditionalOptionsRequests = MapBuilderfromJsonFile.build(RootAditionalOptionsRequest.class, Constants.ADDITIONALOPTIONS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootGeoRequest> geoRequests = MapBuilderfromJsonFile.build(RootGeoRequest.class, Constants.GEOS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootTechnologyRequest> technologyRequests = MapBuilderfromJsonFile.build(RootTechnologyRequest.class, Constants.TECHNOLOGIES_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootDayPartRequest> dayPartRequests = MapBuilderfromJsonFile.build(RootDayPartRequest.class, Constants.DAYPARTS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootCategoryRequest> categoryRequests = MapBuilderfromJsonFile.build(RootCategoryRequest.class, Constants.CATEGORIES_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootAddtionalOptionResponse> addtionalOptionResponses = MapBuilderfromJsonFile.build(RootAddtionalOptionResponse.class, Constants.ADDTIONALOPTION_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootGeoResponse> geoResponses = MapBuilderfromJsonFile.build(RootGeoResponse.class, Constants.GEOS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootTechnologyResponse> technologyResponses = MapBuilderfromJsonFile.build(RootTechnologyResponse.class, Constants.TECHNOLOGIES_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootDayPartResponse> dayPartResponses = MapBuilderfromJsonFile.build(RootDayPartResponse.class, Constants.DAYPARTS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootCategoryResponse> categoryResponses = MapBuilderfromJsonFile.build(RootCategoryResponse.class, Constants.CATEGORIES_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootGeoListResponse> geoListResponses = MapBuilderfromJsonFile.build(RootGeoListResponse.class, Constants.ZIPCODES_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        for (int i = 0; i < placementResponseList.size(); i++) {
            if (conversionPixelRequests != null) {
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setConversionPixelRequestsList(conversionPixelRequests.get(i));
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setConversionPixelResponsesList(conversionPixelResponses.get(i));
            }
            if(dealsAndSeatsRequests!=null){
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootDealsAndSeatsRequestList(dealsAndSeatsRequests.get(i));
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDealsAndSeatsResponseList(dealsAndSeatsResponses.get(i));
            }
            if(aditionalOptionsRequests!=null){
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setAddtionalOptionsRequestList(aditionalOptionsRequests.get(i));
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setAddtionalOptionsResponseList(addtionalOptionResponses.get(i));
            }
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootPlacementRequestListUpdated(placementRequestList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setPlacementResponseList(placementResponseList.get(i));

            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootFraudRequestList(fraudRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setFraudResponseList(fraudsResponses.get(i));

            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootMediaListRequestList(mediaListRequests.get(i));
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootSmartListRequestList(smartListRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setMediaInventoryResponseMediaList(mediaInventoryResponsesMediaList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setMediaInventoryResponseSmartList(mediaInventoryResponseSmartList.get(i));


            DependencyManager.getInjector().getInstance(RequestBuilder.class).setTargetingRequestGeoList(geoRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setTargetingResponseGeoList(geoResponses.get(i));
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setTechnologyRequestList(technologyRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setTechnologyResponseList(technologyResponses.get(i));
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setDayPartRequestList(dayPartRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDayPartResponseList(dayPartResponses.get(i));
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setCategoryRequestList(categoryRequests.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setCategoryResponseList(categoryResponses.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setGeoListsZipCodeList(geoListResponses.get(i));

        }

    }

    @Step
    public boolean verifyTotalPlacementCountGreaterThanOne(String productType) {
        List<RootPlacementResponse> placementResponseList = MapBuilderfromJsonFile.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        boolean campaignWithPlacementProductType = false;
        this.productType = productType;
        for (RootPlacementResponse placementResponse : placementResponseList) {
            if (placementResponse.getProductType().equals(ProductType.valueOf(productType).getproductTypeNumber())) {
                campaignWithPlacementProductType = true;
            }
        }
        return campaignWithPlacementProductType && placementResponseList.size() > 1;
    }

    @Step
    public boolean verifyPlacementsCountAfterDelete(Integer noOfPlacementToDelete) {
        List<RootPlacementResponse> placementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getPlacementResponseList();
        return (placementResponseList.size() - noOfPlacementToDelete) >= 1;
    }

    @Step
    public Integer deleteSpecificPlacement(List<String> placementsToDeleteList, String productType) {
        List<RootPlacementResponse> placementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getPlacementResponseList();
        int placementCountDeleted = 0;
        for (String placementName : placementsToDeleteList) {
            int placementNumber = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(placementName)) - 1;
            if (placementResponseList.get(placementNumber).getProductType().equals(ProductType.valueOf(productType).getproductTypeNumber())) {
                placementCountDeleted++;
                this.deletePlacementForProductType(placementResponseList.get(placementNumber).getId(),productType);
            }
        }
        return placementCountDeleted;
    }

    @Step
    public Integer deleteBulkPlacementsForProductType(Integer noOfPlacementToDelete,String placementProductType) {
        List<RootPlacementResponse> placementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getPlacementResponseList();
        int placementCountDeleted = 0;
        for(RootPlacementResponse placementResponse : placementResponseList){
            if(placementResponse.getProductType().equals(ProductType.valueOf(placementProductType).getproductTypeNumber())
                    && placementCountDeleted<noOfPlacementToDelete){
                placementCountDeleted++;
                this.deletePlacementForProductType(placementResponse.getId(),placementProductType);
            }
        }
        return placementCountDeleted;
    }

    @Step
    public void verifyDeletedPlacements(TreeSet<Integer> deletedEntitiesIds, String productType) {
        PlacementBuilder placementBuilder = new PlacementBuilder();
        List<RootPlacementResponse> placementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getPlacementResponseList();
        for (int i = 0; i < placementResponseList.size(); i++) {
            RootPlacementResponse placementResponse = placementResponseList.get(i);
            if (deletedEntitiesIds.contains(placementResponse.getId()) &&
                    placementResponse.getProductType().equals(ProductType.valueOf(productType).getproductTypeNumber())) {
                Response responseDeletePlacement = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .get(placementBuilder.getEndPointUrlForGETRequest(placementResponse.getId(),campaignResponse.getId()));
                assertThat("Status Code for A GET request to a Deleted Placement Should be 404 : NotFound"
                        , responseDeletePlacement.getStatusCode(), equalTo(404));
                this.removePlacementRelatedInfoFromJson(i,placementResponse.getId());
            }
        }
        PlacementBuilder.writeUpdatedPlacementRequestToFile(scenarioTestData);
        FlightBuilder.writeUpdatedFlightRequestToFile(scenarioTestData);
        FraudsBuilder.storeFraudRequestToFile(scenarioTestData);
        TargetingBuilder.writeUpdatedTargetingRequestToJsonFile(scenarioTestData);
        MediaInventoryBuilder.storeMediaInventoryRequestToJsonFile(scenarioTestData);
    }

    @Step
    private void deletePlacementForProductType(Integer placementId,String placementProductType) {
        PlacementBuilder placementBuilder = new PlacementBuilder();
        Response responseDeletePlacement = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                .delete(placementBuilder.getEndPointUrlForDELETERequest(placementId));
        LOG.info("Placement Of Product Type " + ProductType.valueOf(placementProductType).toString()
                + " with Placement Id " + placementId + " deleted");
        assertThat("Verify Status Code for DELETE Method of Placement API for Placement Id" + " " + placementId
                , responseDeletePlacement.getStatusCode(), equalTo(200));
        this.deletedEntitiesIds.add(placementId);
    }


    public void removePlacementRelatedInfoFromJson(Integer placementNumber, Integer placementId){
        List<RootPlacementRequest> placementRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootPlacementRequestListUpdated();
        List<RootPlacementResponse> placementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getPlacementResponseList();

        List<RootFlightRequest> flightRequestsList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootFlightRequestListUpdated();
        List<RootFlightsResponse> flightsResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFlightsResponseList();

        List<RootMediaListRequest> mediaListRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootMediaListRequestList();
        List<RootSmartListRequest> smartListRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootSmartListRequestList();
        List<RootMediaInventoryResponse> mediaInventoryResponsesMediaList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getMediaInventoryResponseMediaList();
        List<RootMediaInventoryResponse> mediaInventoryResponseSmartList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getMediaInventoryResponseSmartList();

        List<RootFraudRequest> fraudRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootFraudRequestList();
        List<RootFraudsResponse> fraudsResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFraudResponseList();

        List<RootAditionalOptionsRequest> aditionalOptionsRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAddtionalOptionsRequestList();
        List<RootAddtionalOptionResponse> addtionalOptionResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAddtionalOptionsResponseList();

        List<RootGeoRequest> geoRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getTargetingRequestGeoList();
        List<RootGeoResponse> geoResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getTargetingResponseGeoList();

        List<RootTechnologyRequest> technologyRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getTechnologyRequestList();
        List<RootTechnologyResponse> technologyResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getTechnologyResponseList();

        List<RootDayPartRequest> dayPartRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getDayPartRequestList();
        List<RootDayPartResponse> dayPartResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDayPartResponseList();

        List<RootCategoryRequest> categoryRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getCategoryRequestList();
        List<RootCategoryResponse> categoryResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getCategoryResponseList();

        List<RootGeoListResponse> geoListResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getGeoListsZipCodeList();

        //Remove Placement Info
        placementRequestList.removeIf(rootPlacementRequest -> (rootPlacementRequest.getPlacement().getId().intValue()==placementId));
        placementResponseList.removeIf(rootPlacementResponse -> (rootPlacementResponse.getId().equals(placementId)));

        //Remove Flights Info
        flightRequestsList.removeIf(rootFlightRequest -> (rootFlightRequest.getPlacementId().intValue()==placementId));
        flightsResponses.removeIf(rootFlightResponse -> (rootFlightResponse.getPlacementId().equals(placementId)));

        //Remove MediaInventory Info
        mediaListRequests.removeIf(rootMediaListRequest -> (rootMediaListRequest.getPlacementId().equals(placementId)));
        smartListRequests.removeIf(rootSmartListRequest -> (rootSmartListRequest.getPlacementId().equals(placementId)));
        mediaInventoryResponsesMediaList.remove(mediaInventoryResponsesMediaList.get(placementNumber));
        mediaInventoryResponseSmartList.remove(mediaInventoryResponseSmartList.get(placementNumber));

        //Remove Deals&Seats Info
        List<RootDealsAndSeatsRequest> dealsAndSeatsRequests = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootDealsAndSeatsRequestList();
        List<RootDealsAndSeatsResponse> dealsAndSeatsResponses = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDealsAndSeatsResponseList();
        if(dealsAndSeatsRequests.size()>0){
            dealsAndSeatsRequests.remove(dealsAndSeatsRequests.get(placementNumber));
            dealsAndSeatsResponses.remove(dealsAndSeatsResponses.get(placementNumber));
            SSPBuilder.writePUTMethodDealsAndSeatsRequestData(scenarioTestData);
            SSPBuilder.writeGETMethodDealsAndSeatsResponseData(scenarioTestData);
        }

        //Remove Conversion Pixel Info
        List<RootConversionPixelRequest> conversionPixelRequests =
                DependencyManager.getInjector().getInstance(RequestBuilder.class).getConversionPixelRequestsList();
        List<RootConversionPixelResponse> conversionPixelResponses =
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).getConversionPixelResponsesList();
        if (conversionPixelRequests.size()>0) {
            conversionPixelRequests.removeIf(rootConversionPixelRequest -> (rootConversionPixelRequest.getPlacementId().equals(placementId)));
            conversionPixelResponses.remove(conversionPixelResponses.get(placementNumber));
            ActivityPixelBuilder.writeConversionPixelPUTRequestData(scenarioTestData);
            ActivityPixelBuilder.writeConversionPixelPUTResponseData(scenarioTestData);
        }

        //Remove Frauds Info
        fraudRequests.removeIf(rootFraudRequest -> (rootFraudRequest.getPlacementId().equals(placementId)));
        fraudsResponses.remove(fraudsResponses.get(placementNumber));

        //Remove Targeting Info
        if(aditionalOptionsRequests.size()>0){
            aditionalOptionsRequests.remove(aditionalOptionsRequests.get(placementNumber));
            addtionalOptionResponses.remove(addtionalOptionResponses.get(placementNumber));
        }

        geoRequests.removeIf(rootGeoRequest -> (rootGeoRequest.getId().equals(placementId)));
        geoListResponses.remove(geoListResponses.get(placementNumber));
        geoResponses.remove(geoResponses.get(placementNumber));

        categoryRequests.remove(categoryRequests.get(placementNumber));
        categoryResponses.remove(categoryResponses.get(placementNumber));

        dayPartRequests.remove(dayPartRequests.get(placementNumber));
        dayPartResponses.remove(dayPartResponses.get(placementNumber));

        technologyRequests.remove(technologyRequests.get(placementNumber));
        technologyResponses.remove(technologyResponses.get(placementNumber));

    }

}

