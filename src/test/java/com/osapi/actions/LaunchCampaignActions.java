/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.gson.JsonIOException;
import com.osapi.builders.CampaignBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class LaunchCampaignActions
{

	private static final Logger LOG = Logger.getLogger(LaunchCampaignActions.class);

	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	private FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	private AuthFilter filterAuth= DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();

	private Response reponseLaunchCampaign;
	private Response responseActiveCampaign;

	
	@Step
	public void userPostAPOSTRequestToLaunchCampaignUrlToLaunchCampaign()
	{
		CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
		campaignBuilder.setRootLaunchCampaignRequest();

		reponseLaunchCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when()
				.body(campaignBuilder.getRootLaunchCampaignRequest(), ObjectMapperType.GSON)
				.put(campaignBuilder.getURLForLaunchCampaign());

		assertThat("Verify Content Type for Launch Campaign Api ", this.getContentTypeforLaunchCampaign(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Launch Campaign Api ", this.getStatusCodeforLaunchCampaign(), equalTo(200));

		responseActiveCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when().get(campaignBuilder.getURLForActiveCampaign());

		String reponseActiveCampaignJSONString = responseActiveCampaign.then().contentType(ContentType.JSON).extract()
				.response().asString();

		LOG.info("Active Campaign Resposne :" + reponseActiveCampaignJSONString);
	}
	
	@Step
	public void userPostAPOSTRequestToLaunchCampaignUrl()  //This method will not Trigger Quarterback and still will launch the campaign
	{
		CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
		campaignBuilder.setRootLaunchCampaignRequest();

		reponseLaunchCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").queryParam("launch", false).when()
				.body(campaignBuilder.getRootLaunchCampaignRequest(), ObjectMapperType.GSON)
				.put(campaignBuilder.getURLForLaunchCampaign());

		assertThat("Verify Content Type for Launch Campaign Api ", this.getContentTypeforLaunchCampaign(),
				equalTo("application/json; charset=utf-8"));
		assertThat("Verify Status code for Launch Campaign Api ", this.getStatusCodeforLaunchCampaign(), equalTo(200));

		responseActiveCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when().get(campaignBuilder.getURLForActiveCampaign());

		String reponseActiveCampaignJSONString = responseActiveCampaign.then().contentType(ContentType.JSON).extract()
				.response().asString();

		LOG.info("Active Campaign Resposne :" + reponseActiveCampaignJSONString);
	}

	@Step
	public String getContentTypeforActiveCampaign()
	{
		return responseActiveCampaign.then().extract().contentType();

	}

	@Step
	public int getStatusCodeforActiveCampaign()
	{
		return responseActiveCampaign.then().extract().statusCode();
	}

	@Step
	public int getStatusCodeforLaunchCampaign()
	{
		return reponseLaunchCampaign.then().extract().statusCode();
	}

	@Step
	public String getContentTypeforLaunchCampaign()
	{
		return reponseLaunchCampaign.then().extract().contentType();

	}

	@Step
	public void storeActiveCampaignResponseToJsonFile()
	{
		String reponseActiveCampaignJSONString = responseActiveCampaign.then().contentType(ContentType.JSON).extract()
				.response().asString();
		try
		{
			fileWriter.writeToJsonFile(ObjectType.ACTIVECAMPAIGN, QueryMethod.RESPONSE)
					.writeResponseSingleJsonObjectToJsonFile(RootCampaignResponse.class,
							reponseActiveCampaignJSONString);
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

}
