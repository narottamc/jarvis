/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.AgencyandBrandBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.factory.DependencyManager;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

/**
 * @author ClearStream.
 */
public class AgencyBrandActions
{
	private static final Logger LOG = Logger.getLogger(AgencyBrandActions.class);
	private Response responseAgency;
	private Response responseBrand;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void userPostAPOSTRequestToCreateAgencyWithName(String agencyName) 
	{
		AgencyandBrandBuilder agencyBuilder = new AgencyandBrandBuilder(scenarioTestData);
		agencyBuilder.updateAgencyDetails(agencyName);
		responseAgency = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when()
				.body(agencyBuilder.getRootAgencyRequest(), ObjectMapperType.GSON)
				.post(Constants.AGENCY_BASEENDPOINT_URL);
		String responseAgencyJSONString = responseAgency.then().contentType(ContentType.JSON).extract().response()
				.asString();
		LOG.info("Agency Response :" + responseAgencyJSONString);
		agencyBuilder.storeAgencyResponseToFile(responseAgencyJSONString);
	}

	@Step
	public RootAgencyResponse validateAgencyResponseFromJson()
	{
		
		return DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootAgencyResponse(scenarioTestData);
	}

	@Step
	public void userPostAPOSTRequestToCreateBrandWithName(String brandName) 
	{
		AgencyandBrandBuilder brandBuilder = new AgencyandBrandBuilder(scenarioTestData);
		brandBuilder.updateBrandDetails(brandName);
		responseBrand = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when()
				.body(brandBuilder.getRootBrandRequest(), ObjectMapperType.GSON).post(Constants.BRAND_BASEENDPOINT_URL);
		String responseBrandJSONString = responseBrand.then().contentType(ContentType.JSON).extract().response()
				.asString();
		LOG.info("Brand Response :" + responseBrandJSONString);
		brandBuilder.storeBrandResponseToFile(responseBrandJSONString);
	}

	@Step
	public RootBrandResponse validateBrandResponseFromJson()
	{
		return DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootBrandResponse(scenarioTestData);
	}

	@Step
	public int getStatusCodeforAgency()
	{
		return responseAgency.then().extract().statusCode();
	}

	@Step
	public String getContentTypeforAgency()
	{
		return responseAgency.then().extract().contentType();
	}

	@Step
	public int getStatusCodeforBrand()
	{
		return responseBrand.then().extract().statusCode();
	}

	@Step
	public String getContentTypeforBrand()
	{
		return responseBrand.then().extract().contentType();

	}

}
