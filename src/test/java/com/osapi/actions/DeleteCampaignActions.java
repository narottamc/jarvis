/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.CampaignBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.quarterback.utils.Constants;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeleteCampaignActions {
    private static final Logger LOG = Logger.getLogger(DeleteCampaignActions.class);
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    private RootCampaignResponse campaignResponse = MapBuilderfromJsonFile.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA
            , scenarioTestData, false).get();
    public RootCampaignResponse getCampaignResponse() {
        return campaignResponse;
    }

    @Step
    public boolean verifyCampaignExistInDraftState(){
        return campaignResponse.getStateId()==0;
    }

    @Step
    public void deleteACampaign(){
        CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
        Response responseDeleteCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                .delete(campaignBuilder.getURLForDeleteCampaign());
        assertThat("Verify Status Code for DELETE Method of Campaign API for Campaign Id " + campaignResponse.getId()
                , responseDeleteCampaign.getStatusCode(), equalTo(200));
    }

    @Step
    public void verifyDeleteCampaign(){
        CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
        Response responseDeleteCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                .get(campaignBuilder.getURLForUpdateCampaign());
        assertThat("Status Code for A GET request to a Deleted Campaign Should be 404 : NotFound " + campaignResponse.getId()
                , responseDeleteCampaign.getStatusCode(), equalTo(404));
        try {
            FileUtils.deleteDirectory(new File(scenarioTestData));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
