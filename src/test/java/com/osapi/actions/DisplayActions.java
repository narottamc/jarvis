/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.awsutils.AWSFileServicesUtils;
import com.osapi.builders.DisplayBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class DisplayActions
{
	private static final Logger LOG = Logger.getLogger(DisplayActions.class);
	private Response responseDisplay;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	private Random randomNum = new Random();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void createDisplayAssets(List<Map<String, String>> displayDetails)
	{
		Integer displayCount = displayDetails.size();
		Integer displayNumber = null;
		String displayAssetType = null;
		String assetSize=null;
		DisplayBuilder displayBuilder = new DisplayBuilder(scenarioTestData);
		LOG.info("Number of Display Assets to be added are" + " " + displayCount);
		for (Map<String, String> displayMap : displayDetails)
		{
			Path uploadedFile = null;
			List<String> keysList = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : displayMap.entrySet())
			{
				keysList.add(keyEntries.getKey());
			}
			displayNumber = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(displayMap.get(keysList.get(0))));
			displayAssetType = displayMap.get(keysList.get(1));
			assetSize =  displayMap.get(keysList.get(2));
			if (displayAssetType.toUpperCase().contentEquals("SCRIPTTAG"))
			{
				displayBuilder.buildDisplayRequest(displayNumber, displayAssetType, null, assetSize);
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(displayBuilder.getRootDisplayRequest(), ObjectMapperType.GSON)
						.post(displayBuilder.getDisplayEndPointURL());
				assertThat("Verify Content Type of Display # " + displayNumber, this.getContentTypeForDisplayRequest(),
						equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status code of Display # " + displayNumber, this.getStatusCodeForDisplayRequest(),
						equalTo(200));
				assertThat("Verify Id for Display is Not Null of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId(),
						IsNull.notNullValue());
				assertThat("Verify Campaign Id is Not Null of Display # " + displayNumber, responseDisplay.then().extract()
								.as(RootDisplayResponse.class, ObjectMapperType.GSON).getCampaignId(),
						IsNull.notNullValue());
				assertThat("Verify Name For the Display Asset of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getName(),
						equalTo(displayBuilder.getRootDisplayRequest().getDisplay().getName()));
				Integer displayId = responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId();
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.get(displayBuilder.getDisplayEndPointURLForGETRequest(displayId));
			}
			else if (displayAssetType.toUpperCase().contentEquals("ZIP"))
			{
				Path source = Paths.get(Constants.DISPLAYASSET_ZIP_UPLOAD);
				File zipFile = null;

				try
				{
					zipFile = Files
							 .copy(source,source.resolveSibling("AUTOZIP" + CommonUtils.getDateTimeStampAsString() + (randomNum.nextInt(50)+1) + ".zip"))
							 .toFile();
					uploadedFile = Paths.get(zipFile.getPath());
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

				String s3KeyName = Constants.S3_BUCKET_MEDIAFILEUPLOADS_ENVIRONMENT + "/" + zipFile.getName();
				displayBuilder.buildDisplayRequest(displayNumber, displayAssetType, s3KeyName,assetSize);
				LOG.info("ZIP File to be uploaded For Display Asset is " + zipFile.getName());
				AWSFileServicesUtils.uploadFileFromLocaltoS3(Constants.S3_BUCKET_MEDIAFILEUPLOADS, s3KeyName, zipFile);
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(displayBuilder.getRootDisplayRequest(), ObjectMapperType.GSON)
						.post(displayBuilder.getDisplayEndPointURL());
				assertThat("Verify Content Type of Display # " + displayNumber, this.getContentTypeForDisplayRequest(),
						equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status code of Display # " + displayNumber, this.getStatusCodeForDisplayRequest(),
						equalTo(200));
				assertThat("Verify Id for Display is Not Null of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId(),
						IsNull.notNullValue());
				assertThat("Verify Campaign Id is Not Null of Display # " + displayNumber, responseDisplay.then().extract()
								.as(RootDisplayResponse.class, ObjectMapperType.GSON).getCampaignId(),
						IsNull.notNullValue());
				assertThat("Verify Name For the Display Asset of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getName(),
						equalTo(displayBuilder.getRootDisplayRequest().getDisplay().getName()));
				Integer displayId = responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId();
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.get(displayBuilder.getDisplayEndPointURLForGETRequest(displayId));
//				assertThat("Verify ThirdyParty Ad Tag For Display Id " + displayId + " In Campaign Id "+ responseDisplay.then().extract()
//								.as(RootDisplayResponse.class, ObjectMapperType.GSON).getCampaignId() + " Is Not Null",
//						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getThirdPartyAdTag(),
//						IsNull.notNullValue());
				try
				{
					Files.deleteIfExists(uploadedFile);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}


			}
			else if (displayAssetType.toUpperCase().contentEquals("IMAGE"))
			{
				Path source = Paths.get(Constants.DISPLAYASSET_IMAGE_UPLOAD);
				File imageFile = null;
				try
				{
					imageFile = Files
							.copy(source,
									source.resolveSibling("AUTOIMG" + CommonUtils.getDateTimeStampAsString() + (randomNum.nextInt(50)+1) + ".jpg"))
							.toFile();
					uploadedFile = Paths.get(imageFile.getPath());
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				String s3KeyName = Constants.S3_BUCKET_MEDIAFILEUPLOADS_ENVIRONMENT + "/" + imageFile.getName();
				displayBuilder.buildDisplayRequest(displayNumber, displayAssetType, s3KeyName,assetSize);
				LOG.info("Image File to be uploaded For Display Asset is " + imageFile.getName());
				AWSFileServicesUtils.uploadFileFromLocaltoS3(Constants.S3_BUCKET_MEDIAFILEUPLOADS, s3KeyName,
						imageFile);
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(displayBuilder.getRootDisplayRequest(), ObjectMapperType.GSON)
						.post(displayBuilder.getDisplayEndPointURL());
				assertThat("Verify Content Type of Display # " + displayNumber, this.getContentTypeForDisplayRequest(),
						equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status code of Display # " + displayNumber, this.getStatusCodeForDisplayRequest(),
						equalTo(200));
				assertThat("Verify Id for Display is Not Null of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId(),
						IsNull.notNullValue());
				assertThat("Verify Campaign Id is Not Null of Display # " + displayNumber, 
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getCampaignId(),
						IsNull.notNullValue());
				assertThat("Verify Name For the Display Asset of Display # " + displayNumber,
						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getName(),
						equalTo(displayBuilder.getRootDisplayRequest().getDisplay().getName()));
				Integer displayId = responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getId();
				responseDisplay = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.get(displayBuilder.getDisplayEndPointURLForGETRequest(displayId));
//				assertThat("Verify ThirdyParty Ad Tag For Display Id " + displayId + " In Campaign Id "+ responseDisplay.then().extract()
//								.as(RootDisplayResponse.class, ObjectMapperType.GSON).getCampaignId() + " Is Not Null",
//						responseDisplay.then().extract().as(RootDisplayResponse.class, ObjectMapperType.GSON).getThirdPartyAdTag(),
//						IsNull.notNullValue());
			}

			DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.setRootDisplayRequestList(displayBuilder.getRootDisplayRequest());
			String responseDisplayJSONString = responseDisplay.then().contentType(ContentType.JSON).extract().response()
					.asString();
			LOG.info("Display Assets Added of Type " + " " + displayAssetType + " " + responseDisplayJSONString);
			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDisplayAssetResponseList(
					MapBuilderfromJsonString.build(RootDisplayResponse.class, responseDisplayJSONString, false).get());

			try
			{
				if (uploadedFile != null)
				{
					Files.deleteIfExists(uploadedFile);
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}
		displayBuilder.writeDisplayRequestToJsonFile();
	}

	@Step
	public int getStatusCodeForDisplayRequest()
	{
		return responseDisplay.then().extract().statusCode();
	}

	@Step
	public String getContentTypeForDisplayRequest()
	{
		return responseDisplay.then().extract().contentType();

	}

	@Step
	public void storeDisplayResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.DISPLAY, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
					RootDisplayResponse.class,
					DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDisplayAssetResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

}
