/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.osapi.builders.FlightBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.utils.Constants;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 */
public class DeleteFlightActions {
    private static final Logger LOG = Logger.getLogger(DeleteFlightActions.class);
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    private RootCampaignResponse campaignResponse = MapBuilderfromJsonFile.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA
            , scenarioTestData, false).get();
    private List<RootPlacementResponse> placementResponseList = MapBuilderfromJsonFile.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA
            , scenarioTestData, true).getAsCollection();
    private String productType;
    private TreeSet<Integer> deletedEntitiesIds = new TreeSet<>();

    public String getProductType() {
        return productType;
    }

    public TreeSet<Integer> getDeletedEntitiesIds() {
        return deletedEntitiesIds;
    }


    @Step
    public void setUpFlightInformation() {
        //Placement's Info
        for (RootPlacementResponse aPlacementResponse : placementResponseList) {
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setPlacementResponseList(aPlacementResponse);
        }
        //Placement's Flight Info
        List<RootFlightRequest> flightRequestsList = MapBuilderfromJsonFile.build(RootFlightRequest.class, Constants.FLIGHTS_REQUEST_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootFlightsResponse> flightsResponses = MapBuilderfromJsonFile.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();

        for (int i = 0; i < flightsResponses.size(); i++) {
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootFlightRequestListUpdated(flightRequestsList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setFlightsResponseList(flightsResponses.get(i));
        }
    }


    @Step
    public boolean verifyTotalFlightsCountGreaterThanOne(String productType) {
        List<RootFlightsResponse> flightsResponseList = MapBuilderfromJsonFile.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        boolean campaignWithPlacementProductType = false;
        boolean flightsMoreThanOneStatus = false;
        this.productType = productType;
        for (RootPlacementResponse placementResponse : placementResponseList) {
            List<RootFlightsResponse> placementFlightList = new LinkedList<>();
            if (placementResponse.getProductType().equals(ProductType.valueOf(productType).getproductTypeNumber())) {
                campaignWithPlacementProductType = true;
                for (RootFlightsResponse aFlightsResponse : flightsResponseList) {
                    if (placementResponse.getId().equals(aFlightsResponse.getPlacementId())) {
                        placementFlightList.add(aFlightsResponse);
                    }
                }
                flightsMoreThanOneStatus = placementFlightList.size()>1;
            }
        }
        return campaignWithPlacementProductType && flightsMoreThanOneStatus;
    }

    @Step
    private void verifyFlightsCountAfterDelete(Integer placementNumber, Integer noOfFlightsToDelete) {
        List<RootFlightsResponse> placementFlightList = this.getFlightsForAPlacement(placementNumber);
        assertThat("Number of Flights To delete is equal to or greater than the Total Number of Flights in Placement # "
                        + placementNumber + "of Campaign " + campaignResponse.getId() + " AtLeast One Flights Should Exist In A Placement",
                (placementFlightList.size() - noOfFlightsToDelete) >= 1, equalTo(true));

    }

    @Step
    private void verifyPlacementExist(Integer placementNumber){
        assertThat(" Placement # " + placementNumber + "of Campaign " + campaignResponse.getId() + " Does Not Exists",
                placementNumber-1<placementResponseList.size(), equalTo(true));
    }


    @Step
    public void deleteFlightsInBulkForAPlacement(String placementNum, Integer noOfFlightsToDelete, String placementProductType) {
        Integer noOfFlightsDeleted = 0;
            int placementNumber = Integer.parseInt(CharMatcher.inRange('0','9').retainFrom(placementNum));
            this.verifyPlacementExist(placementNumber);
            this.verifyFlightsCountAfterDelete(placementNumber,noOfFlightsToDelete);
            List<RootFlightsResponse> placementFlightList = this.getFlightsForAPlacement(placementNumber);
            for (int i = 0; i < placementFlightList.size(); i++) {
                if (placementResponseList.get(placementNumber - 1).getProductType().equals(ProductType.valueOf(placementProductType).getproductTypeNumber())
                        && noOfFlightsDeleted < noOfFlightsToDelete) {
                    this.deleteAFlight(placementNumber, placementFlightList.get(i), i, placementProductType);
                    noOfFlightsDeleted++;
                }
            }
        assertThat("Flight Requested For Delete In the Placement # " + placementNumber + " does not have Placement product Type as " +
                        placementProductType +" in Campaign "+ campaignResponse.getId()
                , noOfFlightsDeleted,equalTo(noOfFlightsToDelete));
    }

    @Step
    public void deleteSpecificFlightsForAPlacement(Map<String, String> placementFlightMap, String placementProductType) {
        Integer noOfFlightsDeleted = 0;
        for (String placementNum : placementFlightMap.keySet()) {
            Integer placementNumber = Integer.parseInt(CharMatcher.inRange('0','9').retainFrom(placementNum));
            List<RootFlightsResponse> placementsFlightList = this.getFlightsForAPlacement(placementNumber);
            Set<String> flightNumberSet = new HashSet<>(Arrays.asList(placementFlightMap.get(placementNum).split(",")));
            this.verifyPlacementExist(placementNumber);
            this.verifyFlightsCountAfterDelete(placementNumber,flightNumberSet.size());
            for (String aFlightNumber : flightNumberSet) {
                Integer flightNumber = Integer.parseInt(CharMatcher.inRange('0','9').retainFrom(aFlightNumber));
                if (placementResponseList.get(placementNumber - 1).getProductType().equals(ProductType.valueOf(placementProductType).getproductTypeNumber())) {
                    noOfFlightsDeleted++;
                    this.deleteAFlight(placementNumber, placementsFlightList.get(flightNumber - 1), flightNumber, placementProductType);
                }
            }
            assertThat("Flight Requested For Delete In the Placement # " + placementNumber + " does not have Placement product Type as " +
                            placementProductType +" in Campaign "+ campaignResponse.getId()
                    , noOfFlightsDeleted,equalTo(flightNumberSet.size()));
        }
    }

    @Step
    private void deleteAFlight(Integer placementNumber, RootFlightsResponse flightResponse, Integer flightNumber, String productType) {
        FlightBuilder flightBuilder = new FlightBuilder();
        RootPlacementResponse placementResponse = placementResponseList.get(placementNumber - 1);
        Response responseDeleteFlight = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                .delete(flightBuilder.getEndPointURLFlightForDeleteRequest(flightResponse.getId()));
        LOG.info("Flight # " + flightNumber + " For Placement Of Product Type " + ProductType.valueOf(productType).toString()
                + " Of Placement # " + placementNumber + " with Placement Id " + placementResponse.getId() + " deleted");
        assertThat("Verify Status Code for DELETE Method of Flights API for Placement Id" + " " + placementResponse.getId()
                , responseDeleteFlight.getStatusCode(), equalTo(204));
        this.deletedEntitiesIds.add(flightResponse.getId());
    }

    @Step
    public void verifyDeletedFlights(TreeSet<Integer> deletedEntitiesIds) {
        List<RootFlightsResponse> flightsResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFlightsResponseList();
        List<RootFlightRequest> flightRequestsList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootFlightRequestListUpdated();
        for (int i = 0; i < flightsResponseList.size(); i++) {
            RootFlightsResponse flightsResponse = flightsResponseList.get(i);
            if (deletedEntitiesIds.contains(flightsResponse.getId())) {
                Response responseDeletedFlight = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .get(FlightBuilder.getEndPointURLFlightForGETRequest(flightsResponse.getId()));
                assertThat("Status Code for A GET request to a Deleted Flight Should be 404 : NotFound"
                        , responseDeletedFlight.getStatusCode(), equalTo(404));
                flightRequestsList.removeIf(rootFlightRequest -> (rootFlightRequest.getId().equals(flightsResponse.getId())));
                flightsResponseList.removeIf(rootFlightResponse -> (rootFlightResponse.getId().equals(flightsResponse.getId())));
            }
        }
        FlightBuilder.writeUpdatedFlightRequestToFile(scenarioTestData);
    }

    @Step
    private List<RootFlightsResponse> getFlightsForAPlacement(Integer placementNumber) {
        List<RootFlightsResponse> flightsResponseList = MapBuilderfromJsonFile.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA,
                scenarioTestData, true).getAsCollection();
        List<RootFlightsResponse> placementFlightList = new LinkedList<>();
        for (RootFlightsResponse flightsResponse : flightsResponseList) {
            if (placementResponseList.get(placementNumber - 1).getId().equals(flightsResponse.getPlacementId())) {
                placementFlightList.add(flightsResponse);
            }
        }
        return placementFlightList;
    }

}
