/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.builders.SegementsBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.segments.RootSegmentResponse;
import com.osapi.models.segments.Segment;
import com.osapi.models.segmentsdmpproviders.DmpClient;
import com.osapi.models.segmentsdmpproviders.RootDmpProvidersResponse;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Clearstream.
 */
public class SegementsActions<K,V> {
    private static final Logger LOG = Logger.getLogger(SegementsActions.class);
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
    private Map<String,V> hashMapConvertedResponseForDataSegments;

    public Map<String, V> getHashMapConvertedResponseForDataSegments() {
        return hashMapConvertedResponseForDataSegments;
    }

    public void setHashMapConvertedResponseForDataSegments(Map<String, V> hashMapConvertedResponseForDataSegments) {
        this.hashMapConvertedResponseForDataSegments = hashMapConvertedResponseForDataSegments;
    }

    @Step
    public void userRequestTheDmpProviderAPIWithPOSTMethod() {
        Response responseDmpProviders = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                .contentType("application/json;charset=UTF-8").when()
                .get(Constants.DMPPROVIDERS_BASEENDPOINT_URL);
        String responseJSONString = responseDmpProviders.then().contentType(ContentType.JSON).extract().response()
                .asString();
        LOG.info("DMP Provider Resposne Is " + responseJSONString);
        DependencyManager.getInjector().getInstance(ResponseBuilder.class).setMasterDmpProviderResponse(
                MapBuilderfromJsonString.build(RootDmpProvidersResponse.class, responseJSONString, true).getAsCollection());
        assertThat("Verify Content Type of Master Data API of DMP PROVIDERS ", responseDmpProviders.then().extract().contentType(),
                equalTo("application/json; charset=utf-8"));
        assertThat("Verify Content Type of Master Data API of DMP PROVIDERS ", responseDmpProviders.then().extract().statusCode()
                , equalTo(200));

    }

    @Step
    public void userRequestEachDmpClientInEveryDmpProviderWithGETMethod() {
        SegementsBuilder segementsBuilder = new SegementsBuilder(scenarioTestData);
        List<RootDmpProvidersResponse> dmpProvidersList = MapBuilderfromJsonFile.buildFromTemplate(RootDmpProvidersResponse.class,
                Constants.DMPPROVIDERS_MASTERDATAJSON, true).getAsCollection();
        Map<String,Map<String,List<Map<Integer,RootSegmentResponse>>>> finalDataSegementsMap = new HashMap<>();
        Map<String,List<Map<Integer,RootSegmentResponse>>> dmpProviderClientMap = new HashMap<>();
        List<Map<Integer,RootSegmentResponse>> dmpClientSegmentMapList = new ArrayList<>();
        Map<Integer,RootSegmentResponse> dmpClientSegmentMap = new HashMap<>();
        for (RootDmpProvidersResponse dmpProviders : dmpProvidersList) {
            for (DmpClient dmpClient : dmpProviders.getDmpClients()) {
                Map<String, Object> queryParamMapInitial = segementsBuilder.getQueryParamMapForDataSegmentsEndPoint(false, dmpClient.getId(),
                        dmpClient.getDmpProviderId(), 0, 1);
                Integer totalSegements = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").queryParams(queryParamMapInitial).when()
                        .get(Constants.DATASEGEMENTS_BASEENDPOINT_URL).as(RootSegmentResponse.class, ObjectMapperType.GSON).getTotal();
                if(totalSegements>0){
                    Map<String, Object> queryParamMapFinal = segementsBuilder.getQueryParamMapForDataSegmentsEndPoint(false, dmpClient.getId(),
                            dmpClient.getDmpProviderId(), 0, totalSegements);
                    Response responseSegments = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                            .contentType("application/json;charset=UTF-8").queryParams(queryParamMapFinal).when()
                            .get(Constants.DATASEGEMENTS_BASEENDPOINT_URL);
                    String responseJSONString = responseSegments.then().contentType(ContentType.JSON).extract().response()
                            .asString();
                    LOG.info("Data Segement Json Response For DataProvider: " + dmpProviders.getDmpName() + " And DMP Client: " + dmpClient.getDmpClient() +
                            " Is "+ responseJSONString + " with Total Segements Count " + totalSegements);
                    dmpClientSegmentMap.put(dmpClient.getId(),MapBuilderfromJsonString.build(RootSegmentResponse.class,responseJSONString,false).get());
                }
            }
        }
        dmpClientSegmentMapList.add(dmpClientSegmentMap);
        dmpProviderClientMap.put("dmpClient",dmpClientSegmentMapList);
        finalDataSegementsMap.put("body",dmpProviderClientMap);
        this.setHashMapConvertedResponseForDataSegments((Map<String, V>) finalDataSegementsMap);
    }

    @Step
    public void storeDmpProviderMasterDataToJsonFile() {
        fileWriter.writeToJsonFile(Constants.DMPPROVIDERS_MASTERDATAJSON, ObjectType.DMPPROVIDERS, QueryMethod.SEGMENTSDATA).writeRequestSingleJsonObjectToJsonFile(DependencyManager
                .getInjector().getInstance(ResponseBuilder.class).getMasterDmpProviderResponse());
    }

    @Step
    public void storeDataSegmentMasterDataToJsonFile(Map<String,V> map){
        ObjectType.DATASEGMENTS.writeEntityResponseToJsonFile(map);
    }


}
