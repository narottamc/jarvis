/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.CampaignBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignrequest.Campaign;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

/**
 * @author ClearStream.
 */
public class CampaignActions
{

	private static final Logger LOG = Logger.getLogger(CampaignActions.class);
	private Response responseCreateCampaign;
	private Response responseUpdateCampaign;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
	
	
	public RootCampaignResponse getResponseCreateCampaign()
	{
		return responseCreateCampaign.then().contentType(ContentType.JSON).extract().response().as(RootCampaignResponse.class, ObjectMapperType.GSON);
	}
	
	public RootCampaignResponse getResponseUpdateCampaign()
	{
		return responseUpdateCampaign.then().contentType(ContentType.JSON).extract().response().as(RootCampaignResponse.class, ObjectMapperType.GSON);
	}

	@Step
	public void createCampaign()
	{
		CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
		campaignBuilder.updateCampaignDetailsWithAgencyAndBrand();
		responseCreateCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when()
				.body(campaignBuilder.getRootNewCampaignRequest(), ObjectMapperType.GSON)
				.post(Constants.CAMPAIGN_BASEENDPOINT_URL);
		String reponseCampaignJSONString = responseCreateCampaign.then().contentType(ContentType.JSON).extract().response()
				.asString();
		LOG.info("Campaign Response :" + reponseCampaignJSONString);
		campaignBuilder.storeUpdatedCampaignRequest(MapBuilderfromJsonString.build(Campaign.class, reponseCampaignJSONString,false).get());
	}
	
	@Step
	public void assignBusinessModelToCampaign(String businessModelName,Integer strategyId)
	{
		CampaignBuilder campaignBuilder = new CampaignBuilder(scenarioTestData);
		campaignBuilder.assignBusinessModelToCampaign(businessModelName,strategyId);
		responseUpdateCampaign = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
				.contentType("application/json;charset=UTF-8").when()
				.body(campaignBuilder.getRootUpdateCampaignRequest(), ObjectMapperType.GSON)
				.put(campaignBuilder.getURLForUpdateCampaign());
		String reponseCampaignJSONString = responseUpdateCampaign.then().contentType(ContentType.JSON).extract().response()
				.asString();
		LOG.info("Updated Campaign Response :" + reponseCampaignJSONString);
		campaignBuilder.storeUpdatedCampaignRequest(MapBuilderfromJsonString.build(Campaign.class, reponseCampaignJSONString,false).get());
		campaignBuilder.storeCampaignesponseToJson(reponseCampaignJSONString);
	}

	@Step
	public RootCampaignResponse valdiateCampaignResponseFromJson()
	{
		return DependencyManager.getInjector().getInstance(ResponseBuilder.class).getRootCampaignResponse(scenarioTestData);
	}

	@Step
	public int getStatusCode() 
	{
		return responseCreateCampaign.then().extract().statusCode();
	}

	@Step
	public String getContentType() 
	{
		return responseCreateCampaign.then().extract().contentType();
	}

}
