/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.builders.FraudsBuilder;
import com.osapi.builders.MediaInventoryBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.enumtypes.YesOrNo;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class MediaInventoryActions
{

	private static final Logger LOG = Logger.getLogger(MediaInventoryActions.class);
	private Response responseMediaInventory;
	private Response reponseFraud;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void user_POST_a_PUT_request_to_associate_Media_Inventory_details_for_placements_as_follows(
			List<Map<String, String>> mediaInventoryDetails)
	{
		Integer placementNumber;
		String ifAttachSmartList;
		String ifAttachMediaList;
		MediaInventoryBuilder mediaInventoryBuilder = new MediaInventoryBuilder(scenarioTestData);
		for (Map<String, String> mediaInventoryMap : mediaInventoryDetails)
		{
			List<String> keysList = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : mediaInventoryMap.entrySet())
			{
				keysList.add(keyEntries.getKey());
			}
			placementNumber = Integer
					.parseInt(CharMatcher.inRange('0', '9').retainFrom(mediaInventoryMap.get(keysList.get(0))));

			ifAttachMediaList = YesOrNo.YES.toString();
			ifAttachSmartList = YesOrNo.YES.toString();

//			if(ifAttachMediaList.contentEquals(YesOrNo.NO.getName().toUpperCase()) && ifAttachSmartList.contentEquals(YesOrNo.NO.getName().toUpperCase())){
//				ifAttachMediaList= YesOrNo.YES.getName().toUpperCase();
//				ifAttachSmartList= YesOrNo.YES.getName().toUpperCase();
//			}

			LOG.info("Is Media List Enabled For Placement "+ifAttachMediaList);
			LOG.info("Is Smart List Enabled For Placement "+ifAttachSmartList);

			if (ifAttachMediaList.contentEquals(YesOrNo.YES.getName().toUpperCase()))
			{
				mediaInventoryBuilder.setRootMediaListRequest(placementNumber);
				String listType = keysList.get(1);

				DependencyManager.getInjector().getInstance(RequestBuilder.class)
						.setRootMediaListRequestList(mediaInventoryBuilder.getRootMediaListRequest());
				
				responseMediaInventory = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(mediaInventoryBuilder.getRootMediaListRequest(), ObjectMapperType.GSON)
						.put(mediaInventoryBuilder.getEndPointURLForMediaInventory(placementNumber,
								listType.toLowerCase()));

				String reponseMediaListJSONString = responseMediaInventory.then().contentType(ContentType.JSON)
						.extract().response().asString();
				LOG.info("Media List Added for Placement" + " " + placementNumber + " " + reponseMediaListJSONString);

				assertThat("Verify Content Type for Media List Api ", this.getContentType(),
						equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status code for Media List Api ", this.getStatusCode(), equalTo(200));
				assertThat("Verify Media Inventory List for Media List Api ",
						responseMediaInventory.then().extract()
								.as(RootMediaInventoryResponse.class, ObjectMapperType.GSON).getLists(),
						IsNull.notNullValue());
				DependencyManager.getInjector().getInstance(ResponseBuilder.class)
						.setMediaInventoryResponseMediaList(MapBuilderfromJsonString
								.build(RootMediaInventoryResponse.class, reponseMediaListJSONString, false).get());

			}
			if (ifAttachSmartList.contentEquals(YesOrNo.YES.getName().toUpperCase()))
			{
				mediaInventoryBuilder.setRootSmartListRequest(placementNumber);
				String listType = keysList.get(2);

				DependencyManager.getInjector().getInstance(RequestBuilder.class)
						.setRootSmartListRequestList(mediaInventoryBuilder.getRootSmartListRequest());

				responseMediaInventory = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(mediaInventoryBuilder.getRootSmartListRequest(), ObjectMapperType.GSON)
						.put(mediaInventoryBuilder.getEndPointURLForMediaInventory(placementNumber,
								listType.toLowerCase()));
				String reponseSmartListJSONString = responseMediaInventory.then().contentType(ContentType.JSON)
						.extract().response().asString();
				LOG.info("Smart List for Placement" + " " + placementNumber + " " + reponseSmartListJSONString);

				assertThat("Verify Content Type for Smart List Api ", this.getContentType(),
						equalTo("application/json; charset=utf-8"));

				assertThat("Verify Status code for Smart List Api ", this.getStatusCode(), equalTo(200));
				
				assertThat("Verify Smart Inventory List for Media List Api ",
						responseMediaInventory.then().extract()
								.as(RootMediaInventoryResponse.class, ObjectMapperType.GSON).getLists(),
						IsNull.notNullValue());

				DependencyManager.getInjector().getInstance(ResponseBuilder.class)
						.setMediaInventoryResponseSmartList(MapBuilderfromJsonString
								.build(RootMediaInventoryResponse.class, reponseSmartListJSONString, false).get());
			}
		}
		MediaInventoryBuilder.storeMediaInventoryRequestToJsonFile(scenarioTestData);
	}

	
	@Step
	public void addFraudsDetailsToThePlacement(List<Map<String, String>> fraudsDetails) {
	
		
		for(Map<String,String> fraudRequestMap : fraudsDetails)
		{
			FraudsBuilder fraudBuilder = new FraudsBuilder(scenarioTestData, fraudRequestMap);
			fraudBuilder.setFraudRequest();
			DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootFraudRequestList(fraudBuilder.getFraudRequest());
			reponseFraud = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(fraudBuilder.getFraudRequest(), ObjectMapperType.GSON)
					.put(fraudBuilder.getEndPointURLForFrauds());
			String responseFraudJsonString = reponseFraud.then().contentType(ContentType.JSON)
					.extract().response().asString();
			LOG.info("Fraud Section Details for Placement" + " " + fraudBuilder.getPlacementNumber() + " " + responseFraudJsonString);
			
			assertThat("Verify Content Type for Frauds Api ", reponseFraud.then().extract().contentType(),
					equalTo("application/json; charset=utf-8"));
			assertThat("Verify Status code for Frauds Api ", reponseFraud.then().extract().statusCode(), equalTo(200));
			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setFraudResponseList(
					MapBuilderfromJsonString.build(RootFraudsResponse.class, responseFraudJsonString, false).get());
		}
		FraudsBuilder.storeFraudRequestToFile(scenarioTestData);
	}
	
	@Step
	public int getStatusCode()
	{
		return responseMediaInventory.then().extract().statusCode();
	}

	@Step
	public String getContentType()
	{
		return responseMediaInventory.then().extract().contentType();

	}

	@Step
	public void storeMediaInventoryResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.MEDIALIST, QueryMethod.RESPONSE)
					.writeResponseJsonArrayObjectToJsonFile(RootMediaInventoryResponse.class, DependencyManager
							.getInjector().getInstance(ResponseBuilder.class).getMediaInventoryResponseMediaList());
			fileWriter.writeToJsonFile(ObjectType.SMARTLIST, QueryMethod.RESPONSE)
					.writeResponseJsonArrayObjectToJsonFile(RootMediaInventoryResponse.class, DependencyManager
							.getInjector().getInstance(ResponseBuilder.class).getMediaInventoryResponseSmartList());
			fileWriter.writeToJsonFile(ObjectType.FRAUDS, QueryMethod.RESPONSE)
					.writeResponseJsonArrayObjectToJsonFile(RootFraudsResponse.class, 
							DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFraudResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

}
