/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.osapi.builders.*;
import com.osapi.enumtypes.DisplayAssetType;
import com.osapi.enumtypes.VideoAssetsAdTagType;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.assetrequest.RootAssetRequest;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.displayrequest.RootDisplayRequest;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.surveyrequest.RootSurveyRequest;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.quarterback.utils.Constants;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Clearstream.
 */
public class DeleteAssetsActions {
    private static final Logger LOG = Logger.getLogger(DeleteAssetsActions.class);
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    private RootCampaignResponse campaignResponse = MapBuilderfromJsonFile.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA
            , scenarioTestData, false).get();
    private String videoAssetType;
    private String displayAssetType;
    private TreeSet<Integer> deletedEntitiesIds = new TreeSet<>();

    public TreeSet<Integer> getDeletedEntitiesIds() {
        return deletedEntitiesIds;
    }

    public String getVideoAssetType() {
        return videoAssetType;
    }

    public String getDisplayAssetType() {
        return displayAssetType;
    }

    public RootCampaignResponse getCampaignResponse() {
        return campaignResponse;
    }

    @Step
    public boolean campaignWithVideoAssets(String videoAssetType) {
        this.videoAssetType = videoAssetType;
        boolean campaignWithVideoAsset = false;
        List<RootAssetRequest> assetRequestList = MapBuilderfromJsonFile.build(RootAssetRequest.class,
                Constants.ASSET_REQUEST_DATA, scenarioTestData, true).getAsCollection();
        List<RootAssetResponse> assetResponseList = MapBuilderfromJsonFile.build(RootAssetResponse.class,
                Constants.ASSET_RESPONSE_DATA, scenarioTestData, true).getAsCollection();
        for (int i = 0; i < assetResponseList.size(); i++) {
            if (assetResponseList.get(i).getAdTagTypeId().equals(VideoAssetsAdTagType.valueOf(videoAssetType.toUpperCase()).getAdTagTypeId())) {
                campaignWithVideoAsset = true;
            }
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootVideoAssetRequestListUpdated(assetRequestList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setAssetResponseList(assetResponseList.get(i));
        }
        return campaignWithVideoAsset;
    }

    @Step
    public Integer deleteVideoAsset(String videoAssetType, Integer noOfAssetsToDelete) {
        List<RootAssetResponse> assetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAssetResponseList();
        AssetBuilder assetBuilder = new AssetBuilder(scenarioTestData);
        Integer adTagTypeId = VideoAssetsAdTagType.valueOf(videoAssetType.toUpperCase()).getAdTagTypeId();
        int noOfAssetDeleted = 0;
        for (RootAssetResponse assetResponse : assetResponseList) {
            if ((assetResponse.getAdTagTypeId().equals(adTagTypeId)) && (noOfAssetDeleted < noOfAssetsToDelete)) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .delete(assetBuilder.getEndPointURLfromCampaignforAsset() + "/" + assetResponse.getId());
                LOG.info("Video Asset Of Type " + videoAssetType + " with Asset Id " + assetResponse.getId()
                        + " in Campaign " + assetResponse.getCampaignId() + " deleted");
                assertThat("Verify Status Code for DELETE Method of Asset API for Asset #" + " " + assetResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(204));
                this.deletedEntitiesIds.add(assetResponse.getId());
                noOfAssetDeleted++;
            }
        }
        return noOfAssetDeleted;
    }

    @Step
    public void verifyDeletedVideoAsset(String videoAssetType, TreeSet<Integer> deletedEntitiesIds) {
        List<RootAssetRequest> assetRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootVideoAssetRequestListUpdated();
        List<RootAssetResponse> assetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAssetResponseList();
        AssetBuilder assetBuilder = new AssetBuilder(scenarioTestData);
        Integer adTagTypeId = VideoAssetsAdTagType.valueOf(videoAssetType.toUpperCase()).getAdTagTypeId();
        for (RootAssetResponse assetResponse : assetResponseList) {
            if (assetResponse.getAdTagTypeId().equals(adTagTypeId) && deletedEntitiesIds.contains(assetResponse.getId())) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .get(assetBuilder.getEndPointURLfromCampaignforAsset() + "/" + assetResponse.getId());
                assertThat("Status Code for A GET request to a Deleted Video Assets Should be 404 : NotFound "+assetResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(404));
            }
        }
        assetRequestList.removeIf(rootAssetRequest -> (deletedEntitiesIds.contains(rootAssetRequest.getAsset().getId())));
        assetResponseList.removeIf(rootAssetResponse -> (deletedEntitiesIds.contains(rootAssetResponse.getId())));
        assetBuilder.writeUpdatedAssetsRequestToFile();
    }

    @Step
    public boolean verifyCampaignWithDisplayAsset(String displayAssetType) {
        this.displayAssetType = displayAssetType;
        boolean campaingWithDisplayAssetType = false;
        List<RootDisplayRequest> displayAssetRequestList = MapBuilderfromJsonFile.build(RootDisplayRequest.class,
                Constants.DISPLAY_REQUEST_DATA, scenarioTestData, true).getAsCollection();
        List<RootDisplayResponse> displayAssetResponseList = MapBuilderfromJsonFile.build(RootDisplayResponse.class,
                Constants.DISPLAY_RESPONSE_DATA, scenarioTestData, true).getAsCollection();
        for (int i = 0; i < displayAssetResponseList.size(); i++) {
            if (displayAssetResponseList.get(i).getDisplayTypeId().intValue()==DisplayAssetType.valueOf(displayAssetType.toUpperCase()).getDisplayTypeId()) {
                campaingWithDisplayAssetType = true;
            }
            DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootDisplayRequestList(displayAssetRequestList.get(i));
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDisplayAssetResponseList(displayAssetResponseList.get(i));
        }
        return campaingWithDisplayAssetType;
    }

    @Step
    public Integer deleteDisplayAsset(String displayAssetType, Integer noOfAssetsToDelete) {
        List<RootDisplayResponse> displayAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDisplayAssetResponseList();
        DisplayBuilder displayBuilder = new DisplayBuilder(scenarioTestData);
        Integer displayTypeId = DisplayAssetType.valueOf(displayAssetType.toUpperCase()).getDisplayTypeId();
        int noOfAssetDeleted = 0;
        for (RootDisplayResponse displayResponse : displayAssetResponseList) {
            if (displayResponse.getDisplayTypeId().equals(displayTypeId) && noOfAssetDeleted < noOfAssetsToDelete) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .delete(displayBuilder.getDisplayEndPointURL() + "/" + displayResponse.getId());
                LOG.info("Display Asset Of Type " + displayAssetType + " with Asset Id " + displayResponse.getId()
                        + " in Campaign " + displayResponse.getCampaignId() + " deleted");
                assertThat("Verify Status Code for DELETE Method of Display Asset API for Asset #" + " " + displayResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(200));
                this.deletedEntitiesIds.add(displayResponse.getId());
                noOfAssetDeleted++;
            }
        }
        return noOfAssetDeleted;

    }

    @Step
    public void verifyDeletedDisplayAssets(String displayAssetType, TreeSet<Integer> deletedEntitiesIds) {
        List<RootDisplayRequest> displayAssetRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootDisplayRequestList();
        List<RootDisplayResponse> displayAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDisplayAssetResponseList();
        DisplayBuilder displayBuilder = new DisplayBuilder(scenarioTestData);
        Integer displayTypeId = DisplayAssetType.valueOf(displayAssetType.toUpperCase()).getDisplayTypeId();
        Set<String> deletedDisplayAssetsNamesSet = new HashSet<>();
        for (RootDisplayResponse displayResponse : displayAssetResponseList) {
            if (displayResponse.getDisplayTypeId().equals(displayTypeId) && deletedEntitiesIds.contains(displayResponse.getId())) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .get(displayBuilder.getDisplayEndPointURL() + "/" + displayResponse.getId());
                LOG.info("Display Asset Of Type " + displayAssetType + " with Asset Id " + displayResponse.getId()
                        + " in Campaign " + displayResponse.getCampaignId() + " deleted");
                assertThat("Status Code for A GET request to a Deleted Display Assets Should be 404 : NotFound"+displayResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(404));
                deletedDisplayAssetsNamesSet.add(displayResponse.getName());
            }
        }
        displayAssetRequestList.removeIf(rootDisplayRequest -> (deletedDisplayAssetsNamesSet.contains(rootDisplayRequest.getDisplay().getName())));
        displayAssetResponseList.removeIf(rootDisplayResponse -> (deletedDisplayAssetsNamesSet.contains(rootDisplayResponse.getName())));
        displayBuilder.writeDisplayRequestToJsonFile();
    }

    @Step
    public boolean verifyCampaignWithSurveyAsset() {
        boolean campaingWithSurveyAssetType = false;
        List<RootSurveyRequest> surveyAssetRequestList = MapBuilderfromJsonFile.build(RootSurveyRequest.class,
                Constants.SURVEY_REQUEST_DATA, scenarioTestData, true).getAsCollection();
        List<RootSurveyResponse> surveyAssetResponseList = MapBuilderfromJsonFile.build(RootSurveyResponse.class,
                Constants.SURVEY_RESPONSE_DATA, scenarioTestData, true).getAsCollection();
        if(surveyAssetResponseList.size()>0){
            campaingWithSurveyAssetType = true;
            for (int i = 0; i < surveyAssetResponseList.size(); i++) {
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootSurveyRequestList(surveyAssetRequestList.get(i));
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setRootSurveyResponseList(surveyAssetResponseList.get(i));
            }
        }

        return campaingWithSurveyAssetType;
    }

    @Step
    public Integer deleteSurveyAssets(Integer noOfAssetsToDelete) {
        List<RootSurveyResponse> surveyAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getRootSurveyResponseList();
        SurveyBuilder surveyBuilder = new SurveyBuilder(scenarioTestData);
        int noOfAssetDeleted = 0;
        for (RootSurveyResponse surveyResponse : surveyAssetResponseList) {
            if (noOfAssetDeleted < noOfAssetsToDelete) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .delete(surveyBuilder.getSurveyEndPointURL() + "/" + surveyResponse.getId());
                LOG.info("Survey Asset Of Type with Asset Id " + surveyResponse.getId() + " deleted");
                assertThat("Verify Status Code for DELETE Method of Survey Asset API for Asset #" + " " + surveyResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(200));
                this.deletedEntitiesIds.add(surveyResponse.getId());
                noOfAssetDeleted++;
            }
        }
        return noOfAssetDeleted;
    }

    @Step
    public void verifyDeletedSurveyAssets(TreeSet<Integer> deletedEntitiesIds) {
        List<RootSurveyRequest> surveyAssetRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootSurveyRequestList();
        List<RootSurveyResponse> surveyAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getRootSurveyResponseList();
        SurveyBuilder surveyBuilder = new SurveyBuilder(scenarioTestData);
        Set<String> deletedSurveyAssetsNameList = new HashSet<>();
        for (RootSurveyResponse surveyResponse : surveyAssetResponseList) {
            if (deletedEntitiesIds.contains(surveyResponse.getId())) {
                Response responseDeleteAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth).when()
                        .get(surveyBuilder.getSurveyEndPointURL() + "/" + surveyResponse.getId());
                assertThat("Status Code for A GET request to a Deleted Survey Assets Should be 404 : NotFound"+surveyResponse.getId()
                        , responseDeleteAsset.getStatusCode(), equalTo(404));
                deletedSurveyAssetsNameList.add(surveyResponse.getName());
            }

        }
        surveyAssetRequestList.removeIf(rootSurveyRequest -> (deletedSurveyAssetsNameList.contains(rootSurveyRequest.getSurvey().getName())));
        surveyAssetResponseList.removeIf(rootSurveyResponse -> (deletedSurveyAssetsNameList.contains(rootSurveyResponse.getName())));
        surveyBuilder.writeSurveyRequestToJsonFile();
    }
}
