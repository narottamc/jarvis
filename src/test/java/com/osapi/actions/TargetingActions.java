/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.builders.TargetingBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.targetingrequest.GeoList;
import com.osapi.models.targetingresponse.*;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class TargetingActions {

    private static final Logger LOG = Logger.getLogger(TargetingActions.class);
    private Response responseTargeting;
    private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
    private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
    private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
            .getScenarioTestDatPath();
    FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

    @Step
    public void user_POST_a_PUT_request_to_associate_Targeting_details_for_placements_as_follows(
            List<Map<String, String>> targetingDetails) {
        Integer placementNumber;
        String ifAttachGeos;
        String ifAttachTechnology;
        String ifAtachDayParts;
        String ifAttachCategories;
        String ifAttachAddtionalOptions;
        TargetingBuilder targetingBuilder = new TargetingBuilder(scenarioTestData);

        for (Map<String, String> targetingMap : targetingDetails) {
            List<String> keysList = new ArrayList<>();
            for (Map.Entry<String, String> keyEntries : targetingMap.entrySet()) {
                keysList.add(keyEntries.getKey());
            }
            placementNumber = Integer
                    .parseInt(CharMatcher.inRange('0', '9').retainFrom(targetingMap.get(keysList.get(0))));
            ifAttachTechnology = targetingMap.get(keysList.get(1)).toUpperCase();
            ifAttachGeos = targetingMap.get(keysList.get(2)).toUpperCase();
            ifAttachCategories = targetingMap.get(keysList.get(3)).toUpperCase();
            ifAtachDayParts = targetingMap.get(keysList.get(4)).toUpperCase();
            ifAttachAddtionalOptions = targetingMap.get(keysList.get(5)).toUpperCase();

            if (ifAttachTechnology.toUpperCase().contentEquals("YES")) {
                targetingBuilder.setRootTechnologyRequest(placementNumber);
                String listType = keysList.get(1);

                DependencyManager.getInjector().getInstance(RequestBuilder.class)
                        .setTechnologyRequestList(targetingBuilder.getRootTechnologyRequest());

                responseTargeting = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(targetingBuilder.getRootTechnologyRequest(), ObjectMapperType.GSON)
                        .put(targetingBuilder.getEndPointURLTargeting(listType.toLowerCase(), placementNumber));
                String responseJSONString = responseTargeting.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                LOG.info("Technologies for Placement" + " " + placementNumber + " " + responseJSONString);

                assertThat("Verify Content Type for Technologies Api ", this.getContentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status code for Technologies Api ", this.getStatusCode(), equalTo(200));

                assertThat("Verify Technlogies should not be Null for Placement # " + placementNumber, responseTargeting.then().extract()
                                .as(RootTechnologyResponse.class, ObjectMapperType.GSON).getTechnologies(),
                        IsNull.notNullValue());

                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setTechnologyResponseList(
                        MapBuilderfromJsonString.build(RootTechnologyResponse.class, responseJSONString, false).get());

            }

            if (ifAttachGeos.toUpperCase().contentEquals("YES")) {
                targetingBuilder.setRootGeoRequest(placementNumber);
                String listType = keysList.get(2);

                DependencyManager.getInjector().getInstance(RequestBuilder.class)
                        .setTargetingRequestGeoList(targetingBuilder.getRootGeoRequest());

                responseTargeting = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(targetingBuilder.getRootGeoRequest(), ObjectMapperType.GSON)
                        .put(targetingBuilder.getEndPointURLTargeting(listType.toLowerCase(), placementNumber));
                String responseJSONString = responseTargeting.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                LOG.info("Geos for Placement" + " " + placementNumber + " " + responseJSONString);

                assertThat("Verify Content Type for Geos Api ", this.getContentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status code for Geos Api ", this.getStatusCode(), equalTo(200));

                assertThat("Verify Geo List should not be Null for Placement # " + placementNumber, responseTargeting.then().extract()
                                .as(RootGeoResponse.class, ObjectMapperType.GSON).getGeos(),
                        IsNull.notNullValue());

                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setTargetingResponseGeoList(
                        MapBuilderfromJsonString.build(RootGeoResponse.class, responseJSONString, false).get());
                List<GeoList> geoListIdList = targetingBuilder.getRootGeoRequest().getGeosTargeting().getGeoLists();

                for (int i = 0; i < geoListIdList.size(); i++) {
                    Response response = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                            .contentType("application/json;charset=UTF-8").when()
                            .get(targetingBuilder.getEndPointURLForZipCodes(geoListIdList.get(i).getId()));
                    String responseZipCodesJSONString = response.then().contentType(ContentType.JSON).extract().response()
                            .asString();
                    LOG.info("Fetching List of ZipCodes for Geo Lists " + geoListIdList.get(i).getId() + " Response Is: "+ responseZipCodesJSONString);
                    assertThat("Verify Content Type for GeoList ZipCode Api ", this.getContentType(),
                            equalTo("application/json; charset=utf-8"));
                    assertThat("Verify Status code for GeoList ZipCode Api ", this.getStatusCode(), equalTo(200));
                    DependencyManager.getInjector().getInstance(ResponseBuilder.class).setGeoListsZipCodeList(MapBuilderfromJsonString.build(
                            RootGeoListResponse.class,responseZipCodesJSONString,false).get());
                }

            }

            if (ifAttachCategories.toUpperCase().contentEquals("YES")) {
                targetingBuilder.setRootCategoryRequest(placementNumber);
                String listType = keysList.get(3);

                DependencyManager.getInjector().getInstance(RequestBuilder.class)
                        .setCategoryRequestList(targetingBuilder.getRootCategoryRequest());

                responseTargeting = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(targetingBuilder.getRootCategoryRequest(), ObjectMapperType.GSON)
                        .put(targetingBuilder.getEndPointURLTargeting(listType.toLowerCase(), placementNumber));
                String responseJSONString = responseTargeting.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                LOG.info("Categories for Placement" + " " + placementNumber + " " + responseJSONString);

                assertThat("Verify Content Type for Categories Api ", this.getContentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status code for Categories Api ", this.getStatusCode(), equalTo(200));

                assertThat("Verify Target Categories List should not be Null for Placement # " + placementNumber, responseTargeting.then().extract()
                                .as(RootCategoryResponse.class, ObjectMapperType.GSON).getTargetCategories(),
                        IsNull.notNullValue());

                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setCategoryResponseList(
                        MapBuilderfromJsonString.build(RootCategoryResponse.class, responseJSONString, false).get());

            }

            if (ifAtachDayParts.toUpperCase().contentEquals("YES")) {
                targetingBuilder.setRootDaypartRequest(placementNumber);
                String listType = keysList.get(4);

                DependencyManager.getInjector().getInstance(RequestBuilder.class)
                        .setDayPartRequestList(targetingBuilder.getRootDaypartRequest());

                responseTargeting = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(targetingBuilder.getRootDaypartRequest(), ObjectMapperType.GSON)
                        .put(targetingBuilder.getEndPointURLTargeting(listType.toLowerCase(), placementNumber));
                String responseJSONString = responseTargeting.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                LOG.info("Day Parts for Placement" + " " + placementNumber + " " + responseJSONString);

                assertThat("Verify Content Type for DayParts Api ", this.getContentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status code for DayParts Api ", this.getStatusCode(), equalTo(200));

                assertThat("Verify Day Parts List should not be Null for Placement # " + placementNumber, responseTargeting.then().extract()
                                .as(RootDayPartResponse.class, ObjectMapperType.GSON).getDayParts(),
                        IsNull.notNullValue());

                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDayPartResponseList(
                        MapBuilderfromJsonString.build(RootDayPartResponse.class, responseJSONString, false).get());

            }

            if (ifAttachAddtionalOptions.toUpperCase().contentEquals("YES")) {
                targetingBuilder.setRootAddtionalOptionRequest(placementNumber);
                String listType = keysList.get(5);

                DependencyManager.getInjector().getInstance(RequestBuilder.class)
                        .setAddtionalOptionsRequestList(targetingBuilder.getRootAddtionalOptionRequest());

                responseTargeting = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(targetingBuilder.getRootAddtionalOptionRequest(), ObjectMapperType.GSON)
                        .put(targetingBuilder.getEndPointURLTargeting(listType.toLowerCase(), placementNumber));
                String responseJSONString = responseTargeting.then().contentType(ContentType.JSON).extract().response()
                        .asString();
                LOG.info("Addtional Options for Placement" + " " + placementNumber + " " + responseJSONString);

                assertThat("Verify Content Type for Additional Options Api ", this.getContentType(),
                        equalTo("application/json; charset=utf-8"));

                assertThat("Verify Status code for Additional Options Api ", this.getStatusCode(), equalTo(200));

                assertThat("Verify AddtionalOptions List should not be Null for Placement # " + placementNumber, responseTargeting.then().extract()
                                .as(RootAddtionalOptionResponse.class, ObjectMapperType.GSON).getPlayerSizeGroups(),
                        IsNull.notNullValue());

                DependencyManager.getInjector().getInstance(ResponseBuilder.class)
                        .setAddtionalOptionsResponseList(MapBuilderfromJsonString
                                .build(RootAddtionalOptionResponse.class, responseJSONString, false).get());

            }
        }
        TargetingBuilder.writeUpdatedTargetingRequestToJsonFile(scenarioTestData);
    }

    @Step
    public int getStatusCode() {
        return responseTargeting.then().extract().statusCode();
    }

    @Step
    public String getContentType() {
        return responseTargeting.then().extract().contentType();

    }

    @Step
    public void storeTargetinResponseToJsonFile() {
        try {
            fileWriter.writeToJsonFile(ObjectType.TECHNOLOGY, QueryMethod.RESPONSE)
                    .writeResponseJsonArrayObjectToJsonFile(RootTechnologyResponse.class, DependencyManager
                            .getInjector().getInstance(ResponseBuilder.class).getTechnologyResponseList());
            fileWriter.writeToJsonFile(ObjectType.GEOS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
                    RootGeoResponse.class,
                    DependencyManager.getInjector().getInstance(ResponseBuilder.class).getTargetingResponseGeoList());
            fileWriter.writeToJsonFile(ObjectType.ZIPCODES,QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
                    RootGeoListResponse.class,
                    DependencyManager.getInjector().getInstance(ResponseBuilder.class).getGeoListsZipCodeList());
            fileWriter.writeToJsonFile(ObjectType.CATEGORIES, QueryMethod.RESPONSE)
                    .writeResponseJsonArrayObjectToJsonFile(RootCategoryResponse.class, DependencyManager.getInjector()
                            .getInstance(ResponseBuilder.class).getCategoryResponseList());
            fileWriter.writeToJsonFile(ObjectType.DAYPART, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
                    RootDayPartResponse.class,
                    DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDayPartResponseList());
            fileWriter.writeToJsonFile(ObjectType.ADDITIONALOPTIONS, QueryMethod.RESPONSE)
                    .writeResponseJsonArrayObjectToJsonFile(RootAddtionalOptionResponse.class, DependencyManager
                            .getInjector().getInstance(ResponseBuilder.class).getAddtionalOptionsResponseList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

//    @Step
//    public void fetchTargetingMasterDataDetailsFromAPI(List<String> targetingAttributes) {
//        for (String targetingAttribute : targetingAttributes) {
//            Response attributeResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
//                    .contentType("application/json;charset=UTF-8").when()
//                    .get(ObjectType.valueOf(targetingAttribute.toUpperCase()).getEndPointURLForObjectType());
//            assertThat("Verify Content Type of Master Data API of " + targetingAttribute.toUpperCase(), attributeResponse.then().extract().contentType(),
//                    equalTo("application/json; charset=utf-8"));
//
//            assertThat("Verify Content Type of Master Data API of " + targetingAttribute.toUpperCase(), attributeResponse.then().extract().statusCode()
//                    , equalTo(200));
//            String responseJSONString = attributeResponse.then().contentType(ContentType.JSON).extract().response()
//                    .asString();
//            LOG.info("JSON Response for Master API of " + targetingAttribute.toUpperCase() + " is " + responseJSONString);
//            Map<ObjectType, String> responseDataMap = new HashMap<>();
//            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setMasterDataJsonResponseMap
//                    (ObjectType.valueOf(targetingAttribute.toUpperCase()), responseJSONString);
//        }
//    }

    @Step
    public void fetchTargetingMasterDataDetailsFromAPI(String targetingAttribute) {
            Response attributeResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                    .contentType("application/json;charset=UTF-8").when()
                    .get(ObjectType.valueOf(targetingAttribute.toUpperCase()).getEndPointURLForObjectType());
            assertThat("Verify Content Type of Master Data API of " + targetingAttribute.toUpperCase(), attributeResponse.then().extract().contentType(),
                    equalTo("application/json; charset=utf-8"));
            assertThat("Verify Content Type of Master Data API of " + targetingAttribute.toUpperCase(), attributeResponse.then().extract().statusCode()
                    , equalTo(200));
            String responseJSONString = attributeResponse.then().contentType(ContentType.JSON).extract().response()
                    .asString();
            LOG.info("JSON Response for Master API of " + targetingAttribute.toUpperCase() + " is " + responseJSONString);
            Map<ObjectType, String> responseDataMap = new HashMap<>();
            DependencyManager.getInjector().getInstance(ResponseBuilder.class).setMasterDataJsonResponseMap
                    (ObjectType.valueOf(targetingAttribute.toUpperCase()), responseJSONString);

    }


    @Step
    public void storeTargetingMasterDataJsonResponseToJsonFile() {
        TargetingBuilder.writeMasterDataJsonResponseToJsonFile(DependencyManager.getInjector().getInstance(ResponseBuilder.class).getMasterDataJsonResponseMap());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(false,CountiresJsonResponse.class,Constants.COUNTRIES_MASTERDATAJSON).getInstanceMap().keySet());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(false,GeoListJsonResponse.class,Constants.GEOLIST_MASTERDATAJSON).getInstanceMap().keySet());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(true,CategoriesJsonResponse.class,Constants.PCATEGORIES_MASTERDATAJSON).getInstanceMultiMap().keySet().size());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(true,RegionJsonResponse.class,Constants.REGIONS_MASTERDATAJSON).getInstanceMultiMap().asMap().get(13).stream().findFirst().get().getCountryId());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(false,DmasJsonResponse.class,Constants.DMAS_MASTERDATAJSON).getInstanceMap().keySet());
//		System.out.println(MapBuilderfromJsonFile.buildFromHashMap
//				(false,LanguageJsonResponse.class,Constants.LANGUAGES_MASTERDATAJSON).getInstanceMap().keySet());


    }
}


