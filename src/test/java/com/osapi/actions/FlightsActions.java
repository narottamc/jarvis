/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.builders.FlightBuilder;
import com.osapi.builders.NewFlightRequestBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.flightrequest.FlightRequestAsset;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.flightsresponse.FlightResponseAssets;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import cucumber.api.java.en.Then;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class FlightsActions
{

	private static final Logger LOG = Logger.getLogger(FlightsActions.class);
	private Response flightList;
	private Response reponseNewFlight;
	private Response responseUpdateFlight;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void userPostAPOSTRequestToFlightEndpointURLToCreateFlightsAsFollowing(
			List<Map<String, String>> flightDetails)
	{
		Integer placementNumber;
		Integer flightsCount;
		NewFlightRequestBuilder newFlightRequest = new NewFlightRequestBuilder(scenarioTestData);
		for (Map<String, String> flightMap : flightDetails)
		{
			List<String> keysList = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : flightMap.entrySet())
			{
				keysList.add(keyEntries.getKey());
			}
			placementNumber = Integer
					.parseInt(CharMatcher.inRange('0', '9').retainFrom(flightMap.get(keysList.get(0))));
			flightsCount = Integer.parseInt(flightMap.get(keysList.get(1)));
			newFlightRequest.setRootNewFlightRequest(placementNumber);
			if (placementNumber == 1)
			{
				for (int i = 1; i <= flightsCount; i++)
				{
					if (i == 1)
					{
						flightList = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
								.contentType("application/json;charset=UTF-8").when()
								.get(newFlightRequest.getEndPointURLFlightList(placementNumber));
						String responseFlightListJSONString = flightList.then().contentType(ContentType.JSON).extract()
								.response().asString();
						LOG.info("Flight List for Placement" + " " + placementNumber + " "
								+ responseFlightListJSONString);

						newFlightRequest.setRootFirstFlightRequest(placementNumber,responseFlightListJSONString,flightsCount);

						assertThat("Verify Content Type for Add FLight API", this.getContentTypeForFlightListRequest(),
								equalTo("application/json; charset=utf-8"));

						assertThat("Verify Status Code for Add FLight API", this.getStatusCodeForFlightListRequest(),
								equalTo(200));

						DependencyManager.getInjector().getInstance(RequestBuilder.class)
								.setRootFlightRequestList(newFlightRequest.getRootFirstFlightRequest());
					}
					else
					{
						reponseNewFlight = SerenityRest.given().auth().preemptive().oauth2(accessToken)
								.filter(filterAuth).contentType("application/json;charset=UTF-8").when()
								.body(newFlightRequest.getRootNewFlightRequest(), ObjectMapperType.GSON)
								.post(newFlightRequest.getEndPointURLFlight(placementNumber));
						String reponsenewFlightJSONString = reponseNewFlight.then().contentType(ContentType.JSON)
								.extract().response().asString();
						LOG.info("New Flight Added for Placement" + " " + placementNumber + " "
								+ reponsenewFlightJSONString);

						newFlightRequest.setRootFlightRequest(placementNumber,reponsenewFlightJSONString,flightsCount);

						assertThat("Verify Content Type for Add FLight API", this.getContentTypeForNewFlightRequest(),
								equalTo("application/json; charset=utf-8"));

						assertThat("Verify Status Code for Add FLight API", this.getStatusCodeForNewFlightRequest(),
								equalTo(200));

						assertThat(
								"Verify Flight Id for Flight #" + " " + i + " " + "of Placement # " + placementNumber,
								reponseNewFlight.then().extract().as(RootFlightRequest.class, ObjectMapperType.GSON)
										.getId(),
								IsNull.notNullValue());

						DependencyManager.getInjector().getInstance(RequestBuilder.class)
								.setRootFlightRequestList(newFlightRequest.getRootFlightRequest());
					}
				}
			}
			else
			{
				for (int i = 1; i <= flightsCount; i++)
				{

					reponseNewFlight = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
							.contentType("application/json;charset=UTF-8").when()
							.body(newFlightRequest.getRootNewFlightRequest(), ObjectMapperType.GSON)
							.post(newFlightRequest.getEndPointURLFlight(placementNumber));
					String reponsenewFlightJSONString = reponseNewFlight.then().contentType(ContentType.JSON).extract()
							.response().asString();
					LOG.info("New Flight Added for Placement" + " " + placementNumber + " "
							+ reponsenewFlightJSONString);

					newFlightRequest.setRootFlightRequest(placementNumber,reponsenewFlightJSONString,flightsCount);

					assertThat("Verify Content Type for Add FLight API", this.getContentTypeForNewFlightRequest(),
							equalTo("application/json; charset=utf-8"));

					assertThat("Verify Status Code for Add FLight API", this.getStatusCodeForNewFlightRequest(),
							equalTo(200));

					assertThat("Verify Flight Id for Flight #" + " " + i + " " + "of Placement # " + placementNumber,
							reponseNewFlight.then().extract().as(RootFlightRequest.class, ObjectMapperType.GSON)
									.getId(),
							IsNull.notNullValue());

					DependencyManager.getInjector().getInstance(RequestBuilder.class)
							.setRootFlightRequestList(newFlightRequest.getRootFlightRequest());
				}
			}
		}
		newFlightRequest.writeFlightRequestToFile();
	}

	@Step
	public int getStatusCodeForFlightListRequest()
	{
		return flightList.then().extract().statusCode();
	}

	@Step
	public String getContentTypeForFlightListRequest()
	{
		return flightList.then().extract().contentType();

	}

	@Step
	public int getStatusCodeForNewFlightRequest()
	{
		return reponseNewFlight!=null ? reponseNewFlight.then().extract().statusCode() : this.getStatusCodeForFlightListRequest();
	}

	@Step
	public String getContentTypeForNewFlightRequest()
	{
		return  reponseNewFlight!=null ? reponseNewFlight.then().extract().contentType() : this.getContentTypeForFlightListRequest();

	}

	@Step
	public void storeAddFlightResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.FLIGHTS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
					RootFlightRequest.class,
					DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootFlightRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

	@Then("^user post a PUT request to associate flights of \"([^\"]*)\" with assets as follows$")
	public void userPostAPUTRequestToAssociateFlightsOfWithAssetsAsFollows(String placement, String assetType,
			List<Map<String, String>> updateFlightDetails)
	{
		Integer placementNumber;
		Integer flightNumber;
		Integer totalNumberOfFlights = updateFlightDetails.size();
		FlightBuilder flightBuilder = null;
		for (Map<String, String> flightMap : updateFlightDetails)
		{
			flightBuilder = new FlightBuilder(scenarioTestData, flightMap);
			placementNumber = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(placement));
			flightBuilder.setRootFlightRequest(placementNumber,assetType,totalNumberOfFlights);
			flightNumber = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(flightMap.get("FlightNumber")));
			DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.setRootFlightRequestListUpdated(flightBuilder.getRootFlightRequest());
			LOG.info("Flight is Updated With Status : "+ flightBuilder.getRootFlightRequest().getActive() + " having Flight Id "+
					flightBuilder.getRootFlightRequest().getId());
			responseUpdateFlight = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(flightBuilder.getRootFlightRequest(), ObjectMapperType.GSON)
					.put(flightBuilder.getEndPointURLFlight(placementNumber));

			String reponseFlightJSONString = responseUpdateFlight.then().contentType(ContentType.JSON).extract().response()
					.asString();

			assertThat("Verify Content Type for Update FLight API", this.getContentTypeForUpdateFlightRequest(),
					equalTo("application/json; charset=utf-8"));

			assertThat("Verify Status Code for Update FLight API", this.getStatusCodeForUpdateFlightRequest(),
					equalTo(200));

			assertThat("Verify Flight Name for Flight #" + " " + flightNumber + " " + "of Placement # " + placementNumber,
					responseUpdateFlight.then().extract().as(RootFlightsResponse.class, ObjectMapperType.GSON).getName(),
					equalTo(flightBuilder.getRootFlightRequest().getName()));
			
			LOG.info("Flight Updated with Assets Details for Placement" + " " + placementNumber + " "
					+ reponseFlightJSONString);

			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setFlightsResponseList(
					MapBuilderfromJsonString.build(RootFlightsResponse.class, reponseFlightJSONString, false).get());
		}
		flightBuilder.clearQueuesForFlightAssetIds(assetType);
		FlightBuilder.writeUpdatedFlightRequestToFile(scenarioTestData);
	}

	@Step
	public void flightUpdateResponseShouldHaveStatusCodeAsAndContentTypeAsJSON(int statusCode)
	{

		responseUpdateFlight.then().statusCode(statusCode).contentType(ContentType.JSON);
	}

	@Step
	public int getStatusCodeForUpdateFlightRequest()
	{
		return responseUpdateFlight.then().extract().statusCode();
	}

	@Step
	public String getContentTypeForUpdateFlightRequest()
	{
		return responseUpdateFlight.then().extract().contentType();

	}

	@Step
	public void storeUpdatedFlightResponsToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.FLIGHTS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile(
					RootFlightsResponse.class,
					DependencyManager.getInjector().getInstance(ResponseBuilder.class).getFlightsResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	public List<FlightRequestAsset> getFlightAssetsFromResponse(Response responseFlight)
	{
		List<FlightRequestAsset> flightRequestAssetsList = new ArrayList<>();
		List<FlightResponseAssets> flightResponseAssets = responseFlight.then().extract()
				.as(RootFlightsResponse.class, ObjectMapperType.GSON).getFlightAssets();
		for (int i = 0; i < flightResponseAssets.size(); i++)
		{
			FlightRequestAsset flightRequestAsset = new FlightRequestAsset();
			flightRequestAsset.setAssetId(flightResponseAssets.get(i).getAssetId());
			flightRequestAsset.setDistribution(flightResponseAssets.get(i).getDistribution());
			flightRequestAsset.setFlightId(flightResponseAssets.get(i).getFlightId());
			flightRequestAssetsList.add(flightRequestAsset);
		}
		return flightRequestAssetsList;
	}
}
