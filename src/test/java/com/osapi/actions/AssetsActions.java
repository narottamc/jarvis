/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.awsutils.AWSFileServicesUtils;
import com.osapi.builders.AssetBuilder;
import com.osapi.builders.RequestBuilder;
import com.osapi.builders.ResponseBuilder;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.enumtypes.VideoAssetsAdTagType;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.assetrequest.RootAssetRequest;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.newassetrequest.RootNewAssetRequest;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */

public class AssetsActions
{

	private static final Logger LOG = Logger.getLogger(AssetsActions.class);
	private Response responseAsset;
	private Response reponseUpdateAsset;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	private RootBrandResponse brandResponse = MapBuilderfromJsonFile.build(RootBrandResponse.class,Constants.BRAND_RESPONSE_DATA,scenarioTestData,false).get();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void userPostAPOSTRequestToAssetEndpointURLToCreateThirdyPartyAssetsOfFollowingTypes(
			List<Map<String, String>> assetDetails)
	{
		Integer assetNumber;
		String assetType;
		String adTagType;
		String assetSize;
		AssetBuilder assetBuilder = new AssetBuilder(scenarioTestData);
		String reponseAssetJSONString = null;

		for (Map<String, String> assetMap : assetDetails)
		{
			Path uploadedFile = null;
			List<String> keys = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : assetMap.entrySet())
			{
				keys.add(keyEntries.getKey());
			}
			assetNumber = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(assetMap.get(keys.get(0))));
			assetType = assetMap.get(keys.get(1));
			adTagType = assetMap.get(keys.get(2));
			assetSize = assetMap.get(keys.get(3));
			String s3KeyName = null;
			if(VideoAssetsAdTagType.valueOf(adTagType.toUpperCase()).getAdTagTypeId()==2) {
				Random randomNum = new Random();
				File mp4File = null;
				Path source = Paths.get(Constants.VIDEOASSET_CLEARSTREAM_UPLOAD);
				try{
					mp4File = Files
							 .copy(source,source.resolveSibling("AUTOMP4" + CommonUtils.getDateTimeStampAsString() + (randomNum.nextInt(50)+1)+ ".mp4"))
							 .toFile();
					uploadedFile = Paths.get(mp4File.getPath());
				}
				catch (IOException e){
					e.printStackTrace();
				}
				LOG.info("Media File to be uploaded For Video ClearStream Ad Tag Type Asset is " + mp4File.getName());
				s3KeyName = Constants.S3_BUCKET_MEDIAFILEUPLOADS_ENVIRONMENT + "/" + mp4File.getName();
				AWSFileServicesUtils.uploadFileFromLocaltoS3(Constants.S3_BUCKET_MEDIAFILEUPLOADS, s3KeyName,
						mp4File);
			}
			assetBuilder.prepareAssetRequestBodyForNewAsset(assetNumber,assetType,adTagType,s3KeyName,assetSize,brandResponse);
			responseAsset = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(assetBuilder.getRootNewAssetRequest(), ObjectMapperType.GSON)
					.post(assetBuilder.getEndPointURLfromCampaignforAsset());
			assertThat("Verify Content Type for Add Asset API for Asset #" + " " + assetNumber, this.getContentType(),
					equalTo("application/json; charset=utf-8"));
			assertThat("Verify Status Code for Add Asset API for Asset #" + " " + assetNumber, this.getStatusCode(),
					equalTo(200));
			assertThat("Asset Id for Asset #" + " " + assetNumber + " " + "should not be Null",
					responseAsset.then().extract().as(RootAssetResponse.class, ObjectMapperType.GSON).getId(),
					IsNull.notNullValue());
			assertThat("Verify Asset Name for Asset #" + " " + assetNumber + " ",
					responseAsset.then().extract().as(RootAssetResponse.class, ObjectMapperType.GSON).getName(),
					equalTo(assetBuilder.getRootNewAssetRequest().getAsset().getName()));
			reponseAssetJSONString = responseAsset.then().contentType(ContentType.JSON).extract().response().asString();
			LOG.info("Asset Response :" + reponseAssetJSONString);
			assetBuilder.prepareAssetRequestBodyForPUTRequest(reponseAssetJSONString,assetType);
			assetBuilder.addAssetResponseToList(reponseAssetJSONString);
			try{
				if (uploadedFile != null){
					Files.deleteIfExists(uploadedFile);
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		assetBuilder.writeUpdatedAssetsRequestToFile();
	}
	
	@Step
	public void generateAndValidateVASTForClearStreaAdTagType() {
		AssetBuilder assetBuilder = new AssetBuilder(scenarioTestData);
		List<RootNewAssetRequest> newAssetRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getNewRootAssetRequestList();
		List<RootAssetRequest> rootAssetRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootVideoAssetRequestListUpdated();
		List<RootAssetResponse> assetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAssetResponseList();
		String reponseUpdateAssetJSONString = null;
		for(int i=0;i<rootAssetRequestList.size();i++)
		{
			if(rootAssetRequestList.get(i).getAsset().getAdTagTypeId()==2) {
				LOG.info("Generate VAST TAG for Asset Id : " + rootAssetRequestList.get(i).getAssetId());
				String s3URL = Constants.S3_BASEURL_ENDPOINT+Constants.S3_BUCKET_MEDIAFILEPROCESSING
						+"/"+newAssetRequestList.get(i).getAsset().getAssetMedia().getFileKey();
				rootAssetRequestList.get(i).getAsset().getAssetMedia().setS3Url(s3URL);
				LOG.info("S3 URL for Asset Id : " + rootAssetRequestList.get(i).getAssetId() +" is "+ s3URL);
				reponseUpdateAsset =  SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(rootAssetRequestList.get(i), ObjectMapperType.GSON)
						.put(assetBuilder.getEndPointURLfromCampaignforAsset()+"/"+rootAssetRequestList.get(i).getAssetId());
				reponseUpdateAssetJSONString = reponseUpdateAsset.then().contentType(ContentType.JSON).extract().response().asString();
				LOG.info("Update Asset Response for Asset Id : " + rootAssetRequestList.get(i).getAssetId() +" "+ reponseUpdateAssetJSONString);
				assertThat("Verify Content Type for Generate VAST In Asset API for Asset Id" + " " + rootAssetRequestList.get(i).getAssetId()
						, reponseUpdateAsset.then().extract().contentType(),
						equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status Code for Generate VAST In Asset API for Asset Id" + " " + rootAssetRequestList.get(i).getAssetId(), 
						reponseUpdateAsset.then().extract().statusCode(),
						equalTo(200));
				assertThat("Verify VAST XML TAG Is Not Null for Asset Id" + " " + rootAssetRequestList.get(i).getAssetId(), 
						reponseUpdateAsset.then().extract().as(RootAssetResponse.class, ObjectMapperType.GSON).getThirdPartyAdTag(),
						IsNull.notNullValue());
				assetResponseList.get(i).setThirdPartyAdTag(reponseUpdateAsset.then().extract().as(RootAssetResponse.class, ObjectMapperType.GSON).getThirdPartyAdTag());
			}
			
		}
		assetBuilder.writeUpdatedAssetsRequestToFile();
	}
	
	@Step
	public void verifyGenerateVASTTag() {
		List<RootAssetResponse> assetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAssetResponseList();
		for(int i=0;i<assetResponseList.size();i++)
		{
			String VASTAdTag = assetResponseList.get(i).getThirdPartyAdTag().replace("?", "");
			LOG.info("VAST TAG URL for Asset Id "+assetResponseList.get(i).getId()+" is "+VASTAdTag);
			Response responseVASTAdTag = SerenityRest.given().contentType(ContentType.XML).get(VASTAdTag);
			LOG.info(responseVASTAdTag.asString());
			String reponseVASTAdTagXMLString = responseVASTAdTag.then().contentType(ContentType.XML).extract().response().asString();
			LOG.info("VAST TAG XML Response for Asset Id "+assetResponseList.get(i).getId()+" is "+reponseVASTAdTagXMLString);
			assertThat("Verify Status Code for Generated VAST for Asset Id" + " " + assetResponseList.get(i).getId(), 
					responseVASTAdTag.then().extract().statusCode(),
					equalTo(200));
			assertThat("Verify Content Type for Generated VAST for Asset Id" + " " + assetResponseList.get(i).getId()
					, responseVASTAdTag.then().extract().contentType(),
					equalTo("text/xml"));
		}
		
	}

	@Step
	public int getStatusCode()
	{
		return responseAsset.then().extract().statusCode();
	}

	@Step
	public String getContentType()
	{
		return responseAsset.then().extract().contentType();

	}

	@Step
	public void storeAssetsResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.VIDEOASSETS, QueryMethod.RESPONSE)
					.writeResponseJsonArrayObjectToJsonFile(RootAssetResponse.class,
							DependencyManager.getInjector().getInstance(ResponseBuilder.class).getAssetResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
}
