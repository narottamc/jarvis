/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.actions;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.builders.*;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.newactivitypixelresponse.NewRootActivityPixelResponse;
import com.osapi.models.placementrequest.Placement;
import com.osapi.models.placementresponse.AssetPlacementResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.sspresponse.RootSSPResponse;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.spi.AuthFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author ClearStream.
 */
public class PlacementActions
{

	private static final Logger LOG = Logger.getLogger(PlacementActions.class);
	Response responseNewPlacement;
	private Response responsePlacement;
	private Response responseFirstPlacement;
	private AuthFilter filterAuth = DependencyManager.getInjector().getInstance(RequestBuilder.class).getFilterAuth();
	private String accessToken = DependencyManager.getInjector().getInstance(RequestBuilder.class).getAccessToken();
	private String scenarioTestData = DependencyManager.getInjector().getInstance(RequestBuilder.class)
			.getScenarioTestDatPath();
	FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);

	@Step
	public void addNewPlacementWithProductType(List<Map<String, String>> placementDetails)
	{
		Integer placementNumber;
		String productType = null;
		Integer totalNumberOfPlacements = placementDetails.size();
		NewPlacementRequestBuilder newPlacmentRequestBuilder = new NewPlacementRequestBuilder(scenarioTestData);
		LOG.info("Number of Placements to be added are" + " " + totalNumberOfPlacements);
		for (Map<String, String> placementRequestMap : placementDetails)
		{
			List<String> keys = new ArrayList<>();
			for (Map.Entry<String, String> keyEntries : placementRequestMap.entrySet())
			{
				keys.add(keyEntries.getKey());
			}
			placementNumber = Integer
					.parseInt(CharMatcher.inRange('0', '9').retainFrom(placementRequestMap.get(keys.get(0))));
			productType = placementRequestMap.get(keys.get(1));
			LOG.info("Placment Number" + " " + placementNumber);
			if (placementNumber == 1)
			{
				responseFirstPlacement = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.get(newPlacmentRequestBuilder.getEndPointUrlForFirstPlacement());
				String reponseFirstPlacementJSONString = responseFirstPlacement.then().contentType(ContentType.JSON)
						.extract().response().asString();
				LOG.info("First Placement Response Is " + reponseFirstPlacementJSONString);
				newPlacmentRequestBuilder.setRootFirstPlacementRequest(productType,totalNumberOfPlacements,reponseFirstPlacementJSONString);
				DependencyManager.getInjector().getInstance(RequestBuilder.class)
						.setRootPlacementRequestList(newPlacmentRequestBuilder.getRootFirstPlacementRequest());
			}
			else
			{
				newPlacmentRequestBuilder.setNewPlacementRequest();
				responseNewPlacement = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
						.contentType("application/json;charset=UTF-8").when()
						.body(newPlacmentRequestBuilder.getNewPlacementRequest(), ObjectMapperType.GSON)
						.post(Constants.PLACEMENT_BASEENDPOINT_URL);
				String reponsenewPlacementJSONString = responseNewPlacement.then().contentType(ContentType.JSON)
						.extract().response().asString();

				assertThat("Verify Content Type for Add Placement API for Placement #" + " " + placementNumber,
						this.getContentTypeForNewPlacement(), equalTo("application/json; charset=utf-8"));
				assertThat("Verify Status Code for Add Placement API for Placement #" + " " + placementNumber,
						this.getStatusCodeForNewPlacement(), equalTo(200));
				assertThat("Verify Placement Id For New Placement #" + " " + placementNumber + " " + "should not be Null",
						responseNewPlacement.then().extract().as(Placement.class, ObjectMapperType.GSON).getId(),
						IsNull.notNullValue());

				LOG.info("New Placement Added" + " " + reponsenewPlacementJSONString + " " + "with Product Type"
						+ productType);
				newPlacmentRequestBuilder.setRootPlacementRequest(reponsenewPlacementJSONString, productType,totalNumberOfPlacements);
				DependencyManager.getInjector().getInstance(RequestBuilder.class)
						.setRootPlacementRequestList(newPlacmentRequestBuilder.getRootPlacementRequest());
			}
			
		}
		newPlacmentRequestBuilder.writeUpdatedPlacementRequestToFile();
	}

	public void addAssetsToPlacements(List<Map<String, String>> placementDetailsMap)
	{

		Integer placementNumber;
		Integer totalNumberOfPlacements = placementDetailsMap.size();
		PlacementBuilder placementBuilder = null;
		for (Map<String, String> placementRequestMap : placementDetailsMap)
		{
			placementBuilder = new PlacementBuilder(scenarioTestData, placementRequestMap);
			placementNumber = Integer.parseInt(CharMatcher.inRange('0', '9')
					.retainFrom(placementRequestMap.get(placementRequestMap.keySet().stream().findFirst().get())));
			LOG.info("Updating Placement Number" + " " + placementNumber + " " + "of" + " "
					+ "Product Type" + " " + placementRequestMap.get("ProductType") + " " + "with VIDEO/HTML Assets");
			placementBuilder.setRootPlacementRequest(placementNumber, totalNumberOfPlacements);
			responsePlacement = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(placementBuilder.getRootPlacementRequest(), ObjectMapperType.GSON)
					.put(placementBuilder.getEndPointUrlForPlacement(placementNumber));
			DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootPlacementRequestListUpdated(placementBuilder.getRootPlacementRequest());
			LOG.info("Request Assets "+ placementBuilder.getRootPlacementRequest().getPlacement().getAssets());
			
			assertThat("Verify Content Type for Update Placement for Placement #" + " " + placementNumber,
					this.getContentType(), equalTo("application/json; charset=utf-8"));

			assertThat("Verify Status Code for Update Placement for Placement #" + " " + placementNumber,
					this.getStatusCode(), equalTo(200));

			assertThat("Verify Placement Name for Placement #" + " " + placementNumber,
					responsePlacement.then().extract().as(RootPlacementResponse.class, ObjectMapperType.GSON).getName(),
					equalTo(placementBuilder.getRootPlacementRequest().getPlacement().getName()));
			
			assertThat("Verify Product Type for Placement #" + " " + placementNumber,
					responsePlacement.then().extract().as(RootPlacementResponse.class, ObjectMapperType.GSON).getProductType(),
					equalTo(placementBuilder.getRootPlacementRequest().getPlacement().getProductType()));
			
			assertThat("Verify Associated Assets for Placement #" + " " + placementNumber,
					this.getAssetListFromPlacementResponse(responsePlacement),
					containsInAnyOrder(placementBuilder.getRootPlacementRequest().getPlacement().getAssets().toArray()));

			String responsePlacementJsonString = responsePlacement.then().contentType(ContentType.JSON).extract()
					.response().asString();
			LOG.info("Placment Response For Placement Number:" + " " + placementNumber + responsePlacementJsonString);
			DependencyManager.getInjector().getInstance(ResponseBuilder.class)
					.setPlacementResponseList(MapBuilderfromJsonString
							.build(RootPlacementResponse.class, responsePlacementJsonString, false).get());
		}
		PlacementBuilder.writeUpdatedPlacementRequestToFile(scenarioTestData);

	}

	@Step
	public void createNewActivityPixels(Integer noOfActivityPixel,String pixelType){
		for(int i =1;i<=noOfActivityPixel; i++){
			ActivityPixelBuilder pixelBuilder = new ActivityPixelBuilder(scenarioTestData);
			String pixelName = "AutoPixel_" + i + "_"+CommonUtils.getDateTimeStampAsString();
			Integer pixelNumber =  pixelType.contentEquals("RETARGETING") ? 1 : 2;
			pixelBuilder.buildRequestBodyForNewActivityPixel(pixelName,pixelNumber);
			Response newpixelResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(pixelBuilder.getNewActivityPixelRequest(), ObjectMapperType.GSON)
					.post(pixelBuilder.getEndPointURLForNewActivityPixel());
			String newpixelResponseJsonString = newpixelResponse.then().contentType(ContentType.JSON).extract()
					.response().asString();
			LOG.info("Activity Pixel #:" + " " + i + " Added "+ newpixelResponseJsonString);
			assertThat("Verify Content Type of POST Request for Activity Pixel #" + " " + i,
					newpixelResponse.then().extract().contentType(), equalTo("application/json; charset=utf-8"));

			assertThat("Verify Status Code of POST Request for Activity Pixel #" + " " + i,
					newpixelResponse.then().extract().statusCode(), equalTo(200));

			assertThat("Verify Name for Activity Pixel #" + " " + i,
					newpixelResponse.then().extract().as(NewRootActivityPixelResponse.class, ObjectMapperType.GSON).getName(),
					equalTo(pixelBuilder.getNewActivityPixelRequest().getName()));

			assertThat("Verify Id for Activity Pixel #" + " " + i,
					newpixelResponse.then().extract().as(NewRootActivityPixelResponse.class, ObjectMapperType.GSON).getId(),
					IsNull.notNullValue());
			DependencyManager.getInjector().getInstance(RequestBuilder.class).setNewActivityPixelRequestsList(pixelBuilder.getNewActivityPixelRequest());
			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setNewactivityPixelResponsesList(
					MapBuilderfromJsonString.build(NewRootActivityPixelResponse.class,newpixelResponseJsonString,false).get());

		}
		ActivityPixelBuilder.writeNewActivityPixelRequestData(scenarioTestData);
	}

	@Step
	public void storeNewCreatedActivityPixelResposne(){
		LOG.info(DependencyManager.getInjector().getInstance(ResponseBuilder.class).getNewactivityPixelResponsesList().size());
		ActivityPixelBuilder.writeNewActitvityPixelResponseData(scenarioTestData);
	}
	@Step
	public void buildActivityPixelInfoWithAddtionalDetails(){
		List<NewRootActivityPixelResponse> newRootActivityPixelResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getNewactivityPixelResponsesList();
		for(int i =0;i<newRootActivityPixelResponseList.size();i++){
			ActivityPixelBuilder pixelBuilder = new ActivityPixelBuilder(scenarioTestData);
			pixelBuilder.buildRequestBodyForExistingActivityPixel(newRootActivityPixelResponseList.get(i));
			Response pixelResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					.contentType("application/json;charset=UTF-8").when()
					.body(pixelBuilder.getActivityPixelRequest(), ObjectMapperType.GSON)
					.put(pixelBuilder.getEndPointURLForPUTActivityPixel(newRootActivityPixelResponseList.get(i).getId()));
			String pixelResponseJsonString = pixelResponse.then().contentType(ContentType.JSON).extract()
					.response().asString();
			LOG.info("Activity Pixel #:" + " " + i + " Updated "+ pixelResponseJsonString);
			assertThat("Verify Content Type of PUT Request for Activity Pixel #" + " " + i,
					pixelResponse.then().extract().contentType(), equalTo("application/json; charset=utf-8"));

			assertThat("Verify Status Code of PUT Request for Activity Pixel #" + " " + i,
					pixelResponse.then().extract().statusCode(), equalTo(200));

			assertThat("Verify Id Is Not Null of PUT Request for Activity Pixel #" + " " + i,
					pixelResponse.then().extract().as(RootActivityPixelResponse.class, ObjectMapperType.GSON).getId(),
					IsNull.notNullValue());
			DependencyManager.getInjector().getInstance(RequestBuilder.class).setActivityPixelRequestsList(pixelBuilder.getActivityPixelRequest());
			DependencyManager.getInjector().getInstance(ResponseBuilder.class).setActivityPixelResponsesList(
					MapBuilderfromJsonString.build(RootActivityPixelResponse.class,pixelResponseJsonString,false).get());
		}
		ActivityPixelBuilder.writeActivityPixelPUTRequestData(scenarioTestData);

	}
	@Step
	public void storeActivityPixelResponseData(){
		ActivityPixelBuilder.writeAcivityPixelPUTResponseData(scenarioTestData);
	}
	@Step
	public void assignConversionPixelToPlacements(Integer numberOfPixelToBeAssociated){
		 List<RootPlacementResponse> rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				 .getPlacementResponseList();
		 for(int i =0;i<rootPlacementResponseList.size();i++){
				ActivityPixelBuilder pixelBuilder = new ActivityPixelBuilder(scenarioTestData);
				pixelBuilder.buildConversionPixelRequestForPlacements(rootPlacementResponseList.get(i).getId(),numberOfPixelToBeAssociated);
			 Response conversionpixelResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
					 .contentType("application/json;charset=UTF-8").when()
					 .body(pixelBuilder.getConversionPixelRequest(), ObjectMapperType.GSON)
					 .put(pixelBuilder.getEndPointURLForConversionPixelsForPlacement(rootPlacementResponseList.get(i).getId()));
			 String conversionpixelResponseJsonString = conversionpixelResponse.then().contentType(ContentType.JSON).extract()
					 .response().asString();
			 LOG.info(" Conversion Pixel Added For Placement #:" + " " + i + conversionpixelResponseJsonString);
			 assertThat("Verify Content Type of PUT Request for Conversion Pixel For Placement #" + " " + i + " ",
					 conversionpixelResponse.then().extract().contentType(), equalTo("application/json; charset=utf-8"));

			 assertThat("Verify Status Code of PUT Request for Conversion Pixel For Placement #" + " " + i + " ",
					 conversionpixelResponse.then().extract().statusCode(), equalTo(200));

			 DependencyManager.getInjector().getInstance(RequestBuilder.class).setConversionPixelRequestsList(pixelBuilder.getConversionPixelRequest());
			 DependencyManager.getInjector().getInstance(ResponseBuilder.class).setConversionPixelResponsesList(
					 MapBuilderfromJsonString.build(RootConversionPixelResponse.class,conversionpixelResponseJsonString,false).get());
		 }
		ActivityPixelBuilder.writeConversionPixelPUTRequestData(scenarioTestData);

	}
	@Step
	public void storeConversionPixelResponse(){
			ActivityPixelBuilder.writeConversionPixelPUTResponseData(scenarioTestData);

	}

	@Step
    public void assignDealsAndSeatsToPlacements(){
        List<RootPlacementResponse> rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
                .getPlacementResponseList();
        for(int i =0;i<rootPlacementResponseList.size();i++){
            if (i == 0) {
                SSPBuilder sspBuilder = new SSPBuilder();
                sspBuilder.buildRequestToAddExistingDealsandSeats(rootPlacementResponseList.get(i).getId());
                Response addNewDealsAndSeatsResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(sspBuilder.getRootSSPRequest(), ObjectMapperType.GSON)
                        .post(sspBuilder.getEndPointURLForAddDealsAndSeats());
                String addNewSeatAndDealResponseJsonString = addNewDealsAndSeatsResponse.then().contentType(ContentType.JSON).extract()
                        .response().asString();
                LOG.info("New Deal And Seat Added Placement #:" + " Deal: " + sspBuilder.getRootSSPRequest().getSellerNetworkDeal() + " Seat: "
                        + sspBuilder.getRootSSPRequest().getSellerNetworkSeat());
                LOG.info("New Deal And Seat Response Placement #:" + " " + i + " " + addNewSeatAndDealResponseJsonString);
                assertThat("Verify Content Type of POST Request for Adding New Deal And Seat For Placement #" + " " + i,
                        addNewDealsAndSeatsResponse.then().extract().contentType(), equalTo("application/json; charset=utf-8"));
                assertThat("Verify Status Code of POST Request for Adding New Deal And Seat For Placement #" + " " + i,
                        addNewDealsAndSeatsResponse.then().extract().statusCode(), equalTo(200));

                String seatAndDealResponseJsonString = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .get(sspBuilder.getEndPointURLForDealsAndSeatsOfPlacement(rootPlacementResponseList.get(i).getId()))
                        .then().contentType(ContentType.JSON).extract()
                        .response().asString();
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDealsAndSeatsResponseList(
                        MapBuilderfromJsonString.build(RootDealsAndSeatsResponse.class,seatAndDealResponseJsonString,false).get());
                sspBuilder.buildRequestToAddDealAndSeatsToPlacement();

                DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootDealsAndSeatsRequestList(sspBuilder.getRootDealsAndSeatsRequest());
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootSSPRequestList(sspBuilder.getRootSSPRequest());
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setRootSSPResponseList(
                        MapBuilderfromJsonString.build(RootSSPResponse.class,addNewSeatAndDealResponseJsonString,false).get());
            }
            else{
                SSPBuilder sspBuilder = new SSPBuilder();
                sspBuilder.buildRequestToAddDealAndSeatsToPlacement();
                Response dealsAndSeatsResponse = SerenityRest.given().auth().preemptive().oauth2(accessToken).filter(filterAuth)
                        .contentType("application/json;charset=UTF-8").when()
                        .body(sspBuilder.getRootDealsAndSeatsRequest(), ObjectMapperType.GSON)
                        .put(sspBuilder.getEndPointURLForDealsAndSeatsOfPlacement(rootPlacementResponseList.get(i).getId()));
                String seatAndDealResponseJsonString = dealsAndSeatsResponse.then().contentType(ContentType.JSON).extract()
                        .response().asString();
                LOG.info("Deals And Seats Response Placement #:" + " " + i + " " + seatAndDealResponseJsonString);
                assertThat("Verify Content Type of PUT Request for Adding Deals And Seats For Placement #" + " " + i,
                        dealsAndSeatsResponse.then().extract().contentType(), equalTo("application/json; charset=utf-8"));
                assertThat("Verify Status Code of PUT Request for Adding Deals And Seats For Placement #" + " " + i,
                        dealsAndSeatsResponse.then().extract().statusCode(), equalTo(200));
                DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootDealsAndSeatsRequestList(sspBuilder.getRootDealsAndSeatsRequest());
                DependencyManager.getInjector().getInstance(ResponseBuilder.class).setDealsAndSeatsResponseList(
                        MapBuilderfromJsonString.build(RootDealsAndSeatsResponse.class,seatAndDealResponseJsonString,false).get());
            }
        }
        SSPBuilder.writePOSTMethodSellerNetworkMastersRequestData(scenarioTestData);
        SSPBuilder.writePUTMethodDealsAndSeatsRequestData(scenarioTestData);
    }

    @Step
    public void storeAddNewSeatDealResponse(){
	    SSPBuilder.writePOSTMethodSellerNetworkMastersResponseData(scenarioTestData);
    }


    @Step
    public void storeDealsAndSeatsResponse(){
	    SSPBuilder.writeGETMethodDealsAndSeatsResponseData(scenarioTestData);
    }

	@Step
	public int getStatusCodeForNewPlacement()
	{
		return responseNewPlacement.then().extract().statusCode();
	}

	@Step
	public String getContentTypeForNewPlacement()
	{
		return responseNewPlacement.then().extract().contentType();

	}

	@Step
	public int getStatusCode()
	{
		return responsePlacement.then().extract().statusCode();
	}

	@Step
	public String getContentType()
	{
		return responsePlacement.then().extract().contentType();

	}

	@Step
	public void storePlacementResponseToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.PLACEMENTS, QueryMethod.RESPONSE)
					.writeResponseJsonArrayObjectToJsonFile(RootPlacementResponse.class, DependencyManager.getInjector()
							.getInstance(ResponseBuilder.class).getPlacementResponseList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}
	
	public List<Integer> getAssetListFromPlacementResponse(Response placement)
	{
		List<Integer> assetIdList = new ArrayList<>();
		List<AssetPlacementResponse> assetsPlacementResponse =  placement.then().extract().as(RootPlacementResponse.class, ObjectMapperType.GSON).getAssets();
		for(int i =0 ; i<assetsPlacementResponse.size();i++)
		{
			assetIdList.add(assetsPlacementResponse.get(i).getId());
		}
		LOG.info("Response Assets " + assetIdList);
		return assetIdList;	
	}

}
