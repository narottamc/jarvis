/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.CampaignMetadataSteps;

import com.quaterbackdatabase.utils.CreatePostgreDBConnection;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.serenitybdd.core.Serenity;
import org.apache.log4j.Logger;
import org.ini4j.InvalidFileFormatException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

/**
 * @author Clearstream
 */
public class HelpersQuarterback
{

	protected static io.restassured.spi.AuthFilter filterAuth;
	private static final Logger LOG = Logger.getLogger(HelpersQuarterback.class);

	@Before("@First")
	public void before(cucumber.api.Scenario scenario)
			throws InvalidFileFormatException, FileNotFoundException, IOException
	{
		// String scenarioTag = null;
		// String scenarioTestDataPath = null;
		// for (String tag : scenario.getSourceTagNames())
		// {
		// LOG.info("Tag: " + tag);
		// if (tag != null)
		// {
		// tag = tag.substring(1);
		// if (tag.toLowerCase().contains("scenario"))
		// {
		// scenarioTag = tag;
		// break;
		// }
		// }
		//
		// }
		//
		// File file = new File(Constants.CAMPAIGNDATA_BASEPATH);
		// LOG.info(Constants.CAMPAIGNDATA_BASEPATH);
		// LOG.info(scenarioTag);
		// if ((scenarioTag != null))
		// {
		// for (File files : file.listFiles())
		// {
		// if (files.getName().contentEquals(scenarioTag))
		// {
		// scenarioTestDataPath = files.getPath();
		// LOG.info((scenarioTestDataPath));
		// }
		// }
		// }
		//
		// Serenity.getCurrentSession().put("scenarioTestDataPath",
		// scenarioTestDataPath);

		Serenity.getCurrentSession().put("dbConnection", HelpersQuarterback.getDatabaseConnection());

	}

	public static Connection getDatabaseConnection()
			throws InvalidFileFormatException, FileNotFoundException, IOException
	{
		CreatePostgreDBConnection connection = new CreatePostgreDBConnection();
		connection.setConnection();
		Connection conn_ = connection.getConnection();
		Serenity.getCurrentSession().put("dbConnObject", conn_);
		return conn_;
	}

	@After("@Last")
	public static void closeDBConnection() throws IOException
	{
		CreatePostgreDBConnection connectionObj = new CreatePostgreDBConnection();
		connectionObj.closeConnection(MetadataDetails.dbConnection);
	}

}
