package com.quarterback.CampaignMetadataSteps;

import com.quarterback.mappers.ConfigData;
import com.quarterback.mappers.ConfigData.Advertiser;
import com.quarterback.mappers.ConfigData.Deal;
import com.quarterback.mappers.ConfigData.LineItem;
import com.quarterback.utils.XmlFileParsingUtils;
import com.quaterbackdatabase.utils.ActiveCampaignMetadata;
import net.serenitybdd.core.Serenity;
import org.apache.log4j.Logger;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MetadataDetails
{
	private static final Logger LOG = Logger.getLogger(MetadataDetails.class);

	public static Connection dbConnection = (Connection) Serenity.getCurrentSession().get("dbConnection");
	private static Map<Integer, Deal> dealMap;
	private static ResultSet medtadataFromDB = ActiveCampaignMetadata.getActiveCampaignMetadataFromDB(dbConnection);

	public static List<Integer> activePlacementsFromDB = ActiveCampaignMetadata.getActivePlacementsFromDB(dbConnection);

	public static List<Integer> getAgenciesFromXML(File xmlFile)
	{
		List<Integer> activeAgenciesInXML = new ArrayList<>();
		ConfigData xmlData = null;
		try
		{
			xmlData = XmlFileParsingUtils.parseXML(xmlFile);
		}
		catch (Exception e)
		{
			LOG.info("Getting agency details from XML file: " + xmlFile.getName());
			e.printStackTrace();
		}
		List<Advertiser> advertisers = xmlData.advertisers;
		for (Advertiser advertiser : advertisers)
		{
			activeAgenciesInXML.add(advertiser.id);
		}
		return activeAgenciesInXML;
	}

	public static List<Integer> getActiveAgenciesFromDB()
	{
		List<Integer> activeAgenciesFromDB = new ArrayList<>();
		try
		{
			while (medtadataFromDB.next())
			{
				activeAgenciesFromDB.add(medtadataFromDB.getInt("agencyId"));
			}
			medtadataFromDB.beforeFirst();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return activeAgenciesFromDB;
	}

	public static List<Integer> getAdditionalAgenciesInXML(File xmlFile)
	{
		List<Integer> activeAgenciesInXML = new ArrayList<>();
		List<Integer> activeAgenciesFromDB = new ArrayList<>();

		activeAgenciesFromDB = getActiveAgenciesFromDB();
		activeAgenciesInXML = getAgenciesFromXML(xmlFile);

		List<Integer> additionalAgenciesInXML = new ArrayList<>(activeAgenciesInXML);

		additionalAgenciesInXML.removeAll(activeAgenciesFromDB);

		return additionalAgenciesInXML;
	}

	public static List<Integer> getAdditionalAgenciesInDB(File xmlFile)
	{
		List<Integer> activeAgenciesInXML = new ArrayList<>();
		List<Integer> activeAgenciesFromDB = new ArrayList<>();

		activeAgenciesFromDB = getActiveAgenciesFromDB();
		activeAgenciesInXML = getAgenciesFromXML(xmlFile);

		List<Integer> additionalAgenciesInDB = new ArrayList<>(activeAgenciesFromDB);

		additionalAgenciesInDB.removeAll(activeAgenciesInXML);

		return additionalAgenciesInDB;

	}

	public static List<Integer> getCampaignsFromXML(File xmlFile)
	{
		List<Integer> activeCampaignsInXML = new ArrayList<>();
		ConfigData xmlData = null;
		try
		{
			xmlData = XmlFileParsingUtils.parseXML(xmlFile);
		}
		catch (Exception e)
		{
			LOG.info("Getting Campaign details from XML file: " + xmlFile.getName());
			e.printStackTrace();
		}
		List<Advertiser> advertisers = xmlData.advertisers;
		for (Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				activeCampaignsInXML.add(campaign.group_id);
			}
		}

		return activeCampaignsInXML;
	}

	public static List<Integer> getActiveCampaignsFromDB()
	{
		List<Integer> activeCampaignsFromDB = new ArrayList<>();
		try
		{
			while (medtadataFromDB.next())
			{
				activeCampaignsFromDB.add(medtadataFromDB.getInt("campaignId"));
			}
			medtadataFromDB.beforeFirst();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return activeCampaignsFromDB;
	}

	public static List<Integer> getAdditionalCampaignsInXML(File xmlFile)
	{
		List<Integer> activeCampaignsInXML = new ArrayList<>();
		List<Integer> activeCampaignsFromDB = new ArrayList<>();

		activeCampaignsFromDB = getActiveCampaignsFromDB();
		activeCampaignsInXML = getCampaignsFromXML(xmlFile);

		List<Integer> additionalCampaignsInXML = new ArrayList<>(activeCampaignsInXML);

		additionalCampaignsInXML.removeAll(activeCampaignsFromDB);

		return additionalCampaignsInXML;

	}

	public static List<Integer> getAdditionalCampaignsInDB(File xmlFile)
	{
		List<Integer> activeCampaignsInXML = new ArrayList<>();
		List<Integer> activeCampaignsFromDB = new ArrayList<>();

		activeCampaignsFromDB = getActiveCampaignsFromDB();
		activeCampaignsInXML = getCampaignsFromXML(xmlFile);

		List<Integer> additionalCampaignsInDB = new ArrayList<>(activeCampaignsFromDB);

		additionalCampaignsInDB.removeAll(activeCampaignsInXML);

		return additionalCampaignsInDB;

	}

	public static List<Integer> getPlacementsFromXML(File xmlFile)
	{
		List<Integer> activePlacementsInXML = new ArrayList<>();

		ConfigData xmlData = null;
		try
		{
			xmlData = XmlFileParsingUtils.parseXML(xmlFile);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOG.info("Getting Placement details from XML file: " + xmlFile.getName());
		}
		List<Advertiser> advertisers = xmlData.advertisers;
		for (Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				activePlacementsInXML.add(campaign.id);
			}
		}

		dealMap = XmlFileParsingUtils.getDealMap(xmlData);
		for (Map.Entry<Integer, Deal> entry : dealMap.entrySet())
		{
			activePlacementsInXML.add(entry.getKey());
		}
		return activePlacementsInXML;
	}

	public static List<Integer> getAdditionalPlacementsInXML(File xmlFile)
	{
		List<Integer> activePlacementsInXML = new ArrayList<>();

		activePlacementsInXML = getPlacementsFromXML(xmlFile);

		List<Integer> additionalPlacementsInXML = new ArrayList<>(activePlacementsInXML);

		additionalPlacementsInXML.removeAll(activePlacementsFromDB);

		return additionalPlacementsInXML;

	}

	public static List<Integer> getAdditionalPlacementsInDB(File xmlFile)
	{
		List<Integer> activePlacementsInXML = new ArrayList<>();

		activePlacementsInXML = getPlacementsFromXML(xmlFile);

		List<Integer> additionalPlacementsInDB = new ArrayList<>(activePlacementsFromDB);

		additionalPlacementsInDB.removeAll(activePlacementsInXML);

		return additionalPlacementsInDB;
	}

	public static List<Integer> getAdsFromXML(File xmlFile)
	{
		List<Integer> activeAdsInXML = new ArrayList<>();

		ConfigData xmlData = null;
		try
		{
			xmlData = XmlFileParsingUtils.parseXML(xmlFile);
		}
		catch (Exception e)
		{
			LOG.info("Getting Ad details from XML file: " + xmlFile.getName());
			e.printStackTrace();
		}
		List<Advertiser> advertisers = xmlData.advertisers;
		for (Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				List<LineItem> lineItems = campaign.line_items;
				for (LineItem lineItem : lineItems)
				{
					activeAdsInXML.add(lineItem.id);
				}
			}
		}
		return activeAdsInXML;
	}

	public static List<Integer> getActiveAdsFromDB()
	{
		List<Integer> activeAdsFromDB = new ArrayList<>();
		try
		{
			while (medtadataFromDB.next())
			{
				activeAdsFromDB.add(medtadataFromDB.getInt("AdId"));
			}
			medtadataFromDB.beforeFirst();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return activeAdsFromDB;
	}

	public static List<Integer> getAdditionalAdsInXML(File xmlFile)
	{
		List<Integer> activeAdsInXML = new ArrayList<>();
		List<Integer> activeAdsFromDB = new ArrayList<>();

		activeAdsFromDB = getActiveAdsFromDB();
		activeAdsInXML = getAdsFromXML(xmlFile);

		List<Integer> additionalAdsInXML = new ArrayList<>(activeAdsInXML);

		additionalAdsInXML.removeAll(activeAdsFromDB);

		return additionalAdsInXML;
	}

	public static List<Integer> getAdditionalAdsInDB(File xmlFile)
	{
		List<Integer> activeAdsInXML = new ArrayList<>();
		List<Integer> activeAdsFromDB = new ArrayList<>();

		activeAdsFromDB = getActiveAdsFromDB();
		activeAdsInXML = getAdsFromXML(xmlFile);

		List<Integer> additionalAdsInDB = new ArrayList<>(activeAdsFromDB);

		additionalAdsInDB.removeAll(activeAdsInXML);

		return additionalAdsInDB;
	}

}
