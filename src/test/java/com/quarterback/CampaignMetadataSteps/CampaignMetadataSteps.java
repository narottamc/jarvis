package com.quarterback.CampaignMetadataSteps;

import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import com.quarterback.utils.RNGValidator;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Assert;

import java.io.File;
import java.sql.Connection;

import static org.hamcrest.MatcherAssert.assertThat;

public class CampaignMetadataSteps
{

	private static Connection dbConnection = (Connection) Serenity.getCurrentSession().get("dbConnection");

	 private File xmlFile = new File(CommonUtils.getCampaignXMLFileName());
	 private File rngFile = new File(Constants.RNG_SCHEMA_FILEPATH);

//	private File xmlFile = new File(
//			"C:\\Users\\pratikkumarp\\workspace\\quarterback-automation\\TestDataQuarterBack\\WWMGNg1BeY-20180814-120820.xml");
//	private File rngFile = new File(
//			"C:\\Users\\pratikkumarp\\workspace\\quarterback-automation\\TestDataQuarterBack\\validate.rng");

	@Given("^generated XML available in the local path$")
	public void generated_XML_available_in_the_local_path()
	{
		Assert.assertTrue("XML file does not exist", xmlFile.exists());
		Assert.assertTrue("RNG file does not exist", rngFile.exists());
	}

	@When("^the generated XML is validated against the RNG schema$")
	public void xml_is_validated_agasint_the_RNG_schema()
	{
		Assert.assertTrue("XML is not validated against the RNG schema", RNGValidator.validate(rngFile, xmlFile));
	}

	@When("^XML should not be generated without agency details$")
	public void xml_should_not_be_generated_without_agency_details()
	{
		Assert.assertNotNull("XML does not contain agency details", MetadataDetails.getAgenciesFromXML(xmlFile));
	}

	@When("^expected agency details should not be empty$")
	public void expected_agency_details_should_not_be_empty()
	{
		Assert.assertNotNull("Expected agency details is empty", MetadataDetails.getActiveAgenciesFromDB());
	}

	@Then("^XML should contain only required Agency Ids$")
	public void xml_should_contain_only_required_Agency_Ids()
	{
		assertThat("Additional agencies available in DB ", MetadataDetails.getAdditionalAgenciesInDB(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@Then("^XML should not contain additional Agencies$")
	public void xml_should_not_contain_additional_Agencies()
	{
		assertThat("Additional agencies available in XML ", MetadataDetails.getAdditionalAgenciesInXML(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@When("^XML should not be generated without Campaign details$")
	public void xml_should_not_be_generated_without_Campaign_details()
	{
		Assert.assertNotNull("XML does not contain Campaign details", MetadataDetails.getCampaignsFromXML(xmlFile));
	}

	@Then("^expected Campaign details should not be empty$")
	public void expected_Campaign_details_should_not_be_empty()
	{
		Assert.assertNotNull("Expected Campaign details is empty", MetadataDetails.getActiveCampaignsFromDB());
	}

	@Then("^XML should contain only required Campaign Ids$")
	public void xml_should_contain_only_required_Campaign_Ids()
	{
		assertThat("Additional Campaign available in DB ", MetadataDetails.getAdditionalCampaignsInDB(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@Then("^XML should not contain additional Campaigns$")
	public void xml_should_not_contain_additional_Campaigns()
	{
		assertThat("Additional Campaigns available in XML ", MetadataDetails.getAdditionalCampaignsInXML(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@When("^XML should not be generated without Placement details$")
	public void xml_should_not_be_generated_without_Placement_details()
	{
		Assert.assertNotNull("XML does not contain Campaign details", MetadataDetails.getPlacementsFromXML(xmlFile));
	}

	@Then("^expected Placement details should not be empty$")
	public void expected_Placement_details_should_not_be_empty()
	{

		Assert.assertNotNull("Expected Placement details is empty", MetadataDetails.activePlacementsFromDB);
	}

	@Then("^XML should contain only required Placement Ids$")
	public void xml_should_contain_only_required_Placement_Ids()
	{
		assertThat("Additional Placement available in DB ", MetadataDetails.getAdditionalPlacementsInDB(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@Then("^XML should not contain additional Placements$")
	public void xml_should_not_contain_additional_Placements()
	{
		assertThat("Additional Placements available in XML ", MetadataDetails.getAdditionalPlacementsInXML(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@When("^XML should not be generated without Ads details$")
	public void xml_should_not_be_generated_without_Ads_details()
	{
		Assert.assertNotNull("XML does not contain Ads details", MetadataDetails.getAdsFromXML(xmlFile));
	}

	@Then("^expected Ads details should not be empty$")
	public void expected_Ads_details_should_not_be_empty()
	{
		Assert.assertNotNull("Expected Ads details is empty", MetadataDetails.getActiveAdsFromDB());
	}

	@Then("^XML should contain only required Ad Ids$")
	public void xml_should_contain_only_required_Ad_Ids()
	{
		assertThat("Additional Ads available in DB ", MetadataDetails.getAdditionalAdsInDB(xmlFile),
				(IsEmptyCollection.empty()));
	}

	@Then("^XML should not contain additional Ads$")
	public void xml_should_not_contain_additional_Ads()
	{
		assertThat("Additional Ads available in XML ", MetadataDetails.getAdditionalAdsInXML(xmlFile),
				(IsEmptyCollection.empty()));
	}

}
