/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.downloadXMLSteps;

import com.osapi.awsutils.AWSFileServicesUtils;
import com.osapi.enumtypes.TestEnvironment;
import com.quarterback.models.TaskCategory;
import com.quarterback.storage.MediaTaskEntity;
import com.quarterback.storage.MediaTaskGroupEntity;
import com.quarterback.storage.MediaTaskStorage;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import com.quarterback.utils.ContextScope;
import com.quarterback.utils.PropertiesUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

/**
 * @author Clearstream
 */

public class DownloadXMLSteps
{
	private static final Logger LOG = LogManager.getLogger(DownloadXMLSteps.class);
	private String s3CamapignXMLPath = null;
	private MediaTaskStorage taskStorage;
	private MediaTaskGroupEntity taskGrpEntity;
	private MediaTaskEntity taskEntityList;
	private ContextScope contextScope = System.getProperty("testEnvironment") != null ? 
			TestEnvironment.valueOf(System.getProperty("testEnvironment").toUpperCase()).getAWSEnvironmentExecution() : ContextScope.DEVELOPMENT;
	private String AWSS3Bucket = System.getProperty("testEnvironment") != null ? 
			TestEnvironment.valueOf(System.getProperty("testEnvironment").toUpperCase()).getAWSS3Bucket() : "clearstream-media-tasks-development";
	@Given("^User have created taskgroup to download xml file$")
	public void user_have_created_taskgroup_to_download_xml_file() throws Exception
	{
		LOG.info("Context for AWS Environment Is "+ contextScope.toString());
		PropertiesUtils.loadAWSProperties();
		taskStorage = CommonUtils.getMediaTaskStorageObj(contextScope);
		taskGrpEntity = CommonUtils.getTakGroupEntity(Constants.LAUNCHCAMPAIGN_USERID, contextScope);
		MediaTaskGroupEntity taskGrpEnity = CommonUtils.waitForTaskGroupToFinish(Constants.LAUNCHCAMPAIGN_USERID,
				contextScope);

		if (taskGrpEnity != null)
		{
			s3CamapignXMLPath = CommonUtils.getS3FilePathByUserId(taskGrpEnity, contextScope);
		}
		else
		{
			return;
		}
	}

	@When("^Taskgroup have status finish in MediaTaskGroup Table$")
	public void taskgroup_have_status_finish_in_MediaTaskGroup_Table() throws Exception
	{
		taskEntityList = taskStorage.getFinishedTasks(taskGrpEntity.task_group_id, TaskCategory.UPLOAD_ARTIFACT_FILE);

	}

	@Then("^User should able to download campaign XMl$")
	public void user_should_able_to_download_campaign_XMl() throws Exception
	{
		LOG.info("S3 Bucket for AWS As per Execution Environment Is "+ AWSS3Bucket);
		AWSFileServicesUtils.downloadAndUnZipFileFromAWSToLocal(AWSS3Bucket, s3CamapignXMLPath,
				taskGrpEntity.task_group_id);
	}

	@Then("^Downloaded file should available in localpath$")
	public void able_to_Unzip_downloaded_file_to_local_path() throws Exception
	{
		Assert.assertTrue("Campaign XML file is not exists on localxpath",
				CommonUtils.isFileExistInDir(AWSFileServicesUtils.localXMLFile));
	}
}
