package com.quarterback.businessmodel.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features =
{ "src/test/resources/features/quarterback/businessmodels/CampaignXMLVerification/CampaignXMLVerificationVideo.feature" }, glue =
{ "com.quarterback.integrationSteps" })
public class XMLVideoTest
{
	
}
