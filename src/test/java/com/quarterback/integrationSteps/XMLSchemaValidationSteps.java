/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.integrationSteps;

import com.quarterback.utils.RNGValidator;
import com.quaterback.actions.XMLActions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.log4j.Logger;
import org.hamcrest.core.IsNull;
import org.junit.Assert;

import java.io.File;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Clearstream.
 *
 */
public class XMLSchemaValidationSteps
{
	@Steps
	XMLActions xmlActions;
	
	private static final Logger LOG = Logger.getLogger(XMLSchemaValidationSteps.class);
	private File xmlFile;
	private File rngSchemaFile;


	@Given("^A Campaign XML is available at local file path$")
	public void aCampaignXMLIsAvailableAtLocalFilePath() throws Throwable {
		xmlFile = xmlActions.getXMLFileFromLocal();
		assertThat("Verify XML Exists For Validation,XML File Does Not Exists  ", xmlFile.exists(), equalTo(true));
		xmlActions.storeXMLFileForParsing(xmlActions.getXMLFileFromLocal());
		LOG.info("XML File For Validation Is "+ xmlActions.getXMLFileFromLocal().getName());
	}

	@When("^XML is validated with latest RNG Schema file$")
	public void xml_is_validated_with_latest_RNG_Schema_file()  {
		 rngSchemaFile = xmlActions.getRNGSchemaFile();
		 assertThat("Verify RNG Schema File Exists For Validation,RNG File Does Not Exists  ", rngSchemaFile, IsNull.notNullValue());
		 LOG.info("RNG File For Validation Is "+ xmlActions.getRNGSchemaFile().getPath());
	}

	@Then("^Campaign XML schema should be as per the supplied RNG Schema file\\.$")
	public void campaign_XML_schema_should_be_as_per_the_supplied_RNG_Schema_file()  {
		 LOG.info("Validatiog Campaign XML File With RNG Schema");
		 Assert.assertTrue("XML is not validated against the RNG schema", RNGValidator.validate(xmlActions.getRNGSchemaFile(), xmlFile));
		 LOG.info("RNG Validation Of Campaing XML "+ xmlFile.getName() + " successfull");
		 xmlActions.buildCampaignXMLParser(xmlFile);
		
	}
}
