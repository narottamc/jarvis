/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.integrationSteps;

import com.google.common.collect.Multimap;
import com.osapi.factory.DependencyManager;
import com.quarterback.factory.SessionVariables;
import com.quaterback.actions.CampaignXMLValidationActions;
import com.quaterback.actions.XMLActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.IsNull;

import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Clearstream.
 *
 */
public class XMLVerificationSteps
{

	@Steps
	XMLActions xmlActions;
	
	@Steps
	CampaignXMLValidationActions xmlValidationActions;
	
	@Given("^User is able to extract the Campaign Details From XML$")
	public void userIsAbleToExtractTheCampaignDetailsFromXML()  {
		xmlActions.buildCampaignXMLParser(xmlActions.getXMLFileFromLocal());
		assertThat("Verify XML Parser Is Not Null", DependencyManager.getInjector().getInstance(SessionVariables.class).getConfigData(),
				IsNull.notNullValue());
	}

	@When("^User validates the advertiser information in Campaign XML$")
	public void userValidatesTheAdvertiserInformationInCampaignXML()  {
		assertThat("Advertiser With Attributes AgencyId : "+xmlValidationActions.getAgencyResponse().getId() + " AgencyName : "
						+xmlValidationActions.getAgencyResponse().getName()+ " Does not Exists",xmlValidationActions.getAgencyDetailFromCampaignXML(),
						IsNull.notNullValue());
	}

	@Then("^User validates the placement and advertiser information in XML$")
	public void userValidatesThePlacementAndAdvertiserInformationInXML()  {
		Map<Integer,Boolean> placementAdvertiserMap = xmlValidationActions.verifyPlacementsMetaInfoInXML();
		Set<Integer> activityPixelSet = xmlValidationActions.verifyActivityPixelDetailsFromXML();
		assertThat("Validation of Placement and Advertiser Information in XML Failed For Placements Ids "+placementAdvertiserMap.keySet(),
				placementAdvertiserMap.keySet(), IsEmptyCollection.empty());
		assertThat("Validation of Activity Pixel Information in XML Failed For Pixel Ids "+activityPixelSet,
				activityPixelSet,IsEmptyCollection.empty());
	}

	@And("^User validates campaign tag details for placements in XML$")
	public void userValidatesCampaignTagDetailsForPlacementsInXML()  {
		 xmlValidationActions.verifyCampaignLevelInfoInXML();
	}

	@And("^User validates the SSP count for placement in XML$")
	public void userValidatesTheSSPCountForPlacementInXML()  {
		Map<Integer,Boolean> sspMetaInfoMap = xmlValidationActions.verifySSPMetaInfoInXML();
		assertThat("Validation of SSP Meta(Count) Information in XML Failed For Placement Ids "+sspMetaInfoMap.keySet(),sspMetaInfoMap.keySet(),
				IsEmptyCollection.empty());;
	}

	@And("^USer validates the SSP Details for placement in XML$")
	public void userValidatesTheSSPDetailsForPlacementInXML()  {
		xmlValidationActions.verifySSPDetailsInfoInXML();
	}

	@And("^User validates the Targeting Details for placement in XML$")
	public void userValidatesTheTargetingDetailsForPlacementInXML()  {
		Map<Integer,Boolean> targetingInfoMap = xmlValidationActions.verifyTargetingInfoInXML();
		assertThat("Validation of Targeting Information in XML Failed For Placement Ids "+targetingInfoMap.keySet(),targetingInfoMap.keySet(),
				IsEmptyCollection.empty());;
	}

	@And("^User validates the total LineItems count of a placement in XML$")
	public void userValidatesTheTotalLineItemsCountOfAPlacementInXML()  {
		Map<Integer,Boolean> lineItemMetaInfoMap = xmlValidationActions.verifyLineItemsMetaInfoPresentInXML();
		assertThat("Validation of LineItems Meta(Count) Information in XML Failed For Placement Ids "+lineItemMetaInfoMap.keySet(),lineItemMetaInfoMap.keySet(),
				IsEmptyCollection.empty());
	}

	@And("^User validates the LineItems details of a placement in XML$")
	public void userValidatesTheLineItemsDetailsOfAPlacementInXML()  {
		xmlValidationActions.verifyLineItemsInfoOfCampaignInXML();
	}

	@And("^User validates the total Creatives count of a placement in XML$")
	public void userValidatesTheTotalCreativesCountOfAPlacementInXML()  {
		Map<Integer,Boolean> creativeMetaInfoMap = xmlValidationActions.verifyCreativesMetaInfoInXML();
		assertThat("Verify Creatives Meta Information in XML Failed For Placement Ids "+creativeMetaInfoMap.keySet()
				,creativeMetaInfoMap.keySet(), IsEmptyCollection.empty());
	}

	@And("^User validates the Creatives details of a placement in XML$")
	public void userValidatesTheCreativesDetailsOfAPlacementInXML()  {
		xmlValidationActions.verifyCreativesInfoOfCampaignInXML();
	}

	@And("^All validations of Campaign in XML should pass successfully\\.$")
	public void allValidationsOfCampaignInXMLShouldPassSuccessfully() {
		Multimap<Integer,String> failedPlacementMap = xmlValidationActions.validateXMLVerificationStatusTable();
		assertThat("XML Validation FAILED For Camapign in XML "+ failedPlacementMap.keySet() +" for Sections " + failedPlacementMap,
				xmlValidationActions.validateXMLVerificationStatusTable().isEmpty(),
				equalTo(true));
	}


}
