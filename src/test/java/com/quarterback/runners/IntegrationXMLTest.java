package com.quarterback.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features =
{ "src/test/resources/features/quarterback/CampaignXMLVerification.feature" }, glue =
{ "com.quarterback.integrationSteps","com.osapi.createcampaign.steps" })
public class IntegrationXMLTest
{
	
}
