/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * @author Clearstream
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features =
{ "src/test/resources/features/quarterback/VerifyCampaignMetadata.feature" }, glue =
{ "com.quarterback.CampaignMetadataSteps" })

public class CampaignMetadataTest
{

}
