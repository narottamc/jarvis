/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.fileutils;

import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.quarterback.utils.Constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ClearStream.
 */
public class FilePathManager
{

	private static Map<ObjectType, String> FILEPATH_RESPONSESUPPLIER;
	private static Map<ObjectType, String> FILEPATH_REQUESTSUPPLIER;
	private static final Map<QueryMethod, Map<ObjectType, String>> QUERYTYPE_SUPPLIER;

	static
	{
		final Map<ObjectType, String> filePathRequest = new HashMap<>();
		filePathRequest.put(ObjectType.SIGN_IN, Constants.LOGIN_REQUEST_DATA);
		filePathRequest.put(ObjectType.AGENCIES, Constants.AGENCY_REQUEST_DATA);
		filePathRequest.put(ObjectType.BRANDS, Constants.BRAND_REQUEST_DATA);
		filePathRequest.put(ObjectType.ACTIVITYPIXELS,Constants.PIXEL_REQUEST_DATA);
		filePathRequest.put(ObjectType.CAMPAIGNS, Constants.CAMPAIGN_REQUEST_DATA);
		filePathRequest.put(ObjectType.PLACEMENTS, Constants.PLACEMENT_REQUEST_DATA);
		filePathRequest.put(ObjectType.CONVERSIONPIXELS,Constants.CONVERSIONPIXELS_REQUEST_DATA);
		filePathRequest.put(ObjectType.VIDEOASSETS, Constants.ASSET_REQUEST_DATA);
		filePathRequest.put(ObjectType.SURVEY, Constants.SURVEY_REQUEST_DATA);
		filePathRequest.put(ObjectType.DISPLAY, Constants.DISPLAY_REQUEST_DATA);
		filePathRequest.put(ObjectType.FLIGHTS, Constants.FLIGHTS_REQUEST_DATA);
		filePathRequest.put(ObjectType.LAUNCHCAMPAIGN, Constants.LAUNCHCAMPAIGN_REQUEST_DATA);
		filePathRequest.put(ObjectType.SMARTLIST, Constants.SMARTLISTS_REQUEST_DATA);
		filePathRequest.put(ObjectType.MEDIALIST, Constants.MEDIALISTS_REQUEST_DATA);
		filePathRequest.put(ObjectType.FRAUDS, Constants.FRAUDS_REQUEST_DATA);
		filePathRequest.put(ObjectType.SSP, Constants.SSP_REQUEST_DATA);
		filePathRequest.put(ObjectType.DEALSANDSEATS,Constants.DEALSANDSEATS_REQUEST_DATA);
		filePathRequest.put(ObjectType.TECHNOLOGY, Constants.TECHNOLOGIES_REQUEST_DATA);
		filePathRequest.put(ObjectType.GEOS, Constants.GEOS_REQUEST_DATA);
		filePathRequest.put(ObjectType.CATEGORIES, Constants.CATEGORIES_REQUEST_DATA);
		filePathRequest.put(ObjectType.DAYPART, Constants.DAYPARTS_REQUEST_DATA);
		filePathRequest.put(ObjectType.ADDITIONALOPTIONS, Constants.ADDITIONALOPTIONS_REQUEST_DATA);

		FILEPATH_REQUESTSUPPLIER = Collections.unmodifiableMap(filePathRequest);
	}

	static
	{
		final Map<ObjectType, String> filePathsResponse = new HashMap<>();
		filePathsResponse.put(ObjectType.AGENCIES, Constants.AGENCY_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.BRANDS, Constants.BRAND_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.CAMPAIGNS, Constants.CAMPAIGN_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.ACTIVITYPIXELS, Constants.PIXEL_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.PLACEMENTS, Constants.PLACEMENT_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.CONVERSIONPIXELS, Constants.CONVERSIONPIXELS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.VIDEOASSETS, Constants.ASSET_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.SURVEY, Constants.SURVEY_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.DISPLAY, Constants.DISPLAY_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.FLIGHTS, Constants.FLIGHTS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.ACTIVECAMPAIGN, Constants.LAUNCHEDCAMPAIGN_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.SMARTLIST, Constants.SMARTLIST_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.MEDIALIST, Constants.MEDIALIST_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.FRAUDS,Constants.FRAUDS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.SSP, Constants.SSP_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.DEALSANDSEATS,Constants.DEALSANDSEATS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.TECHNOLOGY, Constants.TECHNOLOGIES_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.GEOS, Constants.GEOS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.ZIPCODES, Constants.ZIPCODES_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.CATEGORIES, Constants.CATEGORIES_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.DAYPART, Constants.DAYPARTS_RESPONSE_DATA);
		filePathsResponse.put(ObjectType.ADDITIONALOPTIONS, Constants.ADDTIONALOPTION_RESPONSE_DATA);

		FILEPATH_RESPONSESUPPLIER = Collections.unmodifiableMap(filePathsResponse);
	}

	static
	{
		final Map<QueryMethod, Map<ObjectType, String>> methodTypes = new HashMap<>();
		methodTypes.put(QueryMethod.REQUEST, FILEPATH_REQUESTSUPPLIER);
		methodTypes.put(QueryMethod.RESPONSE, FILEPATH_RESPONSESUPPLIER);

		QUERYTYPE_SUPPLIER = Collections.unmodifiableMap(methodTypes);
	}

	public Map<ObjectType, String> supplyQueryMethod(QueryMethod method)
	{
		Map<ObjectType, String> filePathsMap = QUERYTYPE_SUPPLIER.get(method);

		if (filePathsMap == null)
		{
			throw new IllegalArgumentException("Invalid Query Method type: " + filePathsMap);
		}
		return filePathsMap;
	}

	// public String supplyFilePath(ObjectType type) {
	// String filePath = FILEPATH_SUPPLIER.get(type);
	//
	// if (filePath == null) {
	// throw new IllegalArgumentException("Invalid player type: "
	// + filePath);
	// }
	// return filePath;
	// }

}
