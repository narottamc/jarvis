/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.fileutils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream
 */
public class FileWriterUtils {
    private static final Logger LOG = Logger.getLogger(FileWriterUtils.class);
    static String scenarioDataPath;

    public FileWriterUtils(String scenarioDataPath) {
        FileWriterUtils.scenarioDataPath = scenarioDataPath;
    }

    public FileWriterUtils() {

    }

    public interface writeDataToJsonFile {
        <T> void writeResponseSingleJsonObjectToJsonFile(Class<T> clazz, String jsonString)
                throws JsonIOException, InstantiationException, IllegalAccessException;

        <T> void writeResponseJsonArrayObjectToJsonFile(Class<T> clazz, List<T> list)
                throws JsonIOException, InstantiationException, IllegalAccessException;

        <T> void writeRequestSingleJsonObjectToJsonFile(T instance)
                throws JsonIOException, InstantiationException, IllegalAccessException, FileNotFoundException;

        <T,V> void writeHashMapToJsonFile(Map<String,V> map)
                throws JsonIOException, InstantiationException, IllegalAccessException, FileNotFoundException;
    }


    public static class WritingToJsonFile implements writeDataToJsonFile {
        private Gson gson = new GsonBuilder().setPrettyPrinting()
                .disableHtmlEscaping()
                .setFieldNamingStrategy(new com.osapi.models.campaignresponse.FieldNamingStrategy()).serializeNulls()
                .create();
        private FilePathManager filePathManager = new FilePathManager();
        private String filePath;

        public WritingToJsonFile(ObjectType type, QueryMethod method) {
            this.filePath = scenarioDataPath + filePathManager.supplyQueryMethod(method).get(type);
            LOG.info("Writing" + " " + method + " " + "of Entity" + " " + type + " " + "to" + " " + "File Path :" + " "
                    + this.filePath);
        }

        public WritingToJsonFile(String filePath, ObjectType type, QueryMethod method) {
            this.filePath = filePath;
            LOG.info("Writing" + " " + method + " " + "of Entity" + " " + type + " " + "to" + " " + "File Path :" + " "
                    + this.filePath);
        }


        @Override
        public <T> void writeResponseSingleJsonObjectToJsonFile(Class<T> clazz, String jsonString)
                throws JsonIOException, InstantiationException, IllegalAccessException {
            @SuppressWarnings("unchecked")
            T instance = (T) gson.fromJson(jsonString, clazz.newInstance().getClass());
            try (FileWriter writer = new FileWriter(filePath)) {
                gson.toJson(instance, writer);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public <T> void writeResponseJsonArrayObjectToJsonFile(Class<T> clazz, List<T> list)
                throws JsonIOException, InstantiationException, IllegalAccessException {
            TypeToken<Collection<T>> collectionType = new TypeToken<Collection<T>>() {
            };

            try (FileWriter writer = new FileWriter(filePath)) {
                gson.toJson(list, collectionType.getType(), writer);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public <T> void writeRequestSingleJsonObjectToJsonFile(T instance) {
            try (FileWriter writer = new FileWriter(filePath)) {
                gson.toJson(instance, writer);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public <T,V> void writeHashMapToJsonFile(Map<String,V>map) throws JsonIOException {
            try (FileWriter writer = new FileWriter(filePath)) {
                gson.toJson(map, writer);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public WritingToJsonFile writeToJsonFile(ObjectType type, QueryMethod method) {
        return new WritingToJsonFile(type, method);
    }

    public WritingToJsonFile writeToJsonFile(String filePath, ObjectType type, QueryMethod method) {
        return new WritingToJsonFile(filePath, type, method);
    }


}
