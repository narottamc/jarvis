/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.newassetrequest.RootNewAssetRequest;
import com.quarterback.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream.
 *
 */
public enum VideoAssetType
{
	VAST{
		public RootNewAssetRequest create() {
			RootNewAssetRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootNewAssetRequest.class, Constants.NEWVIDEOASSET_REQUEST_DATA, false).get();
			List<Integer> mimeTypes = new ArrayList<>();
			for (int i = 1; i <= 5; i++)
			{
				mimeTypes.add(i);
			}
			request.getAsset().setIsVPaid(false);
			request.getAsset().setIsLinear(true);
			request.getAsset().setmimetypes(mimeTypes);
			request.getAsset().setBundleOption("VAST");
			request.getAsset().setType("vast");
			return request;
		}
		@Override
		public String getType() {
			return "vast";
		}
	},
	VPAIDLinear{
		public RootNewAssetRequest create() {
			RootNewAssetRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootNewAssetRequest.class, Constants.NEWVIDEOASSET_REQUEST_DATA, false).get();
			List<Integer> mimeTypes = new ArrayList<>();
			mimeTypes.add(3);
			mimeTypes.add(2);
			mimeTypes.add(5);
			request.getAsset().setIsVPaid(true);
			request.getAsset().setIsLinear(true);
			request.getAsset().setmimetypes(mimeTypes);
			request.getAsset().setBundleOption("VPAID-JS");
			request.getAsset().setType("linear");
			return request;
		}
		@Override
		public String getType() {
			return "linear";
		}
	},
	VPAIDNonLinear{
		public RootNewAssetRequest create() {
			RootNewAssetRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootNewAssetRequest.class, Constants.NEWVIDEOASSET_REQUEST_DATA, false).get();
			List<Integer> mimeTypes = new ArrayList<>();
			for (int i = 1; i <= 5; i++)
			{
				mimeTypes.add(i);
			}
			request.getAsset().setIsVPaid(true);
			request.getAsset().setIsLinear(false);
			request.getAsset().setmimetypes(mimeTypes);
			request.getAsset().setBundleOption("VAST");
			request.getAsset().setType("vpaid");
			return request;
		}
		@Override
		public String getType() {
			return "vpaid";
		}
	};
	
	public RootNewAssetRequest create() {
		return null;
	}
	
	public String getType() {
		return null;
	}
	
	public RootNewAssetRequest newInstance(VideoAssetType assetType) {
		return assetType.create();
	}
	
}
