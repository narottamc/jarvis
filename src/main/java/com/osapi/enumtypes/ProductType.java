/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.quarterback.factory.BusinessModels;
import com.quarterback.factory.IponWebMacros;
import com.quarterback.mappers.ConfigData;

import java.util.*;

/**
 * @author ClearStream.
 */
public enum ProductType {
    VIDEO("VIDEO", 1) {
        private List<IponWebMacros> getExcludedMacroListForProducType() {
           return  new ArrayList<>(Arrays.asList
                    (IponWebMacros.BID_CONTEXT, IponWebMacros.DISPLAY_CLICK_URL, IponWebMacros.EVENT_URL,
                            IponWebMacros.VIDEO_CLICK_URL, IponWebMacros.VIDEO_IMP));

        }

        @Override
        public Map<String, String> getMacrosForProductType() {
            Map<String, String> macroMap = new HashMap<>();
            for (IponWebMacros macro : IponWebMacros.values()) {
                if (!this.getExcludedMacroListForProducType().contains(macro)) {
                    macroMap.put(macro.toString(), macro.getMacroValue());
                }
            }
            return macroMap;
        }

        @Override
        public List<Map<String, Double>> getBusinessModelMapForProductType(Integer stragtegId, RootFlightsResponse flightsResponse) {
            return BusinessModels.fromCode(stragtegId).getBusinessModelMapForVideo(flightsResponse);
        }

    },
    ALCHEMY("ALCHEMY", 2) {
        private List<IponWebMacros> getExcludedMacroListForProducType() {
            return new ArrayList<>(Arrays.asList
                    (IponWebMacros.DISPLAY_CLICK_URL, IponWebMacros.HEIGHT, IponWebMacros.WIDTH, IponWebMacros.USER_AGENT));
        }

        @Override
        public Map<String, String> getMacrosForProductType() {
            Map<String, String> macroMap = new HashMap<>();
            for (IponWebMacros macro : IponWebMacros.values()) {

                if (!this.getExcludedMacroListForProducType().contains(macro)) {
                    macroMap.put(macro.toString(), macro.getMacroValue());
                }
            }
            return macroMap;
        }

        @Override
        public List<Map<String, Double>> getBusinessModelMapForProductType(Integer stragtegId, RootFlightsResponse flightsResponse) {
            return BusinessModels.fromCode(stragtegId).getBusinessModelMapForAlchemy(flightsResponse);
        }

        @Override
        public String getCanTrackClick(Integer strategyId) {
            return BusinessModels.fromCode(strategyId).getCanTrackClick();
        }

    },
    SURVEY("SURVEY", 3) {
        private List<IponWebMacros> getExcludedMacroListForProducType() {
            return new ArrayList<>(Arrays.asList
                    (IponWebMacros.DISPLAY_CLICK_URL, IponWebMacros.EVENT_URL, IponWebMacros.VIDEO_IMP,
                            IponWebMacros.HEIGHT, IponWebMacros.WIDTH, IponWebMacros.USER_AGENT));
        }

        @Override
        public Map<String, String> getMacrosForProductType() {
            Map<String, String> macroMap = new HashMap<>();
            for (IponWebMacros macro : IponWebMacros.values()) {
                if (!this.getExcludedMacroListForProducType().contains(macro)) {
                    macroMap.put(macro.toString(), macro.getMacroValue());
                }
            }
            return macroMap;
        }

        @Override
        public List<Map<String, Double>> getBusinessModelMapForProductType(Integer stragtegId, RootFlightsResponse flightsResponse) {
            return BusinessModels.fromCode(stragtegId).getBusinessModelMapForSurvey(flightsResponse);
        }

        @Override
        public String getCanTrackClick(Integer strategyId) {
            return BusinessModels.fromCode(strategyId).getCanTrackClick();
        }

    },
    DISPLAY("DISPLAY", 4) {
        private List<IponWebMacros> getExcludedMacroListForProducType() {
           return new ArrayList<>(Arrays.asList
                    (IponWebMacros.BID_CONTEXT, IponWebMacros.EVENT_URL, IponWebMacros.VIDEO_CLICK_URL,
                            IponWebMacros.VIDEO_IMP, IponWebMacros.HEIGHT, IponWebMacros.WIDTH, IponWebMacros.USER_AGENT));

        }

        @Override
        public Map<String, String> getMacrosForProductType() {
            Map<String, String> macroMap = new HashMap<>();
            for (IponWebMacros macro : IponWebMacros.values()) {
                if (!this.getExcludedMacroListForProducType().contains(macro)) {
                    macroMap.put(macro.toString(), macro.getMacroValue());
                }
            }
            return macroMap;
        }

        @Override
        public List<Map<String, Double>> getBusinessModelMapForProductType(Integer stragtegId, RootFlightsResponse flightsResponse) {
            return BusinessModels.fromCode(stragtegId).getBusinessModelMapForDisplay(flightsResponse);
        }

        @Override
        public String getCanTrackClick(Integer strategyId) {
            return BusinessModels.fromCode(strategyId).getCanTrackClick();
        }

        @Override
        public Integer getIsOptimizedPixelFromOS(Integer strategyId, RootConversionPixelResponse conversionPixelResponse) {
            return BusinessModels.fromCode(strategyId).getIsOptimizedPixelFromOS(conversionPixelResponse);
        }

        @Override
        public Integer getIsOptimizedPixelFromXML(Integer strategyId, ConfigData.LineItem lineItemXML) {
            return BusinessModels.fromCode(strategyId).getIsOptimizedPixelFromXML(lineItemXML);
        }

        @Override
        public Set<Integer> getConversionPixelsFromOS(Integer strategyId, RootConversionPixelResponse conversionPixelResponse) {
            return BusinessModels.fromCode(strategyId).getConversionPixelsFromOS(conversionPixelResponse);
        }

        @Override
        public Set<Integer> getConversionPixelsFromXML(Integer strategyId, ConfigData.LineItem lineItemXML) {
            return BusinessModels.fromCode(strategyId).getConversionPixelsFromXML(lineItemXML);
        }

        @Override
        public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(Integer strategyId, String scenarioTestData, Integer size) {
            return BusinessModels.fromCode(strategyId).getConversionPixelResponseListForProductType(scenarioTestData, size);
        }

    },
    VIDEOALCHEMY("VIDEO-ALCHEMY", 5);

    private String productType;
    private int productTypeNumber;

    public String getproducType() {
        return this.productType;
    }

    public Integer getproductTypeNumber() {
        return this.productTypeNumber;
    }

    public Map<String, String> getMacrosForProductType() {
        return null;
    }

    public List<Map<String, Double>> getBusinessModelMapForProductType(Integer stragtegId, RootFlightsResponse flightsResponse) {
        return null;
    }

    public String getCanTrackClick(Integer strategyId) {
        return null;
    }

    public Set<Integer> getConversionPixelsFromOS(Integer strategyId, RootConversionPixelResponse conversionPixelResponse) {
        Set<Integer> converisonPixelSet = new HashSet<>();
        converisonPixelSet.add(0);
        return converisonPixelSet;
    }

    public Integer getIsOptimizedPixelFromOS(Integer strategyId, RootConversionPixelResponse conversionPixelResponse) {
        return 0;
    }

    public Set<Integer> getConversionPixelsFromXML(Integer strategyId, ConfigData.LineItem lineItemXML) {
        Set<Integer> converisonPixelSet = new HashSet<>();
        converisonPixelSet.add(0);
        return converisonPixelSet;
    }

    public Integer getIsOptimizedPixelFromXML(Integer strategyId, ConfigData.LineItem lineItemXML) {
        return 0;
    }

    public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(Integer strategyId, String scenarioTestData, Integer size) {
        List<RootConversionPixelResponse> rootConversionPixelResponses = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            rootConversionPixelResponses.add(new RootConversionPixelResponse());
        }
        return rootConversionPixelResponses;

    }

    public static ProductType fromProductTypeNumber(int productTypeNumber) {
        ProductType productType=null;
        for (ProductType value : ProductType.values()) {
            if (value.productTypeNumber == productTypeNumber) {
                productType=value;
            }
        }
        if(productType==null){
            throw new NullPointerException("Invalid Product Type Number "+productTypeNumber);
        }
        return productType;
    }

    ProductType(String productType, int productTypeNumber) {
        this.productType = productType;
        this.productTypeNumber = productTypeNumber;
    }
}
