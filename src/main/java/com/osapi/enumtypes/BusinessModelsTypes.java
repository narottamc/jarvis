/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import java.util.Random;

/**
 * @author Clearstream.
 *
 */
public enum BusinessModelsTypes
{
	SIMPLE_CPM_WITH_MARGIN_MAXIMIZATION("Simple CPM with Margin Maximization", 1),
	CPM_WITH_COMPLETED_VIEW_RATE("CPM with Completed View Rate",2),
	CPM_WITH_CTR("CPM with CTR",3),
	CPM_WITH_CTR_AND_COMPLETED_VIEW_RATE("CPM with CTR and Completed View Rate",4),
	CPCV_WITH_MARGIN_MAXIMIZATION("CPCV with Margin Maximization",5),
	CPM_WITH_MARGIN_MAXIMIZATION("CPM with Margin Maximization",7),
	CPM_WITH_CTR_MAXIMIZATION("CPM with CTR Maximization",8),
	CPC_WITH_MARGIN_MAXIMIZATION("CPC with Margin Maximization",9),
	CPM_WITH_ACTION_MAXIMIZATION("CPM with Action Maximization",10),
	CPA_WITH_MARGIN_MAXIMIZATION("CPA with Margin Maximization",11),
	CPA_LEARNING("CPA Learning",12);

	
	private String businessModelName;
	private Integer strategyId;
	private static Random random = new Random();

	public Integer getBusinessModelStrategyId()
	{
		return this.strategyId;
	}
	
	public String getBusinessModelName()
	{
		return this.businessModelName;
	}

	BusinessModelsTypes(String businessModelName,Integer strategyId)
	{
		this.businessModelName = businessModelName;
		this.strategyId = strategyId;
		
	}

	public static Double getRandomDoubleValue()
	{
		double value = random.nextInt(30)+1.0;
		return value / 100;
	}

	public static Integer getRandomIntegerValue()
	{
		int value = random.nextInt(20)+1;
		return value;
	}

}
