/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import java.util.Random;

/**
 * @author Clearstream.
 */
public enum SellerNetworks {
    spotx,
    tremor,
    adcon,
    appnexus,
    adconductor;

    public static SellerNetworks getRandomSellerNetwork() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
}
