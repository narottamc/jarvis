/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.google.common.collect.Lists;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.categoriesresponse.CategoriesJsonResponse;
import com.osapi.models.geolistresponse.GeoListJsonResponse;
import com.osapi.models.regionsresponse.RegionJsonResponse;
import com.osapi.models.targetingrequest.Geo;
import com.osapi.models.targetingrequest.GeoList;
import com.osapi.models.targetingrequest.Language;
import com.osapi.models.targetingrequest.TargetCategoryRequest;
import com.quarterback.utils.Constants;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Clearstream.
 */
public enum TargetingCriteria {
    COUNTIRES {
        @Override
        public List<Geo> getGeoAttributeDataForPUTRequest() {
            List<Integer> validCountryIdsList = Lists.newArrayList( MapBuilderfromJsonFile.buildFromHashMap
                    (true, RegionJsonResponse.class, Constants.REGIONS_MASTERDATAJSON).getInstanceMultiMap().keySet());
            List<Geo> geoList = new ArrayList<>();
            Random randomNumber = new Random();
            Geo geoAttribute = new Geo();
            geoAttribute.setCountryId(IntStream.generate(() -> randomNumber.nextInt(validCountryIdsList.size())).distinct().limit(1)
                    .mapToObj(validCountryIdsList::get).collect(Collectors.toList()).get(0));
            geoList.add(geoAttribute);
            return geoList;
        }
    },
    REGIONS {
        @Override
        public List<Geo> getGeoRegionAttributeDataForPUTRequest(Integer countryId) {
            List<Geo> geoList = new ArrayList<>();
            Collection<RegionJsonResponse> regionsCountryMap = MapBuilderfromJsonFile.buildFromHashMap
                    (true, RegionJsonResponse.class, Constants.REGIONS_MASTERDATAJSON).getInstanceMultiMap().asMap().get(countryId);
            for (RegionJsonResponse regionJsonResponse : regionsCountryMap) {
                Geo geoAttribute = new Geo();
                geoAttribute.setRegionId(regionJsonResponse.getId());
                geoList.add(geoAttribute);
            }
            return geoList;
        }
    },
    DMAS {
        @Override
        public List<Geo> getGeoAttributeDataForPUTRequest() {
            List<Geo> geoList = new ArrayList<>();
            Random randomNumber = new Random();
            for (int i = 0; i < 3; i++) {
                Geo geoAttribute = new Geo();
                geoAttribute.setDmaId(randomNumber.ints(1, 1, 210).findFirst().getAsInt());
                geoList.add(geoAttribute);
            }
            return geoList;
        }
    },
    LANGUAGES {
        @Override
        public List<Language> getGeoLanguageAttributeDataForPUTRequest() {
            List<Language> languageList = new ArrayList<>();
            Random randomNumber = new Random();
            for (int i = 0; i < 3; i++) {
                Language language = new Language();
                language.setId(randomNumber.ints(1, 1, 72).findFirst().getAsInt());
                languageList.add(language);
            }
            return languageList;
        }
    },
    GEOLIST {
        @Override
        public List<GeoList> getGeoListAttributeDataForPUTRequest() {
            Map<Integer, GeoListJsonResponse> geoListJsonResponseMap = MapBuilderfromJsonFile.buildFromHashMap(false, GeoListJsonResponse.class,
                    Constants.GEOLIST_MASTERDATAJSON).getInstanceMap();
            List<GeoList> geoList = new ArrayList<>();
            Random randomNumber = new Random();
            Integer assignedGeoListCount = 0;
            while ((assignedGeoListCount < 1)) {
                Integer geoListId = randomNumber.ints(1, 1, 628).findFirst().getAsInt();
                if (geoListJsonResponseMap.keySet().contains(geoListId)) {
                    GeoList geoListObject = new GeoList();
                    geoListObject.setId(geoListId);
                    geoList.add(geoListObject);
                    assignedGeoListCount++;
                }
            }
            return geoList;
        }
    },
    DAYPARTS {
        @Override
        public List<String> getDayPartsAttributeDataForPUTRequest() {
            List<String> hours = new ArrayList<>();
            Random randomNumber = new Random();
            List<Integer> hoursList = randomNumber.ints(5, 1, 24)
                    .boxed().collect(Collectors.toList());
            for (int j = 0; j < hoursList.size(); j++) {
                hours.add(String.valueOf(hoursList.get(j)));
            }
            return hours;
        }
    },
    TARGETCATEGORIES {
        @Override
        public List<TargetCategoryRequest> getTargetCategoryAttributeDataForPUTRequest() {
            List<Integer> parentCategoryIdsList = Lists.newArrayList(MapBuilderfromJsonFile.buildFromHashMap(true,
                    CategoriesJsonResponse.class,Constants.PCATEGORIES_MASTERDATAJSON).getInstanceMultiMap().keySet());
            List<TargetCategoryRequest> targetCategoryRequestList = new ArrayList<>();
            Random randomNumber = new Random();
            for (int i = 0; i < 2; i++) {
                TargetCategoryRequest targetCategoryParentRequest = new TargetCategoryRequest();
                Integer parentCategoryId =IntStream.generate(() -> randomNumber.nextInt(parentCategoryIdsList.size())).distinct().limit(1)
                        .mapToObj(parentCategoryIdsList::get).collect(Collectors.toList()).get(0);
                targetCategoryParentRequest.setId(parentCategoryId);
                targetCategoryRequestList.add(targetCategoryParentRequest);
                Collection<CategoriesJsonResponse> childCategoriesList = MapBuilderfromJsonFile.buildFromHashMap(true,
                        CategoriesJsonResponse.class,Constants.PCATEGORIES_MASTERDATAJSON).getInstanceMultiMap().asMap().get(parentCategoryId);
                for(CategoriesJsonResponse categoriesJsonResponse : childCategoriesList){
                    if(categoriesJsonResponse.getParentId()!=null){
                        TargetCategoryRequest targetCategoryChildRequest = new TargetCategoryRequest();
                        targetCategoryChildRequest.setId(categoriesJsonResponse.getId());
                        targetCategoryRequestList.add(targetCategoryChildRequest);
                    }
                }
            }
            return targetCategoryRequestList;
        }
    };

    public List<String> getDayPartsAttributeDataForPUTRequest() {
        return null;
    }

    public List<GeoList> getGeoListAttributeDataForPUTRequest() {
        return null;
    }

    public List<Geo> getGeoRegionAttributeDataForPUTRequest(Integer countryId) {
        return null;
    }

    public List<Language> getGeoLanguageAttributeDataForPUTRequest() {
        return null;
    }

    public List<Geo> getGeoAttributeDataForPUTRequest() {
        return null;
    }

    public List<TargetCategoryRequest> getTargetCategoryAttributeDataForPUTRequest() {
        return null;
    }


}
