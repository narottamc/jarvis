/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.quarterback.utils.Constants;
import com.quarterback.utils.ContextScope;

/**
 * @author Clearstream.
 *
 */
public enum TestEnvironment
{
	DEVELOPMENT{
		@Override
		public String getAPIBaseEndPointPrefix() {
			return "https://clearstream-os-api-";
		}
		@Override
		public String getAPIBaseEndPointSuffix() {
			return ".herokuapp.com/";
		}
		@Override
		public String getEnvironmentForAPI() {
			return "dev"; 
		}
		@Override
		public ContextScope getAWSEnvironmentExecution() {
			return ContextScope.DEVELOPMENT;
		}
		@Override
		public String getAWSS3Bucket() {
			return "clearstream-media-tasks-development";
		}
		@Override
		public String getHibernateConfigurationPath() {
			return Constants.HIBERNATE_CONFIG_DEV;
		}
	},
	STAGING{
		@Override
		public String getAPIBaseEndPointPrefix() {
			return "https://os-api-";
		}
		@Override
		public String getAPIBaseEndPointSuffix() {
			return ".herokuapp.com/";
		}
		@Override
		public String getEnvironmentForAPI() {
			return "staging"; 
		}
		@Override
		public ContextScope getAWSEnvironmentExecution() {
			return ContextScope.SANDBOX;
		}
		@Override
		public String getAWSS3Bucket() {
			return "clearstream-media-tasks-sandbox";
		}
		@Override
		public String getHibernateConfigurationPath() {
			return Constants.HIBERNATE_CONFIG_STAGING;
		}
	},
	PRODUCTION{
		@Override
		public String getAPIBaseEndPointPrefix() {
			return null;
		}
		@Override
		public String getAPIBaseEndPointSuffix() {
			return null;
		}
		@Override
		public ContextScope getAWSEnvironmentExecution() {
			return ContextScope.PRODUCTION;
		}
	},
	LOCALHOST{
		@Override
		public String getAPIBaseEndPointPrefix() {
			return "http://172.25.63.32";
		}
		@Override
		public String getAPIBaseEndPointSuffix() {
			return ":5000/";
		}
		@Override
		public String getEnvironmentForAPI() {
			return null; 
		}
	};
	
	public String getAPIBaseEndPointPrefix() {
		return null;
	}
	public String getAPIBaseEndPointSuffix() {
		return null;
	}
	public String getEnvironmentForAPI() {
		return null;
	}
	public ContextScope getAWSEnvironmentExecution() {
		return null;
	}
	public String getAWSS3Bucket() {
		return null;
	}
	public String getHibernateConfigurationPath(){
		return null;
	}
}
