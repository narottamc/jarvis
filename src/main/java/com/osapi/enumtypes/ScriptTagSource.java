/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author Clearstream.
 */
public enum ScriptTagSource {

    OTHER("<script type=\"text/javascript\">var mixpoAd={guid:\"61495a14-df83-4e0b-b9b2-4a47b87b9146\",subdomain:\"www\",embedv:\"mixpo.com:dcf9aa1,thorwhal:74f334a,obsidian:f916afc-js\",clicktag:\"CLICK_TRACKER\", customTarget:\"{zipcode}\"};</script><script type=\"text/javascript\" src=\"https://swf.mixpo.com/js/loader.js\"></script>"),
    DCM_ORIGINAL_TAG("<ins class='dcmads' style='display:inline-block;width:300px;height:250px'\n" +
            "    data-dcm-placement='N636.3316759EMXDIGITAL/B21576930.226707194'\n" +
            "    data-dcm-rendering-mode='iframe'\n" +
            "    data-dcm-https-only\n" +
            "    data-dcm-resettable-device-id=''\n" +
            "    data-dcm-click-tracker=''\n" +
            "    data-dcm-app-id=''>\n" +
            "    <script src='https://www.googletagservices.com/dcm/dcmads.js'></script>\n" +
            "</ins>"),
    DCM_MACRO_TAG("<ins class=\"dcmads\" style=\"display:inline-block;width:300px;height:250px\" " +
            "data-dcm-placement=\"N636.3316759EMXDIGITAL/B21576930.226707194\" data-dcm-rendering-mode=\"iframe\" " +
            "data-dcm-https-only=\"\" data-dcm-resettable-device-id=\"\" " +
            "data-dcm-click-tracker=\"${REPORTING_IPON_DISPLAY_CLICK_URL}${EMX_DISPLAY_CLICK_URL}~~~\"" +
            " data-dcm-app-id=\"\">\n" +
            "  <script src=\"https://www.googletagservices.com/dcm/dcmads.js\"></script>\n" +
            "</ins>"),
    SIZMEK_ORIGINAL_TAG("<script src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=23660276&PluID=0&w=300&h=250&ord=" +
            "${TIMESTAMP}&ucm=true&ncu=\"></script>\n" +
            "<noscript>\n" +
            "<a href=\"&rd=https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=23660276&Page=&PluID=0&Pos=1773966002\" " +
            "target=\"_blank\"><img src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=23660276&Page=&PluID=0&Pos=1773966002\"" +
            " border=0 width=300 height=250></a>\n" +
            "</noscript>"),
    SIZMEK_MACRO_TAG("<script src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=23660276&PluID=0&w=300&h=250&ord=" +
            "${TIMESTAMP}&ucm=true&ncu=${REPORTING_IPON_DISPLAY_CLICK_URL_ENC}${EMX_DISPLAY_CLICK_URL_ENC}\"></script>\n" +
            "<noscript>\n" +
            "<a href=\"${REPORTING_IPON_DISPLAY_CLICK_URL}${EMX_DISPLAY_CLICK_URL}" +
            "&rd=https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=23660276&Page=&PluID=0&Pos=1773966002\" " +
            "target=\"_blank\"><img src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=23660276&Page=&PluID=0&Pos=1773966002\" " +
            "border=0 width=300 height=250></a>\n" +
            "</noscript>"),
    FLASHTALKING_SCRIPT_ORIGINAL_TAG("<script language=\"Javascript1.1\" type=\"text/javascript\">\n" +
            "\n" +
            "var ftClick = \"\";\n" +
            "\n" +
            "var ftExpTrack_3340316 = \"\";\n" +
            "\n" +
            "var ftX = \"\";\n" +
            "\n" +
            "var ftY = \"\";\n" +
            "\n" +
            "var ftZ = \"\";\n" +
            "\n" +
            "var ftOBA = 1;\n" +
            "\n" +
            "var ftContent = \"\";\n" +
            "\n" +
            "var ftCustom = \"\";\n" +
            "\n" +
            "var ft728x90_OOBclickTrack = \"\";\n" +
            "\n" +
            "var ftRandom = Math.random()*1000000;\n" +
            "\n" +
            "var ftBuildTag1 = \"<scr\";\n" +
            "\n" +
            "var ftBuildTag2 = \"</\";\n" +
            "\n" +
            "var ftClick_3340316 = ftClick;\n" +
            "\n" +
            "if(typeof(ft_referrer)==\"undefined\"){var ft_referrer=(function(){var r=\"\";if(window==top){r=window.location.href;}else{try{r=window.parent.location.href;}catch(e){}r=(r)?r:document.referrer;}while(encodeURIComponent(r).length>1000){r=r.substring(0,r.length-1);}return r;}());}\n" +
            "\n" +
            "var ftDomain = (window==top)?\"\":(function(){var d=document.referrer,h=(d)?d.match(\"(?::q/q/)+([qw-]+(q.[qw-]+)+)(q/)?\".replace(/q/g,decodeURIComponent(\"%\"+\"5C\")))[1]:\"\";return (h&&h!=location.host)?\"&ft_ifb=1&ft_domain=\"+encodeURIComponent(h):\"\";}());\n" +
            "\n" +
            "var ftTag = ftBuildTag1 + 'ipt language=\"javascript1.1\" type=\"text/javascript\" ';\n" +
            "\n" +
            "ftTag += 'src=\"https://servedby.flashtalking.com/imp/8/96793;3340316;201;js;Clearstream;728x90html5test/?ftx='+ftX+'&fty='+ftY+'&ftadz='+ftZ+'&ftscw='+ftContent+'&ft_custom='+ftCustom+'&ftOBA='+ftOBA+ftDomain+'&ft_agentEnv='+(window.mraid||window.ormma?'1':'0')+'&ft_referrer='+encodeURIComponent(ft_referrer)+'&cachebuster='+ftRandom+'\" id=\"ftscript_728x90\" name=\"ftscript_728x90\"';\n" +
            "\n" +
            "ftTag += '>' + ftBuildTag2 + 'script>';\n" +
            "\n" +
            "document.write(ftTag);\n" +
            "\n" +
            "</script>"),
    FLASHTALKING_NOSCRIPT_ORIGINAL_TAG("<noscript><a href=\"https://servedby.flashtalking.com/click/8/96793;3315744;0;209;0/?" +
            "ft_width=300&ft_height=600&url=20462965\" target=\"_blank\"><img border=\"0\" src=\"" +
            "https://servedby.flashtalking.com/imp/8/96793;3315744;205;gif;Clearstream;300x600test/?\"></a>" +
            "</noscript><script language=\"Javascript1.1\" type=\"text/javascript\">var ftClick = \"\";var ftExpTrack_3315744 = \"\";" +
            "var ftX = \"\";var ftY = \"\";var ftZ = \"\";var ftOBA = 1;var ftContent = \"\";var ftCustom = \"\";var ft300x600_OOBclickTrack = \"\";" +
            "var ftRandom = Math.random()*1000000;var ftBuildTag1 = \"<scr\";var ftBuildTag2 = \"</\";" +
            "var ftClick_3315744 = ftClick;if(typeof(ft_referrer)==\"undefined\"){var ft_referrer=(function(){var r=\"\";if(window==top)" +
            "{r=window.location.href;}else{try{r=window.parent.location.href;}catch(e){}r=(r)?r:document.referrer;}" +
            "while(encodeURIComponent(r).length>1000){r=r.substring(0,r.length-1);}return r;}());}var ftDomain = (window==top)?\"\"" +
            ":(function(){var d=document.referrer,h=(d)?d.match(\"(?::q/q/)+([qw-]+(q.[qw-]+)+)(q/)?\".replace(/q/g,decodeURIComponen" +
            "t(\"%\"+\"5C\")))[1]:\"\";return (h&&h!=location.host)?\"&ft_ifb=1&ft_domain=\"+encodeURIComponent(h):\"\";}());var ftTag" +
            " = ftBuildTag1 + 'ipt language=\"javascript1.1\" type=\"text/javascript\" ';ftTag += 'src=\"https://servedby.flashtalkin" +
            "g.com/imp/8/96793;3315744;201;js;Clearstream;300x600test/?ftx='+ftX+'&fty='+ftY+'&ftadz='+ftZ+'&ftscw='+ftContent+'&" +
            "ft_custom='+ftCustom+'&ftOBA='+ftOBA+ftDomain+'&ft_agentEnv='+(window.mraid||window.ormma?'1':'0')+'&ft_referrer='+" +
            "encodeURIComponent(ft_referrer)+'&cachebuster='+ftRandom+'\" id=\"ftscript_300x600\" name=\"ftscript_300x600\"';f" +
            "tTag += '>' + ftBuildTag2 + 'script>';document.write(ftTag);</script>"),
    FLASHTALKING_SCRIPT_MACRO_TAG("<script language=\"Javascript1.1\" type=\"text/javascript\">\n" +
            "\n" +
            "var ftClick = \"${REPORTING_IPON_DISPLAY_CLICK_URL}${EMX_DISPLAY_CLICK_URL}~~~\";\n" +
            "\n" +
            "var ftExpTrack_3340316 = \"\";\n" +
            "\n" +
            "var ftX = \"\";\n" +
            "\n" +
            "var ftY = \"\";\n" +
            "\n" +
            "var ftZ = \"\";\n" +
            "\n" +
            "var ftOBA = 1;\n" +
            "\n" +
            "var ftContent = \"\";\n" +
            "\n" +
            "var ftCustom = \"\";\n" +
            "\n" +
            "var ft728x90_OOBclickTrack = \"\";\n" +
            "\n" +
            "var ftRandom = Math.random()*1000000;\n" +
            "\n" +
            "var ftBuildTag1 = \"<scr\";\n" +
            "\n" +
            "var ftBuildTag2 = \"</\";\n" +
            "\n" +
            "var ftClick_3340316 = ftClick;\n" +
            "\n" +
            "if(typeof(ft_referrer)==\"undefined\"){var ft_referrer=(function(){var r=\"\";if(window==top){r=window.location.href;}else{try{r=window.parent.location.href;}catch(e){}r=(r)?r:document.referrer;}while(encodeURIComponent(r).length>1000){r=r.substring(0,r.length-1);}return r;}());}\n" +
            "\n" +
            "var ftDomain = (window==top)?\"\":(function(){var d=document.referrer,h=(d)?d.match(\"(?::q/q/)+([qw-]+(q.[qw-]+)+)(q/)?\".replace(/q/g,decodeURIComponent(\"%\"+\"5C\")))[1]:\"\";return (h&&h!=location.host)?\"&ft_ifb=1&ft_domain=\"+encodeURIComponent(h):\"\";}());\n" +
            "\n" +
            "var ftTag = ftBuildTag1 + 'ipt language=\"javascript1.1\" type=\"text/javascript\" ';\n" +
            "\n" +
            "ftTag += 'src=\"https://servedby.flashtalking.com/imp/8/96793;3340316;201;js;Clearstream;728x90html5test/?ftx='+ftX+'&fty='+ftY+'&ftadz='+ftZ+'&ftscw='+ftContent+'&ft_custom='+ftCustom+'&ftOBA='+ftOBA+ftDomain+'&ft_agentEnv='+(window.mraid||window.ormma?'1':'0')+'&ft_referrer='+encodeURIComponent(ft_referrer)+'&cachebuster='+ftRandom+'\" id=\"ftscript_728x90\" name=\"ftscript_728x90\"';\n" +
            "\n" +
            "ftTag += '>' + ftBuildTag2 + 'script>';\n" +
            "\n" +
            "document.write(ftTag);\n" +
            "\n" +
            "</script>"),
    FLASHTALKING_NOSCRIPT_MACRO_TAG("<noscript><a href=\"${REPORTING_IPON_DISPLAY_CLICK_URL}${EMX_DISPLAY_CLICK_URL}~~~" +
            "https://servedby.flashtalking.com/click/8/96793;3315744;0;209;0/?ft_width=300&ft_height=600&url=20462965\"" +
            "target=\"_blank\"><img border=\"0\" src=\"https://servedby.flashtalking.com/imp/8/96793;3315744;205;gif;Clearstream;" +
            "300x600test/?\"></a></noscript><script language=\"Javascript1.1\" type=\"text/javascript\">var ftClick = \"" +
            "${REPORTING_IPON_DISPLAY_CLICK_URL}${EMX_DISPLAY_CLICK_URL}~~~\";var ftExpTrack_3315744 = \"\";var ftX = \"\";" +
            " ftY = \"\";var ftZ = \"\";var ftOBA = 1;var ftContent = \"\";var ftCustom = \"\";var ft300x600_OOBclickTrack = \"\";" +
            " ftRandom = Math.random()*1000000;var ftBuildTag1 = \"<scr\";var ftBuildTag2 = \"</\";" +
            "var ftClick_3315744 = ftClick;if(typeof(ft_referrer)==\"undefined\"){var ft_referrer=(function(){var r=\"\";" +
            "if(window==top){r=window.location.href;}else{try{r=window.parent.location.href;}catch(e){}r=(r)?r:document.referrer;" +
            "}while(encodeURIComponent(r).length>1000){r=r.substring(0,r.length-1);}return r;}());}var ftDomain = (window==top)?" +
            "\"\":(function(){var d=document.referrer,h=(d)?d.match(\"(?::q/q/)+([qw-]+(q.[qw-]+)+)(q/)?\".replace(/q/g,decodeURICom" +
            "ponent(\"%\"+\"5C\")))[1]:\"\";return (h&&h!=location.host)?\"&ft_ifb=1&ft_domain=\"+encodeURIComponent(h):\"\";}());" +
            "var ftTag = ftBuildTag1 + 'ipt language=\"javascript1.1\" type=\"text/javascript\" ';ftTag += 'src=\"" +
            "https://servedby.flashtalking.com/imp/8/96793;3315744;201;js;Clearstream;300x600test/?ftx='+ftX+'&fty='+ftY+'&ftadz='+ftZ+'&" +
            "ftscw='+ftContent+'&ft_custom='+ftCustom+'&ftOBA='+ftOBA+ftDomain+'&ft_agentEnv='+(window.mraid||window.ormma?'1':'0')+'&ft_" +
            "referrer='+encodeURIComponent(ft_referrer)+'&cachebuster='+ftRandom+'\" id=\"ftscript_300x600\" name=\"ftscript_300x600\"';ftTag" +
            " += '>' + ftBuildTag2 + 'script>';document.write(ftTag);</script>");

    private String adScriptTag;

    ScriptTagSource(String adScriptTag) {
        this.adScriptTag = adScriptTag;
    }

    public String getAdScriptTag() {
        return adScriptTag;
    }
}
