/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author ClearStream.
 */
public enum QueryMethod
{
	RESPONSE,
	REQUEST,
	REQUESTTEMPLATES,
	NEWENTITYREQUEST,
	FILERESOURCES,
	AUTHENTICATION,
	TARGETINGDATA,
	SEGMENTSDATA;
}
