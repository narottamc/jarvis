/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import java.util.Random;

/**
 * @author Clearstream.
 */
public enum YesOrNo {
    YES("YES"),
    NO("NO");

    private String name;

    YesOrNo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static YesOrNo getRandomFromYesOrNo() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

}
