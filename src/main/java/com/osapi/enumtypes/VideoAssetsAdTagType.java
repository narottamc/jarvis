/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.osapi.models.newassetrequest.NewAssetMedia;

/**
 * @author Clearstream.
 *
 */
public enum VideoAssetsAdTagType
{
	THIRDPARTYADTAG(1){
		@Override
		public NewAssetMedia getAssetMediaForAdTagType() {
			return null;
		}
	},
	CLEARSTREAMADTAG(2){
		@Override
		public NewAssetMedia getAssetMediaForAdTagType() {
			NewAssetMedia assetMedia = new NewAssetMedia();
			assetMedia.setIsValid(true);
			assetMedia.setErrors(false);
			assetMedia.setFileType("video/mp4");
			assetMedia.setDuration(54);
			assetMedia.setHeight(250);
			assetMedia.setWidth(300);
			assetMedia.setBitrate(10);
			assetMedia.setUploadPerc(100);
			assetMedia.setSuccess("File uploaded successfully!");
			return assetMedia;
		}
	};

	private Integer adTagTypeId;

	public Integer getAdTagTypeId()
	{
		return this.adTagTypeId;
	}

	VideoAssetsAdTagType(Integer adTagTypeId)
	{
		this.adTagTypeId = adTagTypeId;
	}
	
	public NewAssetMedia getAssetMediaForAdTagType() {
		return null;
	}

}
