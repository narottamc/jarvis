/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.categoriesresponse.CategoriesJsonResponse;
import com.osapi.models.countriesresponse.CountiresJsonResponse;
import com.osapi.models.dmasresponse.DmasJsonResponse;
import com.osapi.models.geolistresponse.GeoListJsonResponse;
import com.osapi.models.inventorytypesresponse.InventoryTypesJsonResponse;
import com.osapi.models.languageresponse.LanguageJsonResponse;
import com.osapi.models.playersizeresponse.PlayerSizeJsonResponse;
import com.osapi.models.regionsresponse.RegionJsonResponse;
import com.osapi.models.segments.RootSegmentResponse;
import com.osapi.models.visibilitiesresponse.VisibilitiesJsonResponse;
import com.quarterback.utils.Constants;

import java.util.*;

/**
 * @author Clearstream
 */
public enum ObjectType
{
	SIGN_IN{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "auth/sign_in";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	AGENCIES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "agencies";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT) +"/";
		}
	},
	BRANDS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "brands";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT) +"/";
		}
	},
	CAMPAIGNS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "campaigns";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT) +"/";
		}
	},
	ACTIVECAMPAIGN,
	LAUNCHCAMPAIGN,
	VIDEOASSETS,
	SURVEY,
	DISPLAY,
	PLACEMENTS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "placements";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT) +"/";
		}
	},
	FLIGHTS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "flights";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT)+"/";
		}
	},
	MEDIALIST,
	SMARTLIST,
	GEOS,
	ADDITIONALOPTIONS,
	TECHNOLOGY,
	DAYPART,
	CATEGORIES,
	FRAUDS,
	SSP{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "seller_network_masters";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	DEALSANDSEATS,
	ACTIVITYPIXELS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "activity_pixels";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	CONVERSIONPIXELS,
	USERS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "users";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	ZIPCODES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "geo_lists";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	GEOLISTS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "geo_lists.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.GEOLISTS_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<GeoListJsonResponse> geoListJsonResponses = MapBuilderfromJsonString.
					build(GeoListJsonResponse.class,responseJsonString,true).getAsCollection();
			geoListJsonResponses.sort(Comparator.comparing(GeoListJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,GeoListJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,GeoListJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<geoListJsonResponses.size();i++){
				Map<Integer,GeoListJsonResponse> map = new HashMap<>();
				map.put(geoListJsonResponses.get(i).getId(),geoListJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,GeoListJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("geoList",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.GEOLIST_MASTERDATAJSON,ObjectType.GEOLISTS,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	LANGUAGES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "languages.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.LANGUAGES_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<LanguageJsonResponse> languageJsonResponses = MapBuilderfromJsonString.
					build(LanguageJsonResponse.class,responseJsonString,true).getAsCollection();
			languageJsonResponses.sort(Comparator.comparing(LanguageJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,LanguageJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,LanguageJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<languageJsonResponses.size();i++){
				Map<Integer,LanguageJsonResponse> map = new HashMap<>();
				map.put(languageJsonResponses.get(i).getId(),languageJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,LanguageJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("languages",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.LANGUAGES_MASTERDATAJSON,ObjectType.LANGUAGES,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	REGIONS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "geo/regions.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.REGIONS_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<RegionJsonResponse> regionJsonResponses = MapBuilderfromJsonString.
					build(RegionJsonResponse.class,responseJsonString,true).getAsCollection();
			regionJsonResponses.sort(Comparator.comparing(RegionJsonResponse::getCountryId));
			Map<String,Map<String,List<Map<Integer,RegionJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,RegionJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<regionJsonResponses.size();i++){
				Map<Integer,RegionJsonResponse> map = new HashMap<>();
				map.put(regionJsonResponses.get(i).getCountryId(),regionJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,RegionJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("regions",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.REGIONS_MASTERDATAJSON,ObjectType.REGIONS,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	VISIBILITIES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "visibilities.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.VISIBILITIES_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<VisibilitiesJsonResponse> visibilitiesJsonResponses = MapBuilderfromJsonString.
					build(VisibilitiesJsonResponse.class,responseJsonString,true).getAsCollection();
			visibilitiesJsonResponses.sort(Comparator.comparing(VisibilitiesJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,VisibilitiesJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,VisibilitiesJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<visibilitiesJsonResponses.size();i++){
				Map<Integer,VisibilitiesJsonResponse> map = new HashMap<>();
				map.put(visibilitiesJsonResponses.get(i).getId(),visibilitiesJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,VisibilitiesJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("visibilities",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.VISIBILITIES_MASTERDATAJSON,ObjectType.VISIBILITIES,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	PLAYERSIZEGROUP{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "player_size_groups.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.PLAYERSIZEGROUP_BASEENDPOINT_URL;
		}


		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<PlayerSizeJsonResponse> playerSizeJsonResponses = MapBuilderfromJsonString.
					build(PlayerSizeJsonResponse.class,responseJsonString,true).getAsCollection();
			playerSizeJsonResponses.sort(Comparator.comparing(PlayerSizeJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,PlayerSizeJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,PlayerSizeJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<playerSizeJsonResponses.size();i++){
				Map<Integer,PlayerSizeJsonResponse> map = new HashMap<>();
				map.put(playerSizeJsonResponses.get(i).getId(),playerSizeJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,PlayerSizeJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("playerSizeGroups",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.PLAYERSIZEGROUP_MASTERDATAJSON,ObjectType.PLAYERSIZEGROUP,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	COUNTRIES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "geo/countries.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.COUNTRIES_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<CountiresJsonResponse> countiresJsonResponses = MapBuilderfromJsonString.
					build(CountiresJsonResponse.class,responseJsonString,true).getAsCollection();
			countiresJsonResponses.sort(Comparator.comparing(CountiresJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,CountiresJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,CountiresJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<countiresJsonResponses.size();i++){
				Map<Integer,CountiresJsonResponse> map = new HashMap<>();
				map.put(countiresJsonResponses.get(i).getId(),countiresJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,CountiresJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("countries",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.COUNTRIES_MASTERDATAJSON,ObjectType.COUNTRIES,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	PCATEGORIES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "categories/";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.PCATEGORIES_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<CategoriesJsonResponse> categoriesJsonResponses = MapBuilderfromJsonString.
					build(CategoriesJsonResponse.class,responseJsonString,true).getAsCollection();
			categoriesJsonResponses.sort(Comparator.comparing(CategoriesJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,CategoriesJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,CategoriesJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<categoriesJsonResponses.size();i++){
				Map<Integer,CategoriesJsonResponse> map = new HashMap<>();
				if(categoriesJsonResponses.get(i).getParentId()==null){
					map.put(categoriesJsonResponses.get(i).getId(),categoriesJsonResponses.get(i));
				}
				else{
					map.put(categoriesJsonResponses.get(i).getParentId(),categoriesJsonResponses.get(i));
				}

				list.add(map);
			}

			Map<String,List<Map<Integer,CategoriesJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("pcategories",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.PCATEGORIES_MASTERDATAJSON,ObjectType.PCATEGORIES,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	DMAS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "dmas.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.DMAS_BASEENDPOINT_URL;
		}
		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<DmasJsonResponse> dmasJsonResponses = MapBuilderfromJsonString.
					build(DmasJsonResponse.class,responseJsonString,true).getAsCollection();
			dmasJsonResponses.sort(Comparator.comparing(DmasJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,DmasJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,DmasJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<dmasJsonResponses.size();i++){
				Map<Integer,DmasJsonResponse> map = new HashMap<>();
				map.put(dmasJsonResponses.get(i).getId(),dmasJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,DmasJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("dmas",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.DMAS_MASTERDATAJSON,ObjectType.DMAS,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	INVENTORYTYPES{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "inventory_types.json";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
		@Override
		public String getEndPointURLForObjectType() {
			return Constants.INVENTORYTYPES_BASEENDPOINT_URL;
		}

		@Override
		public <V> Map<String, V> buildHashMapConvertedResponse(String responseJsonString) {
			List<InventoryTypesJsonResponse> inventoryTypesJsonResponses = MapBuilderfromJsonString.build(InventoryTypesJsonResponse.class,responseJsonString
					,true).getAsCollection();
			inventoryTypesJsonResponses.sort(Comparator.comparing(InventoryTypesJsonResponse::getId));
			Map<String,Map<String,List<Map<Integer,InventoryTypesJsonResponse>>>> finalMap = new HashMap<>();
			List<Map<Integer,InventoryTypesJsonResponse>> list = new ArrayList<>();
			for(int i =0;i<inventoryTypesJsonResponses.size();i++){
				Map<Integer,InventoryTypesJsonResponse> map = new HashMap<>();
				map.put(inventoryTypesJsonResponses.get(i).getId(),inventoryTypesJsonResponses.get(i));
				list.add(map);
			}

			Map<String,List<Map<Integer,InventoryTypesJsonResponse>>> semifinalMap = new HashMap<>();
			semifinalMap.put("inventoryTypes",list);
			finalMap.put("body",semifinalMap);
			return (Map<String, V>) finalMap;
		}

		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.INVENTORYTYPES_MASTERDATAJSON,ObjectType.INVENTORYTYPES,QueryMethod.TARGETINGDATA)
					.writeHashMapToJsonFile(map);
		}
	},
	DMPPROVIDERS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "dmp_providers";
		}
		@Override
		public String getAPIVersion() {
			return "";
		}
	},
	DATASEGMENTS{
		@Override
		public String getEntityEndPointSuffixForAPIUrl() {
			return "data_segments";
		}
		@Override
		public String getAPIVersion() {
			return (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion")
					: Constants.API_VERSION_DEFAULT) +"/";
		}
		@Override
		public <V> void writeEntityResponseToJsonFile(Map<String, V> map) {
			FileWriterUtils fileWriterUtils = new FileWriterUtils();
			fileWriterUtils.writeToJsonFile(Constants.DATASEGMENTS_MASTERDATAJSON,ObjectType.DATASEGMENTS,QueryMethod.SEGMENTSDATA)
					.writeHashMapToJsonFile(map);
		}
	};
	
	public String getEntityEndPointSuffixForAPIUrl() {
		return null;
	}
	
	public String getAPIVersion() {
		return null;
	}

	public String getEndPointURLForObjectType(){
		return null;
	}

	public <V> void writeEntityResponseToJsonFile(Map<String,V> map) { }
	public <V> Map<String,V> buildHashMapConvertedResponse(String responseJsonString){ return null; }

}
