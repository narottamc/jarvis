/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import java.util.Random;

/**
 * @author Clearstream.
 */
public enum MediaInventoryType {
    BLACKLIST("BlackList",1),
    WHITELIST("WhiteList",2);

    private String type;
    private Integer code;

    MediaInventoryType(String type, Integer code) {
        this.type = type;
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public Integer getCode() {
        return code;
    }

    public static MediaInventoryType getRandomMediaInventoryType() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

    public static MediaInventoryType fromCode(int code) {
        for (MediaInventoryType value : MediaInventoryType.values())
        {
            if (value.code == code)
            {
                return value;
            }
        }
        return null;
    }
}
