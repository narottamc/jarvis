/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author Clearstream.
 */
public enum DisplayAdScriptTypes {

    OTHER(1){
        @Override
        public String getAdTagScript(Boolean isOriginalAdTagScript,Boolean isScriptTag) {
            return ScriptTagSource.OTHER.getAdScriptTag();
        }
    },
    DCM(2){
        @Override
        public String getAdTagScript(Boolean isOriginalAdTagScript,Boolean isScriptTag) {
            if(isOriginalAdTagScript){
                return ScriptTagSource.DCM_ORIGINAL_TAG.getAdScriptTag();
            }
            else{
                return ScriptTagSource.DCM_MACRO_TAG.getAdScriptTag();
            }
        }
    },
    SIZMEK(3){
        @Override
        public String getAdTagScript(Boolean isOriginalAdTagScript,Boolean isScriptTag) {
            if(isOriginalAdTagScript){
                return ScriptTagSource.SIZMEK_ORIGINAL_TAG.getAdScriptTag();
            }
            else{
                return ScriptTagSource.SIZMEK_MACRO_TAG.getAdScriptTag();
            }
        }
    },
    FLASHTALKING(4){
        @Override
        public String getAdTagScript(Boolean isOriginalAdTagScript,Boolean isScriptTag) {
            if(isOriginalAdTagScript){
                if(isScriptTag){
                    return ScriptTagSource.FLASHTALKING_SCRIPT_ORIGINAL_TAG.getAdScriptTag();
                }
                else{
                    return ScriptTagSource.FLASHTALKING_NOSCRIPT_ORIGINAL_TAG.getAdScriptTag();
                }
            }
            else{
                if(isScriptTag){
                    return ScriptTagSource.FLASHTALKING_SCRIPT_MACRO_TAG.getAdScriptTag();
                }
                else{
                    return ScriptTagSource.FLASHTALKING_NOSCRIPT_MACRO_TAG.getAdScriptTag();
                }
            }
        }
    };

    private Integer scriptTypeId ;

    DisplayAdScriptTypes(Integer scriptTypeId) {
        this.scriptTypeId = scriptTypeId;
    }

    public String getAdTagScript(Boolean isOriginalAdTagScript, Boolean isScriptTag){
        return null;
    }

    public static DisplayAdScriptTypes getDisplayAdScriptTagTypeFromId(Integer scriptTypeId){
        for (DisplayAdScriptTypes value : DisplayAdScriptTypes.values())
        {
            if (value.scriptTypeId == scriptTypeId)
            {
                return value;
            }
        }
        return null;
    }
}
