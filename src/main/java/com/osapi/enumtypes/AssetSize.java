/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

import java.util.Random;

/**
 * @author Clearstream.
 *
 */
public enum AssetSize
{
	SIZE1("300x250"),
	SIZE2("160x600"),
	SIZE3("250x360"),
	SIZE4("300x1050"),
	SIZE5("580x400"),
	SIZE6("980x120"),
	SIZE7("336x280"),
	SIZE8("728x90"),
	SIZE9("930x180"),
	SIZE10("970x250");
	
	private String assetDimension;
	
	private AssetSize(String assetDimension)
	{
		this.assetDimension = assetDimension;
	}
	
	public String getAssetDimension()
	{
		return assetDimension;
	}

	public static AssetSize getRandomSizeDimensions() {
         Random random = new Random();
         return values()[random.nextInt(values().length)];
     }
}
