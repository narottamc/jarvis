/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author Clearstream.
 */
public enum DisplayAssetType {
    SCRIPTTAG(1),
    ZIPFILE(2),
    IMAGE(3);


    private Integer displayAdTagType;

    DisplayAssetType(Integer displayAdTagType) {
        this.displayAdTagType = displayAdTagType;
    }

    public Integer getDisplayTypeId() {
        return displayAdTagType;
    }

    public static DisplayAssetType getDisplayTypeFromCode(Integer displayAdTagType){
        for(DisplayAssetType displayAssetType : DisplayAssetType.values()){
            if(displayAssetType.getDisplayTypeId().equals(displayAdTagType)){
                return displayAssetType;
            }
        }
        return null;
    }
}
