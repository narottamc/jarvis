/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * EnumFileResourcesType.
 *
 * @author ClearStream.
 */
public enum FileResourcesType
{
	DISPLAY_IMAGE,
	DISPLAY_ZIP,
	VIDEOASSET_CLEARSTREAM

}
