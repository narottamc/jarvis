/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author Clearstream.
 *
 */
public enum Users
{
	DISPLAY(1,"Display","User","display@cybage.com","password","password"),
	SURVEY(1,"Survey","User","survey@cybage.com","password","password"),
	VIDEO(1,"Video","User","video@cybage.com","password","password"),
	LAUNCH(1,"Launch","User","launch@cybage.com","password","password"),
	ALCHEMY(1,"Alchemy","User","alchemy@cybage.com","password","password");
	
	private Integer isInternalAdmin;
	private String firstName;
	private String lastName;
	private String emailId;
	private String password;
	private String confirmPassword;
	
	Users(Integer isInternalAdmin, String firstName, String lastName, String emailId, String password,
			String confirmPassword)
	{
		this.isInternalAdmin = isInternalAdmin;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	public Integer getIsInternalAdmin()
	{
		return isInternalAdmin;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public String getPassword()
	{
		return password;
	}

	public String getConfirmPassword()
	{
		return confirmPassword;
	}
	
	
	
	
}
