/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.enumtypes;

/**
 * @author Clearstream.
 */
public enum SQLQuery {
    COUNTRIES("select * from countries where id =?"),
    REGIONS("select * from regions where country_id = ?"),
    DMAS("select * from dmas where id = ?"),
    ZIP("select z.id,z.code,g.geo_list_id from zip_codes z INNER JOIN geo_lists_zip_codes g ON \n" +
            "g.zip_code_id = z.id where g.geo_list_id=? ORDER BY z.code ASC"),
    LANGUAGES("select * from languages where id=?"),
    CATEGORIES("");

    private String sqlQuery;

    SQLQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }
}
