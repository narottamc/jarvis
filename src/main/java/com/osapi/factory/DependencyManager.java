/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.osapi.builders.BuilderInjector;
import org.apache.log4j.Logger;

/**
 * @author ClearStream.
 */
public class DependencyManager
{
	private static Injector injector;
	private static Logger LOG = Logger.getLogger(DependencyManager.class);

	public static void initializeWith(BuilderInjector configuration)
	{
		if (injector == null)
		{
			LOG.info("Initializing injector");
			injector = Guice.createInjector(configuration);
		}
		 getInjector();
	}

	public static Injector getInjector()
	{
		if (injector == null)
		{
			LOG.info("Application hasn't been configured yet.");
			throw new NullPointerException("Injector is Null ; Application hasn't been configured yet.");
		}
		return injector;
	}

	public static void closeInjector()
	{
		if (injector != null)
		{
			injector = null;
			LOG.info("Closing Injector.");
		}
	}

	private DependencyManager()
	{
	}
}
