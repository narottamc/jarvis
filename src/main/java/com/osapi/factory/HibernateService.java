/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.google.inject.Singleton;
import com.osapi.enumtypes.TestEnvironment;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;

/**
 * @author Clearstream.
 */

@Singleton
public class HibernateService implements PostgreConnectionService {
    Logger LOG = Logger.getLogger(HibernateService.class);

    @Override
    public SessionFactory getSessionFactory() {
        SessionFactory sessionFactory = null;
                LOG.info("Initializing Hibernate Configuration for Enviroment "+ Constants.EXECUTION_ENVIRONMENT);
                File hibernateConfigFile = new File(TestEnvironment.valueOf(Constants.EXECUTION_ENVIRONMENT.toUpperCase()).getHibernateConfigurationPath());
                StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                        .configure(hibernateConfigFile).build();
                Metadata metaData = new MetadataSources(standardRegistry)
                        .getMetadataBuilder().build();
                // builds a session factory from the service registry
                sessionFactory = metaData.getSessionFactoryBuilder().build();
            return sessionFactory;
    }

    @Override
    public void closeSessionFactory(SessionFactory sessionFactory) {
        if(sessionFactory.isOpen()){
            LOG.info("Closing Hibernate Connection");
            sessionFactory.close();
            StandardServiceRegistryBuilder.destroy(sessionFactory.getSessionFactoryOptions().getServiceRegistry());
        }
    }
}
