/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.osapi.models.flightrequest.RootFlightRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ClearStream.
 */
public interface RequestBuilderService
{

	LinkedList<Integer> getVideoTypeAssetIdList(String assetType);

	LinkedList<Integer> getSurveyTypeAssetIdList();

	LinkedList<Integer> getDisplayTypeAssetIdList();

	List<RootFlightRequest> getRootFlighRequest(String scenarioTestData);

}
