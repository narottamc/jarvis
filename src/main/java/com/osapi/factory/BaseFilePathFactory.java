/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.osapi.enumtypes.FileResourcesType;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;
import org.ini4j.Ini;

import java.io.IOException;

/**
 * @author ClearStream.
 */
public class BaseFilePathFactory
{

	
	public interface BaseFilePathBuilder
	{
		public String getFilePath();
		public String getUploadFilePath();

	}

	
	public static class FilePathBuilder implements BaseFilePathBuilder
	{
		private static final Logger LOG = Logger.getLogger(FilePathBuilder.class);
		private FileResourcesType fileType;
		private ObjectType entityType;
		private QueryMethod method;
		Ini testDataIni = new Ini();

		public FilePathBuilder(ObjectType entityType, QueryMethod method)
		{
			this.entityType = entityType;
			this.method = method;
			try
			{
				testDataIni.load(Constants.TESTDATACONFIG);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}

		public FilePathBuilder(FileResourcesType entityType, QueryMethod method)
		{
			this.fileType = entityType;
			this.method = method;
			try
			{
				testDataIni.load(Constants.TESTDATACONFIG);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}

		@Override
		public String getFilePath()
		{
			Ini.Section section = testDataIni.get(method.toString());
			String responseFilePath = section.get(entityType.toString());
			return responseFilePath;
		}

		@Override
		public String getUploadFilePath()
		{
			Ini.Section section = testDataIni.get(method.toString());
			String responseFilePath = section.get(fileType.toString());
			return responseFilePath;
		}

	}

	public static FilePathBuilder getFilePathBuilderWith(ObjectType type, QueryMethod method)
	{
		return new FilePathBuilder(type, method);
	}

	public static FilePathBuilder getFilePathBuilderWith(FileResourcesType type, QueryMethod method)
	{
		return new FilePathBuilder(type, method);
	}

}
