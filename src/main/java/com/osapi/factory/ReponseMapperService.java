/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.surveyresponse.RootSurveyResponse;

import java.util.List;

/**
 * @author ClearStream.
 */
public interface ReponseMapperService
{
	RootAgencyResponse getRootAgencyResponse(String scenarioTestDataPath);

	RootBrandResponse getRootBrandResponse(String scenarioTestDataPath);

	RootCampaignResponse getRootCampaignResponse(String scenarioTestDataPath);

	List<RootPlacementResponse> getRootPlacementResponse(String scenarioTestDataPath);

	List<RootFlightsResponse> getRootFlightResponse(String scenarioTestDataPath);

	List<RootAssetResponse> getRootVideoAssetResponse(String scenarioTestDataPath);

	List<RootSurveyResponse> getRootSurveyAssetResponse(String scenarioTestDataPath);

	List<RootDisplayResponse> getRootDisplayAssetResponse(String scenarioTestDataPath);

}
