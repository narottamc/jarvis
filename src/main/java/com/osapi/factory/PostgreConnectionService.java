/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import org.hibernate.SessionFactory;

/**
 * @author Clearstream.
 */
public interface PostgreConnectionService {
    SessionFactory getSessionFactory();
    void closeSessionFactory(SessionFactory sessionFactory);
}
