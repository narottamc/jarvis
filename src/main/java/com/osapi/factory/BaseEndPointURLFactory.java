/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.TestEnvironment;
import org.apache.log4j.Logger;

/**
 * @author Clearstream
 */
public class BaseEndPointURLFactory
{

	public interface BaseEndPointURLBuilder
	{
		String buildBaseEndPointURL();
	}

	public static class EndPointURLBuilder implements BaseEndPointURLBuilder
	{
		private static final Logger LOG = Logger.getLogger(EndPointURLBuilder.class);
		private String environment;
		private String assetType;
		private String BASE_ENDPOINT_PREFIX;
		private String BASE_ENDPOINT_SUFFIX;
		private String BASE_ENDPOINT_VERSION;

		public EndPointURLBuilder()
		{

		}

		public EndPointURLBuilder(ObjectType type, String environment)
		{
			LOG.info("Creating BaseEnd Point URL for "+type.toString());
			LOG.info("Test Execution Enviroment Is "+environment.toUpperCase());
			LOG.info("API Version Is "+ ObjectType.valueOf(type.toString().toUpperCase()).getAPIVersion());
			
			this.assetType = ObjectType.valueOf(type.toString()).getEntityEndPointSuffixForAPIUrl();
			this.environment = TestEnvironment.valueOf(environment.toUpperCase()).getEnvironmentForAPI();
			this.BASE_ENDPOINT_PREFIX = TestEnvironment.valueOf(environment.toUpperCase()).getAPIBaseEndPointPrefix();
			this.BASE_ENDPOINT_SUFFIX = TestEnvironment.valueOf(environment.toUpperCase()).getAPIBaseEndPointSuffix();
			this.BASE_ENDPOINT_VERSION = ObjectType.valueOf(type.toString().toUpperCase()).getAPIVersion();
		}

		@Override
		public String buildBaseEndPointURL()
		{
			String finalBaseEndPointURL=BASE_ENDPOINT_PREFIX + environment + BASE_ENDPOINT_SUFFIX + BASE_ENDPOINT_VERSION +assetType;
			LOG.info("BaseEnd Point URL for "+assetType.toString()+" is "+finalBaseEndPointURL);
			return finalBaseEndPointURL;
		}

	}

	public static EndPointURLBuilder getBaseEndPointURLBuilderWith(ObjectType type, String environment)
	{
		return new EndPointURLBuilder(type, environment);
	}

}
