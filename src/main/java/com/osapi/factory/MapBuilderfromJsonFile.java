/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.osapi.builders.Body;
import com.osapi.builders.CustomJsonDeserializerForMap;
import com.osapi.builders.CustomJsonDeserializerForMultiMap;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream
 */

public class MapBuilderfromJsonFile<T> {
    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
            .setFieldNamingStrategy(new com.osapi.models.campaignresponse.FieldNamingStrategy()).serializeNulls()
            .create();
    private String jsonFilePath;
    private String scenarioTestDataPath;
    private T instance;
    private List<T> instanceList;
    private Map<Integer,T> instanceMap;
    private Multimap<Integer,T> instanceMultiMap;
    private boolean ifCond = true; // default

    @SuppressWarnings("unchecked")
    public MapBuilderfromJsonFile(Class<T> clazz, String jsonFilePath, String scenarioTestDataPath,
                                  boolean isJsonObjectArray) {

        this.setJsonFilePath(jsonFilePath);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new BOMInputStream(new FileInputStream(scenarioTestDataPath+jsonFilePath),
                    false, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE,
                    ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE)));
            //BufferedReader reader = new BufferedReader(new FileReader(scenarioTestDataPath + jsonFilePath));
            if (isJsonObjectArray) {
                Type collectionType = TypeToken.getParameterized(List.class, clazz.newInstance().getClass()).getType();
                instanceList = (List<T>) gson.fromJson(reader, collectionType);
                reader.close();
            } else {
                instance = (T) gson.fromJson(reader, clazz.newInstance().getClass());
                reader.close();
            }

        } catch (JsonSyntaxException | JsonIOException | InstantiationException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
    public MapBuilderfromJsonFile(Class<T> clazz, String jsonFilePath, boolean isJsonObjectArray) {

        this.setJsonFilePath(jsonFilePath);
        try {
           // BufferedReader reader = new BufferedReader(new FileReader(jsonFilePath));
            BufferedReader reader = new BufferedReader(new InputStreamReader(new BOMInputStream(new FileInputStream(jsonFilePath),
                    false, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE,
                    ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE)));
            if (isJsonObjectArray) {
                Type collectionType = TypeToken.getParameterized(List.class, clazz.newInstance().getClass()).getType();
                instanceList = (List<T>) gson.fromJson(reader, collectionType);
                reader.close();
            } else {
                instance = (T) gson.fromJson(reader, clazz.newInstance().getClass());
                reader.close();
            }

        } catch (JsonSyntaxException | JsonIOException | InstantiationException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }

    }

    public MapBuilderfromJsonFile(boolean isMultiMap,Class<T> clazz, String jsonFilePath) {
        this.setJsonFilePath(jsonFilePath);

        try {
            if(isMultiMap){
                Gson gson  = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
                        .registerTypeAdapter(new TypeToken<Body<T>>() {}.getType(), new CustomJsonDeserializerForMultiMap<>(clazz))
                        .setFieldNamingStrategy(new com.osapi.models.campaignresponse.FieldNamingStrategy()).serializeNulls()
                        .create();
                Type type = new TypeToken<Body<T>>() {}.getType();
                FileReader reader = new FileReader(jsonFilePath);
                Body<T> body = gson.fromJson(reader, type);
                instanceMultiMap = body.getMultimap();
               reader.close();
            }
            else {
                Gson gson  = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
                        .registerTypeAdapter(new TypeToken<Body<T>>() {}.getType(), new CustomJsonDeserializerForMap<>(clazz))
                        .setFieldNamingStrategy(new com.osapi.models.campaignresponse.FieldNamingStrategy()).serializeNulls()
                        .create();
                Type type = new TypeToken<Body<T>>() {}.getType();
                FileReader reader = new FileReader(jsonFilePath);
                Body<T> body = gson.fromJson(reader, type);
                instanceMap = body.getMap();
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public T get() {
        return instance;
    }

    public List<T> getAsCollection() {
        return instanceList;
    }

    public Map<Integer,T> getInstanceMap() {
        return instanceMap;
    }

    public Multimap<Integer, T> getInstanceMultiMap() {
        return instanceMultiMap;
    }

    public static <T> MapBuilderfromJsonFile<T> build(Class<T> clazz, String jsonFilePath, String scenarioTestDataPath,
                                                      boolean isJsonObjectArray) {
        return new MapBuilderfromJsonFile<>(clazz, jsonFilePath, scenarioTestDataPath, isJsonObjectArray);
    }

    public static <T> MapBuilderfromJsonFile<T> buildFromTemplate(Class<T> clazz, String jsonFilePath,
                                                                  boolean isJsonObjectArray) {
        return new MapBuilderfromJsonFile<>(clazz, jsonFilePath, isJsonObjectArray);
    }

    public static <T> MapBuilderfromJsonFile<T> buildFromHashMap(boolean isMultiMap,Class<T> clazz, String jsonFilePath) {
        return new MapBuilderfromJsonFile<>(isMultiMap,clazz, jsonFilePath);
    }

    public String getJsonFilePath() {
        return jsonFilePath;
    }

    public void setJsonFilePath(String jsonFilePath) {
        this.jsonFilePath = jsonFilePath;
    }

    public String getScenarioTestDataPath() {
        return scenarioTestDataPath;
    }

    public void setScenarioTestDataPath(String scenarioTestDataPath) {
        this.scenarioTestDataPath = scenarioTestDataPath;
    }

}
