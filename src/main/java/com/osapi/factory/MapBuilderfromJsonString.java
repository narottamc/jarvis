/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.factory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

/**
 * @author Clearstream
 */
public class MapBuilderfromJsonString<T>
{
	private static Gson gson = new GsonBuilder().setPrettyPrinting()
			.setFieldNamingStrategy(new com.osapi.models.campaignresponse.FieldNamingStrategy()).serializeNulls()
			.disableHtmlEscaping()
			.create();
	private String jsonString;
	private T instance;
	private List<T> instanceList;
	private boolean ifCond = true; // default

	@SuppressWarnings("unchecked")
	public MapBuilderfromJsonString(Class<T> clazz, String jsonString, boolean isJsonObjectArray)
	{
		try
		{
			this.setJsonString(jsonString);
			if (isJsonObjectArray)
			{
				Type collectionType = TypeToken.getParameterized(List.class, clazz.newInstance().getClass()).getType();
				instanceList = (List<T>) gson.fromJson(jsonString, collectionType);
			}
			else
			{
				// instance = clazz.newInstance();
				instance = (T) gson.fromJson(jsonString, clazz.newInstance().getClass());
			}

		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	public MapBuilderfromJsonString<T> with(Consumer<T> setter)
	{
		if (ifCond)
			setter.accept(instance);
		return this;
	}

	public T get()
	{
		return instance;
	}

	public List<T> getAsCollection()
	{
		return instanceList;
	}

	public static <T> MapBuilderfromJsonString<T> build(Class<T> clazz, String jsonString, boolean isJsonObjectArray)
	{
		return new MapBuilderfromJsonString<>(clazz, jsonString, isJsonObjectArray);
	}

	public MapBuilderfromJsonString<T> If(BooleanSupplier condition)
	{
		this.ifCond = condition.getAsBoolean();
		return this;
	}

	public MapBuilderfromJsonString<T> endIf()
	{
		this.ifCond = true;
		return this;
	}

	public String getJsonString()
	{
		return jsonString;
	}

	public void setJsonString(String jsonString)
	{
		this.jsonString = jsonString;
	}
}
