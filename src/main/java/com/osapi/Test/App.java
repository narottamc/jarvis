package com.osapi.Test;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.osapi.builders.BuilderInjector;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.targetingresponse.*;
import com.quarterback.comparatorfactory.TargetingComparator;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;
import com.quarterback.utils.Constants;
import com.quarterback.utils.XmlFileParsingUtils;
import com.quarterback.validators.TargetingValidator;

import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class App
{
	public static void main(String[] args) {
		DependencyManager.initializeWith(new BuilderInjector());
		String scenarioTestData ="C:\\Users\\gauravsax\\IdeaProjects\\quarterback-automation\\TestDataOS\\Scenario1";
//		RootDealsAndSeatsResponse dealsAndSeatsResponseOS =
//				MapBuilderfromJsonFile.build(RootDealsAndSeatsResponse.class, Constants.DEALSANDSEATS_RESPONSE_DATA,scenarioTestData,true)
//						.getAsCollection().get(0);

		//Placement From OS
		RootPlacementResponse rootPlacementResponse =
				MapBuilderfromJsonFile.build(RootPlacementResponse.class,Constants.PLACEMENT_RESPONSE_DATA,scenarioTestData,true)
				.getAsCollection().get(0);

		//Targeting
		RootTechnologyResponse technologyResponse =
				MapBuilderfromJsonFile.build(RootTechnologyResponse.class,Constants.TECHNOLOGIES_RESPONSE_DATA,scenarioTestData,true)
						.getAsCollection().get(0);
		RootGeoListResponse geoListResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getGeoListDetails(scenarioTestData).get(0);
		RootGeoResponse geoResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getGeoDetails(scenarioTestData).get(0);
		RootCategoryResponse categoryResponse = MapBuilderfromJsonFile.build(RootCategoryResponse.class,Constants.CATEGORIES_RESPONSE_DATA
				,scenarioTestData,true).getAsCollection().get(0);
		RootDayPartResponse dayPartResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getDayPartDetails(scenarioTestData).get(0);
		RootAddtionalOptionResponse addtionalOptionResponse = DependencyManager.getInjector().getInstance(SessionVariables.class)
				.getAddtionalOptionsDetails(scenarioTestData).get(0);

		//Media Inventory
		Map<Integer, RootMediaInventoryResponse> mediaInventoryResponseMediaList =
				DependencyManager.getInjector().getInstance(SessionVariables.class).getMediaListDetailsForAPlacement(scenarioTestData);
	 	Map<Integer,RootMediaInventoryResponse> mediaInventoryResponseSmartList =
				DependencyManager.getInjector().getInstance(SessionVariables.class).getSmartListDetailsForAPlacement(scenarioTestData);


	 	File xmlFile = new File("C:\\Users\\gauravsax\\IdeaProjects\\quarterback-automation\\TestDataQuarterback\\WGR32V2g1_campaign.xml");
		ConfigData configData = XmlFileParsingUtils.parseXML(xmlFile);
		DependencyManager.getInjector().getInstance(SessionVariables.class).setConfigData(configData);
		ConfigData.Campaign xmlCampaign =
				DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(14691);
		System.out.println("XML Campaign :" + xmlCampaign.id);
		ConfigData.Targeting xmlTargeting = xmlCampaign.targeting;
		System.out.println("SmartList "+ xmlCampaign.smartlist);


		Map<Integer,Boolean>failedPlacementMap = new HashMap<>();

			if(DependencyManager.getInjector().getInstance(SessionVariables.class).shouldPlacementBePresentinXML(scenarioTestData,rootPlacementResponse.getId())){
				boolean isTargetingInfoCorrect = false;
				System.out.println("XML Validation : Validating Targeting Section Info In XML For Placement Id " + rootPlacementResponse.getId());

				TargetingComparator targetingOS = new TargetingValidator.OSTargeting(rootPlacementResponse,0,scenarioTestData);

				TargetingValidator.XMLTargeting targetingXML = new TargetingValidator.XMLTargeting(rootPlacementResponse);

				System.out.println("Targeting Details From OS " + targetingOS.getTargetingDmas());
				System.out.println("Targeting Details From XML " + targetingXML.getTargetingDmas());
				isTargetingInfoCorrect = targetingOS.compareTargetingInfo(targetingXML)==0;
				System.out.println("Targeting Validation Status "+ isTargetingInfoCorrect);

				if(!isTargetingInfoCorrect){
					failedPlacementMap.put(rootPlacementResponse.getId(),isTargetingInfoCorrect);
					System.out.println("XML Validation(TARGETING INFORMATION FAILED) For Placement "+ rootPlacementResponse.getId());
					System.out.println("XML Validation : Validating if A Campaign In XML Is Present with Correct Targeting Details ; For Placement Id "
							+ rootPlacementResponse.getId()
							+ " Expecting A Camapign in XML with Targeting Details " + targetingOS.toString() + " but found Campaign with attributes "
							+ new TargetingValidator.XMLTargeting(rootPlacementResponse).toString());
				}

			}
			int a =2;
			int b=3;
		System.out.println("Failed Placements Id Are "+ failedPlacementMap.keySet());

//		List<RootAssetRequest> assetRequestList = MapBuilderfromJsonFile.build(RootAssetRequest.class,
//				Constants.ASSET_REQUEST_DATA,scenarioTestData,true).getAsCollection();
//		List<RootAssetResponse> assetResponseList = MapBuilderfromJsonFile.build(RootAssetResponse.class,
//				Constants.ASSET_RESPONSE_DATA,scenarioTestData,true).getAsCollection();
//		System.out.println(assetResponseList.size());
//		System.out.println(assetRequestList.size());
//		Integer adTagTypeId = VideoAssetsAdTagType.valueOf("CLEARSTREAMADTAG".toUpperCase()).getAdTagTypeId();
//		System.out.println(adTagTypeId);
//		assetRequestList.removeIf(rootAssetRequest -> (rootAssetRequest.getAsset().getAdTagTypeId().equals(adTagTypeId)));
//		assetResponseList.removeIf(rootAssetResponse -> (rootAssetResponse.getAdTagTypeId().equals(adTagTypeId)));
//
//
//		System.out.println(assetResponseList.size());
//		System.out.println(assetRequestList.size());
	}

	public static Comparator<Map<String,Multimap>> SSPComparator = new Comparator<Map<String,Multimap>>()
	{

		@Override
		public int compare(Map<String, Multimap> o1, Map<String, Multimap> o2) {
			if(o1.keySet().size()==o2.keySet().size()){
				for(String key : o1.keySet()){
					Multimap<String, String> multiMap1 = o1.get(key);
					Multimap<String, String> multiMap2 = o2.get(key);
					Multimap<String, String> firstSecondDifference =
							Multimaps.filterEntries(multiMap1, e -> !multiMap2.containsEntry(e.getKey(), e.getValue()));
					Multimap<String, String> secondFirstDifference =
							Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
					System.out.println(firstSecondDifference);
					System.out.println(secondFirstDifference);
					return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty()==true ? 0:1;
				}
			}
			return 1;
		}
	};
}

