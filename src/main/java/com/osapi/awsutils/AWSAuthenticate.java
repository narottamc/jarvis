/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.awsutils;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;





/**
 * @author ClearStream.
 */
public class AWSAuthenticate
{

	private AmazonS3 s3Client;
	private ClientConfiguration configuration = PredefinedClientConfigurations.defaultConfig();
	private AWSCredentialsProvider credentials = new DefaultAWSCredentialsProviderChain();

	public AWSAuthenticate()
	{
		this.s3Client = AmazonS3ClientBuilder.standard().withCredentials(credentials)
				.withClientConfiguration(configuration).withRegion("us-east-1").build();

	}

	public AmazonS3 getS3Client()
	{
		return s3Client;
	}

	public void setS3Client(AmazonS3 s3Client)
	{
		this.s3Client = s3Client;
	}

	public AWSCredentialsProvider getCredentials()
	{
		return credentials;
	}

	public void setCredentials(AWSCredentialsProvider credentials)
	{
		this.credentials = credentials;
	}
}
