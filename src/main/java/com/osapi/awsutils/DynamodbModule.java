/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.awsutils;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.ConversionSchemas;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;


/**
 * @author ClearStream.
 */
public class DynamodbModule extends AbstractModule
{
	@Override
	protected void configure()
	{

	}

	@Provides
	@Singleton
	AmazonDynamoDB configureDynamoDBClient()
	{
		AWSCredentialsProvider credentials = new DefaultAWSCredentialsProviderChain();
		ClientConfiguration configuration = PredefinedClientConfigurations.dynamoDefault();
		AmazonDynamoDB dynamodb = AmazonDynamoDBClientBuilder.standard().withCredentials(credentials)
				.withClientConfiguration(configuration).withRegion("us-east-1").build();
		// Need for local running
		// AmazonDynamoDB dynamodb =
		// AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
		// new AwsClientBuilder.EndpointConfiguration("http://localhost:8000",
		// "us-east-1"))
		// .build();

		// LOG.info("Connected Amazon DynamoDB service at: {}" + ", " +
		// endpoint);

		return dynamodb;
	}

	@Provides
	@Singleton
	DynamoDB configureDynamoDB(AmazonDynamoDB client)
	{
		return new DynamoDB(client);
	}

	@Provides
	@Singleton
	DynamoDBMapper configureDynamoDBMapper(AmazonDynamoDB dynamodb, DynamoDBMapperConfig config)
	{
		return new DynamoDBMapper(dynamodb, config);
	}

	@Provides
	@Singleton
	DynamoDBMapperConfig prepareDynamoDBMapperConfig()
	{
		DynamoDBMapperConfig.Builder builder = new DynamoDBMapperConfig.Builder();
		return builder.withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.APPEND_SET)
				.withConsistentReads(DynamoDBMapperConfig.ConsistentReads.EVENTUAL)
				.withPaginationLoadingStrategy(DynamoDBMapperConfig.PaginationLoadingStrategy.ITERATION_ONLY)
				.withConversionSchema(ConversionSchemas.V2_COMPATIBLE)
				.withBatchWriteRetryStrategy(new DynamoDBMapperConfig.DefaultBatchWriteRetryStrategy(3)).build();
	}

	// private static final Logger LOG =
	// LogManager.getLogger(DynamodbModule.class);
}
