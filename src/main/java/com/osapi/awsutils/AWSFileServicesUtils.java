/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.awsutils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.quarterback.utils.CommonUtils;
import com.quarterback.utils.Constants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;





/**
 * @author ClearStream.
 */
public class AWSFileServicesUtils
{

	private static final Logger LOG = LogManager.getLogger(AWSFileServicesUtils.class);
	private static AWSAuthenticate awsauthenticate = new AWSAuthenticate();
	public static String localXMLFile;

	public static void downloadAndUnZipFileFromAWSToLocal(String bucket, String key, String taskGroupID)
	{
		LOG.info("Starting File downloading from S3 to " + Constants.CAMPAIGN_XML_ZIP_FILE_PATH + "_" + taskGroupID
				+ ".xml.gz");
		try
		{
			File file = new File(Constants.CAMPAIGN_XML_ZIP_FILE_PATH + "_" + taskGroupID + ".xml.gz");

			S3Object o = awsauthenticate.getS3Client().getObject(bucket, key);
			InputStream reader = new BufferedInputStream(o.getObjectContent());
			OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
			int read = -1;
			while ((read = reader.read()) != -1)
			{
				writer.write(read);
			}
			writer.flush();
			writer.close();
			reader.close();

			LOG.info("File download Done!");
			localXMLFile = Constants.CAMPAIGN_XML_PATH + taskGroupID + "_" + Constants.CAMPAIGN_XML_NAME + ".xml";
			CommonUtils.decompressGzipFile(Constants.CAMPAIGN_XML_ZIP_FILE_PATH + "_" + taskGroupID + ".xml.gz",
					localXMLFile);
			LOG.info("File Unzip Done!");
		}
		catch (AmazonServiceException e)
		{
			LOG.error(e.getErrorMessage());
		}
		catch (FileNotFoundException e)
		{
			LOG.error(e.getMessage());
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage());
		}
	}

	public static void uploadFileFromLocaltoS3(String bucketName, String keyName, File file)
	{
		LOG.info("File uploading from " + file.getPath() + " " + "to S3");
		// File file = new File(localFilePath);
		long contentLength = file.length();
		long partSize = 5 * 1024 * 1024; // Set part size to 5 MB.

		try
		{
			AmazonS3 s3Client = awsauthenticate.getS3Client();

			// Create a list of ETag objects. You retrieve ETags for each object
			// part uploaded,
			// then, after each individual part has been uploaded, pass the list
			// of ETags to
			// the request to complete the upload.
			List<PartETag> partETags = new ArrayList<PartETag>();

			// Initiate the multipart upload.
			InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, keyName);
			InitiateMultipartUploadResult initResponse = s3Client.initiateMultipartUpload(initRequest);

			// Upload the file parts.
			long filePosition = 0;
			for (int i = 1; filePosition < contentLength; i++)
			{
				// Because the last part could be less than 5 MB, adjust the
				// part size as needed.
				partSize = Math.min(partSize, (contentLength - filePosition));

				// Create the request to upload a part.
				UploadPartRequest uploadRequest = new UploadPartRequest().withBucketName(bucketName).withKey(keyName)
						.withUploadId(initResponse.getUploadId()).withPartNumber(i).withFileOffset(filePosition)
						.withFile(file).withPartSize(partSize);
				LOG.info("File uploading to S3 Started");
				// Upload the part and add the response's ETag to our list.
				UploadPartResult uploadResult = s3Client.uploadPart(uploadRequest);
				partETags.add(uploadResult.getPartETag());

				filePosition += partSize;
			}

			// Complete the multipart upload.
			CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, keyName,
					initResponse.getUploadId(), partETags);
			s3Client.completeMultipartUpload(compRequest);
			LOG.info("File Uploaded To S3 successfully");
		}
		catch (AmazonServiceException e)
		{
			// The call was transmitted successfully, but Amazon S3 couldn't
			// process
			// it, so it returned an error response.
			e.printStackTrace();
		}
		catch (SdkClientException e)
		{
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}

	}

}
