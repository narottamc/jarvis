/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.agencyrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class AgencyRequest
{
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("internal")
	@Expose
	private Boolean internal;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Boolean getInternal()
	{
		return internal;
	}

	public void setInternal(Boolean internal)
	{
		this.internal = internal;
	}

}
