/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.agencyrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootAgencyRequest
{
	@SerializedName("agency")
	@Expose
	private AgencyRequest agency;

	public AgencyRequest getAgency()
	{
		return agency;
	}

	public void setAgency(AgencyRequest agency)
	{
		this.agency = agency;
	}

}
