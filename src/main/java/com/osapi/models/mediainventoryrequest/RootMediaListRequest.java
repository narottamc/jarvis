/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.mediainventoryrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class RootMediaListRequest
{
	@SerializedName("placementId")
	@Expose
	private Integer placementId;
	@SerializedName("media_lists")
	@Expose
	private List<MediaList> mediaLists = null;

	public Integer getPlacementId()
	{
		return placementId;
	}

	public void setPlacementId(Integer placementId)
	{
		this.placementId = placementId;
	}

	public List<MediaList> getMediaLists()
	{
		return mediaLists;
	}

	public void setMediaLists(List<MediaList> mediaLists)
	{
		this.mediaLists = mediaLists;
	}

}
