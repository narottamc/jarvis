/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.mediainventoryrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class ItemStateChangeInfo
{
	@SerializedName("itemReturnState")
	@Expose
	private String itemReturnState;
	@SerializedName("itemReturnParams")
	@Expose
	private ItemReturnParams itemReturnParams;
	@SerializedName("itemSourceState")
	@Expose
	private String itemSourceState;

	public String getItemReturnState()
	{
		return itemReturnState;
	}

	public void setItemReturnState(String itemReturnState)
	{
		this.itemReturnState = itemReturnState;
	}

	public ItemReturnParams getItemReturnParams()
	{
		return itemReturnParams;
	}

	public void setItemReturnParams(ItemReturnParams itemReturnParams)
	{
		this.itemReturnParams = itemReturnParams;
	}

	public String getItemSourceState()
	{
		return itemSourceState;
	}

	public void setItemSourceState(String itemSourceState)
	{
		this.itemSourceState = itemSourceState;
	}

}
