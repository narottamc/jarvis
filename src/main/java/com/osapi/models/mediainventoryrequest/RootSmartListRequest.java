package com.osapi.models.mediainventoryrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RootSmartListRequest
{

	@SerializedName("placementId")
	@Expose
	private Integer placementId;
	@SerializedName("smart_lists")
	@Expose
	private List<SmartList> smartLists = null;

	public Integer getPlacementId()
	{
		return placementId;
	}

	public void setPlacementId(Integer placementId)
	{
		this.placementId = placementId;
	}

	public List<SmartList> getSmartLists()
	{
		return smartLists;
	}

	public void setSmartLists(List<SmartList> smartLists)
	{
		this.smartLists = smartLists;
	}

}
