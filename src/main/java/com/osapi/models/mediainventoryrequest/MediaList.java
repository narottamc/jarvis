/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.mediainventoryrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class MediaList
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("itemStateChangeInfo")
	@Expose
	private ItemStateChangeInfo itemStateChangeInfo;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public ItemStateChangeInfo getItemStateChangeInfo()
	{
		return itemStateChangeInfo;
	}

	public void setItemStateChangeInfo(ItemStateChangeInfo itemStateChangeInfo)
	{
		this.itemStateChangeInfo = itemStateChangeInfo;
	}

}
