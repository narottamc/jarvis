/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.mediainventoryrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class ItemReturnParams
{
	@SerializedName("campaignId")
	@Expose
	private Integer campaignId;
	@SerializedName("placementId")
	@Expose
	private String placementId;

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public String getPlacementId()
	{
		return placementId;
	}

	public void setPlacementId(String placementId)
	{
		this.placementId = placementId;
	}

}
