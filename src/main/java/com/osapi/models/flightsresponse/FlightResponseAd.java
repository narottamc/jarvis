/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.flightsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class FlightResponseAd
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("ad_tag")
	@Expose
	private String adTag;
	@SerializedName("ad_tag_secure")
	@Expose
	private String adTagSecure;
	@SerializedName("budget_percentage")
	@Expose
	private Integer budgetPercentage;
	@SerializedName("config")
	@Expose
	private FlightResponseConfig config;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("replaceable")
	@Expose
	private Boolean replaceable;
	@SerializedName("short_ad_tag")
	@Expose
	private Object shortAdTag;
	@SerializedName("short_ad_tag_secure")
	@Expose
	private Object shortAdTagSecure;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("flight_id")
	@Expose
	private Integer flightId;
	@SerializedName("ad_type")
	@Expose
	private String adType;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAdTag()
	{
		return adTag;
	}

	public void setAdTag(String adTag)
	{
		this.adTag = adTag;
	}

	public String getAdTagSecure()
	{
		return adTagSecure;
	}

	public void setAdTagSecure(String adTagSecure)
	{
		this.adTagSecure = adTagSecure;
	}

	public Integer getBudgetPercentage()
	{
		return budgetPercentage;
	}

	public void setBudgetPercentage(Integer budgetPercentage)
	{
		this.budgetPercentage = budgetPercentage;
	}

	public FlightResponseConfig getConfig()
	{
		return config;
	}

	public void setConfig(FlightResponseConfig config)
	{
		this.config = config;
	}

	public Integer getHeight()
	{
		return height;
	}

	public void setHeight(Integer height)
	{
		this.height = height;
	}

	public Boolean getReplaceable()
	{
		return replaceable;
	}

	public void setReplaceable(Boolean replaceable)
	{
		this.replaceable = replaceable;
	}

	public Object getShortAdTag()
	{
		return shortAdTag;
	}

	public void setShortAdTag(Object shortAdTag)
	{
		this.shortAdTag = shortAdTag;
	}

	public Object getShortAdTagSecure()
	{
		return shortAdTagSecure;
	}

	public void setShortAdTagSecure(Object shortAdTagSecure)
	{
		this.shortAdTagSecure = shortAdTagSecure;
	}

	public Integer getWidth()
	{
		return width;
	}

	public void setWidth(Integer width)
	{
		this.width = width;
	}

	public Integer getFlightId()
	{
		return flightId;
	}

	public void setFlightId(Integer flightId)
	{
		this.flightId = flightId;
	}

	public String getAdType()
	{
		return adType;
	}

	public void setAdType(String adType)
	{
		this.adType = adType;
	}

}
