/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.flightsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class FlightResponseAssets
{
	@SerializedName("asset_id")
	@Expose
	private Integer assetId;
	@SerializedName("flight_id")
	@Expose
	private Integer flightId;
	@SerializedName("distribution")
	@Expose
	private Float distribution;

	public Integer getAssetId()
	{
		return assetId;
	}

	public void setAssetId(Integer assetId)
	{
		this.assetId = assetId;
	}

	public Integer getFlightId()
	{
		return flightId;
	}

	public void setFlightId(Integer flightId)
	{
		this.flightId = flightId;
	}

	public Float getDistribution()
	{
		return distribution;
	}

	public void setDistribution(Float distribution)
	{
		this.distribution = distribution;
	}

}
