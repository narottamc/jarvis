/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.flightrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class RootFlightRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("placement_id")
	@Expose
	private Integer placementId;
	@SerializedName("ad_server")
	@Expose
	private String adServer;
	@SerializedName("js_bundle")
	@Expose
	private Object jsBundle;
	@SerializedName("min_viewability")
	@Expose
	private Object minViewability;
	@SerializedName("row_index")
	@Expose
	private Object rowIndex;
	@SerializedName("short_vast")
	@Expose
	private Boolean shortVast;
	@SerializedName("bid_adjustment")
	@Expose
	private Object bidAdjustment;
	@SerializedName("active")
	@Expose
	private Boolean active;
	@SerializedName("business_model")
	@Expose
	private FlightRequestBusinessModel businessModel;
	@SerializedName("flight_assets")
	@Expose
	private List<FlightRequestAsset> flightAssets = null;
	@SerializedName("events")
	@Expose
	private List<Object> events = null;
	@SerializedName("ads_attributes")
	@Expose
	private List<FlightRequestAdsAttribute> adsAttributes = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public Integer getPlacementId()
	{
		return placementId;
	}

	public void setPlacementId(Integer placementId)
	{
		this.placementId = placementId;
	}

	public String getAdServer()
	{
		return adServer;
	}

	public void setAdServer(String adServer)
	{
		this.adServer = adServer;
	}

	public Object getJsBundle()
	{
		return jsBundle;
	}

	public void setJsBundle(Object jsBundle)
	{
		this.jsBundle = jsBundle;
	}

	public Object getMinViewability()
	{
		return minViewability;
	}

	public void setMinViewability(Object minViewability)
	{
		this.minViewability = minViewability;
	}

	public Object getRowIndex()
	{
		return rowIndex;
	}

	public void setRowIndex(Object rowIndex)
	{
		this.rowIndex = rowIndex;
	}

	public Boolean getShortVast()
	{
		return shortVast;
	}

	public void setShortVast(Boolean shortVast)
	{
		this.shortVast = shortVast;
	}

	public Object getBidAdjustment()
	{
		return bidAdjustment;
	}

	public void setBidAdjustment(Object bidAdjustment)
	{
		this.bidAdjustment = bidAdjustment;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public FlightRequestBusinessModel getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(FlightRequestBusinessModel businessModel)
	{
		this.businessModel = businessModel;
	}

	public List<FlightRequestAsset> getFlightAssets()
	{
		return flightAssets;
	}

	public void setFlightAssets(List<FlightRequestAsset> flightAssets)
	{
		this.flightAssets = flightAssets;
	}

	public List<Object> getEvents()
	{
		return events;
	}

	public void setEvents(List<Object> events)
	{
		this.events = events;
	}

	public List<FlightRequestAdsAttribute> getAdsAttributes()
	{
		return adsAttributes;
	}

	public void setAdsAttributes(List<FlightRequestAdsAttribute> adsAttributes)
	{
		this.adsAttributes = adsAttributes;
	}

}
