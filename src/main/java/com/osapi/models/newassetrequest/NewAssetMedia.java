package com.osapi.models.newassetrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewAssetMedia
{
	@SerializedName("isValid")
	@Expose
	private Boolean isValid;
	@SerializedName("errors")
	@Expose
	private Boolean errors;
	@SerializedName("file_type")
	@Expose
	private String fileType;
	@SerializedName("duration")
	@Expose
	private Integer duration;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("bitrate")
	@Expose
	private Integer bitrate;
	@SerializedName("uploadPerc")
	@Expose
	private Integer uploadPerc;
	@SerializedName("success")
	@Expose
	private String success;
	@SerializedName("file_key")
	@Expose
	private String fileKey;

	public Boolean getIsValid() {
	return isValid;
	}

	public void setIsValid(Boolean isValid) {
	this.isValid = isValid;
	}

	public Boolean getErrors() {
	return errors;
	}

	public void setErrors(Boolean errors) {
	this.errors = errors;
	}

	public String getFileType() {
	return fileType;
	}

	public void setFileType(String fileType) {
	this.fileType = fileType;
	}

	public Integer getDuration() {
	return duration;
	}

	public void setDuration(Integer duration) {
	this.duration = duration;
	}

	public Integer getHeight() {
	return height;
	}

	public void setHeight(Integer height) {
	this.height = height;
	}

	public Integer getWidth() {
	return width;
	}

	public void setWidth(Integer width) {
	this.width = width;
	}

	public Integer getBitrate() {
	return bitrate;
	}

	public void setBitrate(Integer bitrate) {
	this.bitrate = bitrate;
	}

	public Integer getUploadPerc() {
	return uploadPerc;
	}

	public void setUploadPerc(Integer uploadPerc) {
	this.uploadPerc = uploadPerc;
	}

	public String getSuccess() {
	return success;
	}

	public void setSuccess(String success) {
	this.success = success;
	}

	public String getFileKey() {
	return fileKey;
	}

	public void setFileKey(String fileKey) {
	this.fileKey = fileKey;
	}
}
