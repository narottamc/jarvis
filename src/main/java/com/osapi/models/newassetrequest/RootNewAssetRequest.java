package com.osapi.models.newassetrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RootNewAssetRequest
{
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("asset_id")
	@Expose
	private Integer assetId;
	@SerializedName("asset")
	@Expose
	private Asset asset;

	public Integer getCampaignId() {
	return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
	}

	public Integer getAssetId() {
	return assetId;
	}

	public void setAssetId(Integer assetId) {
	this.assetId = assetId;
	}

	public Asset getAsset() {
	return asset;
	}

	public void setAsset(Asset asset) {
	this.asset = asset;
	}
}
