/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newactivitypixelrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class RootNewActivityPixelRequest {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("agency_id")
    @Expose
    private Integer agencyId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("pixel_type_id")
    @Expose
    private Integer pixelTypeId;
    @SerializedName("agency")
    @Expose
    private Agency agency;
    @SerializedName("brand")
    @Expose
    private Brand brand;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getPixelTypeId() {
        return pixelTypeId;
    }

    public void setPixelTypeId(Integer pixelTypeId) {
        this.pixelTypeId = pixelTypeId;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}
