/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.placementrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream
 */
public class Placement
{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("placement_group_id")
	@Expose
	private Integer placementGroupId;
	@SerializedName("state_id")
	@Expose
	private Integer stateId;
	@SerializedName("frequency_cap")
	@Expose
	private String frequencyCap;
	@SerializedName("frequency_count")
	@Expose
	private Object frequencyCount;
	@SerializedName("duration")
	@Expose
	private Integer duration;
	@SerializedName("timezone")
	@Expose
	private Integer timezone;
	@SerializedName("salesforce_id")
	@Expose
	private Object salesforceId;
	@SerializedName("total_placements_budget")
	@Expose
	private Integer totalPlacementsBudget;
	@SerializedName("product_type")
	@Expose
	private Integer productType;
	@SerializedName("whitelist_deal")
	@Expose
	private Object whitelistDeal;
	@SerializedName("business_model")
	@Expose
	private BusinessModelPlacementRequest businessModel;
	@SerializedName("ssps")
	@Expose
	private List<SSPPlacementRequest> ssps = new ArrayList<SSPPlacementRequest>();
	@SerializedName("placement_marketplace_deal")
	@Expose
	private Object placementMarketplaceDeal;
	@SerializedName("assets")
	@Expose
	private List<Integer> assets = new ArrayList<Integer>();
	@SerializedName("events")
	@Expose
	private List<EventPlacementRequest> events = new ArrayList<EventPlacementRequest>();
	@SerializedName("visible")
	@Expose
	private Boolean visible;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public Integer getPlacementGroupId()
	{
		return placementGroupId;
	}

	public void setPlacementGroupId(Integer placementGroupId)
	{
		this.placementGroupId = placementGroupId;
	}

	public Integer getStateId()
	{
		return stateId;
	}

	public void setStateId(Integer stateId)
	{
		this.stateId = stateId;
	}

	public String getFrequencyCap()
	{
		return frequencyCap;
	}

	public void setFrequencyCap(String frequencyCap)
	{
		this.frequencyCap = frequencyCap;
	}

	public Object getFrequencyCount()
	{
		return frequencyCount;
	}

	public void setFrequencyCount(Object frequencyCount)
	{
		this.frequencyCount = frequencyCount;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	public Integer getTimezone()
	{
		return timezone;
	}

	public void setTimezone(Integer timezone)
	{
		this.timezone = timezone;
	}

	public Object getSalesforceId()
	{
		return salesforceId;
	}

	public void setSalesforceId(Object salesforceId)
	{
		this.salesforceId = salesforceId;
	}

	public Integer getTotalPlacementsBudget()
	{
		return totalPlacementsBudget;
	}

	public void setTotalPlacementsBudget(Integer totalPlacementsBudget)
	{
		this.totalPlacementsBudget = totalPlacementsBudget;
	}

	public Integer getProductType()
	{
		return productType;
	}

	public void setProductType(Integer productType)
	{
		this.productType = productType;
	}

	public Object getWhitelistDeal()
	{
		return whitelistDeal;
	}

	public void setWhitelistDeal(Object whitelistDeal)
	{
		this.whitelistDeal = whitelistDeal;
	}

	public BusinessModelPlacementRequest getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(BusinessModelPlacementRequest businessModel)
	{
		this.businessModel = businessModel;
	}

	public List<SSPPlacementRequest> getSsps()
	{
		return ssps;
	}

	public void setSsps(List<SSPPlacementRequest> ssps)
	{
		this.ssps = ssps;
	}

	public Object getPlacementMarketplaceDeal()
	{
		return placementMarketplaceDeal;
	}

	public void setPlacementMarketplaceDeal(Object placementMarketplaceDeal)
	{
		this.placementMarketplaceDeal = placementMarketplaceDeal;
	}

	public List<Integer> getAssets()
	{
		return assets;
	}

	public void setAssets(List<Integer> assets)
	{
		this.assets = assets;
	}

	public List<EventPlacementRequest> getEvents()
	{
		return events;
	}

	public void setEvents(List<EventPlacementRequest> events)
	{
		this.events = events;
	}

	public Boolean getVisible()
	{
		return visible;
	}

	public void setVisible(Boolean visible)
	{
		this.visible = visible;
	}

}
