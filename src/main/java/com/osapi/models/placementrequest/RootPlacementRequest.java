/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.placementrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootPlacementRequest
{

	@SerializedName("placement")
	@Expose
	private Placement placement;

	public Placement getPlacement()
	{
		return placement;
	}

	public void setPlacement(Placement placement)
	{
		this.placement = placement;
	}

}
