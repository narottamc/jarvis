/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.launchcampaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootLaunchCampaignRequest
{
	@SerializedName("campaignId")
	@Expose
	private Integer campaignId;
	@SerializedName("validate")
	@Expose
	private String validate;

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public String getValidate()
	{
		return validate;
	}

	public void setValidate(String validate)
	{
		this.validate = validate;
	}

}
