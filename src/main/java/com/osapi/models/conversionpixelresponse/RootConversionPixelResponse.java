/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.conversionpixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootConversionPixelResponse {
    @SerializedName("conversion_pixels")
    @Expose
    private List<ConversionPixel> conversionPixels = null;

    public List<ConversionPixel> getConversionPixels() {
        return conversionPixels;
    }

    public void setConversionPixels(List<ConversionPixel> conversionPixels) {
        this.conversionPixels = conversionPixels;
    }

}
