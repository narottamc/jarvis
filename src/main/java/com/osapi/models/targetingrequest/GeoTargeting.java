/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class GeoTargeting
{
	@SerializedName("geos")
	@Expose
	private List<Geo> geos = null;
	@SerializedName("geo_lists")
	@Expose
	private List<GeoList> geoLists = null;
	@SerializedName("languages")
	@Expose
	private List<Language> languages = null;
	@SerializedName("dma_anti_targeting")
	@Expose
	private Boolean dmaAntiTargeting;
	@SerializedName("zip_anti_targeting")
	@Expose
	private Boolean zipAntiTargeting;
	@SerializedName("pass_empty_language")
	@Expose
	private Boolean passEmptyLanguage;

	public List<Geo> getGeos()
	{
		return geos;
	}

	public void setGeos(List<Geo> geos)
	{
		this.geos = geos;
	}

	public List<GeoList> getGeoLists()
	{
		return geoLists;
	}

	public void setGeoLists(List<GeoList> geoLists)
	{
		this.geoLists = geoLists;
	}

	public List<Language> getLanguages()
	{
		return languages;
	}

	public void setLanguages(List<Language> languages)
	{
		this.languages = languages;
	}

	public Boolean getDmaAntiTargeting()
	{
		return dmaAntiTargeting;
	}

	public void setDmaAntiTargeting(Boolean dmaAntiTargeting)
	{
		this.dmaAntiTargeting = dmaAntiTargeting;
	}

	public Boolean getZipAntiTargeting()
	{
		return zipAntiTargeting;
	}

	public void setZipAntiTargeting(Boolean zipAntiTargeting)
	{
		this.zipAntiTargeting = zipAntiTargeting;
	}

	public Boolean getPassEmptyLanguage()
	{
		return passEmptyLanguage;
	}

	public void setPassEmptyLanguage(Boolean passEmptyLanguage)
	{
		this.passEmptyLanguage = passEmptyLanguage;
	}

}
