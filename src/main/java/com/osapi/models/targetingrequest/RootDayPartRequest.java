/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootDayPartRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("day_parts")
	@Expose
	private List<DayPartRequest> dayParts = null;
	@SerializedName("timezone")
	@Expose
	private Integer timezone;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public List<DayPartRequest> getDayParts()
	{
		return dayParts;
	}

	public void setDayParts(List<DayPartRequest> dayParts)
	{
		this.dayParts = dayParts;
	}

	public Integer getTimezone()
	{
		return timezone;
	}

	public void setTimezone(Integer timezone)
	{
		this.timezone = timezone;
	}

}
