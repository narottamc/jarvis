/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootTechnologyRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("technologies")
	@Expose
	private List<TechnologyRequest> technologies = null;
	@SerializedName("inventory_type_id")
	@Expose
	private String inventoryTypeId;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public List<TechnologyRequest> getTechnologies()
	{
		return technologies;
	}

	public void setTechnologies(List<TechnologyRequest> technologies)
	{
		this.technologies = technologies;
	}

	public String getInventoryTypeId()
	{
		return inventoryTypeId;
	}

	public void setInventoryTypeId(String inventoryTypeId)
	{
		this.inventoryTypeId = inventoryTypeId;
	}

}
