/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class TechnologyRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("technology_type")
	@Expose
	private Integer technologyType;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("raw_browsers")
	@Expose
	private Object rawBrowsers;
	@SerializedName("raw_browser_versions")
	@Expose
	private Object rawBrowserVersions;
	@SerializedName("raw_operating_systems")
	@Expose
	private Object rawOperatingSystems;
	@SerializedName("raw_operating_system_versions")
	@Expose
	private Object rawOperatingSystemVersions;
	@SerializedName("raw_device_makers")
	@Expose
	private Object rawDeviceMakers;
	@SerializedName("raw_device_models")
	@Expose
	private Object rawDeviceModels;
	@SerializedName("raw_device_types")
	@Expose
	private List<String> rawDeviceTypes = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getTechnologyType()
	{
		return technologyType;
	}

	public void setTechnologyType(Integer technologyType)
	{
		this.technologyType = technologyType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Object getRawBrowsers()
	{
		return rawBrowsers;
	}

	public void setRawBrowsers(Object rawBrowsers)
	{
		this.rawBrowsers = rawBrowsers;
	}

	public Object getRawBrowserVersions()
	{
		return rawBrowserVersions;
	}

	public void setRawBrowserVersions(Object rawBrowserVersions)
	{
		this.rawBrowserVersions = rawBrowserVersions;
	}

	public Object getRawOperatingSystems()
	{
		return rawOperatingSystems;
	}

	public void setRawOperatingSystems(Object rawOperatingSystems)
	{
		this.rawOperatingSystems = rawOperatingSystems;
	}

	public Object getRawOperatingSystemVersions()
	{
		return rawOperatingSystemVersions;
	}

	public void setRawOperatingSystemVersions(Object rawOperatingSystemVersions)
	{
		this.rawOperatingSystemVersions = rawOperatingSystemVersions;
	}

	public Object getRawDeviceMakers()
	{
		return rawDeviceMakers;
	}

	public void setRawDeviceMakers(Object rawDeviceMakers)
	{
		this.rawDeviceMakers = rawDeviceMakers;
	}

	public Object getRawDeviceModels()
	{
		return rawDeviceModels;
	}

	public void setRawDeviceModels(Object rawDeviceModels)
	{
		this.rawDeviceModels = rawDeviceModels;
	}

	public List<String> getRawDeviceTypes()
	{
		return rawDeviceTypes;
	}

	public void setRawDeviceTypes(List<String> rawDeviceTypes)
	{
		this.rawDeviceTypes = rawDeviceTypes;
	}

}
