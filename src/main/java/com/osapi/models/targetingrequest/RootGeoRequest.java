/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootGeoRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("geos_targeting")
	@Expose
	private GeoTargeting geosTargeting;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public GeoTargeting getGeosTargeting()
	{
		return geosTargeting;
	}

	public void setGeosTargeting(GeoTargeting geosTargeting)
	{
		this.geosTargeting = geosTargeting;
	}

}
