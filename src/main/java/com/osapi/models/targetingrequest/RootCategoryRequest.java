/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootCategoryRequest
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("target_categories")
	@Expose
	private List<TargetCategoryRequest> targetCategories = null;
	@SerializedName("hit_on_unknown_results")
	@Expose
	private Boolean hitOnUnknownResults;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public List<TargetCategoryRequest> getTargetCategories()
	{
		return targetCategories;
	}

	public void setTargetCategories(List<TargetCategoryRequest> targetCategories)
	{
		this.targetCategories = targetCategories;
	}

	public Boolean getHitOnUnknownResults()
	{
		return hitOnUnknownResults;
	}

	public void setHitOnUnknownResults(Boolean hitOnUnknownResults)
	{
		this.hitOnUnknownResults = hitOnUnknownResults;
	}

}
