/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class Geo
{
	@SerializedName("country_id")
	@Expose
	private Integer countryId;
	@SerializedName("region_id")
	@Expose
	private Integer regionId;
	@SerializedName("dma_id")
	@Expose
	private Integer dmaId;

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getDmaId() {
		return dmaId;
	}

	public void setDmaId(Integer dmaId) {
		this.dmaId = dmaId;
	}


}
