/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.displayrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class DisplayEvent
{
	@SerializedName("event_type")
	@Expose
	private String eventType;
	@SerializedName("tracking_url")
	@Expose
	private String trackingUrl;

	public String getEventType() {
	return eventType;
	}

	public void setEventType(String eventType) {
	this.eventType = eventType;
	}

	public String getTrackingUrl() {
	return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
	this.trackingUrl = trackingUrl;
	}
}
