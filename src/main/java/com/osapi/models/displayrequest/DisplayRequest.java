/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.displayrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class DisplayRequest
{

	@SerializedName("ad_content")
	@Expose
	private String adContent;
	@SerializedName("ad_content_src")
	@Expose
	private String adContentSrc;
	@SerializedName("display_type_id")
	@Expose
	private Integer displayTypeId;
	@SerializedName("landing_page_domain")
	@Expose
	private String landingPageDomain;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("file_key")
	@Expose
	private Object fileKey;
	@SerializedName("file_type")
	@Expose
	private Object fileType;
	@SerializedName("ad_tag_type_id")
	@Expose
	private Integer adTagTypeId;
	@SerializedName("display_events")
	@Expose
	private List<DisplayEvent> displayEvents = null;
	@SerializedName("width")
	@Expose
	private String width;
	@SerializedName("height")
	@Expose
	private String height;
	@SerializedName("script_type_id")
	@Expose
	private Integer scriptTypeId;

	
	public Integer getScriptTypeId()
	{
		return scriptTypeId;
	}

	public void setScriptTypeId(Integer scriptTypeId)
	{
		this.scriptTypeId = scriptTypeId;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
	}

	public String getAdContent()
	{
		return adContent;
	}

	public void setAdContent(String adContent)
	{
		this.adContent = adContent;
	}

	public String getAdContentSrc() {
		return adContentSrc;
	}

	public void setAdContentSrc(String adContentSrc) {
		this.adContentSrc = adContentSrc;
	}

	public Integer getDisplayTypeId()
	{
		return displayTypeId;
	}

	public void setDisplayTypeId(Integer displayTypeId)
	{
		this.displayTypeId = displayTypeId;
	}

	public String getLandingPageDomain()
	{
		return landingPageDomain;
	}

	public void setLandingPageDomain(String landingPageDomain)
	{
		this.landingPageDomain = landingPageDomain;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Object getFileKey()
	{
		return fileKey;
	}

	public void setFileKey(Object fileKey)
	{
		this.fileKey = fileKey;
	}

	public Object getFileType()
	{
		return fileType;
	}

	public void setFileType(Object fileType)
	{
		this.fileType = fileType;
	}

	public Integer getAdTagTypeId()
	{
		return adTagTypeId;
	}

	public void setAdTagTypeId(Integer adTagTypeId)
	{
		this.adTagTypeId = adTagTypeId;
	}

	public List<DisplayEvent> getDisplayEvents()
	{
		return displayEvents;
	}

	public void setDisplayEvents(List<DisplayEvent> displayEvents)
	{
		this.displayEvents = displayEvents;
	}

}
