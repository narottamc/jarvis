/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.displayrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class RootDisplayRequest
{
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("display")
	@Expose
	private DisplayRequest display;

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public DisplayRequest getDisplay()
	{
		return display;
	}

	public void setDisplay(DisplayRequest display)
	{
		this.display = display;
	}

}
