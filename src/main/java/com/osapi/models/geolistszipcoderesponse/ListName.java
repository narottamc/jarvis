/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.geolistszipcoderesponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class ListName {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("zip_codes_count")
    @Expose
    private Integer zipCodesCount;
    @SerializedName("zip_codes")
    @Expose
    private List<ZipCode> zipCodes = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getZipCodesCount() {
        return zipCodesCount;
    }

    public void setZipCodesCount(Integer zipCodesCount) {
        this.zipCodesCount = zipCodesCount;
    }

    public List<ZipCode> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(List<ZipCode> zipCodes) {
        this.zipCodes = zipCodes;
    }
}
