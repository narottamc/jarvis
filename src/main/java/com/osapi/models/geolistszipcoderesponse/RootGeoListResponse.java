/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.geolistszipcoderesponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootGeoListResponse {
    @SerializedName("geoListsZipCodes")
    @Expose
    private List<GeoListsZipCode> geoListsZipCodes = null;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("listName")
    @Expose
    private ListName listName;

    public List<GeoListsZipCode> getGeoListsZipCodes() {
        return geoListsZipCodes;
    }

    public void setGeoListsZipCodes(List<GeoListsZipCode> geoListsZipCodes) {
        this.geoListsZipCodes = geoListsZipCodes;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ListName getListName() {
        return listName;
    }

    public void setListName(ListName listName) {
        this.listName = listName;
    }
}
