/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newactivitypixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class NewRootActivityPixelResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("post_view_hours")
    @Expose
    private Integer postViewHours;
    @SerializedName("post_click_hours")
    @Expose
    private Integer postClickHours;
    @SerializedName("expiration_hours")
    @Expose
    private Integer expirationHours;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("agency_id")
    @Expose
    private Integer agencyId;
    @SerializedName("sync_enabled")
    @Expose
    private Boolean syncEnabled;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("pixel_type_id")
    @Expose
    private Integer pixelTypeId;
    @SerializedName("url")
    @Expose
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPostViewHours() {
        return postViewHours;
    }

    public void setPostViewHours(Integer postViewHours) {
        this.postViewHours = postViewHours;
    }

    public Integer getPostClickHours() {
        return postClickHours;
    }

    public void setPostClickHours(Integer postClickHours) {
        this.postClickHours = postClickHours;
    }

    public Integer getExpirationHours() {
        return expirationHours;
    }

    public void setExpirationHours(Integer expirationHours) {
        this.expirationHours = expirationHours;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public Boolean getSyncEnabled() {
        return syncEnabled;
    }

    public void setSyncEnabled(Boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getPixelTypeId() {
        return pixelTypeId;
    }

    public void setPixelTypeId(Integer pixelTypeId) {
        this.pixelTypeId = pixelTypeId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
