/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.fraudsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 *
 */
public class RootFraudsResponse
{
	@SerializedName("enable_double_verify")
	@Expose
	private Boolean enableDoubleVerify;
	@SerializedName("enable_dv_fraud")
	@Expose
	private Boolean enableDvFraud;
	@SerializedName("use_ias")
	@Expose
	private Boolean useIas;
	@SerializedName("dv_ppd_timeout")
	@Expose
	private Boolean dvPpdTimeout;
	@SerializedName("ias_timeout")
	@Expose
	private Boolean iasTimeout;
	@SerializedName("ias_credentials")
	@Expose
	private List<IASCrendentialsFraudsResponse> iasCredentials = null;
	@SerializedName("use_iab")
	@Expose
	private Boolean useIab;

	public Boolean getEnableDoubleVerify() {
	return enableDoubleVerify;
	}

	public void setEnableDoubleVerify(Boolean enableDoubleVerify) {
	this.enableDoubleVerify = enableDoubleVerify;
	}

	public Boolean getEnableDvFraud() {
	return enableDvFraud;
	}

	public void setEnableDvFraud(Boolean enableDvFraud) {
	this.enableDvFraud = enableDvFraud;
	}

	public Boolean getUseIas() {
	return useIas;
	}

	public void setUseIas(Boolean useIas) {
	this.useIas = useIas;
	}

	public Boolean getDvPpdTimeout() {
	return dvPpdTimeout;
	}

	public void setDvPpdTimeout(Boolean dvPpdTimeout) {
	this.dvPpdTimeout = dvPpdTimeout;
	}

	public Boolean getIasTimeout() {
	return iasTimeout;
	}

	public void setIasTimeout(Boolean iasTimeout) {
	this.iasTimeout = iasTimeout;
	}

	public List<IASCrendentialsFraudsResponse> getIasCredentials() {
	return iasCredentials;
	}

	public void setIasCredentials(List<IASCrendentialsFraudsResponse> iasCredentials) {
	this.iasCredentials = iasCredentials;
	}

	public Boolean getUseIab() {
	return useIab;
	}

	public void setUseIab(Boolean useIab) {
	this.useIab = useIab;
	}
}
