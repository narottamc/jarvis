/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.dealsandseatsrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootDealsAndSeatsRequest {
    @SerializedName("deals")
    @Expose
    private List<Integer> deals = null;
    @SerializedName("seats")
    @Expose
    private List<Integer> seats = null;

    public List<Integer> getDeals() {
        return deals;
    }

    public void setDeals(List<Integer> deals) {
        this.deals = deals;
    }

    public List<Integer> getSeats() {
        return seats;
    }

    public void setSeats(List<Integer> seats) {
        this.seats = seats;
    }
}
