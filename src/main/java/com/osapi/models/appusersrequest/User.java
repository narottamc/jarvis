/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.appusersrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class User
{
	@SerializedName("is_finance")
	@Expose
	private Integer isFinance;
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("agency_id")
	@Expose
	private Integer agencyId;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("password_confirmation")
	@Expose
	private String passwordConfirmation;

	public Integer getIsFinance() {
	return isFinance;
	}

	public void setIsFinance(Integer isFinance) {
	this.isFinance = isFinance;
	}

	public String getFirstName() {
	return firstName;
	}

	public void setFirstName(String firstName) {
	this.firstName = firstName;
	}

	public String getLastName() {
	return lastName;
	}

	public void setLastName(String lastName) {
	this.lastName = lastName;
	}

	public String getEmail() {
	return email;
	}

	public void setEmail(String email) {
	this.email = email;
	}

	public Integer getAgencyId() {
	return agencyId;
	}

	public void setAgencyId(Integer agencyId) {
	this.agencyId = agencyId;
	}

	public String getPassword() {
	return password;
	}

	public void setPassword(String password) {
	this.password = password;
	}

	public String getPasswordConfirmation() {
	return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
	this.passwordConfirmation = passwordConfirmation;
	}
}
