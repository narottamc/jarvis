/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.appusersrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class RootCreateUserRequest
{
	@SerializedName("user")
	@Expose
	private User user;

	public User getUser() {
	return user;
	}

	public void setUser(User user) {
	this.user = user;
	}

}
