/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootSurveyResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("third_party_ad_tag")
	@Expose
	private String thirdPartyAdTag;
	@SerializedName("survey_id")
	@Expose
	private Integer surveyId;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("is_flight_associated")
	@Expose
	private Boolean isFlightAssociated;
	@SerializedName("questions")
	@Expose
	private List<QuestionSurveyResponse> questions = null;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getThirdPartyAdTag() {
	return thirdPartyAdTag;
	}

	public void setThirdPartyAdTag(String thirdPartyAdTag) {
	this.thirdPartyAdTag = thirdPartyAdTag;
	}

	public Integer getSurveyId() {
	return surveyId;
	}

	public void setSurveyId(Integer surveyId) {
	this.surveyId = surveyId;
	}

	public Integer getWidth() {
	return width;
	}

	public void setWidth(Integer width) {
	this.width = width;
	}

	public Integer getHeight() {
	return height;
	}

	public void setHeight(Integer height) {
	this.height = height;
	}

	public Boolean getIsFlightAssociated() {
	return isFlightAssociated;
	}

	public void setIsFlightAssociated(Boolean isFlightAssociated) {
	this.isFlightAssociated = isFlightAssociated;
	}

	public List<QuestionSurveyResponse> getQuestions() {
	return questions;
	}

	public void setQuestions(List<QuestionSurveyResponse> questions) {
	this.questions = questions;
	}
}
