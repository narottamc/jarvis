/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class QuestionSurveyResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("question")
	@Expose
	private String question;
	@SerializedName("response")
	@Expose
	private String response;
	@SerializedName("question_type")
	@Expose
	private Integer questionType;
	@SerializedName("answers")
	@Expose
	private List<AnswerSurveyResponse> answers = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getQuestion()
	{
		return question;
	}

	public void setQuestion(String question)
	{
		this.question = question;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

	public Integer getQuestionType()
	{
		return questionType;
	}

	public void setQuestionType(Integer questionType)
	{
		this.questionType = questionType;
	}

	public List<AnswerSurveyResponse> getAnswers()
	{
		return answers;
	}

	public void setAnswers(List<AnswerSurveyResponse> answers)
	{
		this.answers = answers;
	}

}
