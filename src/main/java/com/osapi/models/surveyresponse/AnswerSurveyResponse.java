/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class AnswerSurveyResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("answer")
	@Expose
	private String answer;
	@SerializedName("pixel")
	@Expose
	private String pixel;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getAnswer()
	{
		return answer;
	}

	public void setAnswer(String answer)
	{
		this.answer = answer;
	}

	public String getPixel()
	{
		return pixel;
	}

	public void setPixel(String pixel)
	{
		this.pixel = pixel;
	}

}
