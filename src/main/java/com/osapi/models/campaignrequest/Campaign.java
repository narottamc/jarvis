/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.osapi.models.campaignresponse.Agency;

import java.util.List;

/**
 * @author Clearstream.
 *
 */
public class Campaign
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private Object name;
	@SerializedName("start_date")
	@Expose
	private Object startDate;
	@SerializedName("end_date")
	@Expose
	private Object endDate;
	@SerializedName("margin_estimate")
	@Expose
	private Object marginEstimate;
	@SerializedName("insertion_order_number")
	@Expose
	private Object insertionOrderNumber;
	@SerializedName("state_id")
	@Expose
	private Integer stateId;
	@SerializedName("is_favorite")
	@Expose
	private Boolean isFavorite;
	@SerializedName("campaign_marketplace_ssp_dsp")
	@Expose
	private Object campaignMarketplaceSspDsp;
	@SerializedName("total_placements_budget")
	@Expose
	private Integer totalPlacementsBudget;
	@SerializedName("categories")
	@Expose
	private List<Object> categories = null;
	@SerializedName("placements_type")
	@Expose
	private List<Object> placementsType = null;
	@SerializedName("agency")
	@Expose
	private Agency agency;
	@SerializedName("business_model")
	@Expose
	private BusinessModelCampaignRequest businessModel;
	@SerializedName("campaign_tags")
	@Expose
	private List<Object> campaignTags = null;
	@SerializedName("campaign_type")
	@Expose
	private CampaignType campaignType;
	@SerializedName("placement_groups")
	@Expose
	private List<PlacementGroup> placementGroups = null;
	@SerializedName("agency_id")
	@Expose
	private Object agencyId;
	@SerializedName("brand_id")
	@Expose
	private Object brandId;
	@SerializedName("campaign_type_id")
	@Expose
	private Integer campaignTypeId;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public Object getName() {
	return name;
	}

	public void setName(Object name) {
	this.name = name;
	}

	public Object getStartDate() {
	return startDate;
	}

	public void setStartDate(Object startDate) {
	this.startDate = startDate;
	}

	public Object getEndDate() {
	return endDate;
	}

	public void setEndDate(Object endDate) {
	this.endDate = endDate;
	}

	public Object getMarginEstimate() {
	return marginEstimate;
	}

	public void setMarginEstimate(Object marginEstimate) {
	this.marginEstimate = marginEstimate;
	}

	public Object getInsertionOrderNumber() {
	return insertionOrderNumber;
	}

	public void setInsertionOrderNumber(Object insertionOrderNumber) {
	this.insertionOrderNumber = insertionOrderNumber;
	}

	public Integer getStateId() {
	return stateId;
	}

	public void setStateId(Integer stateId) {
	this.stateId = stateId;
	}

	public Boolean getIsFavorite() {
	return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
	this.isFavorite = isFavorite;
	}

	public Object getCampaignMarketplaceSspDsp() {
	return campaignMarketplaceSspDsp;
	}

	public void setCampaignMarketplaceSspDsp(Object campaignMarketplaceSspDsp) {
	this.campaignMarketplaceSspDsp = campaignMarketplaceSspDsp;
	}

	public Integer getTotalPlacementsBudget() {
	return totalPlacementsBudget;
	}

	public void setTotalPlacementsBudget(Integer totalPlacementsBudget) {
	this.totalPlacementsBudget = totalPlacementsBudget;
	}

	public List<Object> getCategories() {
	return categories;
	}

	public void setCategories(List<Object> categories) {
	this.categories = categories;
	}

	public List<Object> getPlacementsType() {
	return placementsType;
	}

	public void setPlacementsType(List<Object> placementsType) {
	this.placementsType = placementsType;
	}

	public Agency getAgency() {
	return agency;
	}

	public void setAgency(Agency agency) {
	this.agency = agency;
	}

	public BusinessModelCampaignRequest getBusinessModel() {
	return businessModel;
	}

	public void setBusinessModel(BusinessModelCampaignRequest businessModel) {
	this.businessModel = businessModel;
	}

	public List<Object> getCampaignTags() {
	return campaignTags;
	}

	public void setCampaignTags(List<Object> campaignTags) {
	this.campaignTags = campaignTags;
	}

	public CampaignType getCampaignType() {
	return campaignType;
	}

	public void setCampaignType(CampaignType campaignType) {
	this.campaignType = campaignType;
	}

	public List<PlacementGroup> getPlacementGroups() {
	return placementGroups;
	}

	public void setPlacementGroups(List<PlacementGroup> placementGroups) {
	this.placementGroups = placementGroups;
	}

	public Object getAgencyId() {
	return agencyId;
	}

	public void setAgencyId(Object agencyId) {
	this.agencyId = agencyId;
	}

	public Object getBrandId() {
	return brandId;
	}

	public void setBrandId(Object brandId) {
	this.brandId = brandId;
	}

	public Integer getCampaignTypeId() {
	return campaignTypeId;
	}

	public void setCampaignTypeId(Integer campaignTypeId) {
	this.campaignTypeId = campaignTypeId;
	}
}
