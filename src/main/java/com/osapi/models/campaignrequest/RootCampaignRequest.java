/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class RootCampaignRequest
{
	@SerializedName("campaign")
	@Expose
	private Campaign campaign;

	public Campaign getCampaign() {
	return campaign;
	}

	public void setCampaign(Campaign campaign) {
	this.campaign = campaign;
	}
}
