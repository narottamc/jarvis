/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.appusersreponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class CurrentUserResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("agency_id")
	@Expose
	private Object agencyId;
	@SerializedName("is_finance")
	@Expose
	private Boolean isFinance;
	@SerializedName("provider")
	@Expose
	private String provider;
	@SerializedName("uid")
	@Expose
	private String uid;
	@SerializedName("is_read_only")
	@Expose
	private Boolean isReadOnly;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public String getEmail() {
	return email;
	}

	public void setEmail(String email) {
	this.email = email;
	}

	public String getCreatedAt() {
	return createdAt;
	}

	public void setCreatedAt(String createdAt) {
	this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
	return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
	this.updatedAt = updatedAt;
	}

	public String getFirstName() {
	return firstName;
	}

	public void setFirstName(String firstName) {
	this.firstName = firstName;
	}

	public String getLastName() {
	return lastName;
	}

	public void setLastName(String lastName) {
	this.lastName = lastName;
	}

	public Object getAgencyId() {
	return agencyId;
	}

	public void setAgencyId(Object agencyId) {
	this.agencyId = agencyId;
	}

	public Boolean getIsFinance() {
	return isFinance;
	}

	public void setIsFinance(Boolean isFinance) {
	this.isFinance = isFinance;
	}

	public String getProvider() {
	return provider;
	}

	public void setProvider(String provider) {
	this.provider = provider;
	}

	public String getUid() {
	return uid;
	}

	public void setUid(String uid) {
	this.uid = uid;
	}

	public Boolean getIsReadOnly() {
	return isReadOnly;
	}

	public void setIsReadOnly(Boolean isReadOnly) {
	this.isReadOnly = isReadOnly;
	}
}
