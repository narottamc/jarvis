/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.appusersreponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 *
 */
public class RootUsersResponse
{
	@SerializedName("current_user")
	@Expose
	private CurrentUserResponse currentUser;
	@SerializedName("users")
	@Expose
	private List<UserResponse> users = null;

	public CurrentUserResponse getCurrentUser() {
	return currentUser;
	}

	public void setCurrentUser(CurrentUserResponse currentUser) {
	this.currentUser = currentUser;
	}

	public List<UserResponse> getUsers() {
	return users;
	}

	public void setUsers(List<UserResponse> users) {
	this.users = users;
	}
}
