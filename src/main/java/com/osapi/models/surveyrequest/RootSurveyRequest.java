/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class RootSurveyRequest
{
	@SerializedName("campaign_id")
	@Expose
	private String campaignId;
	@SerializedName("survey")
	@Expose
	private SurveyRequest survey;

	public String getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(String campaignId)
	{
		this.campaignId = campaignId;
	}

	public SurveyRequest getSurvey()
	{
		return survey;
	}

	public void setSurvey(SurveyRequest survey)
	{
		this.survey = survey;
	}

}
