/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class QuestionSurveyRequest
{
	@SerializedName("question")
	@Expose
	private String question;
	@SerializedName("answers")
	@Expose
	private List<String> answers = null;
	@SerializedName("question_type")
	@Expose
	private Integer questionType;
	@SerializedName("response")
	@Expose
	private String response;

	public String getQuestion()
	{
		return question;
	}

	public void setQuestion(String question)
	{
		this.question = question;
	}

	public List<String> getAnswers()
	{
		return answers;
	}

	public void setAnswers(List<String> answers)
	{
		this.answers = answers;
	}

	public Integer getQuestionType()
	{
		return questionType;
	}

	public void setQuestionType(Integer questionType)
	{
		this.questionType = questionType;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

}
