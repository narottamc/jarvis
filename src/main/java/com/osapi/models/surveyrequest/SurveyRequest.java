/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.surveyrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class SurveyRequest
{
	
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("width")
	@Expose
	private String width;
	@SerializedName("height")
	@Expose
	private String height;
	@SerializedName("questions")
	@Expose
	private List<QuestionSurveyRequest> questions = null;

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getWidth() {
	return width;
	}

	public void setWidth(String width) {
	this.width = width;
	}

	public String getHeight() {
	return height;
	}

	public void setHeight(String height) {
	this.height = height;
	}

	public List<QuestionSurveyRequest> getQuestions() {
	return questions;
	}

	public void setQuestions(List<QuestionSurveyRequest> questions) {
	this.questions = questions;
	}


}
