/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.assetresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream
 */
public class RootAssetResponse
{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("ad_tag_type_id")
	@Expose
	private Integer adTagTypeId;
	@SerializedName("bundle_option")
	@Expose
	private String bundleOption;
	@SerializedName("click_tracking_urls")
	@Expose
	private Object clickTrackingUrls;
	@SerializedName("impression_tracking_urls")
	@Expose
	private Object impressionTrackingUrls;
	@SerializedName("is_linear")
	@Expose
	private Boolean isLinear;
	@SerializedName("is_skippable")
	@Expose
	private Boolean isSkippable;
	@SerializedName("is_v_paid")
	@Expose
	private Boolean isVPaid;
	@SerializedName("landing_page_domain")
	@Expose
	private String landingPageDomain;
	@SerializedName("row_index")
	@Expose
	private Integer rowIndex;
	@SerializedName("third_party_ad_tag")
	@Expose
	private String thirdPartyAdTag;
	@SerializedName("vast_secure_url")
	@Expose
	private Object vastSecureUrl;
	@SerializedName("is_flight_associated")
	@Expose
	private Boolean isFlightAssociated;
	@SerializedName("third_party_url")
	@Expose
	private String thirdPartyUrl;
	@SerializedName("mime_types")
	@Expose
	private List<MimeType> mimeTypes = new ArrayList<MimeType>();
	@SerializedName("asset_media")
	@Expose
	private AssetMedia assetMedia;
	@SerializedName("asset_events")
	@Expose
	private List<AssetEvent> assetEvents = new ArrayList<AssetEvent>();
	@SerializedName("asset_companion")
	@Expose
	private Object assetCompanion;
	@SerializedName("width")
	@Expose
	private String width;
	@SerializedName("height")
	@Expose
	private String height;

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public Integer getAdTagTypeId()
	{
		return adTagTypeId;
	}

	public void setAdTagTypeId(Integer adTagTypeId)
	{
		this.adTagTypeId = adTagTypeId;
	}

	public String getBundleOption()
	{
		return bundleOption;
	}

	public void setBundleOption(String bundleOption)
	{
		this.bundleOption = bundleOption;
	}

	public Object getClickTrackingUrls()
	{
		return clickTrackingUrls;
	}

	public void setClickTrackingUrls(Object clickTrackingUrls)
	{
		this.clickTrackingUrls = clickTrackingUrls;
	}

	public Object getImpressionTrackingUrls()
	{
		return impressionTrackingUrls;
	}

	public void setImpressionTrackingUrls(Object impressionTrackingUrls)
	{
		this.impressionTrackingUrls = impressionTrackingUrls;
	}

	public Boolean getIsLinear()
	{
		return isLinear;
	}

	public void setIsLinear(Boolean isLinear)
	{
		this.isLinear = isLinear;
	}

	public Boolean getIsSkippable()
	{
		return isSkippable;
	}

	public void setIsSkippable(Boolean isSkippable)
	{
		this.isSkippable = isSkippable;
	}

	public Boolean getIsVPaid()
	{
		return isVPaid;
	}

	public void setIsVPaid(Boolean isVPaid)
	{
		this.isVPaid = isVPaid;
	}

	public String getLandingPageDomain()
	{
		return landingPageDomain;
	}

	public void setLandingPageDomain(String landingPageDomain)
	{
		this.landingPageDomain = landingPageDomain;
	}

	public Integer getRowIndex()
	{
		return rowIndex;
	}

	public void setRowIndex(Integer rowIndex)
	{
		this.rowIndex = rowIndex;
	}

	public String getThirdPartyAdTag()
	{
		return thirdPartyAdTag;
	}

	public void setThirdPartyAdTag(String thirdPartyAdTag)
	{
		this.thirdPartyAdTag = thirdPartyAdTag;
	}

	public Object getVastSecureUrl()
	{
		return vastSecureUrl;
	}

	public void setVastSecureUrl(Object vastSecureUrl)
	{
		this.vastSecureUrl = vastSecureUrl;
	}

	public Boolean getIsFlightAssociated()
	{
		return isFlightAssociated;
	}

	public void setIsFlightAssociated(Boolean isFlightAssociated)
	{
		this.isFlightAssociated = isFlightAssociated;
	}

	public String getThirdPartyUrl()
	{
		return thirdPartyUrl;
	}

	public void setThirdPartyUrl(String thirdPartyUrl)
	{
		this.thirdPartyUrl = thirdPartyUrl;
	}

	public List<MimeType> getMimeTypes()
	{
		return mimeTypes;
	}

	public void setMimeTypes(List<MimeType> mimeTypes)
	{
		this.mimeTypes = mimeTypes;
	}

	public AssetMedia getAssetMedia()
	{
		return assetMedia;
	}

	public void setAssetMedia(AssetMedia assetMedia)
	{
		this.assetMedia = assetMedia;
	}

	public List<AssetEvent> getAssetEvents()
	{
		return assetEvents;
	}

	public void setAssetEvents(List<AssetEvent> assetEvents)
	{
		this.assetEvents = assetEvents;
	}

	public Object getAssetCompanion()
	{
		return assetCompanion;
	}

	public void setAssetCompanion(Object assetCompanion)
	{
		this.assetCompanion = assetCompanion;
	}
}
