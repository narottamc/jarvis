/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.assetresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class AssetMedia 
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("s3_url")
	@Expose
	private String s3Url;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("duration")
	@Expose
	private Integer duration;
	@SerializedName("bitrate")
	@Expose
	private Integer bitrate;
	@SerializedName("file_type")
	@Expose
	private String fileType;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public String getS3Url() {
	return s3Url;
	}

	public void setS3Url(String s3Url) {
	this.s3Url = s3Url;
	}

	public Integer getWidth() {
	return width;
	}

	public void setWidth(Integer width) {
	this.width = width;
	}

	public Integer getHeight() {
	return height;
	}

	public void setHeight(Integer height) {
	this.height = height;
	}

	public Integer getDuration() {
	return duration;
	}

	public void setDuration(Integer duration) {
	this.duration = duration;
	}

	public Integer getBitrate() {
	return bitrate;
	}

	public void setBitrate(Integer bitrate) {
	this.bitrate = bitrate;
	}

	public String getFileType() {
	return fileType;
	}

	public void setFileType(String fileType) {
	this.fileType = fileType;
	}

}
