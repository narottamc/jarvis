/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.conversionpixelrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootConversionPixelRequest {
    @SerializedName("placement_id")
    @Expose
    private Integer placementId;
    @SerializedName("placement_pixel_ids")
    @Expose
    private List<Integer> placementPixelIds = null;
    @SerializedName("optimized_pixel_id")
    @Expose
    private Integer optimizedPixelId;

    public Integer getPlacementId() {
        return placementId;
    }

    public void setPlacementId(Integer placementId) {
        this.placementId = placementId;
    }

    public List<Integer> getPlacementPixelIds() {
        return placementPixelIds;
    }

    public void setPlacementPixelIds(List<Integer> placementPixelIds) {
        this.placementPixelIds = placementPixelIds;
    }

    public Integer getOptimizedPixelId() {
        return optimizedPixelId;
    }

    public void setOptimizedPixelId(Integer optimizedPixelId) {
        this.optimizedPixelId = optimizedPixelId;
    }
}
