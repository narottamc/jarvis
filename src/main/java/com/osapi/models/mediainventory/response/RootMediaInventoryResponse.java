/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.mediainventory.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class RootMediaInventoryResponse
{
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("lists")
	@Expose
	private java.util.List<MediaInventoryResponseList> lists = null;

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public java.util.List<MediaInventoryResponseList> getLists()
	{
		return lists;
	}

	public void setLists(java.util.List<MediaInventoryResponseList> lists)
	{
		this.lists = lists;
	}

}
