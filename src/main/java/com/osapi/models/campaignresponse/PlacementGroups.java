/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignresponse;

import gherkin.deps.com.google.gson.annotations.Expose;
import gherkin.deps.com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class PlacementGroups
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("placement_count")
	@Expose
	private Integer placementCount;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public Integer getPlacementCount()
	{
		return placementCount;
	}

	public void setPlacementCount(Integer placementCount)
	{
		this.placementCount = placementCount;
	}

}
