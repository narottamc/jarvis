/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignresponse;

import com.google.gson.FieldNamingPolicy;

import java.lang.reflect.Field;

/**
 * @author Clearstream
 */
public class FieldNamingStrategy implements com.google.gson.FieldNamingStrategy
{

	@Override
	public String translateName(Field field)
	{
		String fieldName;

		fieldName = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field);

		return fieldName;
	}

}
