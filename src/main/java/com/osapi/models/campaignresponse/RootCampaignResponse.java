/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignresponse;

import gherkin.deps.com.google.gson.annotations.Expose;
import gherkin.deps.com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class RootCampaignResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("margin_estimate")
	@Expose
	private Double marginEstimate;
	@SerializedName("insertion_order_number")
	@Expose
	private String insertionOrderNumber;
	@SerializedName("state_id")
	@Expose
	private Integer stateId;
	@SerializedName("is_favorite")
	@Expose
	private Boolean isFavorite;
	@SerializedName("campaign_marketplace_ssp_dsp")
	@Expose
	private Object campaignMarketplaceSspDsp;
	@SerializedName("total_placements_budget")
	@Expose
	private Integer totalPlacementsBudget;
	@SerializedName("categories")
	@Expose
	private List<Category> categories = null;
	@SerializedName("placements_type")
	@Expose
	private List<Object> placementsType = null;
	@SerializedName("agency")
	@Expose
	private Agency agency;
	@SerializedName("brand")
	@Expose
	private Brand brand;
	@SerializedName("business_model")
	@Expose
	private BusinessModel businessModel;
	@SerializedName("campaign_tags")
	@Expose
	private List<Object> campaignTags = null;
	@SerializedName("campaign_type")
	@Expose
	private CampaignType campaignType;
	@SerializedName("placement_groups")
	@Expose
	private List<PlacementGroups> placementGroups = null;
	@SerializedName("placements")
	@Expose
	private List<Placement> placements = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public Double getMarginEstimate()
	{
		return marginEstimate;
	}

	public void setMarginEstimate(Double marginEstimate)
	{
		this.marginEstimate = marginEstimate;
	}

	public String getInsertionOrderNumber()
	{
		return insertionOrderNumber;
	}

	public void setInsertionOrderNumber(String insertionOrderNumber)
	{
		this.insertionOrderNumber = insertionOrderNumber;
	}

	public Integer getStateId()
	{
		return stateId;
	}

	public void setStateId(Integer stateId)
	{
		this.stateId = stateId;
	}

	public Boolean getIsFavorite()
	{
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite)
	{
		this.isFavorite = isFavorite;
	}

	public Object getCampaignMarketplaceSspDsp()
	{
		return campaignMarketplaceSspDsp;
	}

	public void setCampaignMarketplaceSspDsp(Object campaignMarketplaceSspDsp)
	{
		this.campaignMarketplaceSspDsp = campaignMarketplaceSspDsp;
	}

	public Integer getTotalPlacementsBudget()
	{
		return totalPlacementsBudget;
	}

	public void setTotalPlacementsBudget(Integer totalPlacementsBudget)
	{
		this.totalPlacementsBudget = totalPlacementsBudget;
	}

	public List<Category> getCategories()
	{
		return categories;
	}

	public void setCategories(List<Category> categories)
	{
		this.categories = categories;
	}

	public List<Object> getPlacementsType()
	{
		return placementsType;
	}

	public void setPlacementsType(List<Object> placementsType)
	{
		this.placementsType = placementsType;
	}

	public Agency getAgency()
	{
		return agency;
	}

	public void setAgency(Agency agency)
	{
		this.agency = agency;
	}

	public Brand getBrand()
	{
		return brand;
	}

	public void setBrand(Brand brand)
	{
		this.brand = brand;
	}

	public BusinessModel getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(BusinessModel businessModel)
	{
		this.businessModel = businessModel;
	}

	public List<Object> getCampaignTags()
	{
		return campaignTags;
	}

	public void setCampaignTags(List<Object> campaignTags)
	{
		this.campaignTags = campaignTags;
	}

	public CampaignType getCampaignType()
	{
		return campaignType;
	}

	public void setCampaignType(CampaignType campaignType)
	{
		this.campaignType = campaignType;
	}

	public List<PlacementGroups> getPlacementGroups()
	{
		return placementGroups;
	}

	public void setPlacementGroups(List<PlacementGroups> placementGroups)
	{
		this.placementGroups = placementGroups;
	}

	public List<Placement> getPlacements()
	{
		return placements;
	}

	public void setPlacements(List<Placement> placements)
	{
		this.placements = placements;
	}

}
