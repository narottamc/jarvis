/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignresponse;

import gherkin.deps.com.google.gson.annotations.Expose;
import gherkin.deps.com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class Placement
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("ssps")
	@Expose
	private List<SSP> ssps = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<SSP> getSsps()
	{
		return ssps;
	}

	public void setSsps(List<SSP> ssps)
	{
		this.ssps = ssps;
	}

}
