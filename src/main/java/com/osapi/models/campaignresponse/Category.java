/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.campaignresponse;

import gherkin.deps.com.google.gson.annotations.Expose;
import gherkin.deps.com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class Category
{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("parent_id")
	@Expose
	private Object parentId;
	@SerializedName("iab_id")
	@Expose
	private String iabId;
	@SerializedName("category_id")
	@Expose
	private Object categoryId;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Object getParentId()
	{
		return parentId;
	}

	public void setParentId(Object parentId)
	{
		this.parentId = parentId;
	}

	public String getIabId()
	{
		return iabId;
	}

	public void setIabId(String iabId)
	{
		this.iabId = iabId;
	}

	public Object getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(Object categoryId)
	{
		this.categoryId = categoryId;
	}

}
