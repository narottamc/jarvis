/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class TechnologyResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("technology_type")
	@Expose
	private Integer technologyType;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("raw_operating_systems")
	@Expose
	private List<String> rawOperatingSystems = null;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getTechnologyType()
	{
		return technologyType;
	}

	public void setTechnologyType(Integer technologyType)
	{
		this.technologyType = technologyType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<String> getRawOperatingSystems()
	{
		return rawOperatingSystems;
	}

	public void setRawOperatingSystems(List<String> rawOperatingSystems)
	{
		this.rawOperatingSystems = rawOperatingSystems;
	}

}
