/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class PlayerSizeResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("max_width")
	@Expose
	private Integer maxWidth;
	@SerializedName("min_width")
	@Expose
	private Integer minWidth;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getMaxWidth()
	{
		return maxWidth;
	}

	public void setMaxWidth(Integer maxWidth)
	{
		this.maxWidth = maxWidth;
	}

	public Integer getMinWidth()
	{
		return minWidth;
	}

	public void setMinWidth(Integer minWidth)
	{
		this.minWidth = minWidth;
	}
}
