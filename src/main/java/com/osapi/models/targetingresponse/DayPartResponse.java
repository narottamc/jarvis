/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class DayPartResponse
{
	@SerializedName("day_of_week")
	@Expose
	private Integer dayOfWeek;
	@SerializedName("hours")
	@Expose
	private List<String> hours = null;

	public Integer getDayOfWeek()
	{
		return dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek)
	{
		this.dayOfWeek = dayOfWeek;
	}

	public List<String> getHours()
	{
		return hours;
	}

	public void setHours(List<String> hours)
	{
		this.hours = hours;
	}
}
