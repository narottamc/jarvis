/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootCategoryResponse
{
	@SerializedName("target_categories")
	@Expose
	private List<TargetCategoryResponse> targetCategories = null;
	@SerializedName("hit_on_unknown_results")
	@Expose
	private Boolean hitOnUnknownResults;

	public List<TargetCategoryResponse> getTargetCategories()
	{
		return targetCategories;
	}

	public void setTargetCategories(List<TargetCategoryResponse> targetCategories)
	{
		this.targetCategories = targetCategories;
	}

	public Boolean getHitOnUnknownResults()
	{
		return hitOnUnknownResults;
	}

	public void setHitOnUnknownResults(Boolean hitOnUnknownResults)
	{
		this.hitOnUnknownResults = hitOnUnknownResults;
	}

}
