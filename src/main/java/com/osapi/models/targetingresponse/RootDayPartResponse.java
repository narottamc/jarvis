/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootDayPartResponse
{
	@SerializedName("day_parts")
	@Expose
	private List<DayPartResponse> dayParts = null;
	@SerializedName("timezone")
	@Expose
	private Integer timezone;

	public List<DayPartResponse> getDayParts()
	{
		return dayParts;
	}

	public void setDayParts(List<DayPartResponse> dayParts)
	{
		this.dayParts = dayParts;
	}

	public Integer getTimezone()
	{
		return timezone;
	}

	public void setTimezone(Integer timezone)
	{
		this.timezone = timezone;
	}
}
