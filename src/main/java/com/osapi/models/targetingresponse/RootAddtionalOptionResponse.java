/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootAddtionalOptionResponse
{
	@SerializedName("player_size_groups")
	@Expose
	private List<PlayerSizeResponse> playerSizeGroups = null;
	@SerializedName("visibilities")
	@Expose
	private List<Visibility> visibilities = null;
	@SerializedName("duration")
	@Expose
	private Integer duration;

	public List<PlayerSizeResponse> getPlayerSizeGroups()
	{
		return playerSizeGroups;
	}

	public void setPlayerSizeGroups(List<PlayerSizeResponse> playerSizeGroups)
	{
		this.playerSizeGroups = playerSizeGroups;
	}
	public List<Visibility> getVisibilities()
	{
		return visibilities;
	}

	public void setVisibilities(List<Visibility> visibilities)
	{
		this.visibilities = visibilities;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

}
