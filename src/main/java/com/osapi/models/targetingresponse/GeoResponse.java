/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class GeoResponse
{
	@SerializedName("region_id")
	@Expose
	private Integer regionId;
	@SerializedName("dma_id")
	@Expose
	private Integer dmaId;
	@SerializedName("country_id")
	@Expose
	private Integer countryId;

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Integer getDmaId() {
		return dmaId;
	}

	public void setDmaId(Integer dmaId) {
		this.dmaId = dmaId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}


}
