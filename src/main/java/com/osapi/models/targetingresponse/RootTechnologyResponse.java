/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.targetingresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootTechnologyResponse
{
	@SerializedName("technologies")
	@Expose
	private List<TechnologyResponse> technologies = null;
	@SerializedName("inventory_type_id")
	@Expose
	private Integer inventoryTypeId;

	public List<TechnologyResponse> getTechnologies()
	{
		return technologies;
	}

	public void setTechnologies(List<TechnologyResponse> technologies)
	{
		this.technologies = technologies;
	}

	public Integer getInventoryTypeId()
	{
		return inventoryTypeId;
	}

	public void setInventoryTypeId(Integer inventoryTypeId)
	{
		this.inventoryTypeId = inventoryTypeId;
	}

}
