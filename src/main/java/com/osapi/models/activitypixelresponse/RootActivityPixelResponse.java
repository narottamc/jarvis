/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootActivityPixelResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("url_img")
    @Expose
    private String urlImg;
    @SerializedName("url_js")
    @Expose
    private String urlJs;
    @SerializedName("sync_enabled")
    @Expose
    private Boolean syncEnabled;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pixel_type_id")
    @Expose
    private Integer pixelTypeId;
    @SerializedName("post_view_hours")
    @Expose
    private Integer postViewHours;
    @SerializedName("post_click_hours")
    @Expose
    private Integer postClickHours;
    @SerializedName("expiration_hours")
    @Expose
    private Integer expirationHours;
    @SerializedName("agency")
    @Expose
    private Agency agency;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("activity_pixel_parameters")
    @Expose
    private List<ActivityPixelParameter> activityPixelParameters = null;
    @SerializedName("activity_pixels_placements")
    @Expose
    private List<ActivityPixelsPlacement> activityPixelsPlacements = null;
    @SerializedName("conversion_setting")
    @Expose
    private ConversionSetting conversionSetting;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getUrlJs() {
        return urlJs;
    }

    public void setUrlJs(String urlJs) {
        this.urlJs = urlJs;
    }

    public Boolean getSyncEnabled() {
        return syncEnabled;
    }

    public void setSyncEnabled(Boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPixelTypeId() {
        return pixelTypeId;
    }

    public void setPixelTypeId(Integer pixelTypeId) {
        this.pixelTypeId = pixelTypeId;
    }

    public Integer getPostViewHours() {
        return postViewHours;
    }

    public void setPostViewHours(Integer postViewHours) {
        this.postViewHours = postViewHours;
    }

    public Integer getPostClickHours() {
        return postClickHours;
    }

    public void setPostClickHours(Integer postClickHours) {
        this.postClickHours = postClickHours;
    }

    public Integer getExpirationHours() {
        return expirationHours;
    }

    public void setExpirationHours(Integer expirationHours) {
        this.expirationHours = expirationHours;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public List<ActivityPixelParameter> getActivityPixelParameters() {
        return activityPixelParameters;
    }

    public void setActivityPixelParameters(List<ActivityPixelParameter> activityPixelParameters) {
        this.activityPixelParameters = activityPixelParameters;
    }

    public List<ActivityPixelsPlacement> getActivityPixelsPlacements() {
        return activityPixelsPlacements;
    }

    public void setActivityPixelsPlacements(List<ActivityPixelsPlacement> activityPixelsPlacements) {
        this.activityPixelsPlacements = activityPixelsPlacements;
    }

    public ConversionSetting getConversionSetting() {
        return conversionSetting;
    }

    public void setConversionSetting(ConversionSetting conversionSetting) {
        this.conversionSetting = conversionSetting;
    }

}
