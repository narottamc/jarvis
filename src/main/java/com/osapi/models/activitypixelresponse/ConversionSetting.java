/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class ConversionSetting {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("domain")
    @Expose
    private Object domain;
    @SerializedName("attribution_model")
    @Expose
    private AttributionModel attributionModel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getDomain() {
        return domain;
    }

    public void setDomain(Object domain) {
        this.domain = domain;
    }

    public AttributionModel getAttributionModel() {
        return attributionModel;
    }

    public void setAttributionModel(AttributionModel attributionModel) {
        this.attributionModel = attributionModel;
    }
}
