/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class Brand {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("social_setting")
    @Expose
    private com.osapi.models.activitypixelresponse.SocialSetting socialSetting;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public com.osapi.models.activitypixelresponse.SocialSetting getSocialSetting() {
        return socialSetting;
    }
    public void setSocialSetting(com.osapi.models.activitypixelresponse.SocialSetting socialSetting) {
        this.socialSetting = socialSetting;
    }

}
