/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class SocialSetting {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fb_picture")
    @Expose
    private Object fbPicture;
    @SerializedName("tw_picture")
    @Expose
    private Object twPicture;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFbPicture() {
        return fbPicture;
    }

    public void setFbPicture(Object fbPicture) {
        this.fbPicture = fbPicture;
    }

    public Object getTwPicture() {
        return twPicture;
    }

    public void setTwPicture(Object twPicture) {
        this.twPicture = twPicture;
    }
}
