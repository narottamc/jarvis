/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.placementresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream
 */
public class RootPlacementResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("start_date")
	@Expose
	private Object startDate;
	@SerializedName("end_date")
	@Expose
	private Object endDate;
	@SerializedName("placement_group_id")
	@Expose
	private Integer placementGroupId;
	@SerializedName("state_id")
	@Expose
	private Integer stateId;
	@SerializedName("frequency_cap")
	@Expose
	private Integer frequencyCap;
	@SerializedName("frequency_count")
	@Expose
	private Object frequencyCount;
	@SerializedName("duration")
	@Expose
	private Integer duration;
	@SerializedName("timezone")
	@Expose
	private Integer timezone;
	@SerializedName("salesforce_id")
	@Expose
	private Object salesforceId;
	@SerializedName("total_placements_budget")
	@Expose
	private Integer totalPlacementsBudget;
	@SerializedName("product_type")
	@Expose
	private Integer productType;
	@SerializedName("whitelist_deal")
	@Expose
	private Object whitelistDeal;
	@SerializedName("business_model")
	@Expose
	private BusinessModelPlacementResponse businessModel;
	@SerializedName("ssps")
	@Expose
	private List<SSPPlacementResponse> ssps = new ArrayList<SSPPlacementResponse>();
	@SerializedName("placement_marketplace_deal")
	@Expose
	private Object placementMarketplaceDeal;
	@SerializedName("assets")
	@Expose
	private List<AssetPlacementResponse> assets = new ArrayList<AssetPlacementResponse>();

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Object getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Object startDate)
	{
		this.startDate = startDate;
	}

	public Object getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Object endDate)
	{
		this.endDate = endDate;
	}

	public Integer getPlacementGroupId()
	{
		return placementGroupId;
	}

	public void setPlacementGroupId(Integer placementGroupId)
	{
		this.placementGroupId = placementGroupId;
	}

	public Integer getStateId()
	{
		return stateId;
	}

	public void setStateId(Integer stateId)
	{
		this.stateId = stateId;
	}

	public Integer getFrequencyCap()
	{
		return frequencyCap;
	}

	public void setFrequencyCap(Integer frequencyCap)
	{
		this.frequencyCap = frequencyCap;
	}

	public Object getFrequencyCount()
	{
		return frequencyCount;
	}

	public void setFrequencyCount(Object frequencyCount)
	{
		this.frequencyCount = frequencyCount;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	public Integer getTimezone()
	{
		return timezone;
	}

	public void setTimezone(Integer timezone)
	{
		this.timezone = timezone;
	}

	public Object getSalesforceId()
	{
		return salesforceId;
	}

	public void setSalesforceId(Object salesforceId)
	{
		this.salesforceId = salesforceId;
	}

	public Integer getTotalPlacementsBudget()
	{
		return totalPlacementsBudget;
	}

	public void setTotalPlacementsBudget(Integer totalPlacementsBudget)
	{
		this.totalPlacementsBudget = totalPlacementsBudget;
	}

	public Integer getProductType()
	{
		return productType;
	}

	public void setProductType(Integer productType)
	{
		this.productType = productType;
	}

	public Object getWhitelistDeal()
	{
		return whitelistDeal;
	}

	public void setWhitelistDeal(Object whitelistDeal)
	{
		this.whitelistDeal = whitelistDeal;
	}

	public BusinessModelPlacementResponse getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(BusinessModelPlacementResponse businessModel)
	{
		this.businessModel = businessModel;
	}

	public List<SSPPlacementResponse> getSsps()
	{
		return ssps;
	}

	public void setSsps(List<SSPPlacementResponse> ssps)
	{
		this.ssps = ssps;
	}

	public Object getPlacementMarketplaceDeal()
	{
		return placementMarketplaceDeal;
	}

	public void setPlacementMarketplaceDeal(Object placementMarketplaceDeal)
	{
		this.placementMarketplaceDeal = placementMarketplaceDeal;
	}

	public List<AssetPlacementResponse> getAssets()
	{
		return assets;
	}

	public void setAssets(List<AssetPlacementResponse> assets)
	{
		this.assets = assets;
	}

}
