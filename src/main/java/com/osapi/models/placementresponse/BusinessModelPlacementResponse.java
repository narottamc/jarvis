/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.placementresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class BusinessModelPlacementResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("budget")
	@Expose
	private Integer budget;
	@SerializedName("margin")
	@Expose
	private Double margin;
	@SerializedName("cpm_bid")
	@Expose
	private Integer cpmBid;
	@SerializedName("cap")
	@Expose
	private Object cap;
	@SerializedName("cmvr")
	@Expose
	private Double cmvr;
	@SerializedName("cpcv")
	@Expose
	private Integer cpcv;
	@SerializedName("ctr")
	@Expose
	private Double ctr;
	@SerializedName("cpc")
	@Expose
	private Integer cpc;
	@SerializedName("cpa")
	@Expose
	private Integer cpa;
	@SerializedName("strategy_id")
	@Expose
	private Integer strategyId;
	@SerializedName("outstream_cpm")
	@Expose
	private Integer outstreamCpm;
	@SerializedName("outstream_margin")
	@Expose
	private Double outstreamMargin;
	@SerializedName("pacer_budget")
	@Expose
	private Integer pacerBudget;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("placement_id")
	@Expose
	private Integer placementId;
	@SerializedName("flight_id")
	@Expose
	private Integer flightId;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public Integer getBudget() {
	return budget;
	}

	public void setBudget(Integer budget) {
	this.budget = budget;
	}

	public Double getMargin() {
	return margin;
	}

	public void setMargin(Double margin) {
	this.margin = margin;
	}

	public Integer getCpmBid() {
	return cpmBid;
	}

	public void setCpmBid(Integer cpmBid) {
	this.cpmBid = cpmBid;
	}

	public Object getCap() {
	return cap;
	}

	public void setCap(Object cap) {
	this.cap = cap;
	}

	public Double getCmvr() {
	return cmvr;
	}

	public void setCmvr(Double cmvr) {
	this.cmvr = cmvr;
	}

	public Integer getCpcv() {
	return cpcv;
	}

	public void setCpcv(Integer cpcv) {
	this.cpcv = cpcv;
	}

	public Double getCtr() {
	return ctr;
	}

	public void setCtr(Double ctr) {
	this.ctr = ctr;
	}

	public Integer getCpc() {
	return cpc;
	}

	public void setCpc(Integer cpc) {
	this.cpc = cpc;
	}

	public Integer getCpa() {
	return cpa;
	}

	public void setCpa(Integer cpa) {
	this.cpa = cpa;
	}

	public Integer getStrategyId() {
	return strategyId;
	}

	public void setStrategyId(Integer strategyId) {
	this.strategyId = strategyId;
	}

	public Integer getOutstreamCpm() {
	return outstreamCpm;
	}

	public void setOutstreamCpm(Integer outstreamCpm) {
	this.outstreamCpm = outstreamCpm;
	}

	public Double getOutstreamMargin() {
	return outstreamMargin;
	}

	public void setOutstreamMargin(Double outstreamMargin) {
	this.outstreamMargin = outstreamMargin;
	}

	public Integer getPacerBudget() {
	return pacerBudget;
	}

	public void setPacerBudget(Integer pacerBudget) {
	this.pacerBudget = pacerBudget;
	}

	public Integer getCampaignId() {
	return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
	}

	public Integer getPlacementId() {
	return placementId;
	}

	public void setPlacementId(Integer placementId) {
	this.placementId = placementId;
	}

	public Integer getFlightId() {
	return flightId;
	}

	public void setFlightId(Integer flightId) {
	this.flightId = flightId;
	}
}
