/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.ssprequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class RootSSPRequest {
    @SerializedName("ssp_id")
    @Expose
    private Integer sspId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seller_network_seat")
    @Expose
    private String sellerNetworkSeat;
    @SerializedName("seller_network_deal")
    @Expose
    private String sellerNetworkDeal;
    @SerializedName("placement_id")
    @Expose
    private String placementId;

    public Integer getSspId() {
        return sspId;
    }

    public void setSspId(Integer sspId) {
        this.sspId = sspId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSellerNetworkSeat() {
        return sellerNetworkSeat;
    }

    public void setSellerNetworkSeat(String sellerNetworkSeat) {
        this.sellerNetworkSeat = sellerNetworkSeat;
    }

    public String getSellerNetworkDeal() {
        return sellerNetworkDeal;
    }

    public void setSellerNetworkDeal(String sellerNetworkDeal) {
        this.sellerNetworkDeal = sellerNetworkDeal;
    }

    public String getPlacementId() {
        return placementId;
    }

    public void setPlacementId(String placementId) {
        this.placementId = placementId;
    }
}
