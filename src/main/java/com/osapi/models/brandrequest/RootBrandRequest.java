/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.brandrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootBrandRequest
{
	@SerializedName("brand")
	@Expose
	private BrandRequest brand;

	public BrandRequest getBrand()
	{
		return brand;
	}

	public void setBrand(BrandRequest brand)
	{
		this.brand = brand;
	}

}
