/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.brandrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class SocialSettingAttributes
{
	@SerializedName("tw_page_url")
	@Expose
	private Object twPageUrl;
	@SerializedName("yt_page_url")
	@Expose
	private Object ytPageUrl;
	@SerializedName("fb_page_url")
	@Expose
	private Object fbPageUrl;

	public Object getTwPageUrl()
	{
		return twPageUrl;
	}

	public void setTwPageUrl(Object twPageUrl)
	{
		this.twPageUrl = twPageUrl;
	}

	public Object getYtPageUrl()
	{
		return ytPageUrl;
	}

	public void setYtPageUrl(Object ytPageUrl)
	{
		this.ytPageUrl = ytPageUrl;
	}

	public Object getFbPageUrl()
	{
		return fbPageUrl;
	}

	public void setFbPageUrl(Object fbPageUrl)
	{
		this.fbPageUrl = fbPageUrl;
	}

}
