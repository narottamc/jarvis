/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.brandrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */

public class BrandRequest
{
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("code")
	@Expose
	private String code;
	@SerializedName("landing_page_url")
	@Expose
	private String landingPageUrl;
	@SerializedName("internal")
	@Expose
	private Boolean internal;
	@SerializedName("social_setting_attributes")
	@Expose
	private SocialSettingAttributes socialSettingAttributes;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getLandingPageUrl()
	{
		return landingPageUrl;
	}

	public void setLandingPageUrl(String landingPageUrl)
	{
		this.landingPageUrl = landingPageUrl;
	}

	public Boolean getInternal()
	{
		return internal;
	}

	public void setInternal(Boolean internal)
	{
		this.internal = internal;
	}

	public SocialSettingAttributes getSocialSettingAttributes()
	{
		return socialSettingAttributes;
	}

	public void setSocialSettingAttributes(SocialSettingAttributes socialSettingAttributes)
	{
		this.socialSettingAttributes = socialSettingAttributes;
	}
}
