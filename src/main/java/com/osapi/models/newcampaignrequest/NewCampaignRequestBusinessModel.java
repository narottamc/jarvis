/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newcampaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class NewCampaignRequestBusinessModel
{
	@SerializedName("budget")
	@Expose
	private Integer budget;
	@SerializedName("margin")
	@Expose
	private Double margin;
	@SerializedName("cpm_bid")
	@Expose
	private Double cpmBid;
	@SerializedName("cap")
	@Expose
	private Object cap;
	@SerializedName("cmvr")
	@Expose
	private Double cmvr;
	@SerializedName("cpcv")
	@Expose
	private Integer cpcv;
	@SerializedName("ctr")
	@Expose
	private Double ctr;
	@SerializedName("cpc")
	@Expose
	private Integer cpc;
	@SerializedName("cpa")
	@Expose
	private Object cpa;
	@SerializedName("strategy_id")
	@Expose
	private Integer strategyId;
	@SerializedName("outstream_cpm")
	@Expose
	private Object outstreamCpm;
	@SerializedName("outstream_margin")
	@Expose
	private Object outstreamMargin;
	@SerializedName("pacer_budget")
	@Expose
	private Integer pacerBudget;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("placement_id")
	@Expose
	private Object placementId;
	@SerializedName("flight_id")
	@Expose
	private Object flightId;

	public Integer getBudget() {
	return budget;
	}

	public void setBudget(Integer budget) {
	this.budget = budget;
	}

	public Double getMargin() {
	return margin;
	}

	public void setMargin(Double margin) {
	this.margin = margin;
	}

	public Double getCpmBid() {
	return cpmBid;
	}

	public void setCpmBid(Double cpmBid) {
	this.cpmBid = cpmBid;
	}

	public Object getCap() {
	return cap;
	}

	public void setCap(Object cap) {
	this.cap = cap;
	}

	public Double getCmvr() {
	return cmvr;
	}

	public void setCmvr(Double cmvr) {
	this.cmvr = cmvr;
	}

	public Integer getCpcv() {
	return cpcv;
	}

	public void setCpcv(Integer cpcv) {
	this.cpcv = cpcv;
	}

	public Double getCtr() {
	return ctr;
	}

	public void setCtr(Double ctr) {
	this.ctr = ctr;
	}

	public Integer getCpc() {
	return cpc;
	}

	public void setCpc(Integer cpc) {
	this.cpc = cpc;
	}

	public Object getCpa() {
	return cpa;
	}

	public void setCpa(Object cpa) {
	this.cpa = cpa;
	}

	public Integer getStrategyId() {
	return strategyId;
	}

	public void setStrategyId(Integer strategyId) {
	this.strategyId = strategyId;
	}

	public Object getOutstreamCpm() {
	return outstreamCpm;
	}

	public void setOutstreamCpm(Object outstreamCpm) {
	this.outstreamCpm = outstreamCpm;
	}

	public Object getOutstreamMargin() {
	return outstreamMargin;
	}

	public void setOutstreamMargin(Object outstreamMargin) {
	this.outstreamMargin = outstreamMargin;
	}

	public Integer getPacerBudget() {
	return pacerBudget;
	}

	public void setPacerBudget(Integer pacerBudget) {
	this.pacerBudget = pacerBudget;
	}

	public Integer getCampaignId() {
	return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
	}

	public Object getPlacementId() {
	return placementId;
	}

	public void setPlacementId(Object placementId) {
	this.placementId = placementId;
	}

	public Object getFlightId() {
	return flightId;
	}

	public void setFlightId(Object flightId) {
	this.flightId = flightId;
	}

}
