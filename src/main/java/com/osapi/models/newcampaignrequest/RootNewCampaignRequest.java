/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newcampaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RootNewCampaignRequest
{
	@SerializedName("campaign")
	@Expose
	private NewCampaignRequestCampaign campaign;

	public NewCampaignRequestCampaign getCampaign()
	{
		return campaign;
	}

	public void setCampaign(NewCampaignRequestCampaign campaign)
	{
		this.campaign = campaign;

	}
}
