/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newcampaignrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream
 */
public class NewCampaignRequestCampaign
{
	@SerializedName("placement_groups")
	@Expose
	private List<NewCampaignRequestPlacementGroup> placementGroups = null;
	@SerializedName("business_model")
	@Expose
	private NewCampaignRequestBusinessModel businessModel;
	@SerializedName("marketplace_deal")
	@Expose
	private NewCampaignRequestMarketPlaceDeal marketplaceDeal;
	@SerializedName("categories")
	@Expose
	private List<NewCampaignRequestCategory> categories = null;
	@SerializedName("campaign_tags")
	@Expose
	private List<Object> campaignTags = null;
	@SerializedName("agency")
	@Expose
	private NewCampaignRequestAgency agency;
	@SerializedName("selectedAgency")
	@Expose
	private NewCampaignRequestSelectedAgency selectedAgency;
	@SerializedName("brand")
	@Expose
	private NewCampaignRequestBrand brand;
	@SerializedName("selectedbrand")
	@Expose
	private NewCampaignRequestSelectedBrand selectedbrand;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("insertion_order_number")
	@Expose
	private String insertionOrderNumber;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("campaign_type")
	@Expose
	private NewCampaignRequestCampaignType campaignType;
	@SerializedName("margin_estimate")
	@Expose
	private String marginEstimate;
	@SerializedName("agency_id")
	@Expose
	private Integer agencyId;
	@SerializedName("brand_id")
	@Expose
	private Integer brandId;
	@SerializedName("campaign_type_id")
	@Expose
	private Integer campaignTypeId;

	public List<NewCampaignRequestPlacementGroup> getPlacementGroups()
	{
		return placementGroups;
	}

	public void setPlacementGroups(List<NewCampaignRequestPlacementGroup> placementGroups)
	{
		this.placementGroups = placementGroups;
	}

	public NewCampaignRequestBusinessModel getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(NewCampaignRequestBusinessModel businessModel)
	{
		this.businessModel = businessModel;
	}

	public NewCampaignRequestMarketPlaceDeal getMarketplaceDeal()
	{
		return marketplaceDeal;
	}

	public void setMarketplaceDeal(NewCampaignRequestMarketPlaceDeal marketplaceDeal)
	{
		this.marketplaceDeal = marketplaceDeal;
	}

	public List<NewCampaignRequestCategory> getCategories()
	{
		return categories;
	}

	public void setCategories(List<NewCampaignRequestCategory> categories)
	{
		this.categories = categories;
	}

	public List<Object> getCampaignTags()
	{
		return campaignTags;
	}

	public void setCampaignTags(List<Object> campaignTags)
	{
		this.campaignTags = campaignTags;
	}

	public NewCampaignRequestAgency getAgency()
	{
		return agency;
	}

	public void setAgency(NewCampaignRequestAgency agency)
	{
		this.agency = agency;
	}

	public NewCampaignRequestSelectedAgency getSelectedAgency()
	{
		return selectedAgency;
	}

	public void setSelectedAgency(NewCampaignRequestSelectedAgency selectedAgency)
	{
		this.selectedAgency = selectedAgency;
	}

	public NewCampaignRequestBrand getBrand()
	{
		return brand;
	}

	public void setBrand(NewCampaignRequestBrand brand)
	{
		this.brand = brand;
	}

	public NewCampaignRequestSelectedBrand getSelectedbrand()
	{
		return selectedbrand;
	}

	public void setSelectedbrand(NewCampaignRequestSelectedBrand selectedbrand)
	{
		this.selectedbrand = selectedbrand;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getInsertionOrderNumber()
	{
		return insertionOrderNumber;
	}

	public void setInsertionOrderNumber(String insertionOrderNumber)
	{
		this.insertionOrderNumber = insertionOrderNumber;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public NewCampaignRequestCampaignType getCampaignType()
	{
		return campaignType;
	}

	public void setCampaignType(NewCampaignRequestCampaignType campaignType)
	{
		this.campaignType = campaignType;
	}

	public String getMarginEstimate()
	{
		return marginEstimate;
	}

	public void setMarginEstimate(String marginEstimate)
	{
		this.marginEstimate = marginEstimate;
	}

	public Integer getAgencyId()
	{
		return agencyId;
	}

	public void setAgencyId(Integer agencyId)
	{
		this.agencyId = agencyId;
	}

	public Integer getBrandId()
	{
		return brandId;
	}

	public void setBrandId(Integer brandId)
	{
		this.brandId = brandId;
	}

	public Integer getCampaignTypeId()
	{
		return campaignTypeId;
	}

	public void setCampaignTypeId(Integer campaignTypeId)
	{
		this.campaignTypeId = campaignTypeId;
	}

}
