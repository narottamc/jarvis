/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newplacementrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class RootNewPlacementRequest
{
	@SerializedName("placement")
	@Expose
	private NewPlacement placement;

	public NewPlacement getPlacement()
	{
		return placement;
	}

	public void setPlacement(NewPlacement placement)
	{
		this.placement = placement;
	}

}
