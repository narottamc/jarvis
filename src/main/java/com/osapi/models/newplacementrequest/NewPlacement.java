/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newplacementrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ClearStream.
 */
public class NewPlacement
{
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("placement_group_id")
	@Expose
	private Integer placementGroupId;
	@SerializedName("events")
	@Expose
	private List<NewPlacementEvent> events = new ArrayList<NewPlacementEvent>();
	@SerializedName("languages")
	@Expose
	private List<NewPlacementLanguage> languages = new ArrayList<NewPlacementLanguage>();
	@SerializedName("timezone")
	@Expose
	private String timezone;
	@SerializedName("hit_on_unknown_results")
	@Expose
	private Boolean hitOnUnknownResults;
	@SerializedName("business_model")
	@Expose
	private NewPlacementBusinessModel businessModel;
	@SerializedName("pass_empty_language")
	@Expose
	private Boolean passEmptyLanguage;
	@SerializedName("ssps")
	@Expose
	private List<Object> ssps = new ArrayList<Object>();
	@SerializedName("advertiser_categories")
	@Expose
	private List<NewPlacementAdvertiserCategory> advertiserCategories = new ArrayList<NewPlacementAdvertiserCategory>();
	@SerializedName("duration")
	@Expose
	private Integer duration;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getPlacementGroupId()
	{
		return placementGroupId;
	}

	public void setPlacementGroupId(Integer placementGroupId)
	{
		this.placementGroupId = placementGroupId;
	}

	public List<NewPlacementEvent> getEvents()
	{
		return events;
	}

	public void setEvents(List<NewPlacementEvent> events)
	{
		this.events = events;
	}

	public List<NewPlacementLanguage> getLanguages()
	{
		return languages;
	}

	public void setLanguages(List<NewPlacementLanguage> languages)
	{
		this.languages = languages;
	}

	public String getTimezone()
	{
		return timezone;
	}

	public void setTimezone(String timezone)
	{
		this.timezone = timezone;
	}

	public Boolean getHitOnUnknownResults()
	{
		return hitOnUnknownResults;
	}

	public void setHitOnUnknownResults(Boolean hitOnUnknownResults)
	{
		this.hitOnUnknownResults = hitOnUnknownResults;
	}

	public NewPlacementBusinessModel getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(NewPlacementBusinessModel businessModel)
	{
		this.businessModel = businessModel;
	}

	public Boolean getPassEmptyLanguage()
	{
		return passEmptyLanguage;
	}

	public void setPassEmptyLanguage(Boolean passEmptyLanguage)
	{
		this.passEmptyLanguage = passEmptyLanguage;
	}

	public List<Object> getSsps()
	{
		return ssps;
	}

	public void setSsps(List<Object> ssps)
	{
		this.ssps = ssps;
	}

	public List<NewPlacementAdvertiserCategory> getAdvertiserCategories()
	{
		return advertiserCategories;
	}

	public void setAdvertiserCategories(List<NewPlacementAdvertiserCategory> advertiserCategories)
	{
		this.advertiserCategories = advertiserCategories;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

}
