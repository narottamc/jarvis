/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.segmentsdmpproviders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootDmpProvidersResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dmp_name")
    @Expose
    private String dmpName;
    @SerializedName("dmp_clients")
    @Expose
    private List<DmpClient> dmpClients = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDmpName() {
        return dmpName;
    }

    public void setDmpName(String dmpName) {
        this.dmpName = dmpName;
    }

    public List<DmpClient> getDmpClients() {
        return dmpClients;
    }

    public void setDmpClients(List<DmpClient> dmpClients) {
        this.dmpClients = dmpClients;
    }
}
