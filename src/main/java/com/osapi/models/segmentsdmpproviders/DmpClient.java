/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.segmentsdmpproviders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class DmpClient {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dmp_client")
    @Expose
    private String dmpClient;
    @SerializedName("dmp_provider_id")
    @Expose
    private Integer dmpProviderId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("client_id")
    @Expose
    private Object clientId;
    @SerializedName("prefix")
    @Expose
    private Object prefix;
    @SerializedName("party_level")
    @Expose
    private Object partyLevel;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("activated_on")
    @Expose
    private Object activatedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDmpClient() {
        return dmpClient;
    }

    public void setDmpClient(String dmpClient) {
        this.dmpClient = dmpClient;
    }

    public Integer getDmpProviderId() {
        return dmpProviderId;
    }

    public void setDmpProviderId(Integer dmpProviderId) {
        this.dmpProviderId = dmpProviderId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getClientId() {
        return clientId;
    }

    public void setClientId(Object clientId) {
        this.clientId = clientId;
    }

    public Object getPrefix() {
        return prefix;
    }

    public void setPrefix(Object prefix) {
        this.prefix = prefix;
    }

    public Object getPartyLevel() {
        return partyLevel;
    }

    public void setPartyLevel(Object partyLevel) {
        this.partyLevel = partyLevel;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Object getActivatedOn() {
        return activatedOn;
    }

    public void setActivatedOn(Object activatedOn) {
        this.activatedOn = activatedOn;
    }
}
