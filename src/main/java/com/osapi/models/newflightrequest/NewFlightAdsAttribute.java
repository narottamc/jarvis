/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newflightrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ClearStream.
 */
public class NewFlightAdsAttribute
{
	@SerializedName("ad_type")
	@Expose
	private String adType;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("budget_percentage")
	@Expose
	private String budgetPercentage;
	@SerializedName("width")
	@Expose
	private String width;
	@SerializedName("height")
	@Expose
	private String height;

	public String getAdType()
	{
		return adType;
	}

	public void setAdType(String adType)
	{
		this.adType = adType;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBudgetPercentage()
	{
		return budgetPercentage;
	}

	public void setBudgetPercentage(String budgetPercentage)
	{
		this.budgetPercentage = budgetPercentage;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(String width)
	{
		this.width = width;
	}

	public String getHeight()
	{
		return height;
	}

	public void setHeight(String height)
	{
		this.height = height;
	}
}
