/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.newflightrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.osapi.models.placementresponse.BusinessModelPlacementResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ClearStream.
 */
public class RootNewFlightRequest
{
	@SerializedName("ad_server")
	@Expose
	private String adServer;
	@SerializedName("bid_adjustment")
	@Expose
	private Object bidAdjustment;
	@SerializedName("business_model")
	@Expose
	private BusinessModelPlacementResponse businessModel;
	@SerializedName("events")
	@Expose
	private List<Object> events = new ArrayList<Object>();
	@SerializedName("flight_assets")
	@Expose
	private List<Object> flightAssets = new ArrayList<Object>();
	@SerializedName("min_viewability")
	@Expose
	private Object minViewability;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("placement_id")
	@Expose
	private Integer placementId;
	@SerializedName("row_index")
	@Expose
	private Object rowIndex;
	@SerializedName("short_vast")
	@Expose
	private Boolean shortVast;
	@SerializedName("ads_attributes")
	@Expose
	private List<NewFlightAdsAttribute> adsAttributes = new ArrayList<NewFlightAdsAttribute>();

	public String getAdServer()
	{
		return adServer;
	}

	public void setAdServer(String adServer)
	{
		this.adServer = adServer;
	}

	public Object getBidAdjustment()
	{
		return bidAdjustment;
	}

	public void setBidAdjustment(Object bidAdjustment)
	{
		this.bidAdjustment = bidAdjustment;
	}

	public BusinessModelPlacementResponse getBusinessModel()
	{
		return businessModel;
	}

	public void setBusinessModel(BusinessModelPlacementResponse businessModel)
	{
		this.businessModel = businessModel;
	}

	public List<Object> getEvents()
	{
		return events;
	}

	public void setEvents(List<Object> events)
	{
		this.events = events;
	}

	public List<Object> getFlightAssets()
	{
		return flightAssets;
	}

	public void setFlightAssets(List<Object> flightAssets)
	{
		this.flightAssets = flightAssets;
	}

	public Object getMinViewability()
	{
		return minViewability;
	}

	public void setMinViewability(Object minViewability)
	{
		this.minViewability = minViewability;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getPlacementId()
	{
		return placementId;
	}

	public void setPlacementId(Integer placementId)
	{
		this.placementId = placementId;
	}

	public Object getRowIndex()
	{
		return rowIndex;
	}

	public void setRowIndex(Object rowIndex)
	{
		this.rowIndex = rowIndex;
	}

	public Boolean getShortVast()
	{
		return shortVast;
	}

	public void setShortVast(Boolean shortVast)
	{
		this.shortVast = shortVast;
	}

	public List<NewFlightAdsAttribute> getAdsAttributes()
	{
		return adsAttributes;
	}

	public void setAdsAttributes(List<NewFlightAdsAttribute> adsAttributes)
	{
		this.adsAttributes = adsAttributes;
	}
}
