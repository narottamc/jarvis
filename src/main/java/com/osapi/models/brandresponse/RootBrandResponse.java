/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.brandresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class RootBrandResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("code")
	@Expose
	private String code;
	@SerializedName("landing_page_url")
	@Expose
	private String landingPageUrl;
	@SerializedName("internal")
	@Expose
	private Boolean internal;
	@SerializedName("social_setting")
	@Expose
	private SocialSetting socialSetting;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getLandingPageUrl()
	{
		return landingPageUrl;
	}

	public void setLandingPageUrl(String landingPageUrl)
	{
		this.landingPageUrl = landingPageUrl;
	}

	public Boolean getInternal()
	{
		return internal;
	}

	public void setInternal(Boolean internal)
	{
		this.internal = internal;
	}

	public SocialSetting getSocialSetting()
	{
		return socialSetting;
	}

	public void setSocialSetting(SocialSetting socialSetting)
	{
		this.socialSetting = socialSetting;
	}

}
