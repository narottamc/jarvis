/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.brandresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream
 */
public class SocialSetting
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("tw_page_url")
	@Expose
	private Object twPageUrl;
	@SerializedName("fb_page_url")
	@Expose
	private Object fbPageUrl;
	@SerializedName("yt_page_url")
	@Expose
	private Object ytPageUrl;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Object getTwPageUrl()
	{
		return twPageUrl;
	}

	public void setTwPageUrl(Object twPageUrl)
	{
		this.twPageUrl = twPageUrl;
	}

	public Object getFbPageUrl()
	{
		return fbPageUrl;
	}

	public void setFbPageUrl(Object fbPageUrl)
	{
		this.fbPageUrl = fbPageUrl;
	}

	public Object getYtPageUrl()
	{
		return ytPageUrl;
	}

	public void setYtPageUrl(Object ytPageUrl)
	{
		this.ytPageUrl = ytPageUrl;
	}

}
