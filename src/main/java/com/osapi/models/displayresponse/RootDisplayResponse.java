/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.displayresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author ClearStream.
 */
public class RootDisplayResponse
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("ad_tag_type_id")
	@Expose
	private Integer adTagTypeId;
	@SerializedName("landing_page_domain")
	@Expose
	private String landingPageDomain;
	@SerializedName("display_type_id")
	@Expose
	private Integer displayTypeId;
	@SerializedName("third_party_ad_tag")
	@Expose
	private Object thirdPartyAdTag;
	@SerializedName("ad_content")
	@Expose
	private String adContent;
	@SerializedName("display_events")
	@Expose
	private List<Object> displayEvents = null;
	@SerializedName("width")
	@Expose
	private Integer width;
	@SerializedName("height")
	@Expose
	private Integer height;
	@SerializedName("script_type_id")
	@Expose
	private Integer scriptTypeId;
	
	public Integer getScriptTypeId()
	{
		return scriptTypeId;
	}

	public void setScriptTypeId(Integer scriptTypeId)
	{
		this.scriptTypeId = scriptTypeId;
	}

	public Integer getWidth()
	{
		return width;
	}

	public void setWidth(Integer width)
	{
		this.width = width;
	}

	public Integer getHeight()
	{
		return height;
	}

	public void setHeight(Integer height)
	{
		this.height = height;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getCampaignId()
	{
		return campaignId;
	}

	public void setCampaignId(Integer campaignId)
	{
		this.campaignId = campaignId;
	}

	public Integer getAdTagTypeId()
	{
		return adTagTypeId;
	}

	public void setAdTagTypeId(Integer adTagTypeId)
	{
		this.adTagTypeId = adTagTypeId;
	}

	public String getLandingPageDomain()
	{
		return landingPageDomain;
	}

	public void setLandingPageDomain(String landingPageDomain)
	{
		this.landingPageDomain = landingPageDomain;
	}

	public Integer getDisplayTypeId()
	{
		return displayTypeId;
	}

	public void setDisplayTypeId(Integer displayTypeId)
	{
		this.displayTypeId = displayTypeId;
	}

	public Object getThirdPartyAdTag()
	{
		return thirdPartyAdTag;
	}

	public void setThirdPartyAdTag(Object thirdPartyAdTag)
	{
		this.thirdPartyAdTag = thirdPartyAdTag;
	}

	public String getAdContent()
	{
		return adContent;
	}

	public void setAdContent(String adContent)
	{
		this.adContent = adContent;
	}

	public List<Object> getDisplayEvents()
	{
		return displayEvents;
	}

	public void setDisplayEvents(List<Object> displayEvents)
	{
		this.displayEvents = displayEvents;
	}

}
