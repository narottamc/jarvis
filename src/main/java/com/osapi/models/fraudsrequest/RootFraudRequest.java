/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.fraudsrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class RootFraudRequest
{
	@SerializedName("placementId")
	@Expose
	private Integer placementId;
	@SerializedName("enable_double_verify")
	@Expose
	private Boolean enableDoubleVerify;
	@SerializedName("enable_dv_fraud")
	@Expose
	private Boolean enableDvFraud;
	@SerializedName("use_iab")
	@Expose
	private Boolean useIab;
	@SerializedName("use_ias")
	@Expose
	private Boolean useIas;
	@SerializedName("dv_ppd_timeout")
	@Expose
	private Boolean dvPpdTimeout;
	@SerializedName("ias_timeout")
	@Expose
	private Boolean iasTimeout;
	@SerializedName("ias_credentials")
	@Expose
	private IASCredentialsFraudRequest iasCredentials;

	public Integer getPlacementId() {
	return placementId;
	}

	public void setPlacementId(Integer placementId) {
	this.placementId = placementId;
	}

	public Boolean getEnableDoubleVerify() {
	return enableDoubleVerify;
	}

	public void setEnableDoubleVerify(Boolean enableDoubleVerify) {
	this.enableDoubleVerify = enableDoubleVerify;
	}

	public Boolean getEnableDvFraud() {
	return enableDvFraud;
	}

	public void setEnableDvFraud(Boolean enableDvFraud) {
	this.enableDvFraud = enableDvFraud;
	}

	public Boolean getUseIab() {
	return useIab;
	}

	public void setUseIab(Boolean useIab) {
	this.useIab = useIab;
	}

	public Boolean getUseIas() {
	return useIas;
	}

	public void setUseIas(Boolean useIas) {
	this.useIas = useIas;
	}

	public Boolean getDvPpdTimeout() {
	return dvPpdTimeout;
	}

	public void setDvPpdTimeout(Boolean dvPpdTimeout) {
	this.dvPpdTimeout = dvPpdTimeout;
	}

	public Boolean getIasTimeout() {
	return iasTimeout;
	}

	public void setIasTimeout(Boolean iasTimeout) {
	this.iasTimeout = iasTimeout;
	}

	public IASCredentialsFraudRequest getIasCredentials() {
	return iasCredentials;
	}

	public void setIasCredentials(IASCredentialsFraudRequest iasCredentials) {
	this.iasCredentials = iasCredentials;
	}

}
