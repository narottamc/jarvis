/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.fraudsrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class IASCredentialsFraudRequest
{
	@SerializedName("advEntityId")
	@Expose
	private String advEntityId;
	@SerializedName("pubEntityId")
	@Expose
	private String pubEntityId;

	public String getAdvEntityId() {
	return advEntityId;
	}

	public void setAdvEntityId(String advEntityId) {
	this.advEntityId = advEntityId;
	}

	public String getPubEntityId() {
	return pubEntityId;
	}

	public void setPubEntityId(String pubEntityId) {
	this.pubEntityId = pubEntityId;
	}

}
