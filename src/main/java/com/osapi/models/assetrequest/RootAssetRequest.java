/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.assetrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 *
 */
public class RootAssetRequest
{
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("asset_id")
	@Expose
	private Integer assetId;
	@SerializedName("asset")
	@Expose
	private Asset asset;

	public Integer getCampaignId() {
	return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
	}

	public Integer getAssetId() {
	return assetId;
	}

	public void setAssetId(Integer assetId) {
	this.assetId = assetId;
	}

	public Asset getAsset() {
	return asset;
	}

	public void setAsset(Asset asset) {
	this.asset = asset;
	}
}
