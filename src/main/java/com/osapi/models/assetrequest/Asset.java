/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.assetrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.osapi.models.assetresponse.AssetMedia;

import java.util.List;

/**
 * @author Clearstream.
 *
 */
public class Asset
{
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("campaign_id")
	@Expose
	private Integer campaignId;
	@SerializedName("ad_tag_type_id")
	@Expose
	private Integer adTagTypeId;
	@SerializedName("bundle_option")
	@Expose
	private String bundleOption;
	@SerializedName("click_tracking_urls")
	@Expose
	private Object clickTrackingUrls;
	@SerializedName("impression_tracking_urls")
	@Expose
	private Object impressionTrackingUrls;
	@SerializedName("is_linear")
	@Expose
	private Boolean isLinear;
	@SerializedName("is_skippable")
	@Expose
	private Boolean isSkippable;
	@SerializedName("is_v_paid")
	@Expose
	private Boolean isVPaid;
	@SerializedName("landing_page_domain")
	@Expose
	private String landingPageDomain;
	@SerializedName("row_index")
	@Expose
	private Integer rowIndex;
	@SerializedName("third_party_ad_tag")
	@Expose
	private Object thirdPartyAdTag;
	@SerializedName("vast_secure_url")
	@Expose
	private Object vastSecureUrl;
	@SerializedName("is_flight_associated")
	@Expose
	private Boolean isFlightAssociated;
	@SerializedName("third_party_url")
	@Expose
	private Object thirdPartyUrl;
	@SerializedName("width")
	@Expose
	private String width;
	@SerializedName("height")
	@Expose
	private String height;
	@SerializedName("mime_types")
	@Expose
	private List<Integer> mime_types = null;
	@SerializedName("asset_media")
	@Expose
	private AssetMedia assetMedia;
	@SerializedName("asset_events")
	@Expose
	private List<AssetEvent> assetEvents = null;
	@SerializedName("asset_companion")
	@Expose
	private AssetCompanion assetCompanion;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("companionEnabled")
	@Expose
	private Boolean companionEnabled;
	@SerializedName("mimeTypes1")
	@Expose
	private MimeTypes mimeTypes;
	@SerializedName("formatedName")
	@Expose
	private String formatedName;
	@SerializedName("is_generate_vast")
	@Expose
	private Boolean isGenerateVast;

	public Integer getId() {
	return id;
	}

	public void setId(Integer id) {
	this.id = id;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public Integer getCampaignId() {
	return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
	}

	public Integer getAdTagTypeId() {
	return adTagTypeId;
	}

	public void setAdTagTypeId(Integer adTagTypeId) {
	this.adTagTypeId = adTagTypeId;
	}

	public String getBundleOption() {
	return bundleOption;
	}

	public void setBundleOption(String bundleOption) {
	this.bundleOption = bundleOption;
	}

	public Object getClickTrackingUrls() {
	return clickTrackingUrls;
	}

	public void setClickTrackingUrls(Object clickTrackingUrls) {
	this.clickTrackingUrls = clickTrackingUrls;
	}

	public Object getImpressionTrackingUrls() {
	return impressionTrackingUrls;
	}

	public void setImpressionTrackingUrls(Object impressionTrackingUrls) {
	this.impressionTrackingUrls = impressionTrackingUrls;
	}

	public Boolean getIsLinear() {
	return isLinear;
	}

	public void setIsLinear(Boolean isLinear) {
	this.isLinear = isLinear;
	}

	public Boolean getIsSkippable() {
	return isSkippable;
	}

	public void setIsSkippable(Boolean isSkippable) {
	this.isSkippable = isSkippable;
	}

	public Boolean getIsVPaid() {
	return isVPaid;
	}

	public void setIsVPaid(Boolean isVPaid) {
	this.isVPaid = isVPaid;
	}

	public String getLandingPageDomain() {
	return landingPageDomain;
	}

	public void setLandingPageDomain(String landingPageDomain) {
	this.landingPageDomain = landingPageDomain;
	}

	public Integer getRowIndex() {
	return rowIndex;
	}

	public void setRowIndex(Integer rowIndex) {
	this.rowIndex = rowIndex;
	}

	public Object getThirdPartyAdTag() {
	return thirdPartyAdTag;
	}

	public void setThirdPartyAdTag(Object thirdPartyAdTag) {
	this.thirdPartyAdTag = thirdPartyAdTag;
	}

	public Object getVastSecureUrl() {
	return vastSecureUrl;
	}

	public void setVastSecureUrl(Object vastSecureUrl) {
	this.vastSecureUrl = vastSecureUrl;
	}

	public Boolean getIsFlightAssociated() {
	return isFlightAssociated;
	}

	public void setIsFlightAssociated(Boolean isFlightAssociated) {
	this.isFlightAssociated = isFlightAssociated;
	}

	public Object getThirdPartyUrl() {
	return thirdPartyUrl;
	}

	public void setThirdPartyUrl(Object thirdPartyUrl) {
	this.thirdPartyUrl = thirdPartyUrl;
	}

	public String getWidth() {
	return width;
	}

	public void setWidth(String width) {
	this.width = width;
	}

	public String getHeight() {
	return height;
	}

	public void setHeight(String height) {
	this.height = height;
	}

	public List<Integer> getmime_types() {
	return mime_types;
	}

	public void setmime_types(List<Integer> mime_types) {
	this.mime_types = mime_types;
	}

	public AssetMedia getAssetMedia() {
	return assetMedia;
	}

	public void setAssetMedia(AssetMedia assetMedia) {
	this.assetMedia = assetMedia;
	}

	public List<AssetEvent> getAssetEvents() {
	return assetEvents;
	}

	public void setAssetEvents(List<AssetEvent> assetEvents) {
	this.assetEvents = assetEvents;
	}

	public AssetCompanion getAssetCompanion() {
	return assetCompanion;
	}

	public void setAssetCompanion(AssetCompanion assetCompanion) {
	this.assetCompanion = assetCompanion;
	}

	public String getType() {
	return type;
	}

	public void setType(String type) {
	this.type = type;
	}

	public Boolean getCompanionEnabled() {
	return companionEnabled;
	}

	public void setCompanionEnabled(Boolean companionEnabled) {
	this.companionEnabled = companionEnabled;
	}

	public MimeTypes getMimeTypes() {
	return mimeTypes;
	}

	public void setMimeTypes(MimeTypes mimeTypes) {
	this.mimeTypes = mimeTypes;
	}

	public String getFormatedName() {
	return formatedName;
	}

	public void setFormatedName(String formatedName) {
	this.formatedName = formatedName;
	}

	public Boolean getIsGenerateVast() {
	return isGenerateVast;
	}

	public void setIsGenerateVast(Boolean isGenerateVast) {
	this.isGenerateVast = isGenerateVast;
	}

}
