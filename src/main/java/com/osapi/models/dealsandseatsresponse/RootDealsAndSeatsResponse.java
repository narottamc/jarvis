/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.dealsandseatsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class RootDealsAndSeatsResponse {

    @SerializedName("seller_network_deals")
    @Expose
    private List<SellerNetworkDeal> sellerNetworkDeals = null;
    @SerializedName("seller_network_seats")
    @Expose
    private List<SellerNetworkSeat> sellerNetworkSeats = null;

    public List<SellerNetworkDeal> getSellerNetworkDeals() {
        return sellerNetworkDeals;
    }

    public void setSellerNetworkDeals(List<SellerNetworkDeal> sellerNetworkDeals) {
        this.sellerNetworkDeals = sellerNetworkDeals;
    }

    public List<SellerNetworkSeat> getSellerNetworkSeats() {
        return sellerNetworkSeats;
    }

    public void setSellerNetworkSeats(List<SellerNetworkSeat> sellerNetworkSeats) {
        this.sellerNetworkSeats = sellerNetworkSeats;
    }
}
