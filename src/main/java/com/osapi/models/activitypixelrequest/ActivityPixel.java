/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Clearstream.
 */
public class ActivityPixel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("agency_id")
    @Expose
    private Integer agencyId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("pixel_type_id")
    @Expose
    private Integer pixelTypeId;
    @SerializedName("post_click_hours")
    @Expose
    private Integer postClickHours;
    @SerializedName("post_view_hours")
    @Expose
    private Integer postViewHours;
    @SerializedName("expiration_hours")
    @Expose
    private Integer expirationHours;
    @SerializedName("sync_enabled")
    @Expose
    private Boolean syncEnabled;
    @SerializedName("activity_pixel_parameters_attributes")
    @Expose
    private List<ActivityPixelParametersAttribute> activityPixelParametersAttributes = null;
    @SerializedName("activity_pixels_placement_ids")
    @Expose
    private List<Integer> activityPixelsPlacementIds = null;
    @SerializedName("conversion_setting_attributes")
    @Expose
    private ConversionSettingAttributes conversionSettingAttributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getPixelTypeId() {
        return pixelTypeId;
    }

    public void setPixelTypeId(Integer pixelTypeId) {
        this.pixelTypeId = pixelTypeId;
    }

    public Integer getPostClickHours() {
        return postClickHours;
    }

    public void setPostClickHours(Integer postClickHours) {
        this.postClickHours = postClickHours;
    }

    public Integer getPostViewHours() {
        return postViewHours;
    }

    public void setPostViewHours(Integer postViewHours) {
        this.postViewHours = postViewHours;
    }

    public Integer getExpirationHours() {
        return expirationHours;
    }

    public void setExpirationHours(Integer expirationHours) {
        this.expirationHours = expirationHours;
    }

    public Boolean getSyncEnabled() {
        return syncEnabled;
    }

    public void setSyncEnabled(Boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
    }

    public List<ActivityPixelParametersAttribute> getActivityPixelParametersAttributes() {
        return activityPixelParametersAttributes;
    }

    public void setActivityPixelParametersAttributes(List<ActivityPixelParametersAttribute> activityPixelParametersAttributes) {
        this.activityPixelParametersAttributes = activityPixelParametersAttributes;
    }

    public List<Integer> getActivityPixelsPlacementIds() {
        return activityPixelsPlacementIds;
    }

    public void setActivityPixelsPlacementIds(List<Integer> activityPixelsPlacementIds) {
        this.activityPixelsPlacementIds = activityPixelsPlacementIds;
    }

    public ConversionSettingAttributes getConversionSettingAttributes() {
        return conversionSettingAttributes;
    }

    public void setConversionSettingAttributes(ConversionSettingAttributes conversionSettingAttributes) {
        this.conversionSettingAttributes = conversionSettingAttributes;
    }
}
