/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class RootActivityPixelRequest {

    @SerializedName("activity_pixel")
    @Expose
    private ActivityPixel activityPixel;

    public ActivityPixel getActivityPixel() {
        return activityPixel;
    }

    public void setActivityPixel(ActivityPixel activityPixel) {
        this.activityPixel = activityPixel;
    }
}
