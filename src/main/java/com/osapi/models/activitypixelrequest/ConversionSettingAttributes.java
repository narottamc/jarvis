/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.models.activitypixelrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Clearstream.
 */
public class ConversionSettingAttributes {
    @SerializedName("domain")
    @Expose
    private Object domain;
    @SerializedName("attribution_model_id")
    @Expose
    private Integer attributionModelId;

    public Object getDomain() {
        return domain;
    }

    public void setDomain(Object domain) {
        this.domain = domain;
    }

    public Integer getAttributionModelId() {
        return attributionModelId;
    }

    public void setAttributionModelId(Integer attributionModelId) {
        this.attributionModelId = attributionModelId;
    }
}
