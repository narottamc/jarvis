/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignrequest.BusinessModelCampaignRequest;
import com.osapi.models.campaignrequest.Campaign;
import com.osapi.models.campaignrequest.RootCampaignRequest;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.launchcampaignrequest.RootLaunchCampaignRequest;
import com.osapi.models.newcampaignrequest.NewCampaignRequestCampaign;
import com.osapi.models.newcampaignrequest.RootNewCampaignRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

/**
 * @author ClearStream.
 */
public class CampaignBuilder
{
	private static final Logger LOG = Logger.getLogger(CampaignBuilder.class);
	private String scenarioDataPath;
	private RootLaunchCampaignRequest rootLaunchCampaignRequest;
	private RootNewCampaignRequest rootNewCampaignRequest;
	private RootCampaignRequest rootUpdateCampaignRequest;
	FileWriterUtils fileWriter;

	public CampaignBuilder(String scenarioTestData)
	{

		this.fileWriter = new FileWriterUtils(scenarioTestData);
		this.scenarioDataPath = scenarioTestData;
	}

	public String getScenarioDataPath()
	{
		return scenarioDataPath;
	}

	public void setScenarioDataPath(String scenarioDataPath)
	{
		this.scenarioDataPath = scenarioDataPath;
	}

	public RootLaunchCampaignRequest getRootLaunchCampaignRequest()
	{
		return rootLaunchCampaignRequest;
	}

	public void setRootLaunchCampaignRequest()
	{
		RootCampaignResponse rootCampaignReponse = DependencyManager.getInjector()
				.getInstance(ResponseBuilder.class).getRootCampaignResponse(scenarioDataPath);
		Integer campaignID = rootCampaignReponse.getId();
		RootLaunchCampaignRequest request = MapBuilderfromJsonFile.buildFromTemplate(RootLaunchCampaignRequest.class,
				Constants.LAUNCHCAMPAIGNREQUEST_TEMPLATE_DATA, false).get();
		request.setCampaignId(campaignID);
		this.rootLaunchCampaignRequest = request;
		fileWriter.writeToJsonFile(ObjectType.LAUNCHCAMPAIGN, QueryMethod.REQUEST)
				.writeRequestSingleJsonObjectToJsonFile(request);
	}

	public RootNewCampaignRequest getRootNewCampaignRequest()
	{
		return rootNewCampaignRequest;
	}

	public void setRootNewCampaignRequest(RootNewCampaignRequest rootNewCampaignRequest)
	{
		this.rootNewCampaignRequest = rootNewCampaignRequest;
	}

	public RootCampaignRequest getRootUpdateCampaignRequest()
	{
		return rootUpdateCampaignRequest;
	}

	public void setRootUpdateCampaignRequest(RootCampaignRequest rootCampaignRequest)
	{
		this.rootUpdateCampaignRequest = rootCampaignRequest;
	}

	public void storeUpdatedCampaignRequest(Campaign campaign) 
	{
		RootCampaignRequest requestCampaign = MapBuilderfromJsonFile.build(RootCampaignRequest.class, 
				Constants.CAMPAIGN_REQUEST_DATA, scenarioDataPath, false).get();
		requestCampaign.setCampaign(campaign);
		try
		{
			fileWriter.writeToJsonFile(ObjectType.CAMPAIGNS, QueryMethod.REQUEST)
					.writeRequestSingleJsonObjectToJsonFile(requestCampaign);
		}
		catch (JsonIOException e)
		{

			e.printStackTrace();
		}
	}
	
	public void storeCampaignesponseToJson(String responseCampaignJsonString) 
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.CAMPAIGNS, QueryMethod.RESPONSE)
					.writeResponseSingleJsonObjectToJsonFile(RootCampaignResponse.class, responseCampaignJsonString);
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	public void updateCampaignDetailsWithAgencyAndBrand()
	{
		RootNewCampaignRequest rootCampaignRequest = (MapBuilderfromJsonFile
				.buildFromTemplate(RootNewCampaignRequest.class, Constants.NEWCAMPAIGN_REQUEST_DATA, false).get());
		RootAgencyResponse rootAgencyResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootAgencyResponse(scenarioDataPath);
		RootBrandResponse rootBrandResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootBrandResponse(scenarioDataPath);

		Integer agencyId = rootAgencyResponse.getId();
		String agencyName = rootAgencyResponse.getName();
		Boolean agencyInternal = rootAgencyResponse.getInternal();

		LOG.info(agencyId + agencyName + agencyInternal);

		Integer brandId = rootBrandResponse.getId();
		String brandName = rootBrandResponse.getName();
		String brandLandingPageURL = rootBrandResponse.getLandingPageUrl();
		String brandCode = rootBrandResponse.getCode();

		LOG.info(brandId + brandName + brandLandingPageURL + brandCode);

		NewCampaignRequestCampaign campaign = rootCampaignRequest.getCampaign();

		campaign.setAgencyId(agencyId);
		campaign.getAgency().setName(agencyName);
		campaign.getAgency().setId(agencyId);

		campaign.getSelectedAgency().setId(agencyId);
		campaign.getSelectedAgency().setName(agencyName);
		campaign.getSelectedAgency().setInternal(agencyInternal);

		campaign.setBrandId(brandId);
		campaign.getBrand().setName(brandName);
		campaign.getBrand().setCode(brandCode);
		campaign.getBrand().setLandingPageUrl(brandLandingPageURL);
		campaign.getBrand().setId(brandId);

		campaign.getSelectedbrand().setId(brandId);
		campaign.getSelectedbrand().setName(brandName);
		campaign.getSelectedbrand().setCode(brandCode);
		campaign.getSelectedbrand().setUrl(brandLandingPageURL);
		campaign.getSelectedbrand().setDisplayName(brandCode + " - " + brandName);

		this.setRootNewCampaignRequest(rootCampaignRequest);

		fileWriter.writeToJsonFile(ObjectType.CAMPAIGNS, QueryMethod.REQUEST)
				.writeRequestSingleJsonObjectToJsonFile(rootCampaignRequest);
	}
	
	public void assignBusinessModelToCampaign(String businessModel,Integer strategyId)
	{
		RootCampaignRequest requestCampaign = MapBuilderfromJsonFile.build(RootCampaignRequest.class, 
				Constants.CAMPAIGN_REQUEST_DATA, scenarioDataPath, false).get();
		BusinessModelBuilder businessModelBuilder = new BusinessModelBuilder();
		LOG.info("Allocating Business Model "+ businessModel + " to Campaign");
		BusinessModelCampaignRequest businessModelCampaign = businessModelBuilder.getBusinessModelHandler(strategyId).buildBusinessModelForType();
		requestCampaign.getCampaign().setBusinessModel(businessModelCampaign);
		this.setRootUpdateCampaignRequest(requestCampaign);
	}

	public String getURLForLaunchCampaign()
	{
		RootCampaignResponse rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioDataPath);
		Integer campaignID = rootCampaignResponse.getId();
		String ENDPOINT_URL = Constants.CAMPAIGN_BASEENDPOINT_URL + "/" + campaignID + "/start";
		LOG.info("Launch Campaign URL " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}

	public String getURLForActiveCampaign()
	{
		RootCampaignResponse rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioDataPath);
		Integer campaignID = rootCampaignResponse.getId();
		String ENDPOINT_URL = Constants.CAMPAIGN_BASEENDPOINT_URL + "/" + campaignID;
		LOG.info("Active Campaign URL " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}
	
	public String getURLForUpdateCampaign()
	{
		RootCampaignRequest requestCampaign = MapBuilderfromJsonFile.build(RootCampaignRequest.class, 
				Constants.CAMPAIGN_REQUEST_DATA, scenarioDataPath, false).get();
		Integer campaignID = requestCampaign.getCampaign().getId();
		String ENDPOINT_URL = Constants.CAMPAIGN_BASEENDPOINT_URL + "/" + campaignID;
		LOG.info("Update Campaign URL " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}

	public String getURLForDeleteCampaign()
	{
		RootCampaignRequest requestCampaign = MapBuilderfromJsonFile.build(RootCampaignRequest.class,
				Constants.CAMPAIGN_REQUEST_DATA, scenarioDataPath, false).get();
		Integer campaignID = requestCampaign.getCampaign().getId();
		String campaignURL = Constants.CAMPAIGN_BASEENDPOINT_URL.replace("v2/","");
		String ENDPOINT_URL = campaignURL + "/" + campaignID;
		LOG.info("Delete Campaign URL " + ENDPOINT_URL);
		return  ENDPOINT_URL;
	}

}
