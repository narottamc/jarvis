/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.MediaInventoryType;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.mediainventoryrequest.*;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * @author ClearStream.
 */
public class MediaInventoryBuilder
{

	private static final Logger LOG = Logger.getLogger(MediaInventoryBuilder.class);

	
	private RootCampaignResponse rootCampaignResponse;
	private List<RootPlacementResponse> rootPlacementResponseList;
	private RootMediaListRequest rootMediaListRequest;
	private RootSmartListRequest rootSmartListRequest;

	public MediaInventoryBuilder(String scenarioTestData)
	{
		rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestData);
		rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getPlacementResponseList();
	}

	public RootMediaListRequest getRootMediaListRequest()
	{
		return rootMediaListRequest;
	}

	public void setRootMediaListRequest(Integer placementNumber)
	{
		Integer type = MediaInventoryType.getRandomMediaInventoryType().getCode();
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		Integer campaignId = rootCampaignResponse.getId();
		LOG.info("Creating MediaList Request For Placment Id "+ placementId +" of Type "+ MediaInventoryType.fromCode(type));
		RootMediaListRequest request = MapBuilderfromJsonFile.buildFromHashMap(false,RootMediaListRequest.class
				,Constants.MEDIALISTS_REQUEST_TEMPLATE_DATA).getInstanceMap().get(type);

		for (MediaList mediaList : request.getMediaLists())
		{
			ItemStateChangeInfo itemState = mediaList.getItemStateChangeInfo();
			itemState.getItemReturnParams().setCampaignId(campaignId);
			itemState.getItemReturnParams().setPlacementId(placementId.toString());
		}
		request.setPlacementId(placementId);
		this.rootMediaListRequest = request;

	}

	public RootSmartListRequest getRootSmartListRequest()
	{
		return rootSmartListRequest;
	}

	public void setRootSmartListRequest(Integer placementNumber)
	{
		Integer type = MediaInventoryType.getRandomMediaInventoryType().getCode();
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		Integer campaignId = rootCampaignResponse.getId();
		LOG.info("Creating SmartList Request For Placment Id "+ placementId +" of Type "+ MediaInventoryType.fromCode(type));
		RootSmartListRequest request = MapBuilderfromJsonFile.buildFromHashMap(false,RootSmartListRequest.class
				,Constants.SMARTLISTS_REQUEST_TEMPLATE_DATA).getInstanceMap().get(type);

		for (SmartList smartList : request.getSmartLists())
		{
			ItemStateChangeInfo itemState = smartList.getItemStateChangeInfo();
			itemState.getItemReturnParams().setCampaignId(campaignId);
			itemState.getItemReturnParams().setPlacementId(placementId.toString());
		}
		request.setPlacementId(placementId);
		this.rootSmartListRequest = request;
	}

	public String getEndPointURLForMediaInventory(Integer placementNumber, String listType)
	{
		String ENDPOINT_URL = null;
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId().intValue();
		String placmentURL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/", "");
		switch (listType.toLowerCase())
		{
		case "medialist":
			ENDPOINT_URL = placmentURL + "/" + placementId + "/" + "media_lists";
			LOG.info(
					"Media List URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " " + ENDPOINT_URL);
			break;

		case "smartlist":
			ENDPOINT_URL = placmentURL + "/" + placementId + "/" + "smart_lists";
			LOG.info(
					"Smart List URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " " + ENDPOINT_URL);
			break;
		}
		return ENDPOINT_URL;
	}

	public static void  storeMediaInventoryRequestToJsonFile(String scenarioTestData){
		FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
		try
		{
			fileWriter.writeToJsonFile(ObjectType.MEDIALIST, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootMediaListRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootMediaListRequestList());
			fileWriter.writeToJsonFile(ObjectType.SMARTLIST, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootSmartListRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootSmartListRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

}
