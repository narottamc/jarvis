/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.AssetSize;
import com.osapi.enumtypes.DisplayAdScriptTypes;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.displayrequest.DisplayEvent;
import com.osapi.models.displayrequest.DisplayRequest;
import com.osapi.models.displayrequest.RootDisplayRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author ClearStream.
 */
public class DisplayBuilder
{

	
	public interface DisplayRequestBuilder
	{
		RootDisplayRequest buildDisplayRequestWithType(Integer DisplayNumber, String diplayType,String assetSize);
	}

	private static final Logger LOG = Logger.getLogger(DisplayBuilder.class);
	private RootDisplayRequest rootDisplayRequest;
	private RootCampaignResponse rootCampaignResponse;
	private Map<String, DisplayRequestBuilder> displayTypeHandler = new HashMap<>();
	private RootBrandResponse brandResponse;
	FileWriterUtils fileWriter;

	public DisplayBuilder(String scenarioTestDataPath)
	{
		this.rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestDataPath);
		this.fileWriter = new FileWriterUtils(scenarioTestDataPath);
		this.brandResponse = MapBuilderfromJsonFile.build(RootBrandResponse.class,Constants.BRAND_RESPONSE_DATA,scenarioTestDataPath,false).get();

	}

	public RootDisplayRequest getRootDisplayRequest()
	{
		return rootDisplayRequest;
	}

	public void setRootDisplayRequest(RootDisplayRequest rootDisplayRequest)
	{
		this.rootDisplayRequest = rootDisplayRequest;
	}

	public Map<String, DisplayRequestBuilder> getDisplayTypeHandler()
	{
		return displayTypeHandler;
	}

	public void setDisplayTypeHandler(String fileKey)
	{
		displayTypeHandler.put("SCRIPTTAG", new ScriptTagAssetBuilder());
		displayTypeHandler.put("ZIP", new ZipDisplayAssetBuilder(fileKey));
		displayTypeHandler.put("IMAGE", new ImageDisplayAssetBuilder(fileKey));

	}

	public void buildDisplayRequest(Integer displayNumber, String displayType, String fileKey,String assetSize)
	{
		this.setDisplayTypeHandler(fileKey);
		DisplayRequestBuilder displayRequestBuilder = this.getDisplayTypeHandler().get(displayType.toUpperCase());
		this.setRootDisplayRequest(displayRequestBuilder.buildDisplayRequestWithType(displayNumber, displayType,assetSize));
	}

	public String getDisplayEndPointURL()
	{
		Integer campaignID = rootCampaignResponse.getId();
		String campaignURL = Constants.CAMPAIGN_BASEENDPOINT_URL.replace("v2/", "");
		String ENDPOINT_URL = campaignURL + "/" + campaignID + "/displays";
		LOG.info("Display URL " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}
	public String getDisplayEndPointURLForGETRequest(Integer displayId)
	{
		Integer campaignID = rootCampaignResponse.getId();
		String campaignURL = Constants.CAMPAIGN_BASEENDPOINT_URL.replace("v2/", "");
		String ENDPOINT_URL = campaignURL + "/" + campaignID + "/displays/"+displayId;
		LOG.info("Display URL For GET Request " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}

	public void writeDisplayRequestToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.DISPLAY, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
					RootDisplayRequest.class,
					DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootDisplayRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	
	public class ScriptTagAssetBuilder implements DisplayRequestBuilder
	{

		@Override
		public RootDisplayRequest buildDisplayRequestWithType(Integer displayNumber, String diplayType,String assetSize)
		{
			final String SCRIPT_ADTAG_CONTENT = "<script type=\"text/javascript\">var mixpoAd={guid:\"61495a14-df83-4e0b-b9b2-4a47b87b9146\",subdomain:\"www\",embedv:\"mixpo.com:dcf9aa1,thorwhal:74f334a,obsidian:f916afc-js\",clicktag:\"CLICK_TRACKER\", customTarget:\"{zipcode}\"};</script><script type=\"text/javascript\" src=\"https://swf.mixpo.com/js/loader.js\"></script>";
			Random randomNumber = new Random();
			RootDisplayRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootDisplayRequest.class, Constants.DISPLAYREQUEST_TEMPLATE_DATA, false).get();
			Integer scriptTypeId = randomNumber.ints(1,1,4).findFirst().getAsInt();
			Boolean isScriptTag = Math.random()<0.5;
			String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
			DisplayRequest displayRequest = request.getDisplay();
			displayRequest.setName("Display" + displayNumber + "_" + diplayType);
			displayRequest.setAdContent(DisplayAdScriptTypes.getDisplayAdScriptTagTypeFromId(scriptTypeId).getAdTagScript(false,isScriptTag));
			displayRequest.setAdContentSrc(DisplayAdScriptTypes.getDisplayAdScriptTagTypeFromId(scriptTypeId).getAdTagScript(true,isScriptTag));
			displayRequest.setDisplayTypeId(1);
			displayRequest.setScriptTypeId(scriptTypeId);
			displayRequest.setLandingPageDomain(brandResponse.getLandingPageUrl());
			displayRequest.setAdTagTypeId(2);
			displayRequest.setHeight(assetDimension.split("x")[1]);
			displayRequest.setWidth(assetDimension.split("x")[0]);
			request.setCampaignId(rootCampaignResponse.getId());
			return request;
		}

	}
	
	public class ZipDisplayAssetBuilder implements DisplayRequestBuilder
	{
		private String fileKey;

		public ZipDisplayAssetBuilder(String fileKey)
		{
			this.fileKey = fileKey;
		}

		@Override
		public RootDisplayRequest buildDisplayRequestWithType(Integer displayNumber, String diplayType,String assetSize)
		{
			RootDisplayRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootDisplayRequest.class, Constants.DISPLAYREQUEST_TEMPLATE_DATA, false).get();
			String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
			DisplayRequest displayRequest = request.getDisplay();
			displayRequest.setName("Display" + displayNumber + "_" + diplayType);
			displayRequest.setDisplayTypeId(2);
			displayRequest.setLandingPageDomain(brandResponse.getLandingPageUrl());
			displayRequest.setAdTagTypeId(2);
			displayRequest.setFileKey(fileKey);
			displayRequest.setFileType("application/x-zip-compressed");
			displayRequest.setAdContent(null);
			displayRequest.setHeight(assetDimension.split("x")[1]);
			displayRequest.setWidth(assetDimension.split("x")[0]);
			request.setCampaignId(rootCampaignResponse.getId());
			return request;
		}

	}

	
	public class ImageDisplayAssetBuilder implements DisplayRequestBuilder
	{
		private String fileKey;

		public ImageDisplayAssetBuilder(String fileKey)
		{
			this.fileKey = fileKey;
		}

		@Override
		public RootDisplayRequest buildDisplayRequestWithType(Integer displayNumber, String diplayType,String assetSize)
		{
			RootDisplayRequest request = MapBuilderfromJsonFile
					.buildFromTemplate(RootDisplayRequest.class, Constants.DISPLAYREQUEST_TEMPLATE_DATA, false).get();
			String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
			DisplayRequest displayRequest = request.getDisplay();
			displayRequest.setName("Display" + displayNumber + "_" + diplayType);
			displayRequest.setDisplayTypeId(3);
			displayRequest.setLandingPageDomain(brandResponse.getLandingPageUrl());
			displayRequest.setFileKey(fileKey);
			displayRequest.setFileType("image/jpeg");
			displayRequest.setAdTagTypeId(2);
			List<DisplayEvent> displayEventList = new ArrayList<DisplayEvent>();
			DisplayEvent displayEvent = new DisplayEvent();
			displayEvent.setEventType("ClickThrough");
			displayEvent.setTrackingUrl("www.google.com");
			displayEventList.add(displayEvent);
			displayRequest.setHeight(assetDimension.split("x")[1]);
			displayRequest.setWidth(assetDimension.split("x")[0]);
			displayRequest.setDisplayEvents(displayEventList);
			request.setCampaignId(rootCampaignResponse.getId());
			return request;
		}

	}

}
