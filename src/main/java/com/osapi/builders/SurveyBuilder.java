/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.AssetSize;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.surveyrequest.QuestionSurveyRequest;
import com.osapi.models.surveyrequest.RootSurveyRequest;
import com.osapi.models.surveyrequest.SurveyRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * @author ClearStream.
 */
public class SurveyBuilder
{
	private static final Logger LOG = Logger.getLogger(SurveyBuilder.class);
	private String scenarioTestDataPath;
	private RootSurveyRequest rootSurveyRequest;
	private RootCampaignResponse rootCampaignResponse;
	FileWriterUtils fileWriter;
	private static final String customResponseString = "<!DOCTYPE html>" + "<html>" + "<body>"
			+ "<p><font color=\"red\">Thank You for your Response</font></p>" + "</body>" + "</html>";

	public SurveyBuilder(String scenarioTestDataPath)
	{
		this.scenarioTestDataPath = scenarioTestDataPath;
		this.rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestDataPath);
		this.fileWriter = new FileWriterUtils(scenarioTestDataPath);
	}

	public RootSurveyRequest getRootSurveyRequest()
	{
		return rootSurveyRequest;
	}

	public void setRootSurveyRequest(RootSurveyRequest rootSurveyRequest)
	{
		this.rootSurveyRequest = rootSurveyRequest;
	}

	public void buildSurveyRequest(Integer surveyNumber, String answerType, String customResponse,String assetSize)
	{
		Integer campaignId = rootCampaignResponse.getId();
		RootSurveyRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootSurveyRequest.class, Constants.SURVEYREQUEST_TEMPLATE_DATA, false).get();
		SurveyRequest surveyRequest = request.getSurvey();
		String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
		List<QuestionSurveyRequest> questionsList = surveyRequest.getQuestions();
		if (answerType.toLowerCase().contentEquals("multiple"))
		{
			questionsList.get(0).setQuestionType(2);
		}
		if (customResponse.toLowerCase().contentEquals("yes"))
		{
			questionsList.get(0).setResponse(customResponseString);
		}
		surveyRequest.setQuestions(questionsList);
		surveyRequest.setName("Survey_" + surveyNumber + "_" + answerType);
		surveyRequest.setHeight(assetDimension.split("x")[1]);
		surveyRequest.setWidth(assetDimension.split("x")[0]);
		request.setSurvey(surveyRequest);
		request.setCampaignId(campaignId.toString());
		
		this.setRootSurveyRequest(request);
	}

	public String getSurveyEndPointURL()
	{
		Integer campaignID = rootCampaignResponse.getId();
		String campaignURL = Constants.CAMPAIGN_BASEENDPOINT_URL.replace("v2/", "");
		String ENDPOINT_URL = campaignURL + "/" + campaignID + "/surveys";
		LOG.info("Survey URL " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}

	public void writeSurveyRequestToJsonFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.SURVEY, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
					RootSurveyRequest.class,
					DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootSurveyRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
