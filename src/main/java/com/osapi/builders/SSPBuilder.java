/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.enumtypes.SellerNetworks;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.dealsandseatsrequest.RootDealsAndSeatsRequest;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.ssprequest.RootSSPRequest;
import com.osapi.models.sspresponse.RootSSPResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Clearstream.
 */
public class SSPBuilder {
    public static Random random = new Random();
    private static final Logger LOG = Logger.getLogger(SSPBuilder.class);
    private RootSSPRequest rootSSPRequest;
    private RootDealsAndSeatsRequest rootDealsAndSeatsRequest;
    private  String sspName = SellerNetworks.getRandomSellerNetwork().toString();
    private  String sellerNetworkDeal = "Auto_" + String.valueOf(random.nextInt(5000));
    private  String sellerNetworkSeat = "Auto_" + String.valueOf(random.nextInt(5000));

    public RootSSPRequest getRootSSPRequest() {
        return rootSSPRequest;
    }

    public void setRootSSPRequest(RootSSPRequest rootSSPRequest) {
        this.rootSSPRequest = rootSSPRequest;
    }

    public RootDealsAndSeatsRequest getRootDealsAndSeatsRequest() {
        return rootDealsAndSeatsRequest;
    }

    public void setRootDealsAndSeatsRequest(RootDealsAndSeatsRequest rootDealsAndSeatsRequest) {
        this.rootDealsAndSeatsRequest = rootDealsAndSeatsRequest;
    }

    public void buildRequestToAddExistingDealsandSeats(Integer placementId) {
        RootSSPRequest rootSSPRequest = MapBuilderfromJsonFile.buildFromTemplate(RootSSPRequest.class, Constants.SSPREQUEST_TEMPLATE_DATA, false).get();
        rootSSPRequest.setSspId(1);
        rootSSPRequest.setName(sspName);
        rootSSPRequest.setPlacementId(String.valueOf(placementId));
        rootSSPRequest.setSellerNetworkSeat(sellerNetworkDeal);
        rootSSPRequest.setSellerNetworkDeal(sellerNetworkSeat);
        this.setRootSSPRequest(rootSSPRequest);
    }

    public void buildRequestToAddDealAndSeatsToPlacement(){
        RootDealsAndSeatsRequest dealsAndSeatsRequest = MapBuilderfromJsonFile.buildFromTemplate(RootDealsAndSeatsRequest.class,
                Constants.DEALSANDSSEATSPLACEMENTREQUEST_TEMPLATE_DATA, false).get();
        List<Integer> dealsIds = new ArrayList<>();
        List<Integer> seatsIds = new ArrayList<>();
        RootDealsAndSeatsResponse dealsAndSeatsResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDealsAndSeatsResponseList().get(0);
        for (int j = 0; j < dealsAndSeatsResponse.getSellerNetworkDeals().size(); j++) {
            dealsIds.add(dealsAndSeatsResponse.getSellerNetworkDeals().get(j).getId());
        }
        for (int k = 0; k < dealsAndSeatsResponse.getSellerNetworkSeats().size(); k++) {
            seatsIds.add(dealsAndSeatsResponse.getSellerNetworkSeats().get(k).getId());
        }
        dealsAndSeatsRequest.setDeals(dealsIds);
        dealsAndSeatsRequest.setSeats(seatsIds);
        this.setRootDealsAndSeatsRequest(dealsAndSeatsRequest);
    }

    public static void writePOSTMethodSellerNetworkMastersRequestData(String scenarioTestData) {
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.SSP, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile
                    (RootSSPRequest.class, DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootSSPRequestList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    public static void writePOSTMethodSellerNetworkMastersResponseData(String scenarioTestData) {
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.SSP, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile
                    (RootSSPResponse.class, DependencyManager.getInjector().getInstance(ResponseBuilder.class).getRootSSPResponseList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static void writeGETMethodDealsAndSeatsResponseData(String scenarioTestData) {
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.DEALSANDSEATS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile
                    (RootDealsAndSeatsResponse.class, DependencyManager.getInjector().getInstance(ResponseBuilder.class).getDealsAndSeatsResponseList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static void writePUTMethodDealsAndSeatsRequestData(String scenarioTestData) {
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.DEALSANDSEATS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile
                    (RootDealsAndSeatsRequest.class, DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootDealsAndSeatsRequestList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    public String getEndPointURLForAddDealsAndSeats() {
        String endPointUrl = null;
        endPointUrl = Constants.SELLERNETWORKMASTERS_BASEENDPOINT_URL;
        LOG.info("End Point URL For POST Request of Seller Network Masters is " + endPointUrl);
        return endPointUrl;
    }

    public String getEndPointURLForDealsAndSeatsOfPlacement(Integer placementId) {
        String endPointURL = null;
        String placmentURL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/", "");
        endPointURL = placmentURL + "/" + placementId + "/" + "deals_and_seats";
        LOG.info("End Point URL For GET Request for Placement's Deals And Seats is " + endPointURL);
        return endPointURL;
    }

}
