/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.enumtypes.Users;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.appusersrequest.RootCreateUserRequest;
import com.osapi.models.appusersrequest.User;
import com.osapi.models.authentication.RootLogInRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;
import org.ini4j.Ini;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Clearstream.
 *
 */
public class AuthenticationBuilder
{
	private static final Logger LOG = Logger.getLogger(AuthenticationBuilder.class);
	private RootCreateUserRequest rootCreateUserRequest;
	private RootLogInRequest rootLogInRequest;
	private RootLogInRequest guestLogInRequest;
	private FileWriterUtils fileWriter = new FileWriterUtils(DependencyManager.getInjector().getInstance(RequestBuilder.class).getScenarioTestDatPath());
	
	public RootLogInRequest getRootLogInRequest()
	{
		return rootLogInRequest;
	}

	public void setRootLogInRequest(RootLogInRequest rootLogInRequest)
	{
		this.rootLogInRequest = rootLogInRequest;
	}
	
	public RootLogInRequest getGuestLogInRequest()
	{
		return guestLogInRequest;
	}

	public void setGuestLogInRequest(RootLogInRequest guestLogInRequest)
	{
		this.guestLogInRequest = guestLogInRequest;
	}
	
	public RootCreateUserRequest getRootCreateUserRequest()
	{
		return rootCreateUserRequest;
	}

	public void setRootCreateUserRequest(RootCreateUserRequest rootCreateUserRequest)
	{
		this.rootCreateUserRequest = rootCreateUserRequest;
	}

	public  void buildLogInRequest(String userType) 
	{
		RootLogInRequest request = MapBuilderfromJsonFile.buildFromTemplate(RootLogInRequest.class, Constants.LOGINREQUEST_TEMPLATE_DATA, false).get();
		String emailId = this.getCrendentialsForUserType(userType).keySet().stream().findFirst().get();
		request.setEmail(emailId);
		request.setPassword(this.getCrendentialsForUserType(userType).get(emailId));
		if(DependencyManager.getInjector().getInstance(RequestBuilder.class).getScenarioTestDatPath()!=null) {
			fileWriter.writeToJsonFile(ObjectType.SIGN_IN, QueryMethod.REQUEST).writeRequestSingleJsonObjectToJsonFile(request);
		}
		this.setRootLogInRequest(request);
	}
	
	public void buildCreateUserRequest(String user) {
		User newUser = new User();
		newUser.setIsFinance(Users.valueOf(user.toUpperCase()).getIsInternalAdmin());
		newUser.setFirstName(Users.valueOf(user.toUpperCase()).getFirstName());
		newUser.setLastName(Users.valueOf(user.toUpperCase()).getLastName());
		newUser.setEmail(Users.valueOf(user.toUpperCase()).getEmailId());
		newUser.setPassword(Users.valueOf(user.toUpperCase()).getPassword());
		newUser.setPasswordConfirmation(Users.valueOf(user.toUpperCase()).getConfirmPassword());
		RootCreateUserRequest createNewUser = new RootCreateUserRequest();
		createNewUser.setUser(newUser);
		this.setRootCreateUserRequest(createNewUser);
	}
	
	public  Map<String,String> getCrendentialsForUserType(String userType) 
	{
		Map<String,String> userCrendentials = new HashMap<String,String>();
		Ini testDataIni = new Ini();
		try
		{
			testDataIni.load(Constants.TESTDATACONFIG);
			LOG.info("Lodaing Crendentials for"+ " " + userType + " " + "user");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		Ini.Section section = testDataIni.get(QueryMethod.AUTHENTICATION.toString());
		String credentials = section.get(userType);
		userCrendentials.put(credentials.split(";")[0], credentials.split(";")[1]);
		return userCrendentials;
	}

	

	
}
