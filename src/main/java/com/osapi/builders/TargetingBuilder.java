/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.enumtypes.TargetingCriteria;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.targetingrequest.*;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ClearStream.
 */
public class TargetingBuilder {
    private static final Logger LOG = Logger.getLogger(TargetingBuilder.class);

    private String scenarioTestDataPath;
    private RootCampaignResponse rootCampaignResponse;
    private List<RootPlacementResponse> rootPlacementResponseList;
    private RootTechnologyRequest rootTechnologyRequest;
    private RootGeoRequest rootGeoRequest;
    private RootCategoryRequest rootCategoryRequest;
    private RootDayPartRequest rootDaypartRequest;
    private RootAditionalOptionsRequest rootAddtionalOptionRequest;


    public TargetingBuilder(String scenarioTestData) {
        this.scenarioTestDataPath = scenarioTestData;
        rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
                .getRootCampaignResponse(scenarioTestData);
        rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
                .getPlacementResponseList();
    }

    public RootTechnologyRequest getRootTechnologyRequest() {
        return rootTechnologyRequest;
    }

    public void setRootTechnologyRequest(Integer placementNumber) {
        Random randomNumber = new Random();
        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        RootTechnologyRequest request = MapBuilderfromJsonFile
                .buildFromTemplate(RootTechnologyRequest.class, Constants.TECHNOLOGIES_REQUEST_TEMPLATE_DATA, false)
                .get();
        request.setId(placementId);
        request.setInventoryTypeId(String.valueOf(randomNumber.ints(1, 1, 2).findFirst().getAsInt()));
        this.rootTechnologyRequest = request;
    }

    public RootGeoRequest getRootGeoRequest() {
        return rootGeoRequest;
    }

    public void setRootGeoRequest(Integer placementNumber) {
        GeoTargeting geoTargeting = new GeoTargeting();

        List<Geo> countryList = TargetingCriteria.COUNTIRES.getGeoAttributeDataForPUTRequest();
        List<Geo> regionsList = TargetingCriteria.REGIONS.getGeoRegionAttributeDataForPUTRequest(countryList.get(0).getCountryId());
        List<Geo> dmasList = TargetingCriteria.DMAS.getGeoAttributeDataForPUTRequest();
        List<Language> languageList = TargetingCriteria.LANGUAGES.getGeoLanguageAttributeDataForPUTRequest();
        List<GeoList> zipCodesList = TargetingCriteria.GEOLIST.getGeoListAttributeDataForPUTRequest();

        geoTargeting.setGeos(Stream.of(countryList, regionsList, dmasList).flatMap(Collection::stream).collect(Collectors.toList()));
        geoTargeting.setGeoLists(zipCodesList);
        geoTargeting.setLanguages(languageList);
        geoTargeting.setZipAntiTargeting(Math.random() < 0.5);
        geoTargeting.setPassEmptyLanguage(Math.random() < 0.5);
        geoTargeting.setDmaAntiTargeting(Math.random() < 0.5);

        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        RootGeoRequest request = MapBuilderfromJsonFile
                .buildFromTemplate(RootGeoRequest.class, Constants.GEOS_REQUEST_TEMPLATE_DATA, false).get();
        request.setId(placementId);
        request.setGeosTargeting(geoTargeting);
        this.rootGeoRequest = request;
    }

    public RootCategoryRequest getRootCategoryRequest() {
        return rootCategoryRequest;
    }

    public void setRootCategoryRequest(Integer placementNumber) {
        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        RootCategoryRequest request = MapBuilderfromJsonFile
                .buildFromTemplate(RootCategoryRequest.class, Constants.CATEGORIES_REQUEST_TEMPLATE_DATA, false).get();
        request.setId(placementId);
        request.setTargetCategories(TargetingCriteria.TARGETCATEGORIES.getTargetCategoryAttributeDataForPUTRequest());
        request.setHitOnUnknownResults(Math.random() < 0.5);
        this.rootCategoryRequest = request;
    }

    public RootDayPartRequest getRootDaypartRequest() {
        return rootDaypartRequest;
    }

    public void setRootDaypartRequest(Integer placementNumber) {
        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        RootDayPartRequest request = MapBuilderfromJsonFile
                .buildFromTemplate(RootDayPartRequest.class, Constants.DAYPARTS_REQUEST_TEMPLATE_DATA, false).get();
        request.setId(placementId);
        for (int i = 0; i < request.getDayParts().size(); i++) {
            request.getDayParts().get(i).setHours(TargetingCriteria.DAYPARTS.getDayPartsAttributeDataForPUTRequest());
        }
        this.rootDaypartRequest = request;
    }

    public RootAditionalOptionsRequest getRootAddtionalOptionRequest() {
        return rootAddtionalOptionRequest;
    }

    public void setRootAddtionalOptionRequest(Integer placementNumber) {
        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        RootAditionalOptionsRequest request = MapBuilderfromJsonFile.buildFromTemplate(
                RootAditionalOptionsRequest.class, Constants.ADDTIONALOPTION_REQUEST_TEMPLATE_DATA, false).get();
        request.setId(placementId);
        for (int i = 0; i < request.getVisibilities().size(); i++) {
            request.getVisibilities().get(i).setSelected(Math.random() < 0.5);
        }
        for (int i = 0; i < request.getPlayerSizeGroups().size(); i++) {
            request.getPlayerSizeGroups().get(i).setSelected(Math.random() < 0.5);
        }
        this.rootAddtionalOptionRequest = request;
    }

    public String getEndPointURLTargeting(String type, Integer placementNumber) {
        String ENDPOINT_URL = null;
        Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
        String placmentURL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/", "");
        switch (type.toLowerCase()) {

            case "technologies":
                ENDPOINT_URL = placmentURL + "/" + placementId + "/" + type.toLowerCase();
                LOG.info("Technlogies URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " "
                        + ENDPOINT_URL);
                break;

            case "geos":
                ENDPOINT_URL = placmentURL + "/" + placementId + "/" + type.toLowerCase();
                LOG.info("Geos  URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " " + ENDPOINT_URL);
                break;

            case "categories":
                ENDPOINT_URL = placmentURL + "/" + placementId + "/" + type.toLowerCase();
                LOG.info(
                        "Categories URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " " + ENDPOINT_URL);
                break;

            case "dayparts":
                ENDPOINT_URL = placmentURL + "/" + placementId + "/" + "day_parts";
                LOG.info("Day Parts URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " " + ENDPOINT_URL);
                break;

            case "additionaloptions":
                ENDPOINT_URL = placmentURL + "/" + placementId + "/" + "additional_options";
                LOG.info("Additional Options URL" + " " + "for Placement" + " " + placementNumber + " " + "is" + " "
                        + ENDPOINT_URL);
                break;
        }
        return ENDPOINT_URL;
    }

    public String getEndPointURLForZipCodes(Integer geoListId) {
        return Constants.ZIPCODES_BASEENDPOINT_URL + "/" + geoListId;
    }

    public static void writeMasterDataJsonResponseToJsonFile(Map<ObjectType, String> responseDataMap) {
        for (ObjectType key : responseDataMap.keySet()) {
            key.writeEntityResponseToJsonFile(key.buildHashMapConvertedResponse(responseDataMap.get(key)));
        }
    }

    public static void writeUpdatedTargetingRequestToJsonFile(String scenarioTestDataPath) {
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestDataPath);
        try {
            fileWriter.writeToJsonFile(ObjectType.TECHNOLOGY, QueryMethod.REQUEST)
                    .writeResponseJsonArrayObjectToJsonFile(RootTechnologyRequest.class, DependencyManager.getInjector()
                            .getInstance(RequestBuilder.class).getTechnologyRequestList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {

            e.printStackTrace();
        }

        try {
            fileWriter.writeToJsonFile(ObjectType.GEOS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
                    RootGeoRequest.class,
                    DependencyManager.getInjector().getInstance(RequestBuilder.class).getTargetingRequestGeoList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {

            e.printStackTrace();
        }

        try {
            fileWriter.writeToJsonFile(ObjectType.CATEGORIES, QueryMethod.REQUEST)
                    .writeResponseJsonArrayObjectToJsonFile(RootCategoryRequest.class,
                            DependencyManager.getInjector().getInstance(RequestBuilder.class).getCategoryRequestList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {

            e.printStackTrace();
        }

        try {
            fileWriter.writeToJsonFile(ObjectType.DAYPART, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
                    RootDayPartRequest.class,
                    DependencyManager.getInjector().getInstance(RequestBuilder.class).getDayPartRequestList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {

            e.printStackTrace();
        }

        try {
            fileWriter.writeToJsonFile(ObjectType.ADDITIONALOPTIONS, QueryMethod.REQUEST)
                    .writeResponseJsonArrayObjectToJsonFile(RootAditionalOptionsRequest.class, DependencyManager
                            .getInjector().getInstance(RequestBuilder.class).getAddtionalOptionsRequestList());
        } catch (JsonIOException | InstantiationException | IllegalAccessException e) {

            e.printStackTrace();
        }
    }
}
