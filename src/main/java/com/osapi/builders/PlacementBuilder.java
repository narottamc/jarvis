/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.ProductType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.placementrequest.RootPlacementRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author ClearStream.
 */
public class PlacementBuilder
{

	public interface AssetsPlacementBuilder
	{
		List<Integer> getAssetsList(Integer placementNumber);
	}

	private static final Logger LOG = Logger.getLogger(PlacementBuilder.class);

	private String scenarioTestDataPath;
	private RootPlacementRequest rootPlacementRequest;
	private RootCampaignResponse rootCampaignResponse;
	private List<RootPlacementRequest> rootPlacementRequestList;
	FileWriterUtils fileWriter;
	private Map<ProductType, AssetsPlacementBuilder> productHandler = new HashMap<ProductType, AssetsPlacementBuilder>();
	private static Map<String, String> placementRequestMap;
	private static List<String> placementRequestMapKeysList;

	public PlacementBuilder()
	{

	}

	public PlacementBuilder(String scenarioTestData, Map<String, String> placementRequestMap)
	{
		this.scenarioTestDataPath = scenarioTestData;
		this.rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestData);
		rootPlacementRequestList = MapBuilderfromJsonFile
				.build(RootPlacementRequest.class, Constants.PLACEMENT_REQUEST_DATA, scenarioTestData, true)
				.getAsCollection();
		this.fileWriter = new FileWriterUtils(scenarioTestDataPath);
		this.setProductHandler();
		PlacementBuilder.placementRequestMap = placementRequestMap;
		this.setPlacementRequestMapKeysList(placementRequestMap);

	}

	public Map<String, String> getPlacementRequestMap()
	{
		return placementRequestMap;
	}

	public static List<String> getPlacementRequestMapKeysList()
	{
		return placementRequestMapKeysList;
	}

	public void setPlacementRequestMapKeysList(Map<String, String> placementRequestMap)
	{
		List<String> keysList = new ArrayList<>();
		for (Map.Entry<String, String> keyEntries : placementRequestMap.entrySet())
		{
			keysList.add(keyEntries.getKey());
		}
		PlacementBuilder.placementRequestMapKeysList = keysList;
	}

	@Singleton
	public static class VideoAssetRequestBuilder implements AssetsPlacementBuilder
	{
		private Queue<Integer> vastAssetIdQueue = new LinkedList<>();
		private Queue<Integer> vpaidAssetIdQueue = new LinkedList<>();

		@Inject
		public VideoAssetRequestBuilder()
		{

		}

		public void setVastAssetIdQueue()
		{
			Queue<Integer> vastAssetIds = new LinkedList<>();
			LinkedList<Integer> vastAssetIdList = DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.getVideoTypeAssetIdList("vast");
			for (int i = 0; i < vastAssetIdList.size(); i++)
			{
				vastAssetIds.add(vastAssetIdList.get(i));
			}
			this.vastAssetIdQueue = vastAssetIds;
		}

		public Queue<Integer> getVpaidAssetIdQueue()
		{
			return vpaidAssetIdQueue;
		}

		public void setVpaidAssetIdQueue()
		{
			Queue<Integer> vpaidAssetIds = new LinkedList<>();
			LinkedList<Integer> vpaidAssetIdList = DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.getVideoTypeAssetIdList("vpaid");
			for (int i = 0; i < vpaidAssetIdList.size(); i++)
			{
				vpaidAssetIds.add(vpaidAssetIdList.get(i));
			}
			this.vpaidAssetIdQueue = vpaidAssetIds;
		}

		public Queue<Integer> getVastAssetIdQueue()
		{
			return vastAssetIdQueue;
		}

		@Override
		public List<Integer> getAssetsList(Integer placementNumber)
		{
			Integer numberOfVASTAsset = Integer
					.parseInt(placementRequestMap.get(getPlacementRequestMapKeysList().get(2)));
			Integer numberOfVpaidAsset = Integer
					.parseInt(placementRequestMap.get(getPlacementRequestMapKeysList().get(3)));
			List<Integer> assetIds = new LinkedList<>();
			if (getVastAssetIdQueue().isEmpty() || getVastAssetIdQueue().size() < numberOfVASTAsset)
			{
				this.setVastAssetIdQueue();
				LOG.info("(ReInitializing) VAST Assets Are" + " " + getVastAssetIdQueue());
			}
			for (int j = 1; j <= numberOfVASTAsset; j++)
			{
				assetIds.add(getVastAssetIdQueue().peek());
				getVastAssetIdQueue().poll();
				LOG.info("Vast Assets Available in Queue After Association with Placement are "+getVastAssetIdQueue());
			}
			LOG.info("Added VAST Assets to Placement" + " " + placementNumber + " " + "are" + " " + assetIds);

			if (getVpaidAssetIdQueue().isEmpty() || getVpaidAssetIdQueue().size() < numberOfVpaidAsset)
			{
				this.setVpaidAssetIdQueue();
				LOG.info("(ReInitializing) VPAID Assets Are" + " " + getVpaidAssetIdQueue());
			}
			for (int k = 1; k <= numberOfVpaidAsset; k++)
			{
				assetIds.add(getVpaidAssetIdQueue().peek());
				getVpaidAssetIdQueue().poll();
				LOG.info("Vpaid Assets Available in Queue After Association with Placement are "+getVpaidAssetIdQueue());
			}

			LOG.info("Added VPAID Assets to Placement with VAST Assets" + " " + placementNumber + " " + "are" + " "
					+ assetIds);
			return assetIds;
		}

	}

	@Singleton
	public static class SurveyAssetRequestBuilder implements AssetsPlacementBuilder
	{
		private Queue<Integer> surveyAssetIdQueue = new LinkedList<>();

		public Queue<Integer> getSurveyAssetIdQueue()
		{
			return surveyAssetIdQueue;
		}

		public void setSurveyAssetIdQueue()
		{
			Queue<Integer> surveyAssetIds = new LinkedList<>();
			LinkedList<Integer> surveyAssetIdList = DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.getSurveyTypeAssetIdList();
			for (int i = 0; i < surveyAssetIdList.size(); i++)
			{
				surveyAssetIds.add(surveyAssetIdList.get(i));
			}
			this.surveyAssetIdQueue = surveyAssetIds;
		}

		@Override
		public List<Integer> getAssetsList(Integer placementNumber)
		{
			Integer numberOfSurveyAsset = Integer
					.parseInt(placementRequestMap.get(getPlacementRequestMapKeysList().get(4)));
			List<Integer> assetIds = new LinkedList<>();
			if (getSurveyAssetIdQueue().isEmpty() || getSurveyAssetIdQueue().size() < numberOfSurveyAsset)
			{
				this.setSurveyAssetIdQueue();
				LOG.info("(ReInitializing) Survey Assets Are" + " " + getSurveyAssetIdQueue());
			}
			for (int j = 1; j <= numberOfSurveyAsset; j++)
			{
				assetIds.add(getSurveyAssetIdQueue().peek());
				getSurveyAssetIdQueue().poll();
				LOG.info("Assets Available in Queue After Association with Placement are "+getSurveyAssetIdQueue());
			}
			LOG.info("Added Survey Assets to Placement" + " " + placementNumber + " " + "are" + " " + assetIds);
			return assetIds;
		}

	}

	@Singleton
	public static class DisplayAssetRequestBuilder implements AssetsPlacementBuilder
	{
		private Queue<Integer> diplayAssetIdQueue = new LinkedList<>();

		public Queue<Integer> getDiplayAssetIdQueue()
		{
			return diplayAssetIdQueue;
		}

		public void setDiplayAssetIdQueue()
		{
			Queue<Integer> displayAssetIds = new LinkedList<>();
			LinkedList<Integer> displayAssetIdList = DependencyManager.getInjector().getInstance(RequestBuilder.class)
					.getDisplayTypeAssetIdList();
			for (int i = 0; i < displayAssetIdList.size(); i++)
			{
				displayAssetIds.add(displayAssetIdList.get(i));
			}
			this.diplayAssetIdQueue = displayAssetIds;
		}

		@Override
		public List<Integer> getAssetsList(Integer placementNumber)
		{
			Integer numberOfDisplayAsset = Integer
					.parseInt(placementRequestMap.get(getPlacementRequestMapKeysList().get(4)));
			List<Integer> assetIds = new LinkedList<>();
			if (getDiplayAssetIdQueue().isEmpty() || getDiplayAssetIdQueue().size() < numberOfDisplayAsset)
			{
				this.setDiplayAssetIdQueue();
				LOG.info("(ReInitializing) Display Assets Are" + " " + getDiplayAssetIdQueue());
			}
			for (int j = 1; j <= numberOfDisplayAsset; j++)
			{
				assetIds.add(getDiplayAssetIdQueue().peek());
				getDiplayAssetIdQueue().poll();
				LOG.info("Assets Available in Queue After Association with Placement are "+getDiplayAssetIdQueue());
			}
			LOG.info("Added Display Assets to Placement" + " " + placementNumber + " " + "are" + " " + assetIds);
			return assetIds;
		}
	}

	public List<RootPlacementRequest> getRootPlacementRequestList()
	{
		return rootPlacementRequestList;
	}

	public void setRootPlacementRequestList(List<RootPlacementRequest> rootPlacementRequestList)
	{
		this.rootPlacementRequestList = rootPlacementRequestList;
	}

	public RootPlacementRequest getRootPlacementRequest()
	{
		return rootPlacementRequest;
	}

	public AssetsPlacementBuilder getProductHandler(ProductType productType)
	{
		return productHandler.get(productType);
	}

	public void setProductHandler()
	{
		productHandler.put(ProductType.VIDEO,
				DependencyManager.getInjector().getInstance(VideoAssetRequestBuilder.class));
		productHandler.put(ProductType.SURVEY,
				DependencyManager.getInjector().getInstance(SurveyAssetRequestBuilder.class));
		productHandler.put(ProductType.DISPLAY,
				DependencyManager.getInjector().getInstance(DisplayAssetRequestBuilder.class));
		productHandler.put(ProductType.ALCHEMY,
				DependencyManager.getInjector().getInstance(VideoAssetRequestBuilder.class));
	}

	public void setRootPlacementRequest(Integer placementNumber, Integer totalNoOfPlacements)
	{
		AssetsPlacementBuilder assetsRequestHandler = this.getProductHandler(ProductType.valueOf(placementRequestMap.get(getPlacementRequestMapKeysList().get(1))));
		RootPlacementRequest rootPlacementRequest = rootPlacementRequestList.get(placementNumber - 1);
		rootPlacementRequest.getPlacement().setName("Auto Placement" + " " + (placementNumber));
		rootPlacementRequest.getPlacement().setStartDate("2018-12-22T00:00:00-04:00");
		rootPlacementRequest.getPlacement().setEndDate("2018-12-31T00:00:00-04:00");
		rootPlacementRequest.getPlacement().setAssets(assetsRequestHandler.getAssetsList(placementNumber));
		this.rootPlacementRequest = rootPlacementRequest;
	}

	
	public String getEndPointUrlForPlacement(Integer placementNumber)
	{
		Integer placementID = rootPlacementRequestList.get(placementNumber - 1).getPlacement().getId();
		String ENDPOINT_URL = Constants.PLACEMENT_BASEENDPOINT_URL + "/" + placementID;
		LOG.info("Placement URL " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}

	public String getEndPointUrlForDELETERequest(Integer placementID)
	{
		String ENDPOINT_URL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/","")+"/"+placementID;
		LOG.info("Placement URL For DELETE Request " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}

	public String getEndPointUrlForGETRequest(Integer placementID,Integer campaignId)
	{
		String ENDPOINT_URL = Constants.PLACEMENT_BASEENDPOINT_URL + "/" + placementID+"?campaign_id="+campaignId;
		LOG.info("Placement URL For GET Request " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}


	public static void writeUpdatedPlacementRequestToFile(String scenarioTestDataPath)
	{
		FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestDataPath);
		try
		{
			fileWriter.writeToJsonFile(ObjectType.PLACEMENTS, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootPlacementRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootPlacementRequestListUpdated());
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

}
