/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.common.collect.Multimap;

import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 */
public class Body<T> {
    private List<T> list;
    private Map<Integer,T> map;
    private Multimap<Integer,T> multimap;

    public Body(List<T> list) {
        this.list = list;
    }

    public Body(Map<Integer, T> map) {
        this.map = map;
    }

    public Body(Multimap<Integer, T> multimap) {
        this.multimap = multimap;
    }

    public List<T> getList() {
        return list;
    }

    public Map<Integer, T> getMap() {
        return map;
    }
    public Multimap<Integer, T> getMultimap() {
        return multimap;
    }
}
