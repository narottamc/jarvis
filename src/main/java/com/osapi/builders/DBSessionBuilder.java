/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.osapi.factory.PostgreConnectionService;
import org.hibernate.SessionFactory;

/**
 * @author Clearstream.
 */
@Singleton
public class DBSessionBuilder {
    private PostgreConnectionService postgreConnectionService;
    public static SessionFactory sessionFactory;

    @Inject
    public void setPostgreConnectionService(PostgreConnectionService postgreConnectionService) {
        this.postgreConnectionService = postgreConnectionService;
    }

    public void closeSessionFactory() {
         postgreConnectionService.closeSessionFactory(DBSessionBuilder.sessionFactory);
    }

    public void initializeDBSession(){
         DBSessionBuilder.sessionFactory = postgreConnectionService.getSessionFactory();
    }

}
