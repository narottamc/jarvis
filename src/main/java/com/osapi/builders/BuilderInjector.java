/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.inject.AbstractModule;
import com.osapi.builders.PlacementBuilder.AssetsPlacementBuilder;
import com.osapi.builders.PlacementBuilder.VideoAssetRequestBuilder;
import com.osapi.factory.HibernateService;
import com.osapi.factory.PostgreConnectionService;
import com.osapi.factory.ReponseMapperService;
import com.osapi.factory.RequestBuilderService;
import com.quarterback.factory.SessionVariables;
import com.quarterback.factory.SessionVariablesService;

/**
 * @author ClearStream.
 */
public class BuilderInjector extends AbstractModule
{

	@Override
	protected void configure()
	{
		bind(ReponseMapperService.class).to(ResponseBuilder.class);
		bind(RequestBuilderService.class).to(RequestBuilder.class);
		bind(AssetsPlacementBuilder.class).to(VideoAssetRequestBuilder.class);
		bind(SessionVariablesService.class).to(SessionVariables.class);
		bind(PostgreConnectionService.class).to(HibernateService.class);
	}

}
