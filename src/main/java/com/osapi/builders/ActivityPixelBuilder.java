/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.activitypixelrequest.ActivityPixel;
import com.osapi.models.activitypixelrequest.ActivityPixelParametersAttribute;
import com.osapi.models.activitypixelrequest.ConversionSettingAttributes;
import com.osapi.models.activitypixelrequest.RootActivityPixelRequest;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.conversionpixelrequest.RootConversionPixelRequest;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.newactivitypixelrequest.Agency;
import com.osapi.models.newactivitypixelrequest.Brand;
import com.osapi.models.newactivitypixelrequest.RootNewActivityPixelRequest;
import com.osapi.models.newactivitypixelresponse.NewRootActivityPixelResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Clearstream.
 */
public class ActivityPixelBuilder {

    private static final Logger LOG = Logger.getLogger(ActivityPixelBuilder.class);
    private RootAgencyResponse agencyResponse;
    private RootBrandResponse brandResponse;
    private RootNewActivityPixelRequest newactivityPixelRequest;
    private RootActivityPixelRequest activityPixelRequest;
    private RootConversionPixelRequest conversionPixelRequest;


    public ActivityPixelBuilder(String scenarioTestDataPath) {
        this.agencyResponse = MapBuilderfromJsonFile.build(RootAgencyResponse.class, Constants.AGENCY_RESPONSE_DATA,scenarioTestDataPath,false).get();
        this.brandResponse = MapBuilderfromJsonFile.build(RootBrandResponse.class,Constants.BRAND_RESPONSE_DATA,scenarioTestDataPath,false).get();

    }

    public RootNewActivityPixelRequest getNewActivityPixelRequest() {
        return newactivityPixelRequest;
    }

    public void setNewActivityPixelRequest(RootNewActivityPixelRequest activityPixelRequest) {
        this.newactivityPixelRequest = activityPixelRequest;
    }

    public void setActivityPixelRequest(RootActivityPixelRequest activityPixelRequest) {
        this.activityPixelRequest = activityPixelRequest;
    }

    public RootActivityPixelRequest getActivityPixelRequest() {
        return activityPixelRequest;
    }


    public RootConversionPixelRequest getConversionPixelRequest() {
        return conversionPixelRequest;
    }

    public void setConversionPixelRequest(RootConversionPixelRequest conversionPixelRequest) {
        this.conversionPixelRequest = conversionPixelRequest;
    }

    public void buildRequestBodyForNewActivityPixel(String pixelName, Integer pixelType){
        RootNewActivityPixelRequest rootActivityPixelRequest = MapBuilderfromJsonFile.buildFromTemplate(RootNewActivityPixelRequest.class,
                Constants.NEWPIXEL_REQUEST_DATA,false).get();
        rootActivityPixelRequest.setName(pixelName);
        rootActivityPixelRequest.setAgencyId(agencyResponse.getId());
        rootActivityPixelRequest.setBrandId(brandResponse.getId());
        rootActivityPixelRequest.setPixelTypeId(pixelType);
        Agency agency = new Agency();
        agency.setId(agencyResponse.getId());
        agency.setName(agencyResponse.getName());
        agency.setInternal(agencyResponse.getInternal());
        Brand brand = new Brand();
        brand.setId(brandResponse.getId());
        brand.setName(brandResponse.getName());
        brand.setCode(brandResponse.getCode());
        brand.setUrl(brandResponse.getLandingPageUrl());
        brand.setDisplayName(brandResponse.getName());
        rootActivityPixelRequest.setAgency(agency);
        rootActivityPixelRequest.setBrand(brand);
        this.setNewActivityPixelRequest(rootActivityPixelRequest);
    }

    public void buildRequestBodyForExistingActivityPixel(NewRootActivityPixelResponse activityPixelResponse){
        RootActivityPixelRequest activityPixel = MapBuilderfromJsonFile.buildFromTemplate(RootActivityPixelRequest.class,
                Constants.PIXELREQUEST_TEMPLATE_DATA,false).get();
        ActivityPixel pixel = new ActivityPixel();
        pixel.setName(activityPixelResponse.getName());
        pixel.setAgencyId(activityPixelResponse.getAgencyId());
        pixel.setBrandId(activityPixelResponse.getBrandId());
        pixel.setPixelTypeId(activityPixelResponse.getPixelTypeId());
        pixel.setPostClickHours(activityPixelResponse.getPostClickHours());
        pixel.setPostViewHours(activityPixelResponse.getPostViewHours());
        pixel.setExpirationHours(activityPixelResponse.getExpirationHours());
        pixel.setSyncEnabled(activityPixelResponse.getSyncEnabled());
        ConversionSettingAttributes conversionSettingAttributes = new ConversionSettingAttributes();
        conversionSettingAttributes.setDomain("abc.com");
        Random randomInt = new Random();
        conversionSettingAttributes.setAttributionModelId(randomInt.nextInt(2)+1);
        ActivityPixelParametersAttribute activityPixelParametersAttribute = new ActivityPixelParametersAttribute();
        activityPixelParametersAttribute.setName("AutoTestParameter");
        activityPixelParametersAttribute.setValue("AutoTestParameterValue");
        List<ActivityPixelParametersAttribute> parametersAttributesList = new ArrayList<>();
        parametersAttributesList.add(activityPixelParametersAttribute);
        pixel.setActivityPixelParametersAttributes(parametersAttributesList);
        activityPixel.setActivityPixel(pixel);
        this.setActivityPixelRequest(activityPixel);
    }
    public void buildConversionPixelRequestForPlacements(Integer placementId,Integer countOfPixels){
        RootConversionPixelRequest rootConversionPixelRequest = MapBuilderfromJsonFile.buildFromTemplate(RootConversionPixelRequest.class,
                Constants.CONVERSIONPIXELREQUEST_TEMPLATE_DATA,false).get();
        rootConversionPixelRequest.setPlacementId(placementId);
        List<Integer> associatedPixelIdPlacementList = ActivityPixelBuilder.pickNRandomElemenetsFromList(countOfPixels,
                ActivityPixelBuilder.getActivityPixelIdsList());
        LOG.info("Accociated Activity Pixel Ids With Placement Id "+ " are " + associatedPixelIdPlacementList);
        rootConversionPixelRequest.setPlacementPixelIds(associatedPixelIdPlacementList);
        Integer associatedCPAPixelId  = ActivityPixelBuilder.pickNRandomElemenetsFromList(1,associatedPixelIdPlacementList).get(0);
        LOG.info("Accociated CPA Pixel Id With Placement Id "+ " is " + associatedCPAPixelId);
        rootConversionPixelRequest.setOptimizedPixelId(associatedCPAPixelId);
        this.setConversionPixelRequest(rootConversionPixelRequest);
    }
    public static void writeNewActivityPixelRequestData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.ACTIVITYPIXELS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile
                    (RootNewActivityPixelRequest.class, DependencyManager.getInjector().getInstance(RequestBuilder.class).getNewActivityPixelRequestsList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    public static void writeNewActitvityPixelResponseData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.ACTIVITYPIXELS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile
                    (NewRootActivityPixelResponse.class, DependencyManager.getInjector().getInstance(ResponseBuilder.class).getNewactivityPixelResponsesList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
    public static void writeActivityPixelPUTRequestData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.ACTIVITYPIXELS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile
                    (RootActivityPixelRequest.class, DependencyManager.getInjector().getInstance(RequestBuilder.class).getActivityPixelRequestsList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
    public static void writeAcivityPixelPUTResponseData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.ACTIVITYPIXELS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile
                    (RootActivityPixelResponse.class, DependencyManager.getInjector().getInstance(ResponseBuilder.class).getActivityPixelResponsesList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
    public static void writeConversionPixelPUTRequestData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.CONVERSIONPIXELS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile
                    (RootConversionPixelRequest.class, DependencyManager.getInjector().getInstance(RequestBuilder.class).getConversionPixelRequestsList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
    public static void writeConversionPixelPUTResponseData(String scenarioTestData){
        FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestData);
        try {
            fileWriter.writeToJsonFile(ObjectType.CONVERSIONPIXELS, QueryMethod.RESPONSE).writeResponseJsonArrayObjectToJsonFile
                    (RootConversionPixelResponse.class, DependencyManager.getInjector().getInstance(ResponseBuilder.class).getConversionPixelResponsesList());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static List<Integer> pickNRandomElemenetsFromList(Integer noOfElements,List<Integer> sourceList){
        if (noOfElements > sourceList.size()) {
            throw new IllegalArgumentException("not enough elements");
        }
        Random random = new Random();
        return IntStream
                .generate(() -> random.nextInt(sourceList.size()))
                .distinct()
                .limit(noOfElements)
                .mapToObj(sourceList::get)
                .collect(Collectors.toList());

    }

    public static List<Integer>getActivityPixelIdsList(){
        List<RootActivityPixelResponse> activityPixelResponseList  = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
                .getActivityPixelResponsesList();
        List<Integer> integerList = new ArrayList<>();
        for(int i =0;i<activityPixelResponseList.size();i++){
            integerList.add(activityPixelResponseList.get(i).getId());
        }
        return  integerList;
    }


    public String getEndPointURLForNewActivityPixel(){
        String endPointUrl=null;
        endPointUrl = Constants.PIXELS_BASEENDPOINT_URL;
        LOG.info("End Point URL For POST Request of Activity Pixel is "+endPointUrl);
        return  endPointUrl;
    }

    public String getEndPointURLForPUTActivityPixel(Integer activityPixelId){
        String endPointURL = null;
        String apiVersion = (System.getProperty("apiVersion") != null ? System.getProperty("apiVersion") : Constants.API_VERSION_DEFAULT) + "/";
        String tempURL = Constants.PIXELS_BASEENDPOINT_URL.replace("activity_pixels",apiVersion+"activity_pixels");
        endPointURL = tempURL + "/" + activityPixelId;
        LOG.info("End Point URL For PUT Request of Activity Pixel is "+endPointURL);
        return  endPointURL;
    }

    public String getEndPointURLForConversionPixelsForPlacement(Integer placementId){
        String endPointURL = null;
        String placmentURL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/", "");
        endPointURL = placmentURL + "/" + placementId + "/" + "conversion_pixels";
        LOG.info("End Point URL For PUT Request of Conversion Pixel is "+endPointURL);
        return  endPointURL ;
    }

}
