/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * @author Clearstream.
 */
public class CustomJsonDeserializerForMultiMap<T> implements JsonDeserializer<Body<T>>{
    private final Class<T> clazz;

    public CustomJsonDeserializerForMultiMap(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Body<T> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject body = json.getAsJsonObject().getAsJsonObject("body");
        JsonArray arr = body.entrySet().iterator().next().getValue().getAsJsonArray();
        Multimap<Integer,T> multiMap = ArrayListMultimap.create();
        for(JsonElement element : arr) {
            //System.out.println("Key Is "+ element.getAsJsonObject().entrySet().iterator().next().getKey());
            JsonElement innerElement1 = element.getAsJsonObject().entrySet().iterator().next().getValue();
            //System.out.println("Value Is " + innerElement1);
            multiMap.put(Integer.parseInt(element.getAsJsonObject().entrySet().iterator().next().getKey()),context.deserialize(innerElement1,clazz));
            //list.add(context.deserialize(innerElement1, clazz));
        }
        return new Body<>(multiMap);
    }
}
