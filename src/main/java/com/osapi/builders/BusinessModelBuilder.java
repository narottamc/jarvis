/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.enumtypes.BusinessModelsTypes;
import com.osapi.models.campaignrequest.BusinessModelCampaignRequest;

import java.util.HashMap;

/**
 * @author Clearstream.
 *
 */
public class BusinessModelBuilder
{
	private HashMap<Integer,BusinessModelTemplate> businessModelHandler;
	
	public BusinessModelBuilder()
	{
		this.businessModelHandler = new HashMap<>();
		this.setBusinessModelHandler();
	}
	public BusinessModelTemplate getBusinessModelHandler(Integer strategyId)
	{
		return businessModelHandler.get(strategyId);
	}

	public void setBusinessModelHandler()
	{
		//Video Business Models
		businessModelHandler.put(BusinessModelsTypes.SIMPLE_CPM_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId(), new SimpleCPMWithMarginMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_COMPLETED_VIEW_RATE.getBusinessModelStrategyId(), new CPMWithCompletedViewRate());
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_CTR.getBusinessModelStrategyId(), new CPMWithCTR());
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_CTR_AND_COMPLETED_VIEW_RATE.getBusinessModelStrategyId(), new CPMWithCTRAndCompletedViewRate());
		businessModelHandler.put(BusinessModelsTypes.CPCV_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId(), new CPCVWithMarginMaximization());
		
		//Display Business Models
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId(), new CPMWithMarginMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_CTR_MAXIMIZATION.getBusinessModelStrategyId(), new CPMWithCTRMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPC_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId(), new CPCWithMarginMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPM_WITH_ACTION_MAXIMIZATION.getBusinessModelStrategyId(), new CPMWithActionMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPA_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId(), new CPAWithMarginMaximization());
		businessModelHandler.put(BusinessModelsTypes.CPA_LEARNING.getBusinessModelStrategyId(), new CPALearning());
	}
	
	
	//Implementation of Various Business Models
	public interface BusinessModelTemplate {
		BusinessModelCampaignRequest buildBusinessModelForType();
	}
	
	
	public static class SimpleCPMWithMarginMaximization  implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.SIMPLE_CPM_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithCompletedViewRate implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCmvr(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_COMPLETED_VIEW_RATE.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithCTR implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCtr(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_CTR.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithCTRAndCompletedViewRate implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCmvr(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCtr(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_CTR_AND_COMPLETED_VIEW_RATE.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPCVWithMarginMaximization implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpcv(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPCV_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithMarginMaximization implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithCTRMaximization implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCtr(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_CTR_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	public static class CPCWithMarginMaximization implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpc(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPC_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPMWithActionMaximization implements BusinessModelTemplate
	{		
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPM_WITH_ACTION_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPAWithMarginMaximization implements BusinessModelTemplate
	{
		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpa(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPA_WITH_MARGIN_MAXIMIZATION.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}
	
	public static class CPALearning implements BusinessModelTemplate
	{

		@Override
		public BusinessModelCampaignRequest buildBusinessModelForType()
		{
			BusinessModelCampaignRequest businessModel = new BusinessModelCampaignRequest();
			businessModel.setCpmBid(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setStrategyId(BusinessModelsTypes.CPA_LEARNING.getBusinessModelStrategyId());
			return businessModel;
		}
		
	}

	
	
}
