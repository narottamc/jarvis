/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.targetingrequest.*;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 */
public class SegementsBuilder {
    private static final Logger LOG = Logger.getLogger(SegementsBuilder.class);
    private String scenarioTestDataPath;

    public SegementsBuilder(String scenarioTestDataPath) {
        this.scenarioTestDataPath = scenarioTestDataPath;
    }

    public Map<String,Object> getQueryParamMapForDataSegmentsEndPoint(Boolean activated,Integer dmpClientId,Integer dmpProviderId,Integer skip,Integer take){
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put("activated",activated);
        queryParams.put("dmp_client_id",dmpClientId);
        queryParams.put("dmp_provider_id",dmpProviderId);
        queryParams.put("skip",skip);
        queryParams.put("take",take);
        return queryParams;
    }

}
