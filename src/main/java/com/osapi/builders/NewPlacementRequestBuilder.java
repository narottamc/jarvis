/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.osapi.enumtypes.BusinessModelsTypes;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.ProductType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.BusinessModel;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.newplacementrequest.NewPlacementBusinessModel;
import com.osapi.models.newplacementrequest.RootNewPlacementRequest;
import com.osapi.models.placementrequest.BusinessModelPlacementRequest;
import com.osapi.models.placementrequest.EventPlacementRequest;
import com.osapi.models.placementrequest.Placement;
import com.osapi.models.placementrequest.RootPlacementRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ClearStream.
 */
public class NewPlacementRequestBuilder
{

	
	public interface ProductTypePlacementBuilder
	{
		Integer getProductType();
		BusinessModelPlacementRequest buildBusinessModelForPlacementRequest(Integer totalNumberOfPlacements , BusinessModelPlacementRequest businessModel);
	}

	private static final Logger LOG = Logger.getLogger(NewPlacementRequestBuilder.class);
	private static RootCampaignResponse rootCampaignResponse;
	private RootNewPlacementRequest addnewPlacementRequest;
	private RootPlacementRequest rootPlacementRequest;
	private RootPlacementRequest rootFirstPlacementRequest;
	private NewPlacementBusinessModel newPlacementBusinessModel;
	FileWriterUtils fileWriter;

	private Map<ProductType, ProductTypePlacementBuilder> productHandler = new HashMap<ProductType, ProductTypePlacementBuilder>();

	public NewPlacementRequestBuilder(String scenarioTestData)
	{
		rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestData);
		this.setNewPlacementBusinessModel();
		this.setProductHandler();
		this.fileWriter = new FileWriterUtils(scenarioTestData);
	}

	public RootNewPlacementRequest getNewPlacementRequest()
	{
		return addnewPlacementRequest;
	}

	public void setNewPlacementRequest()
	{
		RootNewPlacementRequest newRequest = MapBuilderfromJsonFile
				.buildFromTemplate(RootNewPlacementRequest.class, Constants.NEWPLACEMENT_REQUEST_DATA, false).get();
		Integer placementGroupId = rootCampaignResponse.getPlacementGroups().get(0).getId();
		newRequest.getPlacement().setPlacementGroupId(placementGroupId);
		newRequest.getPlacement().setBusinessModel(this.getNewPlacementBusinessModel());
		this.addnewPlacementRequest = newRequest;
	}
	
	public RootPlacementRequest getRootPlacementRequest()
	{
		return rootPlacementRequest;
	}

	public RootPlacementRequest getRootFirstPlacementRequest()
	{
		return rootFirstPlacementRequest;
	}
	
	public NewPlacementBusinessModel getNewPlacementBusinessModel()
	{
		return newPlacementBusinessModel;
	}

	public void setNewPlacementBusinessModel()
	{
		BusinessModel businessModelCampaignResponse = rootCampaignResponse.getBusinessModel();
		NewPlacementBusinessModel newPlacementBusinessModel = new NewPlacementBusinessModel();
		newPlacementBusinessModel.setBudget(0);
		newPlacementBusinessModel.setCpmBid(0);
		newPlacementBusinessModel.setMargin(0.1);
		newPlacementBusinessModel.setCap("all");
		newPlacementBusinessModel.setCmvr(businessModelCampaignResponse.getCmvr());
		newPlacementBusinessModel.setCpcv(businessModelCampaignResponse.getCpcv());
		newPlacementBusinessModel.setCtr(businessModelCampaignResponse.getCtr());
		newPlacementBusinessModel.setCpc(businessModelCampaignResponse.getCpc());
		newPlacementBusinessModel.setCpa(businessModelCampaignResponse.getCpa());
		newPlacementBusinessModel.setStrategyId(businessModelCampaignResponse.getStrategyId());
		newPlacementBusinessModel.setOutstreamCpm(0);
		newPlacementBusinessModel.setOutstreamMargin(0.9);
		newPlacementBusinessModel.setPacerBudget(businessModelCampaignResponse.getPacerBudget());
		this.newPlacementBusinessModel = newPlacementBusinessModel;
	}

	public ProductTypePlacementBuilder getProductHandler(ProductType productType)
	{
		return productHandler.get(productType);
	}

	public void setProductHandler()
	{
		productHandler.put(ProductType.VIDEO, new VideoProductTypePlacement());
		productHandler.put(ProductType.ALCHEMY, new AlchemyProductTypePlacement());
		productHandler.put(ProductType.SURVEY, new SurveyProductTypePlacement());
		productHandler.put(ProductType.DISPLAY, new DisplayProductTypePlacement());
	}

	public void setRootFirstPlacementRequest(String productType,Integer totalNumberOfPlacements,String responseJsonString)
	{
		EventPlacementRequest eventType = new EventPlacementRequest();
		eventType.setEventType("start");
		List<EventPlacementRequest> eventTypeList = new ArrayList<EventPlacementRequest>();
		eventTypeList.add(eventType);
		RootPlacementRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootPlacementRequest.class, Constants.PLACEMENTREQUEST_TEMPLATE_DATA, false).get();
		Placement firstPlacement = MapBuilderfromJsonString.build(Placement.class, responseJsonString, false).get();
		request.setPlacement(firstPlacement);
		request.getPlacement()
				.setProductType(this.getProductHandler(ProductType.valueOf(productType)).getProductType());
		request.getPlacement().setBusinessModel(this.getProductHandler(ProductType.valueOf(productType))
				.buildBusinessModelForPlacementRequest(totalNumberOfPlacements,firstPlacement.getBusinessModel()));
		request.getPlacement().setEvents(eventTypeList);
		request.getPlacement().setVisible(true);
		this.rootFirstPlacementRequest = request;
	}

	public void setRootPlacementRequest(String responseJsonString, String productType,Integer totalNumberOfPlacements)
	{
		EventPlacementRequest eventType = new EventPlacementRequest();
		eventType.setEventType("start");
		List<EventPlacementRequest> eventTypeList = new ArrayList<EventPlacementRequest>();
		eventTypeList.add(eventType);
		RootPlacementRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootPlacementRequest.class, Constants.PLACEMENTREQUEST_TEMPLATE_DATA, false).get();
		Placement placement = MapBuilderfromJsonString.build(Placement.class, responseJsonString, false).get();
		request.setPlacement(placement);
		request.getPlacement().setBusinessModel(this.getProductHandler(ProductType.valueOf(productType))
				.buildBusinessModelForPlacementRequest(totalNumberOfPlacements,placement.getBusinessModel()));
		request.getPlacement().setProductType(this.getProductHandler(ProductType.valueOf(productType)).getProductType());
		request.getPlacement().setEvents(eventTypeList);
		request.getPlacement().setVisible(true);
		this.rootPlacementRequest = request;

	}
	
	public String getEndPointUrlForFirstPlacement()
	{
		Integer placementID = rootCampaignResponse.getPlacements().get(0).getId();
		Integer campaignID = rootCampaignResponse.getId();
		String ENDPOINT_URL = Constants.PLACEMENT_BASEENDPOINT_URL + "/" + placementID + "?campaign_id="+ campaignID;
		LOG.info("First Default Placement URL " + ENDPOINT_URL);
		return ENDPOINT_URL;

	}
	public void writeUpdatedPlacementRequestToFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.PLACEMENTS, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootPlacementRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootPlacementRequestList());
		}
		catch (InstantiationException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	
	public static class VideoProductTypePlacement implements ProductTypePlacementBuilder
	{
		Integer productType = 1;

		@Override
		public Integer getProductType()
		{
			return productType;
		}

		@Override
		public BusinessModelPlacementRequest buildBusinessModelForPlacementRequest(Integer totalNumberOfPlacements,BusinessModelPlacementRequest businessModel)
		{
			BusinessModel businessModelCampaignResponse = rootCampaignResponse.getBusinessModel();
			businessModel.setBudget((rootCampaignResponse.getBusinessModel().getBudget()/totalNumberOfPlacements));
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpmBid(businessModelCampaignResponse.getCpmBid()-1==0 ? businessModelCampaignResponse.getCpmBid() : businessModelCampaignResponse.getCpmBid()-1);
			return businessModel;
		}
	}


	public static class AlchemyProductTypePlacement implements ProductTypePlacementBuilder
	{
		Integer productType = 2;

		@Override
		public Integer getProductType()
		{
			return productType;
		}
		
		@Override
		public BusinessModelPlacementRequest buildBusinessModelForPlacementRequest(Integer totalNumberOfPlacements,BusinessModelPlacementRequest businessModel)
		{
			BusinessModel businessModelCampaignResponse = rootCampaignResponse.getBusinessModel();
			businessModel.setBudget((rootCampaignResponse.getBusinessModel().getBudget()/totalNumberOfPlacements));
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpmBid(businessModelCampaignResponse.getCpmBid()-1==0 ? businessModelCampaignResponse.getCpmBid() : businessModelCampaignResponse.getCpmBid()-1);
			businessModel.setOutstreamCpm(BusinessModelsTypes.getRandomIntegerValue());
			businessModel.setOutstreamMargin(BusinessModelsTypes.getRandomDoubleValue());
			return businessModel;
		}
	}


	public static class SurveyProductTypePlacement implements ProductTypePlacementBuilder
	{
		Integer productType = 3;

		@Override
		public Integer getProductType()
		{
			return productType;
		}

		@Override
		public BusinessModelPlacementRequest buildBusinessModelForPlacementRequest(Integer totalNumberOfPlacements,BusinessModelPlacementRequest businessModel)
		{
			BusinessModel businessModelCampaignResponse = rootCampaignResponse.getBusinessModel();
			businessModel.setBudget((rootCampaignResponse.getBusinessModel().getBudget()/totalNumberOfPlacements));
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpmBid(businessModelCampaignResponse.getCpmBid()-1==0 ? businessModelCampaignResponse.getCpmBid() : businessModelCampaignResponse.getCpmBid()-1);
			return businessModel;
		}
	}


	public static class DisplayProductTypePlacement implements ProductTypePlacementBuilder
	{
		Integer productType = 4;

		@Override
		public Integer getProductType()
		{
			return productType;
		}

		@Override
		public BusinessModelPlacementRequest buildBusinessModelForPlacementRequest(Integer totalNumberOfPlacements,BusinessModelPlacementRequest businessModel)
		{
			BusinessModel businessModelCampaignResponse = rootCampaignResponse.getBusinessModel();
			businessModel.setBudget((rootCampaignResponse.getBusinessModel().getBudget()/totalNumberOfPlacements));
			businessModel.setMargin(BusinessModelsTypes.getRandomDoubleValue());
			businessModel.setCpmBid(businessModelCampaignResponse.getCpmBid()-1==0 ? businessModelCampaignResponse.getCpmBid() : businessModelCampaignResponse.getCpmBid()-1);
			return businessModel;
		}
	}
}
