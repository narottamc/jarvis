/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.fraudsrequest.IASCredentialsFraudRequest;
import com.osapi.models.fraudsrequest.RootFraudRequest;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 *
 */
public class FraudsBuilder
{
	private static final Logger LOG = Logger.getLogger(FraudsBuilder.class);
	
	private Integer placementNumber;

	private RootFraudRequest fraudRequest;
	private RootPlacementResponse rootPlacementResponse;
	private Map<String,String> fraudsRequestMap;
	private static List<String> fraudsRequestMapKeysList;
	
	public FraudsBuilder(String scenarioTestDataPath,Map<String,String> fraudsRequestMap)
	{

		this.fraudsRequestMap =  fraudsRequestMap;
		FraudsBuilder.setFraudsRequestMapKeysList(fraudsRequestMap);
		this.setPlacementNumber();
		this.rootPlacementResponse = MapBuilderfromJsonFile.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA,
				 scenarioTestDataPath, true).getAsCollection().get(placementNumber-1);
	}



	public Integer getPlacementNumber()
	{
		return placementNumber;
	}

	public void setPlacementNumber()
	{
		this.placementNumber = Integer
				.parseInt(CharMatcher.inRange('0', '9').retainFrom(fraudsRequestMap.get(fraudsRequestMapKeysList.get(0))));
	}
	
	public static List<String> getFraudsRequestMapKeysList()
	{
		return fraudsRequestMapKeysList;
	}

	public static void setFraudsRequestMapKeysList(Map<String,String> fraudsRequestMap)
	{
		List<String> keysList = new ArrayList<>();
		for (Map.Entry<String, String> keyEntries : fraudsRequestMap.entrySet())
		{
			keysList.add(keyEntries.getKey());
		}
		FraudsBuilder.fraudsRequestMapKeysList = keysList;
	}

	public RootFraudRequest getFraudRequest()
	{
		return fraudRequest;
	}

	public void setFraudRequest()
	{
		LOG.info("Adding Fraud Secition Details For Placement "+rootPlacementResponse.getId());
		RootFraudRequest fraudRequest = MapBuilderfromJsonFile.buildFromTemplate(RootFraudRequest.class, Constants.FRAUDSREQUEST_TEMPLATE_DATA, false).get();
		fraudRequest.setEnableDoubleVerify(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(1))));
		fraudRequest.setEnableDvFraud(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(2)).split("\\(")[0]));
		fraudRequest.setDvPpdTimeout(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(2)).split("\\(")[1].replace(")", "")));
		fraudRequest.setUseIab(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(3))));
		fraudRequest.setUseIas(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(4)).split("\\(")[0]));
		fraudRequest.setIasTimeout(Boolean.valueOf(fraudsRequestMap.get(fraudsRequestMapKeysList.get(4)).split("\\(")[1].replace(")", "")));
		if(!fraudsRequestMap.get(fraudsRequestMapKeysList.get(5)).isEmpty()){
		IASCredentialsFraudRequest iasCrendetialsFrauds = new IASCredentialsFraudRequest();
		iasCrendetialsFrauds.setAdvEntityId(fraudsRequestMap.get(fraudsRequestMapKeysList.get(5)));
		iasCrendetialsFrauds.setPubEntityId(fraudsRequestMap.get(fraudsRequestMapKeysList.get(6)));
		fraudRequest.setIasCredentials(iasCrendetialsFrauds);
		}
		fraudRequest.setPlacementId(rootPlacementResponse.getId());
		this.fraudRequest = fraudRequest;
	}
	
	public String getEndPointURLForFrauds() {
		String placmentURL = Constants.PLACEMENT_BASEENDPOINT_URL.replace("v2/", "");
		String endPointURL = placmentURL + "/" + rootPlacementResponse.getId() + "/frauds";
		LOG.info("Fraud URL For Placement "+ rootPlacementResponse.getId() + " is "+ endPointURL);
		return endPointURL;
	}
	
	public static void storeFraudRequestToFile(String scenarioTestDataPath) {
		FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestDataPath);
		try
		{
			fileWriter.writeToJsonFile(ObjectType.FRAUDS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(RootFraudRequest.class, 
					DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootFraudRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
