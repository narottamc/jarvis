/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.common.base.CharMatcher;
import com.google.gson.JsonIOException;
import com.google.inject.Singleton;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.ProductType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.flightrequest.FlightRequestAsset;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author ClearStream.
 */
public class FlightBuilder
{

	public interface AssetsFlightBuilder
	{
		List<Integer> getFlightAssetList(Integer placementNumber);
		void clearFlightAssetIdQueue();
	}

	private static final Logger LOG = Logger.getLogger(FlightBuilder.class);

	private RootFlightRequest rootFlightRequest;
	private static List<RootPlacementResponse> rootPlacementResponseList;
	private List<RootFlightRequest> rootFlightRequestList;
	private static List<RootAssetResponse> rootAssetResponseList;
	private Map<ProductType, AssetsFlightBuilder> flightproductHandler = new HashMap<ProductType, AssetsFlightBuilder>();
	private List<String> flightMapKeysList;
	private static Integer flightNumber;
	private static String assetType;
	private static Integer assetCount;


	public FlightBuilder(String scenarioTestDataPath, Map<String, String> flightRequestMap)
	{
		rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getPlacementResponseList();
		rootAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getAssetResponseList();
		rootFlightRequestList = DependencyManager.getInjector().getInstance(RequestBuilder.class)
				.getRootFlightRequestList();
		this.setFlightProductHandler();
		this.setFlightMapKeysList(flightRequestMap);
		FlightBuilder.flightNumber = Integer.parseInt(
				CharMatcher.inRange('0', '9').retainFrom(flightRequestMap.get(getFlightMapKeysList().get(0))));
		FlightBuilder.assetType = flightRequestMap.get(getFlightMapKeysList().get(1));
		FlightBuilder.assetCount = Integer.parseInt(flightRequestMap.get(getFlightMapKeysList().get(2)));
	}

	public FlightBuilder() {
	}

	public List<String> getFlightMapKeysList()
	{
		return flightMapKeysList;
	}

	public void setFlightMapKeysList(Map<String, String> flightRequestMap)
	{
		List<String> keysList = new ArrayList<>();
		for (Map.Entry<String, String> keyEntries : flightRequestMap.entrySet())
		{
			keysList.add(keyEntries.getKey());
		}
		this.flightMapKeysList = keysList;
	}

	public RootFlightRequest getRootFlightRequest()
	{
		return rootFlightRequest;
	}

	public AssetsFlightBuilder getFlightProductHandler(ProductType productType)
	{
		return flightproductHandler.get(productType);
	}

	public void setFlightProductHandler()
	{
		flightproductHandler.put(ProductType.VIDEO,
				DependencyManager.getInjector().getInstance(VideoAssetFlightBuilder.class));
		flightproductHandler.put(ProductType.SURVEY,
				DependencyManager.getInjector().getInstance(SurveyAssetFlightBuilder.class));
		flightproductHandler.put(ProductType.DISPLAY,
				DependencyManager.getInjector().getInstance(DisplayAssetFlightBuilder.class));
		flightproductHandler.put(ProductType.ALCHEMY,
				DependencyManager.getInjector().getInstance(VideoAssetFlightBuilder.class));
	}

	public Map<Integer, RootFlightRequest> getFlightId(Integer placementNumber)
	{
		Map<Integer, RootFlightRequest> flightRequestMap = new HashMap<Integer, RootFlightRequest>();
		Integer flightId = null;
		RootFlightRequest request = null;
		RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
		Integer placementID = rootPlacementResponse.getId();
		List<RootFlightRequest> flightRequestListOfPlacement = new LinkedList<RootFlightRequest>();
		for (int i = 0; i < rootFlightRequestList.size(); i++)
		{
			if (rootFlightRequestList.get(i).getPlacementId().intValue() == placementID)
			{
				flightRequestListOfPlacement.add(rootFlightRequestList.get(i));
			}
		}

		for (int i = 0; i < flightRequestListOfPlacement.size(); i++)
		{
			if (flightNumber - 1 == i)
			{
				request = flightRequestListOfPlacement.get(i);
				flightId = flightRequestListOfPlacement.get(flightNumber - 1).getId();
				flightRequestMap.put(flightId, request);
				break;
			}

		}
		LOG.info(
				"Updating Details for Flight Number" + " " + flightNumber + " " + "of Placement" + " " + placementNumber
						+ " " + "having Flight Id" + " " + flightRequestMap.keySet().stream().findFirst().get());
		return flightRequestMap;

	}

	
	public List<FlightRequestAsset> setUpAssetsForFlights(List<Integer> associatedAssetIdsFlight, Integer flightId,
			Integer placementNumber)
	{
		List<FlightRequestAsset> flighassetlist = new ArrayList<>();
		float[] aDdistributionArray = new float[assetCount];
		for (int i = 0; i < aDdistributionArray.length; i++)
		{
			float maxDistribution = 100.000f;
			aDdistributionArray[i] = maxDistribution / assetCount;
		}
		for (int i = 0; i < associatedAssetIdsFlight.size(); i++)
		{
			FlightRequestAsset flightasset = new FlightRequestAsset();
			flightasset.setAssetId(associatedAssetIdsFlight.get(i));
			flightasset.setDistribution(aDdistributionArray[i]);
			flightasset.setFlightId(flightId);
			flighassetlist.add(flightasset);
			LOG.info("Asset of type" + " " + assetType + " " + "added to Flight Number" + " " + flightNumber + " "
					+ "for Placement" + " " + placementNumber + " " + "for Flight ID" + " " + flightId + " "
					+ "with Distribution" + " " + aDdistributionArray[i]);
		}

		return flighassetlist;
	}

	public void setRootFlightRequest(Integer placementNumber,String assetType, Integer totalFlightCount)
	{
		RootFlightRequest request = null;
		Integer flightId = null;
		RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
		for (Integer key : this.getFlightId(placementNumber).keySet())
		{
			request = this.getFlightId(placementNumber).get(key);
			flightId = key;
		}
		AssetsFlightBuilder flightAssetsRequestHandler = this.getFlightProductHandler(ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()));
		List<Integer> associatedAssetIdsFlight = flightAssetsRequestHandler.getFlightAssetList(placementNumber);
		String flightName = "Test Flight" + " " + flightNumber + "Placement" +
				placementNumber + " " + "AssetType " + assetType;
		request.setFlightAssets(this.setUpAssetsForFlights(associatedAssetIdsFlight, flightId, placementNumber));
		request.setName(flightName);
		request.setStartDate("2018-12-24T00:00:00-04:00");
		request.setEndDate("2018-12-29T00:00:00-04:00");
		request.setActive(Math.random()<0.5);
		for(int i =0;i<request.getAdsAttributes().size();i++){
			request.getAdsAttributes().get(i).setName(flightName + " - ("+ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()) +")");
		}
		this.rootFlightRequest = request;
	}
	
	public static void writeUpdatedFlightRequestToFile(String scenarioTestDataPath)
	{
		FileWriterUtils fileWriter = new FileWriterUtils(scenarioTestDataPath);
		try
		{
			fileWriter.writeToJsonFile(ObjectType.FLIGHTS, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootFlightRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootFlightRequestListUpdated());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
	public void clearQueuesForFlightAssetIds(String assetType)
	{
		this.getFlightProductHandler(ProductType.valueOf(assetType)).clearFlightAssetIdQueue();
	}


	@Singleton
	public static class VideoAssetFlightBuilder implements AssetsFlightBuilder
	{
		private Queue<Integer> vastAssetIdFlightQueue = new LinkedList<>();
		private Queue<Integer> vpaidLinearAssetIdFlightQueue = new LinkedList<>();
		private Queue<Integer> vpadiNonLinearAssetIdFlightQueue = new LinkedList<>();

		public Queue<Integer> getVpaidLinearAssetIdFlightQueue()
		{
			return vpaidLinearAssetIdFlightQueue;
		}

		public void setVpaidLinearAssetIdFlightQueue(LinkedList<Integer> availableVideoAssetIdInPlacement)
		{
			Queue<Integer> vpaidLinearAssetIds = new LinkedList<>();
			for (int i = 0; i < availableVideoAssetIdInPlacement.size(); i++)
			{
				for (int j = 0; j < rootAssetResponseList.size(); j++)
				{
					Integer assetId = availableVideoAssetIdInPlacement.get(i);
					if (assetId == rootAssetResponseList.get(j).getId().intValue()
							&& rootAssetResponseList.get(j).getIsLinear() && rootAssetResponseList.get(j).getIsVPaid())
					{
						vpaidLinearAssetIds.add(assetId);
					}
				}
			}
			this.vpaidLinearAssetIdFlightQueue = vpaidLinearAssetIds;
		}

		public Queue<Integer> getVpadiNonLinearAssetIdFlightQueue()
		{
			return vpadiNonLinearAssetIdFlightQueue;
		}

		public void setVpadiNonLinearAssetIdFlightQueue(LinkedList<Integer> availableVideoAssetIdInPlacement)
		{
			Queue<Integer> vpaidNonLinearAssetIds = new LinkedList<>();
			for (int i = 0; i < availableVideoAssetIdInPlacement.size(); i++)
			{
				for (int j = 0; j < rootAssetResponseList.size(); j++)
				{
					Integer assetId = availableVideoAssetIdInPlacement.get(i);
					if (assetId == rootAssetResponseList.get(j).getId().intValue()
							&& !(rootAssetResponseList.get(j).getIsLinear())
							&& rootAssetResponseList.get(j).getIsVPaid())
					{
						vpaidNonLinearAssetIds.add(assetId);
					}
				}
			}
			this.vpadiNonLinearAssetIdFlightQueue = vpaidNonLinearAssetIds;
		}

		public Queue<Integer> getVastAssetIdFlightQueue()
		{
			return vastAssetIdFlightQueue;
		}

		public void setVastAssetIdFlightQueue(LinkedList<Integer> availableVideoAssetIdInPlacement)
		{
			Queue<Integer> vastAssetIds = new LinkedList<>();
			for (int i = 0; i < availableVideoAssetIdInPlacement.size(); i++)
			{
				for (int j = 0; j < rootAssetResponseList.size(); j++)
				{
					Integer assetId = availableVideoAssetIdInPlacement.get(i);
					if (assetId == rootAssetResponseList.get(j).getId().intValue()
							&& !(rootAssetResponseList.get(j).getIsVPaid()))
					{
						vastAssetIds.add(assetId);
					}
				}
			}
			this.vastAssetIdFlightQueue = vastAssetIds;
		}

		@Override
		public List<Integer> getFlightAssetList(Integer placementNumber)
		{
			RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
			LinkedList<Integer> availableAssetIds = new LinkedList<Integer>();
			for (int i = 0; i < rootPlacementResponse.getAssets().size(); i++)
			{
				availableAssetIds.add(rootPlacementResponse.getAssets().get(i).getId());
			}
			LOG.info("Available assets for associating with Flights Are :" + " " + availableAssetIds);
			List<Integer> associatedAssetIdsFlight = new ArrayList<>();

			for (int i = 0; i < assetCount; i++)
			{
				if (assetType.toLowerCase().contentEquals("vast"))
				{
					if (i == 0 && this.getVastAssetIdFlightQueue().isEmpty())
					{
						this.setVastAssetIdFlightQueue(availableAssetIds);
						LOG.info("(Initializing) VAST Video Assets Associated With Placement Are" + " "
								+ getVastAssetIdFlightQueue());
					}
					if (!(getVastAssetIdFlightQueue().isEmpty()))
					{
						associatedAssetIdsFlight.add(getVastAssetIdFlightQueue().peek());
						getVastAssetIdFlightQueue().poll();
						LOG.info("Vast Assets available After Association with Flights Are" + " "
								+ getVastAssetIdFlightQueue());
					}

				}
				else if (assetType.toLowerCase().contentEquals("vpaidlinear"))
				{
					if (i == 0 && this.vpaidLinearAssetIdFlightQueue.isEmpty())
					{
						this.setVpaidLinearAssetIdFlightQueue(availableAssetIds);
						LOG.info("(Initializing) VPAID Linear Video Assets Associated With Placement Are" + " "
								+ getVpaidLinearAssetIdFlightQueue());
					}
					if (!(getVpaidLinearAssetIdFlightQueue().isEmpty()))
					{
						associatedAssetIdsFlight.add(getVpaidLinearAssetIdFlightQueue().peek());
						getVpaidLinearAssetIdFlightQueue().poll();
						LOG.info("VPAID Linear Assets available After Association with Flights Are" + " "
								+ getVpaidLinearAssetIdFlightQueue());
					}

				}
				else if (assetType.toLowerCase().contentEquals("vpaidnonlinear"))
				{
					if (i == 0 && this.getVpadiNonLinearAssetIdFlightQueue().isEmpty())
					{
						this.setVpadiNonLinearAssetIdFlightQueue(availableAssetIds);
						LOG.info("(Initializing) VPAID Non Linear Video Assets Associated With Placement Are" + " "
								+ getVpadiNonLinearAssetIdFlightQueue());
					}
					if (!(getVpadiNonLinearAssetIdFlightQueue().isEmpty()))
					{
						associatedAssetIdsFlight.add(getVpadiNonLinearAssetIdFlightQueue().peek());
						getVpadiNonLinearAssetIdFlightQueue().poll();
						LOG.info("VPAID NonLinear Assets available After Association with Flights Are" + " "
								+ getVpadiNonLinearAssetIdFlightQueue());
					}

				}

			}
			if (associatedAssetIdsFlight.size() < assetCount)
			{
				LOG.warn("Available Assets to be associated with Flight Number" + " " + flightNumber + " "
						+ "of Placement Number" + " " + placementNumber + " " + "of type" + " " + assetType + " "
						+ "are" + " " + associatedAssetIdsFlight.size() + " " + "in Count" + " "
						+ "which is less than requested" + " " + assetCount + " " + "Create More Assets of Type" + " "
						+ assetType);
			}

			return associatedAssetIdsFlight;
		}

		
		@Override
		public void clearFlightAssetIdQueue()
		{
			if(!this.getVastAssetIdFlightQueue().isEmpty())
			{
				LOG.info("Clearing VAST AssetId Queue");
				this.getVastAssetIdFlightQueue().clear();
			}
			if(!this.getVpadiNonLinearAssetIdFlightQueue().isEmpty())
			{
				LOG.info("Clearing VPAID Non-Linear AssetId Queue");
				this.getVpadiNonLinearAssetIdFlightQueue().clear();
			}
			if(!this.getVpaidLinearAssetIdFlightQueue().isEmpty())
			{
				LOG.info("Clearing VPAID Linear AssetId Queue");
				this.getVpaidLinearAssetIdFlightQueue().clear();
			}
		}

	}

	
	@Singleton
	public static class SurveyAssetFlightBuilder implements AssetsFlightBuilder
	{
		private Queue<Integer> surveyAssetIdFlightQueue = new LinkedList<>();

		public Queue<Integer> getSurveyAssetIdFlightQueue()
		{
			return surveyAssetIdFlightQueue;
		}

		public void setSurveyAssetIdFlightQueue(LinkedList<Integer> availableSurvyAssetIdInPlacement)
		{
			Queue<Integer> surveyAssetFlightIds = new LinkedList<>();
			for (int i = 0; i < availableSurvyAssetIdInPlacement.size(); i++)
			{
				surveyAssetFlightIds.add(availableSurvyAssetIdInPlacement.get(i));
			}
			this.surveyAssetIdFlightQueue = surveyAssetFlightIds;
		}

		@Override
		public List<Integer> getFlightAssetList(Integer placementNumber)
		{
			RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
			LinkedList<Integer> availableAssetIds = new LinkedList<Integer>();

			for (int i = 0; i < rootPlacementResponse.getAssets().size(); i++)
			{
				availableAssetIds.add(rootPlacementResponse.getAssets().get(i).getId());
			}
			LOG.info("Available assets for associating with Flights Are :" + " " + availableAssetIds);
			List<Integer> associatedAssetIdsFlight = new ArrayList<>();
			for (int i = 0; i < assetCount; i++)
			{
				if (i == 0 && this.getSurveyAssetIdFlightQueue().isEmpty())
				{
					this.setSurveyAssetIdFlightQueue(availableAssetIds);
					LOG.info("(Initializing) Survey Assets Queue To be associated in Flights" + " "
							+ getSurveyAssetIdFlightQueue());
				}
				if (!(getSurveyAssetIdFlightQueue().isEmpty()))
				{
					associatedAssetIdsFlight.add(getSurveyAssetIdFlightQueue().peek());
					getSurveyAssetIdFlightQueue().poll();
					LOG.info("Survey Assets available After Association with Flights Are" + " "
							+ getSurveyAssetIdFlightQueue());

				}

			}
			if (associatedAssetIdsFlight.size() < assetCount)
			{
				LOG.warn("Available Assets to be associated with Flight Number" + " " + flightNumber + " "
						+ "of Placement Number" + " " + placementNumber + " " + "of type" + " " + assetType + " "
						+ "are" + " " + associatedAssetIdsFlight.size() + " " + "in Count" + " "
						+ "which is less than requested" + " " + assetCount + " " + "Create More Assets of Type" + " "
						+ assetType);
			}

			return associatedAssetIdsFlight;
		}

		
		@Override
		public void clearFlightAssetIdQueue()
		{
			if(!this.getSurveyAssetIdFlightQueue().isEmpty())
			{
				LOG.info("Clearing Survey AssetId Queue");
				this.getSurveyAssetIdFlightQueue().clear();
			}
			
		}

	}

	
	@Singleton
	public static class DisplayAssetFlightBuilder implements AssetsFlightBuilder
	{
		private Queue<Integer> displayAssetIdFlightQueue = new LinkedList<>();

		public Queue<Integer> getDisplayAssetIdFlightQueue()
		{
			return displayAssetIdFlightQueue;
		}

		public void setDisplayAssetIdFlightQueue(LinkedList<Integer> availableDisplayAssetIdInPlacement)
		{
			Queue<Integer> displayAssetFlightIds = new LinkedList<>();
			for (int i = 0; i < availableDisplayAssetIdInPlacement.size(); i++)
			{
				displayAssetFlightIds.add(availableDisplayAssetIdInPlacement.get(i));
			}
			this.displayAssetIdFlightQueue = displayAssetFlightIds;
		}

		@Override
		public List<Integer> getFlightAssetList(Integer placementNumber)
		{
			RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
			LinkedList<Integer> availableAssetIds = new LinkedList<Integer>();

			for (int i = 0; i < rootPlacementResponse.getAssets().size(); i++)
			{
				availableAssetIds.add(rootPlacementResponse.getAssets().get(i).getId());
			}
			LOG.info("Available assets for associating with Flights Are :" + " " + availableAssetIds);
			List<Integer> associatedAssetIdsFlight = new ArrayList<>();
			for (int i = 0; i < assetCount; i++)
			{
				if (i == 0 && this.getDisplayAssetIdFlightQueue().isEmpty())
				{
					this.setDisplayAssetIdFlightQueue(availableAssetIds);
					LOG.info("(Initializing) Display Assets Queue To be added in Flights" + " "
							+ getDisplayAssetIdFlightQueue());
				}
				if (!(getDisplayAssetIdFlightQueue().isEmpty()))
				{
					associatedAssetIdsFlight.add(getDisplayAssetIdFlightQueue().peek());
					getDisplayAssetIdFlightQueue().poll();
					LOG.info("Display Assets available After Association with Flights Are" + " "
							+ getDisplayAssetIdFlightQueue());

				}

			}
			if (associatedAssetIdsFlight.size() < assetCount)
			{
				LOG.warn("Available Assets to be associated with Flight Number" + " " + flightNumber + " "
						+ "of Placement Number" + " " + placementNumber + " " + "of type" + " " + assetType + " "
						+ "are" + " " + associatedAssetIdsFlight.size() + " " + "in Count" + " "
						+ "which is less than requested" + " " + assetCount + " " + "Create More Assets of Type" + " "
						+ assetType);
			}

			return associatedAssetIdsFlight;
		}

		
		@Override
		public void clearFlightAssetIdQueue()
		{
			if(!this.getDisplayAssetIdFlightQueue().isEmpty())
			{
				LOG.info("Clearing Display AssetId Queue");
				this.getDisplayAssetIdFlightQueue().clear();
			}
		}

	}

	public String getEndPointURLFlight(Integer placementNumber)
	{
		Integer flightId = null;
		RootPlacementResponse rootPlacementResponse = rootPlacementResponseList.get(placementNumber - 1);
		Integer placementID = rootPlacementResponse.getId();
		for (Integer key : this.getFlightId(placementNumber).keySet())
		{
			flightId = key;
		}
		String ENDPOINT_URL = Constants.FLIGHT_BASEENDPOINT_URL + "/" + flightId + "?placementId=" + placementID;
		LOG.info("Creating URL for Flight Number" + " " + flightNumber + " " + "of Placement" + " " + placementNumber
				+ " " + "having Flight Id" + " " + flightId);
		LOG.info("URL is" + " " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}
	
	public static String getEndPointURLFlightForGETRequest(Integer flightId)
	{
		String ENDPOINT_URL = Constants.FLIGHT_BASEENDPOINT_URL + "/" + flightId;
		LOG.info("URL For GET Request of Flight API is" + " " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}

	public String getEndPointURLFlightForDeleteRequest(Integer flightId){
		String ENDPOINT_URL = Constants.FLIGHT_BASEENDPOINT_URL + "/" + flightId;
		LOG.info("URL For Delete Request of Flight API is" + " " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}
}
