/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 */
public class CustomJsonDeserializerForMap<T> implements JsonDeserializer<Body<T>>{
    private final Class<T> clazz;

    public CustomJsonDeserializerForMap(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Body<T> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject body = json.getAsJsonObject().getAsJsonObject("body");
        JsonArray arr = body.entrySet().iterator().next().getValue().getAsJsonArray();
        List<T> list = new ArrayList<>();
        Map<Integer,T> map = new HashMap<>();
        for(JsonElement element : arr) {
            //System.out.println("Key Is "+ element.getAsJsonObject().entrySet().iterator().next().getKey());
            JsonElement innerElement1 = element.getAsJsonObject().entrySet().iterator().next().getValue();
            //System.out.println("Value Is " + innerElement1);
            map.put(Integer.parseInt(element.getAsJsonObject().entrySet().iterator().next().getKey()),context.deserialize(innerElement1,clazz));
            //list.add(context.deserialize(innerElement1, clazz));
        }
        return new Body<>(map);
    }
}
