/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.osapi.enumtypes.ObjectType;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.ReponseMapperService;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.newactivitypixelresponse.NewRootActivityPixelResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.segmentsdmpproviders.RootDmpProvidersResponse;
import com.osapi.models.sspresponse.RootSSPResponse;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.osapi.models.targetingresponse.*;
import com.quarterback.utils.Constants;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author ClearStream.
 */
@Singleton
public class ResponseBuilder implements ReponseMapperService
{
	private ReponseMapperService responseMapperService;
	private List<RootAssetResponse> videoAssetResponseList;
	private List<RootSurveyResponse> surveyAssetResponseList;
	private List<RootDisplayResponse> displayAssetResponseList;
	private List<RootPlacementResponse> placementResponseList;
	private List<NewRootActivityPixelResponse> newactivityPixelResponsesList;
	private List<RootActivityPixelResponse> activityPixelResponsesList;
	private List<RootConversionPixelResponse> conversionPixelResponsesList;
	private List<RootSSPResponse> rootSSPResponseList;
	private List<RootDealsAndSeatsResponse> dealsAndSeatsResponseList;
	private List<RootFlightsResponse> flightsResponseList;
	private List<RootMediaInventoryResponse> mediaInventoryResponseMediaList;
	private List<RootMediaInventoryResponse> mediaInventoryResponseSmartList;
	private List<RootFraudsResponse>fraudResponseList;
	private List<RootGeoResponse> targetingResponseGeoList;
	private List<RootGeoListResponse> geoListsZipCodeList;
	private List<RootAddtionalOptionResponse> addtionalOptionsResponseList;
	private List<RootCategoryResponse> categoryResponseList;
	private List<RootTechnologyResponse> technologyResponseList;
	private List<RootDayPartResponse> dayPartResponseList;
	private Map<ObjectType,String>masterDataJsonResponseMap;
	private List<RootDmpProvidersResponse>masterDmpProviderResponse;

	@Inject
	public ResponseBuilder(ReponseMapperService responseMapperService)
	{
		this.responseMapperService = responseMapperService;
		this.surveyAssetResponseList = new LinkedList<RootSurveyResponse>();
		this.videoAssetResponseList = new LinkedList<RootAssetResponse>();
		this.displayAssetResponseList = new LinkedList<RootDisplayResponse>();
		this.newactivityPixelResponsesList = new LinkedList<NewRootActivityPixelResponse>();
		this.activityPixelResponsesList = new LinkedList<RootActivityPixelResponse>();
		this.conversionPixelResponsesList = new LinkedList<RootConversionPixelResponse>();
		this.rootSSPResponseList = new LinkedList<RootSSPResponse>();
		this.dealsAndSeatsResponseList = new LinkedList<RootDealsAndSeatsResponse>();
		this.placementResponseList = new LinkedList<RootPlacementResponse>();
		this.flightsResponseList = new LinkedList<RootFlightsResponse>();
		this.mediaInventoryResponseMediaList = new LinkedList<RootMediaInventoryResponse>();
		this.mediaInventoryResponseSmartList = new LinkedList<RootMediaInventoryResponse>();
		this.fraudResponseList = new LinkedList<RootFraudsResponse>();
		this.addtionalOptionsResponseList = new LinkedList<RootAddtionalOptionResponse>();
		this.categoryResponseList = new LinkedList<RootCategoryResponse>();
		this.targetingResponseGeoList = new LinkedList<RootGeoResponse>();
		this.geoListsZipCodeList = new LinkedList<RootGeoListResponse>();
		this.dayPartResponseList = new LinkedList<RootDayPartResponse>();
		this.technologyResponseList = new LinkedList<RootTechnologyResponse>();
		this.masterDataJsonResponseMap = new HashMap<ObjectType,String>();
	}

	public List<RootDmpProvidersResponse> getMasterDmpProviderResponse() {
		return masterDmpProviderResponse;
	}

	public void setMasterDmpProviderResponse(List<RootDmpProvidersResponse> masterDmpProviderResponse) {
		this.masterDmpProviderResponse = masterDmpProviderResponse;
	}

	public Map<ObjectType, String> getMasterDataJsonResponseMap() {
		return masterDataJsonResponseMap;
	}

	public void setMasterDataJsonResponseMap(ObjectType type , String responseJsonString) {
		this.masterDataJsonResponseMap.put(type,responseJsonString);
	}

	public ReponseMapperService getResponseMapperService()
	{
		return responseMapperService;
	}

	public void setResponseMapperService(ReponseMapperService responseMapperService)
	{
		this.responseMapperService = responseMapperService;
	}

	public List<RootAssetResponse> getAssetResponseList()
	{
		return videoAssetResponseList;
	}

	public void setAssetResponseList(RootAssetResponse rootAssetResponse)
	{
		this.videoAssetResponseList.add(rootAssetResponse);
	}

	public List<RootSurveyResponse> getRootSurveyResponseList()
	{
		return surveyAssetResponseList;
	}

	public void setRootSurveyResponseList(RootSurveyResponse rootSurveyResponse)
	{
		this.surveyAssetResponseList.add(rootSurveyResponse);
	}

	public List<RootDisplayResponse> getDisplayAssetResponseList()
	{
		return displayAssetResponseList;
	}

	public void setDisplayAssetResponseList(RootDisplayResponse displayAssetResponse)
	{
		this.displayAssetResponseList.add(displayAssetResponse);
	}

	public List<RootSSPResponse> getRootSSPResponseList() {
		return rootSSPResponseList;
	}

	public void setRootSSPResponseList(RootSSPResponse rootSSPResponse) {
		this.rootSSPResponseList.add(rootSSPResponse);
	}

	public List<RootDealsAndSeatsResponse> getDealsAndSeatsResponseList() {
		return dealsAndSeatsResponseList;
	}

	public void setDealsAndSeatsResponseList(RootDealsAndSeatsResponse dealsAndSeatsResponse) {
		this.dealsAndSeatsResponseList.add(dealsAndSeatsResponse);
	}

	public List<NewRootActivityPixelResponse> getNewactivityPixelResponsesList() {
		return newactivityPixelResponsesList;
	}

	public void setNewactivityPixelResponsesList(NewRootActivityPixelResponse newactivityPixelResponses) {
		this.newactivityPixelResponsesList.add(newactivityPixelResponses);
	}

	public List<RootActivityPixelResponse> getActivityPixelResponsesList() {
		return activityPixelResponsesList;
	}

	public void setActivityPixelResponsesList(RootActivityPixelResponse activityPixelResponses) {
		this.activityPixelResponsesList.add(activityPixelResponses);
	}

	public List<RootConversionPixelResponse> getConversionPixelResponsesList() {
		return conversionPixelResponsesList;
	}

	public void setConversionPixelResponsesList(RootConversionPixelResponse conversionPixelResponses) {
		this.conversionPixelResponsesList.add(conversionPixelResponses);
	}

	public List<RootPlacementResponse> getPlacementResponseList()
	{
		return placementResponseList;
	}

	public void setPlacementResponseList(RootPlacementResponse rootPlacementResponse)
	{
		this.placementResponseList.add(rootPlacementResponse);
	}

	public List<RootFlightsResponse> getFlightsResponseList()
	{
		return flightsResponseList;
	}

	public void setFlightsResponseList(RootFlightsResponse flightsResponse)
	{
		this.flightsResponseList.add(flightsResponse);
	}

	public List<RootMediaInventoryResponse> getMediaInventoryResponseMediaList()
	{
		return mediaInventoryResponseMediaList;
	}

	public void setMediaInventoryResponseMediaList(RootMediaInventoryResponse mediaInventoryResponseMediaList)
	{
		this.mediaInventoryResponseMediaList.add(mediaInventoryResponseMediaList);
	}

	public List<RootMediaInventoryResponse> getMediaInventoryResponseSmartList()
	{
		return mediaInventoryResponseSmartList;
	}

	public void setMediaInventoryResponseSmartList(RootMediaInventoryResponse mediaInventoryResponseSmartList)
	{
		this.mediaInventoryResponseSmartList.add(mediaInventoryResponseSmartList);
	}
	
	public List<RootFraudsResponse> getFraudResponseList()
	{
		return fraudResponseList;
	}

	public void setFraudResponseList(RootFraudsResponse fraudResponse)
	{
		this.fraudResponseList.add(fraudResponse);
	}

	public List<RootGeoResponse> getTargetingResponseGeoList()
	{
		return targetingResponseGeoList;
	}

	public void setTargetingResponseGeoList(RootGeoResponse targetingResponseGeo)
	{
		this.targetingResponseGeoList.add(targetingResponseGeo);
	}

	public List<RootGeoListResponse> getGeoListsZipCodeList() {
		return geoListsZipCodeList;
	}

	public void setGeoListsZipCodeList(RootGeoListResponse geoListsZipCode) {
		this.geoListsZipCodeList.add(geoListsZipCode);
	}

	public List<RootAddtionalOptionResponse> getAddtionalOptionsResponseList()
	{
		return addtionalOptionsResponseList;
	}

	public void setAddtionalOptionsResponseList(RootAddtionalOptionResponse addtionalOptionsResponse)
	{
		this.addtionalOptionsResponseList.add(addtionalOptionsResponse);
	}

	public List<RootCategoryResponse> getCategoryResponseList()
	{
		return categoryResponseList;
	}

	public void setCategoryResponseList(RootCategoryResponse categoryResponse)
	{
		this.categoryResponseList.add(categoryResponse);
	}

	public List<RootTechnologyResponse> getTechnologyResponseList()
	{
		return technologyResponseList;
	}

	public void setTechnologyResponseList(RootTechnologyResponse technologyResponse)
	{
		this.technologyResponseList.add(technologyResponse);
	}

	public List<RootDayPartResponse> getDayPartResponseList()
	{
		return dayPartResponseList;
	}

	public void setDayPartResponseList(RootDayPartResponse dayPartResponse)
	{
		this.dayPartResponseList.add(dayPartResponse);
	}

	@Override
	public RootAgencyResponse getRootAgencyResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootAgencyResponse.class, Constants.AGENCY_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}

	@Override
	public RootBrandResponse getRootBrandResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootBrandResponse.class, Constants.BRAND_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}

	@Override
	public RootCampaignResponse getRootCampaignResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}

	@Override
	public List<RootAssetResponse> getRootVideoAssetResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootAssetResponse.class, Constants.ASSET_RESPONSE_DATA, scenarioTestDataPath, true)
				.getAsCollection();
	}

	@Override
	public List<RootPlacementResponse> getRootPlacementResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA, scenarioTestDataPath, true)
				.getAsCollection();
	}

	@Override
	public List<RootFlightsResponse> getRootFlightResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	@Override
	public List<RootSurveyResponse> getRootSurveyAssetResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootSurveyResponse.class, Constants.SURVEY_RESPONSE_DATA, scenarioTestDataPath, true)
				.getAsCollection();
	}

	@Override
	public List<RootDisplayResponse> getRootDisplayAssetResponse(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile
				.build(RootDisplayResponse.class, Constants.DISPLAY_RESPONSE_DATA, scenarioTestDataPath, true)
				.getAsCollection();
	}

}
