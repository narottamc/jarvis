/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.RequestBuilderService;
import com.osapi.models.activitypixelrequest.RootActivityPixelRequest;
import com.osapi.models.assetrequest.RootAssetRequest;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.conversionpixelrequest.RootConversionPixelRequest;
import com.osapi.models.dealsandseatsrequest.RootDealsAndSeatsRequest;
import com.osapi.models.displayrequest.RootDisplayRequest;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.fraudsrequest.RootFraudRequest;
import com.osapi.models.mediainventoryrequest.RootMediaListRequest;
import com.osapi.models.mediainventoryrequest.RootSmartListRequest;
import com.osapi.models.newactivitypixelrequest.RootNewActivityPixelRequest;
import com.osapi.models.newassetrequest.RootNewAssetRequest;
import com.osapi.models.placementrequest.RootPlacementRequest;
import com.osapi.models.ssprequest.RootSSPRequest;
import com.osapi.models.surveyrequest.RootSurveyRequest;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.osapi.models.targetingrequest.*;
import com.quarterback.utils.Constants;
import io.restassured.spi.AuthFilter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ClearStream.
 */
@Singleton
public class RequestBuilder implements RequestBuilderService
{

	private RequestBuilderService requestMapperService;
	private String scenarioTestDatPath;
	private AuthFilter filterAuth;
	private String accessToken;

	private List<RootDisplayRequest> rootDisplayRequestList;
	private List<RootSurveyRequest> rootSurveyRequestList;
	private List<RootNewAssetRequest> rootVideoAssetRequestList;
	private List<RootAssetRequest>rootVideoAssetRequestListUpdated;
	private List<RootAssetResponse> rootVideoAssetResponseList;
	private List<RootSurveyResponse> rootSurveyResponseList;
	private List<RootDisplayResponse> rootDisplayResponseList;
	private List<RootPlacementRequest> rootPlacementRequestList;
	private List<RootNewActivityPixelRequest> newActivityPixelRequestsList;
	private List<RootActivityPixelRequest> activityPixelRequestsList;
	private List<RootConversionPixelRequest> conversionPixelRequestsList;
	private List<RootPlacementRequest> rootPlacementRequestListUpdated;
	private List<RootFraudRequest> rootFraudRequestList;
	private List<RootSSPRequest> rootSSPRequestList;
	private List<RootDealsAndSeatsRequest> rootDealsAndSeatsRequestList;
	private List<RootFlightRequest> rootFlightRequestList;
	private List<RootFlightRequest> rootFlightRequestListUpdated;
	private List<RootMediaListRequest> rootMediaListRequestList;
	private List<RootSmartListRequest> rootSmartListRequestList;
	private List<RootGeoRequest> targetingRequestGeoList;
	private List<RootAditionalOptionsRequest> addtionalOptionsRequestList;
	private List<RootCategoryRequest> categoryRequestList;
	private List<RootTechnologyRequest> technologyRequestList;
	private List<RootDayPartRequest> dayPartRequestList;

	@Inject
	public RequestBuilder(RequestBuilderService requestMapperService)
	{
		this.requestMapperService = requestMapperService;
		this.rootDisplayRequestList = new LinkedList<RootDisplayRequest>();
		this.rootVideoAssetRequestList = new LinkedList<RootNewAssetRequest>();
		this.rootVideoAssetRequestListUpdated = new LinkedList<RootAssetRequest>();
		this.rootSurveyRequestList = new LinkedList<RootSurveyRequest>();
		this.rootVideoAssetResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getAssetResponseList();
		this.rootSurveyResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootSurveyResponseList();
		this.rootDisplayResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getDisplayAssetResponseList();
		this.rootPlacementRequestList = new LinkedList<RootPlacementRequest>();
		this.rootSSPRequestList = new LinkedList<RootSSPRequest>();
		this.rootDealsAndSeatsRequestList = new LinkedList<RootDealsAndSeatsRequest>();
		this.newActivityPixelRequestsList = new LinkedList<RootNewActivityPixelRequest>();
		this.activityPixelRequestsList = new LinkedList<RootActivityPixelRequest>();
		this.conversionPixelRequestsList = new LinkedList<RootConversionPixelRequest>();
		this.rootPlacementRequestListUpdated = new LinkedList<RootPlacementRequest>();
		this.rootFraudRequestList = new LinkedList<RootFraudRequest>();
		this.rootFlightRequestList = new LinkedList<RootFlightRequest>();
		this.rootFlightRequestListUpdated = new LinkedList<RootFlightRequest>();
		this.rootMediaListRequestList = new LinkedList<RootMediaListRequest>();
		this.rootSmartListRequestList = new LinkedList<RootSmartListRequest>();
		this.addtionalOptionsRequestList = new LinkedList<RootAditionalOptionsRequest>();
		this.categoryRequestList = new LinkedList<RootCategoryRequest>();
		this.dayPartRequestList = new LinkedList<RootDayPartRequest>();
		this.technologyRequestList = new LinkedList<RootTechnologyRequest>();
		this.targetingRequestGeoList = new LinkedList<RootGeoRequest>();

	}

	public RequestBuilderService getRequestMapperService()
	{
		return requestMapperService;
	}

	public void setRequestMapperService(RequestBuilderService requestMapperService)
	{
		this.requestMapperService = requestMapperService;
	}

	public List<Integer> getDefaultDealIds(){
		return Arrays.asList(2,5,6,7,8,9,10,11,13,15,18,19,20,21,22,23,24,25,26,27);
	}

	public List<Integer> getDefaultSeatIds(){
		return Arrays.asList(2);
	}

	public AuthFilter getFilterAuth()
	{
		return filterAuth;
	}

	public void setFilterAuth(AuthFilter filterAuth)
	{
		this.filterAuth = filterAuth;
	}

	public String getScenarioTestDatPath()
	{
		return scenarioTestDatPath;
	}

	public void setScenarioTestDatPath(String scenarioTestDatPath)
	{
		this.scenarioTestDatPath = scenarioTestDatPath;
	}

	public String getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	public List<RootDisplayRequest> getRootDisplayRequestList()
	{
		return rootDisplayRequestList;
	}

	public void setRootDisplayRequestList(RootDisplayRequest rootDisplayRequest)
	{
		this.rootDisplayRequestList.add(rootDisplayRequest);
	}

	public List<RootSurveyRequest> getRootSurveyRequestList()
	{
		return rootSurveyRequestList;
	}

	public void setRootSurveyRequestList(RootSurveyRequest rootSurveyRequest)
	{
		this.rootSurveyRequestList.add(rootSurveyRequest);
	}

	public List<RootNewAssetRequest> getNewRootAssetRequestList()
	{
		return rootVideoAssetRequestList;
	}

	public void setRootNewAssetRequestList(RootNewAssetRequest rootAssetRequest)
	{
		this.rootVideoAssetRequestList.add(rootAssetRequest);
	}

	public List<RootAssetRequest> getRootVideoAssetRequestListUpdated()
	{
		return rootVideoAssetRequestListUpdated;
	}

	public void setRootVideoAssetRequestListUpdated(RootAssetRequest rootVideoAssetRequest)
	{
		this.rootVideoAssetRequestListUpdated.add(rootVideoAssetRequest);
	}

	public List<RootDealsAndSeatsRequest> getRootDealsAndSeatsRequestList() {
		return rootDealsAndSeatsRequestList;
	}

	public void setRootDealsAndSeatsRequestList(RootDealsAndSeatsRequest rootDealsAndSeatsRequest) {
		this.rootDealsAndSeatsRequestList.add(rootDealsAndSeatsRequest);
	}

	public List<RootSSPRequest> getRootSSPRequestList() {
		return rootSSPRequestList;
	}

	public void setRootSSPRequestList(RootSSPRequest rootSSPRequest) {
		this.rootSSPRequestList.add(rootSSPRequest);
	}

	public List<RootNewActivityPixelRequest> getNewActivityPixelRequestsList() {
		return newActivityPixelRequestsList;
	}

	public void setNewActivityPixelRequestsList(RootNewActivityPixelRequest newActivityPixelRequests) {
		this.newActivityPixelRequestsList.add(newActivityPixelRequests);
	}

	public List<RootActivityPixelRequest> getActivityPixelRequestsList() {
		return activityPixelRequestsList;
	}

	public void setActivityPixelRequestsList(RootActivityPixelRequest activityPixelRequests) {
		this.activityPixelRequestsList.add(activityPixelRequests);
	}

	public List<RootConversionPixelRequest> getConversionPixelRequestsList() {
		return conversionPixelRequestsList;
	}

	public void setConversionPixelRequestsList(RootConversionPixelRequest conversionPixelRequests) {
		this.conversionPixelRequestsList.add(conversionPixelRequests);
	}

	public List<RootPlacementRequest> getRootPlacementRequestList()
	{
		return rootPlacementRequestList;
		
	}

	public void setRootPlacementRequestList(RootPlacementRequest rootPlacementRequest)
	{
		this.rootPlacementRequestList.add(rootPlacementRequest);
	}
	
	public List<RootPlacementRequest> getRootPlacementRequestListUpdated()
	{
		return rootPlacementRequestListUpdated;
	}

	public void setRootPlacementRequestListUpdated(RootPlacementRequest rootPlacementRequestUpdated)
	{
		this.rootPlacementRequestListUpdated.add(rootPlacementRequestUpdated);
	}

	
	public List<RootFraudRequest> getRootFraudRequestList()
	{
		return rootFraudRequestList;
	}

	public void setRootFraudRequestList(RootFraudRequest rootFraudRequest)
	{
		this.rootFraudRequestList.add(rootFraudRequest);
	}

	public List<RootFlightRequest> getRootFlightRequestList()
	{
		return rootFlightRequestList;
	}

	public void setRootFlightRequestList(RootFlightRequest rootFlightRequest)
	{
		this.rootFlightRequestList.add(rootFlightRequest);
	}

	public List<RootFlightRequest> getRootFlightRequestListUpdated()
	{
		return rootFlightRequestListUpdated;
	}

	public void setRootFlightRequestListUpdated(RootFlightRequest rootFlightRequestUpdated)
	{
		this.rootFlightRequestListUpdated.add(rootFlightRequestUpdated);
	}

	public List<RootMediaListRequest> getRootMediaListRequestList()
	{
		return rootMediaListRequestList;
	}

	public void setRootMediaListRequestList(RootMediaListRequest rootMediaListRequest)
	{
		this.rootMediaListRequestList.add(rootMediaListRequest);
	}

	public List<RootSmartListRequest> getRootSmartListRequestList()
	{
		return rootSmartListRequestList;
	}

	public void setRootSmartListRequestList(RootSmartListRequest rootSmartListRequest)
	{
		this.rootSmartListRequestList.add(rootSmartListRequest);
	}

	public List<RootGeoRequest> getTargetingRequestGeoList()
	{
		return targetingRequestGeoList;
	}

	public void setTargetingRequestGeoList(RootGeoRequest targetingRequestGeo)
	{
		this.targetingRequestGeoList.add(targetingRequestGeo);
	}

	public List<RootAditionalOptionsRequest> getAddtionalOptionsRequestList()
	{
		return addtionalOptionsRequestList;
	}

	public void setAddtionalOptionsRequestList(RootAditionalOptionsRequest addtionalOptionsRequest)
	{
		this.addtionalOptionsRequestList.add(addtionalOptionsRequest);
	}

	public List<RootCategoryRequest> getCategoryRequestList()
	{
		return categoryRequestList;
	}

	public void setCategoryRequestList(RootCategoryRequest categoryRequest)
	{
		this.categoryRequestList.add(categoryRequest);
	}

	public List<RootTechnologyRequest> getTechnologyRequestList()
	{
		return technologyRequestList;
	}

	public void setTechnologyRequestList(RootTechnologyRequest technologyRequest)
	{
		this.technologyRequestList.add(technologyRequest);
	}

	public List<RootDayPartRequest> getDayPartRequestList()
	{
		return dayPartRequestList;
	}

	public void setDayPartRequestList(RootDayPartRequest dayPartRequest)
	{
		this.dayPartRequestList.add(dayPartRequest);
	}

	@Override
	public LinkedList<Integer> getVideoTypeAssetIdList(String assetType)
	{
		LinkedList<Integer> assetsId = new LinkedList<Integer>();
		for (int i = 0; i < rootVideoAssetResponseList.size(); i++)
		{
			if (assetType.toLowerCase().contentEquals("vast"))
			{
				if (rootVideoAssetResponseList.get(i).getIsVPaid().equals(false))
				{
					assetsId.add(rootVideoAssetResponseList.get(i).getId());

				}

			}
			else if (assetType.toLowerCase().contentEquals("vpaid"))
			{
				if (rootVideoAssetResponseList.get(i).getIsVPaid().equals(true))
				{
					assetsId.add(rootVideoAssetResponseList.get(i).getId());
				}
			}
		}
		return assetsId;
	}

	@Override
	public List<RootFlightRequest> getRootFlighRequest(String scenarioTestData)
	{
		return MapBuilderfromJsonFile
				.build(RootFlightRequest.class, Constants.FLIGHTS_REQUEST_DATA, scenarioTestData, true)
				.getAsCollection();
	}

	@Override
	public LinkedList<Integer> getSurveyTypeAssetIdList()
	{
		LinkedList<Integer> surveyAssetId = new LinkedList<>();
		for (int i = 0; i < rootSurveyResponseList.size(); i++)
		{
			surveyAssetId.add(rootSurveyResponseList.get(i).getId());
		}
		return surveyAssetId;
	}

	@Override
	public LinkedList<Integer> getDisplayTypeAssetIdList()
	{
		LinkedList<Integer> displayAssetId = new LinkedList<>();
		for (int i = 0; i < rootDisplayResponseList.size(); i++)
		{
			displayAssetId.add(rootDisplayResponseList.get(i).getId());
		}
		return displayAssetId;
	}

	
}
