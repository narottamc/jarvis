/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */

package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.ProductType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.flightrequest.FlightRequestBusinessModel;
import com.osapi.models.flightrequest.RootFlightRequest;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.newflightrequest.NewFlightAdsAttribute;
import com.osapi.models.newflightrequest.RootNewFlightRequest;
import com.osapi.models.placementresponse.BusinessModelPlacementResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ClearStream.
 */
public class NewFlightRequestBuilder
{

	
	public interface ProductTypeFlightBuilder
	{
		String getProductType();
		String getAdType();
		
	}

	private static final Logger LOG = Logger.getLogger(NewFlightRequestBuilder.class);

	private RootCampaignResponse rootCampaignResponse;
	private static List<RootPlacementResponse> rootPlacementResponseList;
	private RootNewFlightRequest addNewFlightRequest;
	private RootFlightRequest rootFlightRequest;
	private RootFlightRequest rootFirstFlightRequest;
	private Map<Integer, ProductTypeFlightBuilder> productHandler = new HashMap<Integer, ProductTypeFlightBuilder>();
	private FileWriterUtils fileWriter;

	public NewFlightRequestBuilder(String scenarioTestData)
	{
		rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestData);
		rootPlacementResponseList = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getPlacementResponseList();
		this.setProductHandler();
		this.fileWriter = new FileWriterUtils(scenarioTestData);
	}

	public ProductTypeFlightBuilder getProductHandler(Integer productTypeNumber)
	{
		return productHandler.get(productTypeNumber);
	}

	public void setProductHandler()
	{
		productHandler.put(ProductType.VIDEO.getproductTypeNumber(), new VideoProductTypeFlight());
		productHandler.put(ProductType.ALCHEMY.getproductTypeNumber(), new AlchemyProductTypeFlight());
		productHandler.put(ProductType.SURVEY.getproductTypeNumber(), new SurveyProductTypeFlight());
		productHandler.put(ProductType.DISPLAY.getproductTypeNumber(), new DisplayProductTypeFlight());
	}

	public List<RootPlacementResponse> getRootPlacementResponseList()
	{
		return rootPlacementResponseList;
	}

	public void setRootPlacementResponseList(List<RootPlacementResponse> rootPlacementResponseList)
	{
		this.rootPlacementResponseList = rootPlacementResponseList;
	}

	public RootNewFlightRequest getRootNewFlightRequest()
	{
		return addNewFlightRequest;
	}
	
	public FlightRequestBusinessModel getBusinessModelForFlight(Integer placementNumber,Integer flightId,
			Integer flightbusinessModelId,Integer totalNumberOfFlights) {
		BusinessModelPlacementResponse businessModelPlacement = rootPlacementResponseList.get(placementNumber - 1).getBusinessModel();
		FlightRequestBusinessModel flightBusinessModel = new FlightRequestBusinessModel();
		flightBusinessModel.setId(flightbusinessModelId);
		flightBusinessModel.setBudget(businessModelPlacement.getBudget()/ totalNumberOfFlights);
		flightBusinessModel.setMargin(businessModelPlacement.getMargin());
		flightBusinessModel.setCpmBid(businessModelPlacement.getCpmBid());
		flightBusinessModel.setCap("all");
		flightBusinessModel.setCmvr(businessModelPlacement.getCmvr());
		flightBusinessModel.setCpcv(businessModelPlacement.getCpcv());
		flightBusinessModel.setCtr(businessModelPlacement.getCtr());
		flightBusinessModel.setCpc(businessModelPlacement.getCpc());
		flightBusinessModel.setCpa(businessModelPlacement.getCpa());
		flightBusinessModel.setStrategyId(businessModelPlacement.getStrategyId());
		flightBusinessModel.setOutstreamCpm(businessModelPlacement.getOutstreamCpm());
		flightBusinessModel.setOutstreamMargin(businessModelPlacement.getOutstreamMargin());
		flightBusinessModel.setPacerBudget(businessModelPlacement.getPacerBudget());
		flightBusinessModel.setFlightId(flightId);
		return flightBusinessModel;
	}

	public void setRootNewFlightRequest(Integer placementNumber)
	{
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		Integer productType = rootPlacementResponseList.get(placementNumber - 1).getProductType();
		RootNewFlightRequest newRequest = MapBuilderfromJsonFile
				.buildFromTemplate(RootNewFlightRequest.class, Constants.NEWFLIGHT_REQUEST_DATA, false).get();
		newRequest.setBusinessModel(rootPlacementResponseList.get(placementNumber - 1).getBusinessModel());
		newRequest.setPlacementId(placementId);
		List<NewFlightAdsAttribute> flightAds = newRequest.getAdsAttributes();
		for (int i = 0; i < flightAds.size(); i++)
		{
			newRequest.getAdsAttributes().get(i).setAdType(this.getProductHandler(productType).getAdType());
			newRequest.getAdsAttributes().get(i)
					.setName("Untitled - (" + this.getProductHandler(productType).getProductType() + ")");
		}
		this.addNewFlightRequest = newRequest;
	}

	public RootFlightRequest getRootFlightRequest()
	{
		return rootFlightRequest;
	}

	public void setRootFlightRequest(Integer placementNumber, String responseJsonString,Integer totalNumberOfFlights)

	{
		RootFlightsResponse rootFlightResponse = MapBuilderfromJsonString
				.build(RootFlightsResponse.class, responseJsonString, false).get();
		Integer flightID = rootFlightResponse.getId();
		Integer placementID = rootFlightResponse.getPlacementId();
		Integer businessModelID = rootFlightResponse.getBusinessModel().getId();
		List<FlightResponseAd> flightAds = rootFlightResponse.getAds();

		RootFlightRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootFlightRequest.class, Constants.FLIGHTREQUEST_TEMPLATE_DATA, false).get();
		for (int i = 0; i < flightAds.size(); i++)
		{
			request.getAdsAttributes().get(i).setId(flightAds.get(i).getId());
			request.getAdsAttributes().get(i).setName(flightAds.get(i).getName());
			request.getAdsAttributes().get(i).setAdType(flightAds.get(i).getAdType());
			request.getAdsAttributes().get(i).setFlightId(flightAds.get(i).getFlightId());
		}
		request.setBusinessModel(this.getBusinessModelForFlight(placementNumber, flightID, businessModelID, totalNumberOfFlights));
		request.setId(flightID);
		request.setPlacementId(placementID);
		this.rootFlightRequest = request;
	}

	public RootFlightRequest getRootFirstFlightRequest()
	{
		return rootFirstFlightRequest;
	}

	public void setRootFirstFlightRequest(Integer placementNumber,String responseJsonString,Integer totalNumberOfFlights )
	{
		List<RootFlightsResponse> rootFlightResponseList = MapBuilderfromJsonString
				.build(RootFlightsResponse.class, responseJsonString, true).getAsCollection();
		RootFlightsResponse rootFlightResponse = rootFlightResponseList.get(0);

		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		Integer businessModelID = rootFlightResponse.getBusinessModel().getId();
		Integer flightID = rootFlightResponse.getId();
		List<FlightResponseAd> flightAds = rootFlightResponse.getAds();

		RootFlightRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootFlightRequest.class, Constants.FLIGHTREQUEST_TEMPLATE_DATA, false).get();
		for (int i = 0; i < flightAds.size(); i++)
		{
			request.getAdsAttributes().get(i).setId(flightAds.get(i).getId());
			request.getAdsAttributes().get(i).setName(flightAds.get(i).getName());
			request.getAdsAttributes().get(i).setAdType(flightAds.get(i).getAdType());
			request.getAdsAttributes().get(i).setFlightId(flightAds.get(i).getFlightId());
		}
		request.setBusinessModel(this.getBusinessModelForFlight(placementNumber, flightID, businessModelID, totalNumberOfFlights));
		request.setId(flightID);
		request.setPlacementId(placementId);
		this.rootFirstFlightRequest = request;
	}

	public String getEndPointURLFlight(Integer placementNumber)
	{
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		String endPointURL = Constants.FLIGHT_BASEENDPOINT_URL + "?placementId=" + placementId;
		LOG.info("New Flight URL is:" + " " + endPointURL);
		return endPointURL;

	}

	public String getEndPointURLFlightList(Integer placementNumber)
	{
		Integer placementId = rootPlacementResponseList.get(placementNumber - 1).getId();
		Integer camapaignId = rootCampaignResponse.getId();
		String endPointURL = Constants.PLACEMENT_BASEENDPOINT_URL + "/" + placementId + "/flights?campaign_id="
				+ camapaignId;
		LOG.info("Flight List URL for Placement" + " " + placementNumber + " " + "is" + " " + endPointURL);
		return endPointURL;

	}
	
	public void writeFlightRequestToFile()
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.FLIGHTS, QueryMethod.REQUEST)
					.writeResponseJsonArrayObjectToJsonFile(RootFlightRequest.class, DependencyManager.getInjector()
							.getInstance(RequestBuilder.class).getRootFlightRequestList());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	
	public static class VideoProductTypeFlight implements ProductTypeFlightBuilder
	{
		String productType = "VIDEO";
		String adType = "VIDEO";

		@Override
		public String getProductType()
		{
			return productType;
		}

		@Override
		public String getAdType()
		{
			return adType;
		}

	}

	
	public static class AlchemyProductTypeFlight implements ProductTypeFlightBuilder
	{

		String productType = "ALCHEMY";
		String adType = "ALCHEMY";

		@Override
		public String getProductType()
		{
			return productType;
		}

		@Override
		public String getAdType()
		{
			return adType;
		}

	
	}

	
	public static class SurveyProductTypeFlight implements ProductTypeFlightBuilder
	{

		String productType = "SURVEY";
		String adType = "ALCHEMY";

		@Override
		public String getProductType()
		{
			return productType;
		}

		@Override
		public String getAdType()
		{
			return adType;
		}

	}


	public static class DisplayProductTypeFlight implements ProductTypeFlightBuilder
	{

		String productType = "DISPLAY";
		String adType = "ALCHEMY";

		@Override
		public String getProductType()
		{
			return productType;
		}

		@Override
		public String getAdType()
		{
			return adType;
		}

	}

}
