/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.agencyrequest.RootAgencyRequest;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.brandrequest.RootBrandRequest;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

/**
 * @author ClearStream.
 */
public class AgencyandBrandBuilder
{
	private static final Logger LOG = Logger.getLogger(AgencyandBrandBuilder.class);
	private String scenarioDataPath;
	private RootAgencyRequest rootAgencyRequest;
	private RootBrandRequest rootBrandRequest;
	FileWriterUtils fileWriter;

	public AgencyandBrandBuilder()
	{

	}

	public AgencyandBrandBuilder(String scenarioTestData)
	{
		this.fileWriter = new FileWriterUtils(scenarioTestData);
		this.scenarioDataPath = scenarioTestData;
	}

	public RootAgencyRequest getRootAgencyRequest()
	{
		return rootAgencyRequest;
	}

	public void setRootAgencyRequest(RootAgencyRequest rootAgencyRequest)
	{
		this.rootAgencyRequest = rootAgencyRequest;
	}

	public RootBrandRequest getRootBrandRequest()
	{
		return rootBrandRequest;
	}

	public void setRootBrandRequest(RootBrandRequest rootBrandRequest)
	{
		this.rootBrandRequest = rootBrandRequest;
	}

	public String getScenarioDataPath()
	{
		return scenarioDataPath;
	}

	public void setScenarioDataPath(String scenarioDataPath)
	{
		this.scenarioDataPath = scenarioDataPath;
	}

	public void updateAgencyDetails(String agencyName)
	{
		RootAgencyRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootAgencyRequest.class, Constants.AGENCYREQUEST_TEMPLATE_DATA, false).get();
		request.getAgency().setName(agencyName);
		LOG.info("Updated Agency Name with : " + " " + request.getAgency().getName());
		this.setRootAgencyRequest(request); 
		fileWriter.writeToJsonFile(ObjectType.AGENCIES, QueryMethod.REQUEST)
				.writeRequestSingleJsonObjectToJsonFile(request);

	}

	public void updateBrandDetails(String brandName)
	{
		RootBrandRequest request = MapBuilderfromJsonFile
				.buildFromTemplate(RootBrandRequest.class, Constants.BRANDREQUEST_TEMPLATE_DATA, false).get();
		request.getBrand().setName(brandName);
		request.getBrand().setLandingPageUrl("www.autobrand.com");
		LOG.info("Updated Brand Name with : " + " " + request.getBrand().getName());
		this.setRootBrandRequest(request);
		fileWriter.writeToJsonFile(ObjectType.BRANDS, QueryMethod.REQUEST)
				.writeRequestSingleJsonObjectToJsonFile(request);
	}
	
	public void storeBrandResponseToFile(String responseBrandJSONString) 
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.BRANDS, QueryMethod.RESPONSE)
					.writeResponseSingleJsonObjectToJsonFile(RootBrandResponse.class, responseBrandJSONString);
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
	public void storeAgencyResponseToFile(String responseAgencyJSONString) 
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.AGENCIES, QueryMethod.RESPONSE)
					.writeResponseSingleJsonObjectToJsonFile(RootAgencyResponse.class, responseAgencyJSONString);
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{

			e.printStackTrace();
		}
	}

}
