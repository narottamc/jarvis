/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.osapi.builders;

import com.google.gson.JsonIOException;
import com.osapi.enumtypes.*;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.factory.MapBuilderfromJsonString;
import com.osapi.fileutils.FileWriterUtils;
import com.osapi.models.assetrequest.AssetEvent;
import com.osapi.models.assetrequest.RootAssetRequest;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.newassetrequest.RootNewAssetRequest;
import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ClearStream.
 */
public class AssetBuilder
{
	private static final Logger LOG = Logger.getLogger(AssetBuilder.class);
	
	public interface VideoAssetBuilder{
		RootNewAssetRequest buildVideoAssetRequest(Integer assetNumber, String assetType,String s3FileName,String assetSize,RootBrandResponse brandResponse);
	}
	private String scenarioTestDataPath;
	private RootNewAssetRequest rootNewAssetRequest;
	private RootAssetRequest rootAssetRequest;
	private static RootCampaignResponse rootCampaignResponse;
	private Map<Integer,VideoAssetBuilder> assetTypeHandler;
	FileWriterUtils fileWriter;

	public AssetBuilder(String scenarioTestDataPath)
	{
		this.scenarioTestDataPath = scenarioTestDataPath;
		fileWriter = new FileWriterUtils(scenarioTestDataPath);
		rootCampaignResponse = DependencyManager.getInjector().getInstance(ResponseBuilder.class)
				.getRootCampaignResponse(scenarioTestDataPath);
		this.assetTypeHandler = new HashMap<>();
		this.setAssetTypeHandler();
	}

	public String getScenarioTestDataPath()
	{
		return scenarioTestDataPath;
	}

	public void setScenarioTestDataPath(String scenarioTestDataPath)
	{
		this.scenarioTestDataPath = scenarioTestDataPath;
	}

	public RootNewAssetRequest getRootNewAssetRequest()
	{
		return rootNewAssetRequest;
	}
	
	public void setNewRootAssetRequest(Integer assetNumber, String assetType ,String adTagType,String s3FileName,String assetSize,RootBrandResponse brandResponse)
	{
		this.rootNewAssetRequest = this.getAssetTypeHandler(VideoAssetsAdTagType.valueOf(adTagType.toUpperCase()).getAdTagTypeId())
				.buildVideoAssetRequest(assetNumber, assetType, s3FileName,assetSize,brandResponse);
	}
	
	public RootAssetRequest getRootAssetRequest()
	{
		return rootAssetRequest;
	}

	public void setRootAssetRequest(RootAssetRequest rootAssetRequest)
	{
		this.rootAssetRequest = rootAssetRequest;
	}
	
	public VideoAssetBuilder getAssetTypeHandler(Integer adTagTypeId)
	{
		return assetTypeHandler.get(adTagTypeId);
	}

	public void setAssetTypeHandler()
	{
		assetTypeHandler.put(1, new ThirdPartyAdTagVideoAsset());
		assetTypeHandler.put(2, new ClearStreamAdTagVideoAsset());
	}

	public String getEndPointURLfromCampaignforAsset()
	{
		Integer campaignID = rootCampaignResponse.getId();
		String ENDPOINT_URL = Constants.CAMPAIGN_BASEENDPOINT_URL + "/" + campaignID + "/assets";
		LOG.info("Asset URL " + ENDPOINT_URL);
		return ENDPOINT_URL;
	}
	

	public void prepareAssetRequestBodyForNewAsset(Integer assetNumber, String assetType, String adTagType, String s3FileName,String assetSize,RootBrandResponse brandResponse)
	{
		this.setNewRootAssetRequest(assetNumber, assetType,adTagType,s3FileName,assetSize,brandResponse);
		DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootNewAssetRequestList(this.getRootNewAssetRequest());
	}
	
	public void prepareAssetRequestBodyForPUTRequest(String responseJsonString,String assetType) {
		RootAssetResponse assetResponse = MapBuilderfromJsonString.build(RootAssetResponse.class, responseJsonString, false).get();
		List<Integer> mimeTypeAssetRequest = new ArrayList<>();
		for(int i=0;i<assetResponse.getMimeTypes().size();i++) {
			mimeTypeAssetRequest.add(assetResponse.getMimeTypes().get(i).getId());
		}
		List<AssetEvent> assetEventAssetRequest = new ArrayList<>();
		for(int i=0;i<assetResponse.getAssetEvents().size();i++) {
			AssetEvent assetEvent = new AssetEvent();
			assetEvent.setEventType(assetResponse.getAssetEvents().get(i).getEventType());
			assetEvent.setTrackingUrl(assetResponse.getAssetEvents().get(i).getTrackingUrl());
			assetEventAssetRequest.add(assetEvent);
		}
		RootAssetRequest updateAssetRequestBody = MapBuilderfromJsonFile.buildFromTemplate(RootAssetRequest.class, Constants.ASSETREQUEST_TEMPLATE_DATA, false).get();
		updateAssetRequestBody.setAssetId(assetResponse.getId());
		updateAssetRequestBody.setCampaignId(assetResponse.getCampaignId());
		updateAssetRequestBody.getAsset().setId(assetResponse.getId());
		updateAssetRequestBody.getAsset().setName(assetResponse.getName());
		updateAssetRequestBody.getAsset().setCampaignId(assetResponse.getCampaignId());
		updateAssetRequestBody.getAsset().setAdTagTypeId(assetResponse.getAdTagTypeId());
		updateAssetRequestBody.getAsset().setBundleOption(assetResponse.getBundleOption());
		updateAssetRequestBody.getAsset().setClickTrackingUrls(assetResponse.getClickTrackingUrls());
		updateAssetRequestBody.getAsset().setImpressionTrackingUrls(assetResponse.getImpressionTrackingUrls());
		updateAssetRequestBody.getAsset().setIsLinear(assetResponse.getIsLinear());
		updateAssetRequestBody.getAsset().setIsSkippable(assetResponse.getIsSkippable());
		updateAssetRequestBody.getAsset().setIsVPaid(assetResponse.getIsVPaid());
		updateAssetRequestBody.getAsset().setLandingPageDomain(assetResponse.getLandingPageDomain());
		updateAssetRequestBody.getAsset().setRowIndex(assetResponse.getRowIndex());
		updateAssetRequestBody.getAsset().setThirdPartyAdTag(assetResponse.getThirdPartyAdTag());
		updateAssetRequestBody.getAsset().setVastSecureUrl(assetResponse.getVastSecureUrl());
		updateAssetRequestBody.getAsset().setIsFlightAssociated(assetResponse.getIsFlightAssociated());
		updateAssetRequestBody.getAsset().setThirdPartyUrl(assetResponse.getThirdPartyUrl());
		updateAssetRequestBody.getAsset().setWidth(assetResponse.getWidth());
		updateAssetRequestBody.getAsset().setHeight(assetResponse.getHeight());
		updateAssetRequestBody.getAsset().setAssetMedia(assetResponse.getAssetMedia());
		updateAssetRequestBody.getAsset().setAssetEvents(assetEventAssetRequest);
		updateAssetRequestBody.getAsset().setmime_types(mimeTypeAssetRequest);
		updateAssetRequestBody.getAsset().setType(VideoAssetType.valueOf(assetType).getType());
		updateAssetRequestBody.getAsset().setCompanionEnabled(false);
		updateAssetRequestBody.getAsset().setMimeTypes(null);
		updateAssetRequestBody.getAsset().setFormatedName("("+assetResponse.getId()+")"+" "+":"+" "+assetResponse.getName());
		updateAssetRequestBody.getAsset().setIsGenerateVast(true);
		this.setRootAssetRequest(updateAssetRequestBody);
		DependencyManager.getInjector().getInstance(RequestBuilder.class).setRootVideoAssetRequestListUpdated(updateAssetRequestBody);
		
	}

	public void addAssetResponseToList(String responseAssetJsonString)
	{
		DependencyManager.getInjector().getInstance(ResponseBuilder.class).setAssetResponseList(
				MapBuilderfromJsonString.build(RootAssetResponse.class, responseAssetJsonString, false).get());
	}
	
	public void writeUpdatedAssetsRequestToFile() 
	{
		try
		{
			fileWriter.writeToJsonFile(ObjectType.VIDEOASSETS, QueryMethod.REQUEST).writeResponseJsonArrayObjectToJsonFile(
					RootAssetRequest.class,
					DependencyManager.getInjector().getInstance(RequestBuilder.class).getRootVideoAssetRequestListUpdated());
		}
		catch (JsonIOException | InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public static class ThirdPartyAdTagVideoAsset implements VideoAssetBuilder
	{

		@Override
		public RootNewAssetRequest buildVideoAssetRequest(Integer assetNumber, String assetType,String s3FileName,String assetSize,RootBrandResponse brandResponse)
		{
			
			RootNewAssetRequest request = VideoAssetType.valueOf(assetType).create();
			Integer campaignId = rootCampaignResponse.getId();
			String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
			String assetName = "Asset_" + assetNumber + "_" + assetType+"_ThirdPartyAdTag";
			LOG.info("Asset Type is :" + " " + assetType);
			request.setCampaignId(campaignId);
			request.getAsset().setHeight(assetDimension.split("x")[1]);
			request.getAsset().setWidth(assetDimension.split("x")[0]);
			request.getAsset().setName(assetName);
			request.getAsset().setAdTagTypeId(1);
			request.getAsset().setLandingPageDomain(brandResponse.getLandingPageUrl());
			return  request;
		}
		
	}
	
	public static class ClearStreamAdTagVideoAsset implements VideoAssetBuilder
	{
		@Override
		public RootNewAssetRequest buildVideoAssetRequest(Integer assetNumber, String assetType,String s3FileName,String assetSize,RootBrandResponse brandResponse)
		{
			RootNewAssetRequest request = VideoAssetType.valueOf(assetType).create();
			Integer campaignId = rootCampaignResponse.getId();
			String assetDimension = !assetSize.isEmpty() ? AssetSize.valueOf(assetSize.toUpperCase()).getAssetDimension() : "300x250";
			String assetName = "Asset_" + assetNumber + "_" + assetType+"_ClearStreamAdTag";
			LOG.info("Asset Type is :" + " " + assetType);
			request.setCampaignId(campaignId);
			request.getAsset().setName(assetName);
			request.getAsset().setAdTagTypeId(2);
			request.getAsset().setThirdPartyUrl(null);
			request.getAsset().setHeight(assetDimension.split("x")[1]);
			request.getAsset().setWidth(assetDimension.split("x")[0]);
			request.getAsset().setAssetMedia(VideoAssetsAdTagType.CLEARSTREAMADTAG.getAssetMediaForAdTagType());
			request.getAsset().getAssetMedia().setFileKey(s3FileName);
			request.getAsset().setLandingPageDomain(brandResponse.getLandingPageUrl());
			return  request;
		}
		
	}
}
