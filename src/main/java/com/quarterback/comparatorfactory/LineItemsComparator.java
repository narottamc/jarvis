/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.quarterback.validators.LineItemsValidator;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Clearstream.
 *
 */
public interface LineItemsComparator
{
	Map<Integer,String> getLineItemAttributeMap();
	String getStartDateForLineItem();
	String getEndDateForLineItem();
	Set<Integer> getCreativeForLineItem();
	Integer getStrategyIdForLineItem();
	List<Map<String,Double>> getStrategyParameterForLineItem();
	Set<Integer> getTrackingConversionForLineItem();
	Integer getIsOptimizedPixel();
	Set<Integer> getLineItemsId();
	Integer getTotalLineItemsCount();
	
	public default int compareLineItemDetails(LineItemsComparator xmlCampaignDetails) {
	     return ComparisonChain.start()
	    		 .compare(this.getLineItemAttributeMap(), xmlCampaignDetails.getLineItemAttributeMap(), LineItemsValidator.MapComparator)
	    		 .compare(this.getStartDateForLineItem(),xmlCampaignDetails.getStartDateForLineItem())
	    		 .compare(this.getEndDateForLineItem(), xmlCampaignDetails.getEndDateForLineItem())
	    		 .compare(this.getStrategyIdForLineItem(), xmlCampaignDetails.getStrategyIdForLineItem())
	    		 .compare(this.getCreativeForLineItem(), xmlCampaignDetails.getCreativeForLineItem(), LineItemsValidator.SetComparator)
	    		 .compare(this.getStrategyParameterForLineItem(), xmlCampaignDetails.getStrategyParameterForLineItem(), LineItemsValidator.ListComparator)
				 .compare(this.getTrackingConversionForLineItem(),xmlCampaignDetails.getTrackingConversionForLineItem(),LineItemsValidator.SetComparator)
				 .compare(this.getIsOptimizedPixel(),xmlCampaignDetails.getIsOptimizedPixel())
	    		 .result();
	   }
	
	public default int compareAllLineItemInfo(LineItemsComparator xmlCampaignDetails) {
	     return ComparisonChain.start()
	    		 .compare(this.getLineItemsId(), xmlCampaignDetails.getLineItemsId(), LineItemsValidator.SetComparator)
	    		 .compare(this.getTotalLineItemsCount(), xmlCampaignDetails.getTotalLineItemsCount())
	    		 .result();
	   }
}
