/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Multimap;
import com.quarterback.validators.SSPValidator;

import java.util.Map;

/**
 * @author Clearstream.
 */
public interface SSPComparator {

    Map<Integer,String> getSSPAttribute();
    Integer getTotalSSPCount();
    Map<String,Multimap>getSellerNetworkDealsAndSeatsDetails();

    public default int compareSSPMetaInfo(SSPComparator xmlCampaignDetails) {
        return ComparisonChain.start()
                .compare(this.getTotalSSPCount(), xmlCampaignDetails.getTotalSSPCount())
                .compare(this.getSSPAttribute(), xmlCampaignDetails.getSSPAttribute(), SSPValidator.MapComparator)
                .result();
    }

    public default int compareSSPDetailsInfo(SSPComparator xmlCampaignDetails) {
        return ComparisonChain.start()
                .compare(this.getSellerNetworkDealsAndSeatsDetails(), xmlCampaignDetails.getSellerNetworkDealsAndSeatsDetails(), SSPValidator.SSPComparator)
                .result();
    }
}

