/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.quarterback.validators.AdvertiserPlacementValidator;

import java.util.Map;

/**
 * @author Clearstream.
 *
 */
public interface AdvertiserPlacementComparator
{
	Integer getAdvertiserId();
	Integer getPlacementId();
	Map<Integer,Integer> getPlacementAdvertiserMap();
	
	public default int compareTo(AdvertiserPlacementComparator xmlCampaignDetails) {
	     return ComparisonChain.start()
	         .compare(this.getAdvertiserId(),xmlCampaignDetails.getAdvertiserId())
	         .compare(this.getPlacementId(), xmlCampaignDetails.getPlacementId())
	         .compare(this.getPlacementAdvertiserMap(), xmlCampaignDetails.getPlacementAdvertiserMap(),AdvertiserPlacementValidator.MapComparator)
			 .result();
	   }
}
