/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.osapi.factory.DependencyManager;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;
import com.quarterback.validators.CreativesValidator;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Clearstream.
 *
 */
public class CreativeComparator
{
	private List<RootFlightsResponse> flightResponseList;
	private ConfigData.Campaign xmlCampaign;
	
	public CreativeComparator(List<RootFlightsResponse> flightResponseList,Integer placementId)
	{
		this.flightResponseList = flightResponseList;
		this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(placementId);
	}
	public CreativeComparator() {
		
	}
	public  Map<Integer,String> getCreativeAttributes(){
		return null;
	}
	public Integer getCreativeDuration() {
		return null;	
	}
	public Set<String> getMimeTypes(){
		return null;
	}
	public String getLinear() {
		return null;
	}
	public String getSkippable() {
		return null;
	}
	public String getVpaid() {
		return null;
	}
	public String getVastURL(){
		return null;
	}
	public String getVastURLSecure() {
		return null;
	}
	public Map<String,String> getBidSwitchMacros(){
		return null;
	}
	public String getLandingPageDomain() {
		return null;
	}
	public String getSize() {
		return null;
	}
	public String getAdType() {
		return null;
	}
	public String getCanTrackClicks() {
		return null;
	}
	public String getHTMLContent(){
		return null;
	}


	public Set<Integer> getCreativeItemsIdsOS(){
		Set<Integer> creativesId = new HashSet<Integer>();
		for(int i=0;i<flightResponseList.size();i++)
		{
			if(flightResponseList.get(i).getActive()){
				for(FlightResponseAd flightad : flightResponseList.get(i).getAds())
				{
					creativesId.add(flightad.getId());
				}
			}
		}
		return creativesId;
	}
	public Integer getTotalCreativesCountOS() {
		Integer totalCreativesOS = 0;
		for (int i = 0; i < flightResponseList.size(); i++) {
			if (flightResponseList.get(i).getActive()) {
				totalCreativesOS = totalCreativesOS + flightResponseList.get(i).getAds().size();
			}

		}
		return totalCreativesOS;
}
	
	public Set<Integer> getCreativesItemsIdsXML(){
		Set<Integer> creativesIdXML = new HashSet<Integer>();
		for(int i=0;i<xmlCampaign.line_items.size();i++)
		{
			creativesIdXML.add(Integer.parseInt(xmlCampaign.creatives.get(i).id));
		}
		return creativesIdXML;
	}
	
	public Integer getTotalCreativeCountXML() {
		return xmlCampaign.creatives.size();
	}
	public int compareCreativeDetailsForVideo(CreativeComparator xmlCampaign) {
		return ComparisonChain.start()
				.compare(this.getCreativeAttributes(), xmlCampaign.getCreativeAttributes(), CreativesValidator.MapComparatorWithIntegerKey)
				.compare(this.getCreativeDuration(), xmlCampaign.getCreativeDuration())
				.compare(this.getMimeTypes(),xmlCampaign.getMimeTypes(), CreativesValidator.SetComparatorForString)
				.compare(this.getLinear(), xmlCampaign.getLinear())
				.compare(this.getSkippable(), xmlCampaign.getSkippable())
				.compare(this.getVpaid(), xmlCampaign.getVpaid())
				.compare(this.getVastURL(), xmlCampaign.getVastURL())
				.compare(this.getVastURLSecure(), xmlCampaign.getVastURLSecure())
				.compare(this.getBidSwitchMacros(), xmlCampaign.getBidSwitchMacros(), CreativesValidator.MapComparatorWithStringKey)
				.compare(this.getLandingPageDomain(), xmlCampaign.getLandingPageDomain())
				.result();
	}
	
	public  int compareCreativeDetailForHTML(CreativeComparator xmlCampaign) {
		return ComparisonChain.start()
				.compare(this.getCreativeAttributes(), xmlCampaign.getCreativeAttributes(), CreativesValidator.MapComparatorWithIntegerKey)
				.compare(this.getCreativeDuration(), xmlCampaign.getCreativeDuration())
				.compare(this.getLandingPageDomain(), xmlCampaign.getLandingPageDomain())
				.compare(this.getSize(), xmlCampaign.getSize())
				.compare(this.getAdType(), xmlCampaign.getAdType())
				.compare(this.getCanTrackClicks(), xmlCampaign.getCanTrackClicks())
				.compare(this.getHTMLContent(), xmlCampaign.getHTMLContent())
				.result();
	}
	
	public int compareCreativesMetaInfo() {
		return ComparisonChain.start()
			   .compare(this.getTotalCreativesCountOS(), this.getTotalCreativeCountXML())
			   .compare(this.getCreativeItemsIdsOS(), this.getCreativesItemsIdsXML(), CreativesValidator.SetComparatorForInteger)
			   .result();
	}

}
