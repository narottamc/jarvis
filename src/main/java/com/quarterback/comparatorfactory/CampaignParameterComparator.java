/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;

/**
 * @author Clearstream.
 */
public interface CampaignParameterComparator {
    Integer getPlacementId();

    String getPlacementName();

    Integer getCampaignId();

    String getCampaignName();

    String getInsertionOrder();

    boolean getDoubleVerifyEnabled();

    boolean isSmartListEnabled();

    public default int compareTo(CampaignParameterComparator xmlCampaignDetails) {
        return ComparisonChain.start()
                .compare(this.getPlacementId(), xmlCampaignDetails.getPlacementId())
                .compare(this.getPlacementName(), xmlCampaignDetails.getPlacementName())
                .compare(this.getCampaignId(), xmlCampaignDetails.getCampaignId())
                .compare(this.getCampaignName(), xmlCampaignDetails.getCampaignName())
                .compare(this.getInsertionOrder(), xmlCampaignDetails.getInsertionOrder())
                .compareFalseFirst(this.getDoubleVerifyEnabled(), xmlCampaignDetails.getDoubleVerifyEnabled())
                .compareFalseFirst(this.isSmartListEnabled(), xmlCampaignDetails.isSmartListEnabled())
                .result();
    }
}
