/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.osapi.factory.DependencyManager;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.quarterback.factory.SessionVariables;
import com.quarterback.validators.CreativesValidator;

import java.util.List;

/**
 * @author Clearstream.
 *
 */
public enum CreativeComparatorType
{
	VIDEO{
		@Override
		public String getAssetSizeForProductType(RootFlightsResponse flightResponseOS)
		{
			return "300x250";
		}
		@Override
		public CreativeComparator getCreativeCompratorOS(String scenarioTestDataPath, FlightResponseAd flightAd,
														 RootFlightsResponse flightResponse, Integer productType, RootPlacementResponse placementResponse){
			return new CreativesValidator.VideoCreativeOS(scenarioTestDataPath, flightAd, flightResponse, productType,placementResponse);
		}
		@Override
		public CreativeComparator getCreativeCompratorXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd) {
			return new CreativesValidator.VideoCreativeXML(flightResponseOS, flightAd);
		}
		@Override
		public int compareCreativeInfoProductType(CreativeComparator comparator,CreativeComparator xmlCampaign)
		{
			return comparator.compareCreativeDetailsForVideo(xmlCampaign);
		}
		@Override
		public String getLandingPageDomainForProductType(RootBrandResponse brandResponse,
				RootFlightsResponse flightResponseOS)
		{
			return brandResponse.getLandingPageUrl();
		}
	},
	ALCHEMY{
		@Override
		public String getAssetSizeForProductType(RootFlightsResponse flightResponseOS)
		{
			String scenarioTestData = DependencyManager.getInjector().getInstance(SessionVariables.class).getScenarioTestDataPath();
			List<RootAssetResponse> videoAssetResponseList = DependencyManager.getInjector().
					getInstance(SessionVariables.class).getVideoAssetsForAFlight(scenarioTestData, flightResponseOS);
			return videoAssetResponseList.get(0).getWidth()+"x"+videoAssetResponseList.get(0).getHeight();
		}
		@Override
		public CreativeComparator getCreativeCompratorOS(String scenarioTestDataPath, FlightResponseAd flightAd,
				RootFlightsResponse flightResponse, Integer productType, RootPlacementResponse placementResponse){
			return new CreativesValidator.HTMLCreativeOS(scenarioTestDataPath, flightAd, flightResponse,productType);
		}
		@Override
		public CreativeComparator getCreativeCompratorXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd) {
			return new CreativesValidator.HTMLCreativeXML(flightResponseOS, flightAd);
		}
		@Override
		public int compareCreativeInfoProductType(CreativeComparator comparator,CreativeComparator xmlCampaign)
		{
			return comparator.compareCreativeDetailForHTML(xmlCampaign);
		}
		@Override
		public String getLandingPageDomainForProductType(RootBrandResponse brandResponse,
				RootFlightsResponse flightResponseOS)
		{
			return brandResponse.getLandingPageUrl();
		}
		
	},
	SURVEY{
		@Override
		public String getAssetSizeForProductType(RootFlightsResponse flightResponseOS)
		{
			String scenarioTestData = DependencyManager.getInjector().getInstance(SessionVariables.class).getScenarioTestDataPath();
			List<RootSurveyResponse> surveyResponseList = DependencyManager.getInjector().
					getInstance(SessionVariables.class).getSurveyAssetsForAFlight(scenarioTestData, flightResponseOS);
			return surveyResponseList.get(0).getWidth()+"x"+surveyResponseList.get(0).getHeight();
		}
		@Override
		public CreativeComparator getCreativeCompratorOS(String scenarioTestDataPath, FlightResponseAd flightAd,
				RootFlightsResponse flightResponse, Integer productType, RootPlacementResponse placementResponse){
			return new CreativesValidator.HTMLCreativeOS(scenarioTestDataPath, flightAd,flightResponse, productType);
		}
		@Override
		public CreativeComparator getCreativeCompratorXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd) {
			return new CreativesValidator.HTMLCreativeXML(flightResponseOS, flightAd);
		}
		@Override
		public int compareCreativeInfoProductType(CreativeComparator comparator,CreativeComparator xmlCampaign)
		{
			return comparator.compareCreativeDetailForHTML(xmlCampaign);
		}
		@Override
		public String getLandingPageDomainForProductType(RootBrandResponse brandResponse,
				RootFlightsResponse flightResponseOS)
		{
			return brandResponse.getLandingPageUrl();
		}
		
	},
	DISPLAY{
		@Override
		public String getAssetSizeForProductType(RootFlightsResponse flightResponseOS)
		{
			String scenarioTestData = DependencyManager.getInjector().getInstance(SessionVariables.class).getScenarioTestDataPath();
			List<RootDisplayResponse> displayResponseList = DependencyManager.getInjector().
					getInstance(SessionVariables.class).getDisplayAssetsForAFlight(scenarioTestData, flightResponseOS);
			return displayResponseList.get(0).getWidth()+"x"+displayResponseList.get(0).getHeight();
		}
		@Override
		public CreativeComparator getCreativeCompratorOS(String scenarioTestDataPath, FlightResponseAd flightAd,
				RootFlightsResponse flightResponse, Integer productType, RootPlacementResponse placementResponse){
			return new CreativesValidator.HTMLCreativeOS(scenarioTestDataPath, flightAd,flightResponse, productType);
		}
		@Override
		public CreativeComparator getCreativeCompratorXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd) {
			return new CreativesValidator.HTMLCreativeXML(flightResponseOS, flightAd);
		}
		@Override
		public int compareCreativeInfoProductType(CreativeComparator comparator,CreativeComparator xmlCampaign)
		{
			return comparator.compareCreativeDetailForHTML(xmlCampaign);
		}
		@Override
		public String getLandingPageDomainForProductType(RootBrandResponse brandResponse,
				RootFlightsResponse flightResponseOS)
		{
            return brandResponse.getLandingPageUrl();
		}
	};
	

	public CreativeComparator getCreativeCompratorOS(String scenarioTestDataPath, FlightResponseAd flightAd,
			RootFlightsResponse flightResponse, Integer productType, RootPlacementResponse placementResponse){
		return null;
	}

	public CreativeComparator getCreativeCompratorXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd) {
		return null;
	}
	
	public int compareCreativeInfoProductType(CreativeComparator comparator,CreativeComparator xmlCampaign) {
		return 1;
	}
	
	public String getAssetSizeForProductType(RootFlightsResponse flightResponseOS) {
		return null;
	}
	public String getLandingPageDomainForProductType(RootBrandResponse brandResponse,RootFlightsResponse flightResponseOS) {
		return null;
	}
}
