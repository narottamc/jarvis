/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Multimap;
import com.quarterback.validators.ActivityPixelValidator;

/**
 * @author Clearstream.
 */
public interface ActivityPixelComparator {

    Multimap<Integer,String>getActivityPixelDetails();

    default int compareActivityPixelDetails(ActivityPixelComparator xmlCampaignDetails) {
        return ComparisonChain.start()
                .compare(this.getActivityPixelDetails(),xmlCampaignDetails.getActivityPixelDetails(), ActivityPixelValidator.MultiMapComparator)
                .result();
    }
}
