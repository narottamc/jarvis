/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.comparatorfactory;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Multimap;
import com.quarterback.factory.PlayerSize;
import com.quarterback.validators.TargetingValidator;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Clearstream.
 */
public interface TargetingComparator {
    List<String> getTargetingLanguages();

    Multimap<String, Map<String, Boolean>> getTargetingCountries();

    Map<Boolean, TreeSet<String>> getTargetingZipCodes();

    Map<Boolean, List<Integer>> getTargetingDmas();

    Map<String, Map<String, Boolean>> getDayParts();

    Map<String, Boolean> getBrowserVersionDetails();

    Map<String, Boolean> getOSVersionDetails();

    Map<Boolean, TreeSet<String>> getDeviceTypeDetails();

    //Integer getAdvertiserCategories();
    Map<Boolean, Multimap<Integer, Integer>> getSiteAdvertiserCategories();

    String getTypeForSiteCategories();

    Set<String> getVisibilityDetails();

    String getInventoryTypeDetails();

    Map<String, Boolean> getSSPSellerNetwork();

    Multimap<String, Integer> getPageDomainList();

    Multimap<String, Integer> getSiteDomainList();

    String getDomainOperator();

    Set<PlayerSize> getPlayerSizes();


    default int compareTargetingInfo(TargetingValidator.XMLTargeting xmlTargetingDetails) {
        return ComparisonChain.start()
                .compare(this.getTargetingLanguages(), xmlTargetingDetails.getTargetingLanguages(), TargetingValidator.ComparatorForListOfString)
                .compare(this.getTargetingCountries(), xmlTargetingDetails.getTargetingCountries(), TargetingValidator.ComparatorForMultiMapOfStringAndMap)
                .compare(this.getTargetingZipCodes(), xmlTargetingDetails.getTargetingZipCodes(), TargetingValidator.ComparatorForMapOfBooleanAndStringTreeSet)
                .compare(this.getTargetingDmas(), xmlTargetingDetails.getTargetingDmas(), TargetingValidator.ComparatorForMapOfBooleanAndIntegerList)
                .compare(this.getDayParts(), xmlTargetingDetails.getDayParts(), TargetingValidator.ComparatorForMapOfStringAndMap)
                .compare(this.getBrowserVersionDetails(), xmlTargetingDetails.getBrowserVersionDetails(), TargetingValidator.ComparatorForMapOfStringAndBoolean)
                .compare(this.getOSVersionDetails(), xmlTargetingDetails.getOSVersionDetails(), TargetingValidator.ComparatorForMapOfStringAndBoolean)
                .compare(this.getDeviceTypeDetails(), xmlTargetingDetails.getDeviceTypeDetails(), TargetingValidator.ComparatorForMapOfBooleanAndStringTreeSet)
//               .compare(this.getAdvertiserCategories(), xmlTargetingDetails.getAdvertiserCategories())
                .compare(this.getSiteAdvertiserCategories(), xmlTargetingDetails.getSiteAdvertiserCategories(), TargetingValidator.ComparatorForMapOfBooleanAndMultiMap)
                .compare(this.getVisibilityDetails(), xmlTargetingDetails.getVisibilityDetails(), TargetingValidator.ComparatorForSetOfString)
                .compare(this.getInventoryTypeDetails(), xmlTargetingDetails.getInventoryTypeDetails())
                .compare(this.getSSPSellerNetwork(), xmlTargetingDetails.getSSPSellerNetwork(), TargetingValidator.ComparatorForMapOfStringAndBoolean)
                .compare(this.getPageDomainList(), xmlTargetingDetails.getPageDomainList(), TargetingValidator.ComparatorForMultiMapOfStringAndInteger)
                .compare(this.getSiteDomainList(), xmlTargetingDetails.getSiteDomainList(), TargetingValidator.ComparatorForMultiMapOfStringAndInteger)
                .compare(this.getDomainOperator(), xmlTargetingDetails.getDomainOperator())
                .compare(this.getPlayerSizes(), xmlTargetingDetails.getPlayerSizes(), TargetingValidator.PlayerSizeComparator)
                .compare(this.getTypeForSiteCategories(), xmlTargetingDetails.getTypeForSiteCategories())
                .result();
    }

}
