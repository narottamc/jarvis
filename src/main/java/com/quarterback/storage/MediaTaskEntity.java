/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.google.common.base.MoreObjects;
import com.quarterback.models.OutputFileType;
import com.quarterback.models.OutputMechanism;
import com.quarterback.models.TaskCategory;
import com.quarterback.models.TaskStatus;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

/**
 * @author Chunzhao Zheng
 */
public class MediaTaskEntity
{
	public static class Builder
	{
		private String task_group_id_;

		private String task_id_;
		private TaskCategory task_category_;
		private TaskStatus task_status_;

		private Long date_time_;
		private Long timestamp_;

		private OutputMechanism output_mechanism_;
		private String output_file_path_;
		private OutputFileType output_file_type_;

		private String log_stream_;

		private Integer partition_index_;

		private DateTime created_at_;
		private DateTime started_at_;
		private DateTime running_at_;
		private DateTime finished_at_;
		private DateTime failed_at_;

		public MediaTaskEntity.Builder task_group_id(String task_group_id)
		{
			task_group_id_ = task_group_id;
			return this;
		}

		public MediaTaskEntity.Builder task_id(String task_id)
		{
			task_id_ = task_id;
			return this;
		}

		public MediaTaskEntity.Builder task_category(TaskCategory task_category)
		{
			task_category_ = task_category;
			return this;
		}

		public MediaTaskEntity.Builder task_status(TaskStatus task_status)
		{
			task_status_ = task_status;
			return this;
		}

		public MediaTaskEntity.Builder date_time(Long date_time)
		{
			date_time_ = date_time;
			return this;
		}

		public MediaTaskEntity.Builder timestamp(Long timestamp)
		{
			timestamp_ = timestamp;
			return this;
		}

		public MediaTaskEntity.Builder output_mechanism(OutputMechanism output_mechanism)
		{
			output_mechanism_ = output_mechanism;
			return this;
		}

		public MediaTaskEntity.Builder output_file_path(String output_file_path)
		{
			output_file_path_ = output_file_path;
			return this;
		}

		public MediaTaskEntity.Builder output_file_type(OutputFileType output_file_type)
		{
			output_file_type_ = output_file_type;
			return this;
		}

		public MediaTaskEntity.Builder log_stream(String log_stream)
		{
			log_stream_ = log_stream;
			return this;
		}

		public MediaTaskEntity.Builder partition_index(Integer partition_index)
		{
			partition_index_ = partition_index;
			return this;
		}

		public MediaTaskEntity.Builder created_at(DateTime created_at)
		{
			created_at_ = created_at;
			return this;
		}

		public MediaTaskEntity.Builder started_at(DateTime started_at)
		{
			started_at_ = started_at;
			return this;
		}

		public MediaTaskEntity.Builder running_at(DateTime running_at)
		{
			running_at_ = running_at;
			return this;
		}

		public MediaTaskEntity.Builder finished_at(DateTime finished_at)
		{
			finished_at_ = finished_at;
			return this;
		}

		public MediaTaskEntity.Builder failed_at(DateTime failed_at)
		{
			failed_at_ = failed_at;
			return this;
		}

		public MediaTaskEntity build()
		{
			return new MediaTaskEntity(task_group_id_, task_id_, task_category_, task_status_, date_time_, timestamp_,
					output_mechanism_, output_file_path_, output_file_type_, log_stream_, partition_index_, created_at_,
					started_at_, running_at_, finished_at_, failed_at_);
		}
	}

	public String task_group_id;

	public final String task_id;
	public final TaskCategory task_category;
	public final TaskStatus task_status;

	public final Long date_time;
	public final Long timestamp;

	public final OutputMechanism output_mechanism;
	public final String output_file_path;
	private OutputFileType output_file_type;

	public final String log_stream;

	public final Integer partition_index;

	public final DateTime created_at;
	public final DateTime started_at;
	public final DateTime running_at;
	public final DateTime finished_at;
	public final DateTime failed_at;

	public MediaTaskEntity(String task_group_id, String task_id, TaskCategory task_category, TaskStatus task_status,
			Long date_time, Long timestamp, OutputMechanism output_mechanism, String output_file_path,
			OutputFileType output_file_type, String log_stream, @Nullable Integer partition_index,
			@Nullable DateTime created_at, @Nullable DateTime started_at, @Nullable DateTime running_at,
			@Nullable DateTime finished_at, @Nullable DateTime failed_at)
	{
		this.task_group_id = task_group_id;

		this.task_id = task_id;
		this.task_category = task_category;
		this.task_status = task_status;

		this.date_time = date_time;
		this.timestamp = timestamp;

		this.output_mechanism = output_mechanism;
		this.output_file_path = output_file_path;
		this.output_file_type = output_file_type;

		this.log_stream = log_stream;

		this.partition_index = partition_index;

		this.created_at = created_at;
		this.started_at = started_at;
		this.running_at = running_at;
		this.finished_at = finished_at;
		this.failed_at = failed_at;
	}

	@Override
	public String toString()
	{
		MoreObjects.ToStringHelper helper = MoreObjects.toStringHelper("MediaTaskEntity");

		if (task_group_id != null)
		{
			helper = helper.add("task_group_id", task_group_id);
		}
		if (task_id != null)
		{
			helper = helper.add("task_id", task_id);
		}
		if (task_category != null)
		{
			helper = helper.add("task_category", task_category);
		}
		if (task_status != null)
		{
			helper = helper.add("task_status", task_status);
		}
		if (date_time != null)
		{
			helper = helper.add("date_time", date_time);
		}
		if (timestamp != null)
		{
			helper = helper.add("timestamp", timestamp);
		}
		if (output_mechanism != null)
		{
			helper = helper.add("output_mechanism", output_mechanism);
		}
		if (output_file_path != null)
		{
			helper = helper.add("output_file_path", output_file_path);
		}
		if (output_file_type != null)
		{
			helper = helper.add("output_file_type", output_file_type);
		}
		if (log_stream != null)
		{
			helper = helper.add("log_stream", log_stream);
		}
		if (partition_index != null)
		{
			helper = helper.add("partition_index", partition_index);
		}
		if (created_at != null)
		{
			helper = helper.add("created_at", DATE_TIME_FORMAT.print(created_at));
		}
		if (started_at != null)
		{
			helper = helper.add("started_at", DATE_TIME_FORMAT.print(started_at));
		}
		if (running_at != null)
		{
			helper = helper.add("running_at", DATE_TIME_FORMAT.print(running_at));
		}
		if (finished_at != null)
		{
			helper = helper.add("finished_at", DATE_TIME_FORMAT.print(finished_at));
		}
		if (failed_at != null)
		{
			helper = helper.add("failed_at", DATE_TIME_FORMAT.print(failed_at));
		}

		return helper.toString();
	}

	@Nullable
	public static MediaTaskEntity fromOutcome(UpdateItemOutcome outcome)
	{
		Item item = outcome.getItem();
		return item != null ? fromItem(item) : null;
	}

	public static MediaTaskEntity fromItem(Item item)
	{
		MediaTaskEntity.Builder builder = new MediaTaskEntity.Builder();

		if (item.hasAttribute(MediaTaskField.TASK_GROUP_ID.alias()))
		{
			String value = item.getString(MediaTaskField.TASK_GROUP_ID.alias());
			builder.task_group_id(value);
		}

		if (item.hasAttribute(MediaTaskField.TASK_ID.alias()))
		{
			String value = item.getString(MediaTaskField.TASK_ID.alias());
			builder.task_id(value);
		}

		if (item.hasAttribute(MediaTaskField.TASK_CATEGORY.alias()))
		{
			int code = item.getInt(MediaTaskField.TASK_CATEGORY.alias());
			TaskCategory category = TaskCategory.fromCode(code);
			builder.task_category(category);
		}

		if (item.hasAttribute(MediaTaskField.TASK_STATUS.alias()))
		{
			int code = item.getInt(MediaTaskField.TASK_STATUS.alias());
			TaskStatus status = TaskStatus.fromCode(code);
			builder.task_status(status);
		}

		if (item.hasAttribute(MediaTaskField.DATE_TIME.alias()))
		{
			long value = item.getLong(MediaTaskField.DATE_TIME.alias());
			builder.date_time(value);
		}

		if (item.hasAttribute(MediaTaskField.TIMESTAMP.alias()))
		{
			long value = item.getLong(MediaTaskField.TIMESTAMP.alias());
			builder.timestamp(value);
		}

		if (item.hasAttribute(MediaTaskField.OUTPUT_MECHANISM.alias()))
		{
			int code = item.getInt(MediaTaskField.OUTPUT_MECHANISM.alias());
			OutputMechanism value = OutputMechanism.fromCode(code);
			builder.output_mechanism(value);
		}

		if (item.hasAttribute(MediaTaskField.OUTPUT_FILE_PATH.alias()))
		{
			String value = item.getString(MediaTaskField.OUTPUT_FILE_PATH.alias());
			builder.output_file_path(value);
		}

		if (item.hasAttribute(MediaTaskField.OUTPUT_FILE_TYPE.alias()))
		{
			int code = item.getInt(MediaTaskField.OUTPUT_FILE_TYPE.alias());
			OutputFileType out_file_type = OutputFileType.fromCode(code);
			builder.output_file_type(out_file_type);
		}

		if (item.hasAttribute(MediaTaskField.LOG_STREAM.alias()))
		{
			String value = item.getString(MediaTaskField.LOG_STREAM.alias());
			builder.log_stream(value);
		}

		if (item.hasAttribute(MediaTaskField.PARTITION_INDEX.alias()))
		{
			Integer value = item.getInt(MediaTaskField.PARTITION_INDEX.alias());
			builder.partition_index(value);
		}

		if (item.hasAttribute(MediaTaskField.CREATED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.CREATED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.created_at(date_time);
		}

		if (item.hasAttribute(MediaTaskField.STARTED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.STARTED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.started_at(date_time);
		}

		if (item.hasAttribute(MediaTaskField.RUNNING_AT.alias()))
		{
			String value = item.getString(MediaTaskField.RUNNING_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.running_at(date_time);
		}

		if (item.hasAttribute(MediaTaskField.FINISHED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.FINISHED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.finished_at(date_time);
		}

		if (item.hasAttribute(MediaTaskField.FAILED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.FAILED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.failed_at(date_time);
		}

		return builder.build();
	}

	public static UpdateItemSpec toItem(MediaTaskEntity entity)
	{
		UpdateItemSpec item = new UpdateItemSpec();

		KeyAttribute task_id = new KeyAttribute(MediaTaskField.TASK_ID.alias(), entity.task_id);

		item.withPrimaryKey(new PrimaryKey(task_id));

		if (entity.task_group_id != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskField.TASK_GROUP_ID.alias()).put(entity.task_group_id));
		}

		if (entity.task_category != null)
		{
			int code = entity.task_category.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.TASK_CATEGORY.alias()).put(code));
		}

		if (entity.task_status != null)
		{
			int code = entity.task_status.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.TASK_STATUS.alias()).put(code));
		}

		if (entity.date_time != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.DATE_TIME.alias()).put(entity.date_time));
		}

		if (entity.timestamp != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.TIMESTAMP.alias()).put(entity.timestamp));
		}

		if (StringUtils.isNotEmpty(entity.output_file_path))
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskField.OUTPUT_FILE_PATH.alias()).put(entity.output_file_path));
		}

		if (entity.output_mechanism != null)
		{
			int code = entity.output_mechanism.code();
			;
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.OUTPUT_MECHANISM.alias()).put(code));
		}

		if (entity.output_file_type != null)
		{
			int code = entity.output_file_type.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.OUTPUT_FILE_TYPE.alias()).put(code));
		}

		if (StringUtils.isNotEmpty(entity.log_stream))
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.LOG_STREAM.alias()).put(entity.log_stream));
		}

		if (entity.partition_index != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskField.PARTITION_INDEX.alias()).put(entity.partition_index));
		}

		if (entity.created_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.created_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.CREATED_AT.alias()).put(value));
		}

		if (entity.started_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.started_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.STARTED_AT.alias()).put(value));
		}

		if (entity.running_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.running_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.RUNNING_AT.alias()).put(value));
		}

		if (entity.finished_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.finished_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.FINISHED_AT.alias()).put(value));
		}

		if (entity.failed_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.failed_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskField.FAILED_AT.alias()).put(value));
		}

		return item;
	}

	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyyMMdd").withZoneUTC();
	private final static DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
			.withZoneUTC();
}