/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

/**
 * @author Clearstream
 */
public enum MediaTaskGroupField
{
	TASK_GROUP_ID,
	TASK_GROUP_STATUS,

	DATE_TIME,
	TIMESTAMP,

	OUTPUT_MECHANISM,
	LOG_STREAM,

	USER_ID,
	USER_EMAIL,
	USER_CHANNEL,

	CREATED_AT,
	STARTED_AT,
	FINISHED_AT,
	FAILED_AT,;

	MediaTaskGroupField()
	{
		alias_ = name().toLowerCase();
	}

	public String alias()
	{
		return alias_;
	}

	private final String alias_;
}