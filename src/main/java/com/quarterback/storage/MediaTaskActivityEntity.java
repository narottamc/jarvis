/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.google.common.collect.Sets;
import com.quarterback.models.OutputMechanism;
import com.quarterback.models.TaskStatus;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Set;

/**
 * @author Clearstream
 */
public class MediaTaskActivityEntity
{
	public static class Builder
	{
		private String task_group_id_;

		private String task_id_;
		private String task_comment_;
		private TaskStatus task_status_;
		private Set<String> task_payloads_;
		private Integer task_workload_;

		private Long date_time_;
		private Long timestamp_;

		private OutputMechanism output_mechanism_;
		private String log_stream_;

		private DateTime created_at_;
		private DateTime started_at_;
		private DateTime running_at_;
		private DateTime finished_at_;
		private DateTime failed_at_;

		public MediaTaskActivityEntity.Builder task_group_id(String task_group_id)
		{
			task_group_id_ = task_group_id;
			return this;
		}

		public MediaTaskActivityEntity.Builder task_id(String task_id)
		{
			task_id_ = task_id;
			return this;
		}

		public MediaTaskActivityEntity.Builder task_comment(String task_comment)
		{
			task_comment_ = task_comment;
			return this;
		}

		public MediaTaskActivityEntity.Builder task_status(TaskStatus task_status)
		{
			task_status_ = task_status;
			return this;
		}

		public MediaTaskActivityEntity.Builder task_payloads(Collection<String> task_payloads)
		{
			task_payloads_ = Sets.newHashSet(task_payloads);
			return this;
		}

		public MediaTaskActivityEntity.Builder task_workload(Integer activity_workload)
		{
			task_workload_ = activity_workload;
			return this;
		}

		public MediaTaskActivityEntity.Builder date_time(Long date_time)
		{
			date_time_ = date_time;
			return this;
		}

		public MediaTaskActivityEntity.Builder timestamp(Long timestamp)
		{
			timestamp_ = timestamp;
			return this;
		}

		public MediaTaskActivityEntity.Builder output_mechanism(OutputMechanism output_mechanism)
		{
			output_mechanism_ = output_mechanism;
			return this;
		}

		public MediaTaskActivityEntity.Builder log_stream(String log_stream)
		{
			log_stream_ = log_stream;
			return this;
		}

		public MediaTaskActivityEntity.Builder created_at(DateTime created_at)
		{
			created_at_ = created_at;
			return this;
		}

		public MediaTaskActivityEntity.Builder started_at(DateTime started_at)
		{
			started_at_ = started_at;
			return this;
		}

		public MediaTaskActivityEntity.Builder running_at(DateTime running_at)
		{
			running_at_ = running_at;
			return this;
		}

		public MediaTaskActivityEntity.Builder finished_at(DateTime finished_at)
		{
			finished_at_ = finished_at;
			return this;
		}

		public MediaTaskActivityEntity.Builder failed_at(DateTime failed_at)
		{
			failed_at_ = failed_at;
			return this;
		}

		public MediaTaskActivityEntity build()
		{
			return new MediaTaskActivityEntity(task_group_id_, task_id_, task_status_, task_comment_, task_payloads_,
					task_workload_, date_time_, timestamp_, output_mechanism_, log_stream_, created_at_, started_at_,
					running_at_, finished_at_, failed_at_);
		}
	}

	public final String task_group_id;

	public final String task_id;
	public final String task_comment;
	public final TaskStatus task_status;
	public final Set<String> task_payloads;
	public final Integer task_workload;

	public final Long date_time;
	public final Long timestamp;

	public final OutputMechanism output_mechanism;
	public final String log_stream;

	public final DateTime created_at;
	public final DateTime started_at;
	public final DateTime running_at;
	public final DateTime finished_at;
	public final DateTime failed_at;

	public MediaTaskActivityEntity(String task_group_id, String task_id, TaskStatus task_status, String task_comment,
			Set<String> task_payloads, Integer task_workload, Long date_time, Long timestamp,
			OutputMechanism output_mechanism, String log_stream, @Nullable DateTime created_at,
			@Nullable DateTime started_at, @Nullable DateTime running_at, @Nullable DateTime finished_at,
			@Nullable DateTime failed_at)
	{
		this.task_group_id = task_group_id;

		this.task_id = task_id;
		this.task_status = task_status;
		this.task_comment = task_comment;
		this.task_payloads = task_payloads;
		this.task_workload = task_workload;

		this.date_time = date_time;
		this.timestamp = timestamp;

		this.output_mechanism = output_mechanism;
		this.log_stream = log_stream;

		this.created_at = created_at;
		this.started_at = started_at;
		this.running_at = running_at;
		this.finished_at = finished_at;
		this.failed_at = failed_at;
	}

	@Nullable
	public static MediaTaskActivityEntity fromOutcome(UpdateItemOutcome outcome)
	{
		Item item = outcome.getItem();
		return item != null ? fromItem(item) : null;
	}

	public static MediaTaskActivityEntity fromItem(Item item)
	{
		MediaTaskActivityEntity.Builder builder = new MediaTaskActivityEntity.Builder();

		if (item.hasAttribute(MediaTaskActivityField.TASK_GROUP_ID.alias()))
		{
			String value = item.getString(MediaTaskActivityField.TASK_GROUP_ID.alias());
			builder.task_group_id(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.TASK_ID.alias()))
		{
			String value = item.getString(MediaTaskActivityField.TASK_ID.alias());
			builder.task_id(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.TASK_COMMENT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.TASK_COMMENT.alias());
			builder.task_comment(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.TASK_STATUS.alias()))
		{
			int code = item.getInt(MediaTaskActivityField.TASK_STATUS.alias());
			TaskStatus status = TaskStatus.fromCode(code);
			builder.task_status(status);
		}

		if (item.hasAttribute(MediaTaskActivityField.TASK_PAYLOADS.alias()))
		{
			Set<String> values = item.getStringSet(MediaTaskActivityField.TASK_PAYLOADS.alias());
			builder.task_payloads(values);
		}

		if (item.hasAttribute(MediaTaskActivityField.TASK_WORKLOAD.alias()))
		{
			int value = item.getInt(MediaTaskActivityField.TASK_WORKLOAD.alias());
			builder.task_workload(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.DATE_TIME.alias()))
		{
			long value = item.getLong(MediaTaskActivityField.DATE_TIME.alias());
			builder.date_time(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.TIMESTAMP.alias()))
		{
			long value = item.getLong(MediaTaskActivityField.TIMESTAMP.alias());
			builder.timestamp(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.OUTPUT_MECHANISM.alias()))
		{
			int code = item.getInt(MediaTaskActivityField.OUTPUT_MECHANISM.alias());
			OutputMechanism value = OutputMechanism.fromCode(code);
			builder.output_mechanism(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.LOG_STREAM.alias()))
		{
			String value = item.getString(MediaTaskActivityField.LOG_STREAM.alias());
			builder.log_stream(value);
		}

		if (item.hasAttribute(MediaTaskActivityField.CREATED_AT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.CREATED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.created_at(date_time);
		}

		if (item.hasAttribute(MediaTaskActivityField.STARTED_AT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.STARTED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.started_at(date_time);
		}

		if (item.hasAttribute(MediaTaskActivityField.RUNNING_AT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.RUNNING_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.running_at(date_time);
		}

		if (item.hasAttribute(MediaTaskActivityField.FINISHED_AT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.FINISHED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.finished_at(date_time);
		}

		if (item.hasAttribute(MediaTaskActivityField.FAILED_AT.alias()))
		{
			String value = item.getString(MediaTaskActivityField.FAILED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.failed_at(date_time);
		}

		return builder.build();
	}

	public static UpdateItemSpec toItem(MediaTaskActivityEntity entity)
	{
		UpdateItemSpec item = new UpdateItemSpec();

		KeyAttribute task_id = new KeyAttribute(MediaTaskActivityField.TASK_ID.alias(), entity.task_id);

		item.withPrimaryKey(new PrimaryKey(task_id));

		if (entity.task_group_id != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.TASK_GROUP_ID.alias()).put(entity.task_group_id));
		}

		if (entity.task_comment != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.TASK_COMMENT.alias()).put(entity.task_comment));
		}

		if (entity.task_status != null)
		{
			int code = entity.task_status.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.TASK_STATUS.alias()).put(code));
		}

		if (entity.task_payloads != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.TASK_PAYLOADS.alias()).put(entity.task_payloads));
		}

		if (entity.task_workload != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.TASK_WORKLOAD.alias()).put(entity.task_workload));
		}

		if (entity.date_time != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.DATE_TIME.alias()).put(entity.date_time));
		}

		if (entity.timestamp != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.TIMESTAMP.alias()).put(entity.timestamp));
		}

		if (entity.output_mechanism != null)
		{
			int code = entity.output_mechanism.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.OUTPUT_MECHANISM.alias()).put(code));
		}

		if (entity.log_stream != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskActivityField.LOG_STREAM.alias()).put(entity.log_stream));
		}

		if (entity.created_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.created_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.CREATED_AT.alias()).put(value));
		}

		if (entity.started_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.started_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.STARTED_AT.alias()).put(value));
		}

		if (entity.running_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.running_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.RUNNING_AT.alias()).put(value));
		}

		if (entity.finished_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.finished_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.FINISHED_AT.alias()).put(value));
		}

		if (entity.failed_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.failed_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskActivityField.FAILED_AT.alias()).put(value));
		}

		return item;
	}

	private final static DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
			.withZoneUTC();
}