/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

import com.google.common.base.MoreObjects;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.quarterback.models.TaskCategory;
import com.quarterback.models.TaskStatus;

import java.util.Collection;
import java.util.Map;

/**
 * @author Clearstream
 */
public class MediaTaskResultSet
{
	public MediaTaskResultSet()
	{
		entities_ = HashMultiset.create();
		entity_by_category_ = HashMultimap.create();
		entity_by_status_ = HashMultimap.create();
	}

	public MediaTaskResultSet add(MediaTaskEntity entity)
	{
		entities_.add(entity);
		entity_by_category_.put(entity.task_category, entity);
		entity_by_status_.put(entity.task_status, entity);

		return this;
	}

	public Collection<MediaTaskEntity> get(TaskCategory category)
	{
		return entity_by_category_.get(category);
	}

	public Collection<MediaTaskEntity> get(TaskStatus status)
	{
		return entity_by_status_.get(status);
	}

	public int size(TaskStatus status)
	{
		return entity_by_status_.get(status).size();
	}

	public int size()
	{
		return entities_.size();
	}

	public boolean isEmpty()
	{
		return entities_.isEmpty();
	}

	public String overview()
	{
		MoreObjects.ToStringHelper helper = MoreObjects.toStringHelper("MediaTaskResultSet");

		for (Map.Entry<TaskCategory, Collection<MediaTaskEntity>> entry : entity_by_category_.asMap().entrySet())
		{
			helper.add(entry.getKey().name(), entry.getValue().size());
		}

		for (Map.Entry<TaskStatus, Collection<MediaTaskEntity>> entry : entity_by_status_.asMap().entrySet())
		{
			helper.add(entry.getKey().name(), entry.getValue().size());
		}

		return helper.toString();
	}

	private final Multiset<MediaTaskEntity> entities_;
	private final Multimap<TaskCategory, MediaTaskEntity> entity_by_category_;
	private final Multimap<TaskStatus, MediaTaskEntity> entity_by_status_;
}
