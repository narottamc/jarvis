/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.google.common.base.MoreObjects;
import com.google.common.primitives.Longs;
import com.quarterback.models.OutputMechanism;
import com.quarterback.models.TaskGroupStatus;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public class MediaTaskGroupEntity
{
	public static class Builder
	{
		private String task_group_id_;
		private TaskGroupStatus task_group_status_;

		private Long date_time_;
		private Long timestamp_;

		private OutputMechanism output_mechanism_;
		private String log_stream_;

		private Integer user_id_;
		private String user_email_;
		private String user_channel_;

		private DateTime created_at_;
		private DateTime started_at_;
		private DateTime finished_at_;
		private DateTime failed_at_;

		public MediaTaskGroupEntity.Builder task_group_id(String task_group_id)
		{
			task_group_id_ = task_group_id;
			return this;
		}

		public MediaTaskGroupEntity.Builder task_group_status(TaskGroupStatus task_group_status)
		{
			task_group_status_ = task_group_status;
			return this;
		}

		MediaTaskGroupEntity.Builder date_time(Long date_time)
		{
			date_time_ = date_time;
			return this;
		}

		MediaTaskGroupEntity.Builder timestamp(Long timestamp)
		{
			timestamp_ = timestamp;
			return this;
		}

		public MediaTaskGroupEntity.Builder date_time(DateTime date_time)
		{
			String value = DATE_FORMAT.print(date_time);
			date_time_ = Longs.tryParse(value);
			timestamp_ = date_time.getMillis();
			return this;
		}

		public MediaTaskGroupEntity.Builder output_mechanism(OutputMechanism output_mechanism)
		{
			output_mechanism_ = output_mechanism;
			return this;
		}

		public MediaTaskGroupEntity.Builder log_stream(String log_stream)
		{
			log_stream_ = log_stream;
			return this;
		}

		public MediaTaskGroupEntity.Builder user_id(Integer user_id)
		{
			user_id_ = user_id;
			return this;
		}

		public MediaTaskGroupEntity.Builder user_email(String user_email)
		{
			user_email_ = user_email;
			return this;
		}

		public MediaTaskGroupEntity.Builder user_channel(String user_channel)
		{
			user_channel_ = user_channel;
			return this;
		}

		public MediaTaskGroupEntity.Builder created_at(DateTime created_at)
		{
			created_at_ = created_at;
			return this;
		}

		public MediaTaskGroupEntity.Builder started_at(DateTime started_at)
		{
			started_at_ = started_at;
			return this;
		}

		public MediaTaskGroupEntity.Builder finished_at(DateTime finished_at)
		{
			finished_at_ = finished_at;
			return this;
		}

		public MediaTaskGroupEntity.Builder failed_at(DateTime failed_at)
		{
			failed_at_ = failed_at;
			return this;
		}

		public MediaTaskGroupEntity build()
		{
			return new MediaTaskGroupEntity(task_group_id_, task_group_status_, date_time_, timestamp_,
					output_mechanism_, log_stream_, user_id_, user_email_, user_channel_, created_at_, started_at_,
					finished_at_, failed_at_);
		}
	}

	public final String task_group_id;
	public final TaskGroupStatus task_group_status;

	public final Long date_time;
	public final Long timestamp;

	public final OutputMechanism output_mechanism;
	public final String log_stream;

	public final Integer user_id;
	public final String user_email;
	public final String user_channel;

	public final DateTime created_at;
	public final DateTime started_at;
	public final DateTime finished_at;
	public final DateTime failed_at;

	public MediaTaskGroupEntity(String task_group_id, TaskGroupStatus task_group_status, Long date_time, Long timestamp,
			OutputMechanism output_mechanism, String log_stream, Integer user_id, String user_email,
			String user_channel, @Nullable DateTime created_at, @Nullable DateTime started_at,
			@Nullable DateTime finished_at, @Nullable DateTime failed_at)
	{
		this.task_group_id = task_group_id;
		this.date_time = date_time;
		this.task_group_status = task_group_status;

		this.output_mechanism = output_mechanism;

		this.log_stream = log_stream;
		this.timestamp = timestamp;

		this.user_id = user_id;
		this.user_email = user_email;
		this.user_channel = user_channel;

		this.created_at = created_at;
		this.started_at = started_at;
		this.finished_at = finished_at;
		this.failed_at = failed_at;
	}

	@Override
	public String toString()
	{
		MoreObjects.ToStringHelper helper = MoreObjects.toStringHelper("MediaTaskGroupEntity");

		if (task_group_id != null)
		{
			helper = helper.add("task_group_id", task_group_id);
		}
		if (task_group_status != null)
		{
			helper = helper.add("task_group_status", task_group_status);
		}
		if (date_time != null)
		{
			helper = helper.add("date_time", date_time);
		}
		if (timestamp != null)
		{
			helper = helper.add("timestamp", timestamp);
		}
		if (output_mechanism != null)
		{
			helper = helper.add("output_mechanism", output_mechanism);
		}
		if (log_stream != null)
		{
			helper = helper.add("log_stream", log_stream);
		}
		if (user_id != null)
		{
			helper = helper.add("user_id", user_id);
		}
		if (user_channel != null)
		{
			helper = helper.add("user_channel", user_channel);
		}
		if (user_email != null)
		{
			helper = helper.add("user_email", user_email);
		}
		if (created_at != null)
		{
			helper = helper.add("created_at", DATE_TIME_FORMAT.print(created_at));
		}
		if (started_at != null)
		{
			helper = helper.add("started_at", DATE_TIME_FORMAT.print(started_at));
		}
		if (finished_at != null)
		{
			helper = helper.add("finished_at", DATE_TIME_FORMAT.print(finished_at));
		}
		if (failed_at != null)
		{
			helper = helper.add("failed_at", DATE_TIME_FORMAT.print(failed_at));
		}

		return helper.toString();
	}

	@Nullable
	public static MediaTaskGroupEntity fromOutcome(UpdateItemOutcome outcome)
	{
		Item item = outcome.getItem();
		return item != null ? fromItem(item) : null;
	}

	public static MediaTaskGroupEntity fromItem(Item item)
	{
		MediaTaskGroupEntity.Builder builder = new MediaTaskGroupEntity.Builder();

		if (item.hasAttribute(MediaTaskGroupField.TASK_GROUP_ID.alias()))
		{
			String value = item.getString(MediaTaskGroupField.TASK_GROUP_ID.alias());
			builder.task_group_id(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.TASK_GROUP_STATUS.alias()))
		{
			int code = item.getInt(MediaTaskGroupField.TASK_GROUP_STATUS.alias());
			TaskGroupStatus status = TaskGroupStatus.fromCode(code);
			builder.task_group_status(status);
		}

		if (item.hasAttribute(MediaTaskGroupField.DATE_TIME.alias()))
		{
			long value = item.getLong(MediaTaskGroupField.DATE_TIME.alias());
			builder.date_time(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.TIMESTAMP.alias()))
		{
			long value = item.getLong(MediaTaskGroupField.TIMESTAMP.alias());
			builder.timestamp(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.OUTPUT_MECHANISM.alias()))
		{
			int code = item.getInt(MediaTaskGroupField.OUTPUT_MECHANISM.alias());
			OutputMechanism value = OutputMechanism.fromCode(code);
			builder.output_mechanism(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.LOG_STREAM.alias()))
		{
			String value = item.getString(MediaTaskGroupField.LOG_STREAM.alias());
			builder.log_stream(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.USER_ID.alias()))
		{
			int value = item.getInt(MediaTaskGroupField.USER_ID.alias());
			builder.user_id(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.USER_EMAIL.alias()))
		{
			String value = item.getString(MediaTaskGroupField.USER_EMAIL.alias());
			builder.user_email(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.USER_CHANNEL.alias()))
		{
			String value = item.getString(MediaTaskGroupField.USER_CHANNEL.alias());
			builder.user_channel(value);
		}

		if (item.hasAttribute(MediaTaskGroupField.CREATED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.CREATED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.created_at(date_time);
		}

		if (item.hasAttribute(MediaTaskGroupField.STARTED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.STARTED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.started_at(date_time);
		}

		if (item.hasAttribute(MediaTaskGroupField.FINISHED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.FINISHED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.finished_at(date_time);
		}

		if (item.hasAttribute(MediaTaskGroupField.FAILED_AT.alias()))
		{
			String value = item.getString(MediaTaskField.FAILED_AT.alias());
			DateTime date_time = DATE_TIME_FORMAT.parseDateTime(value);
			builder.failed_at(date_time);
		}

		return builder.build();
	}

	public static UpdateItemSpec toItem(MediaTaskGroupEntity entity)
	{
		UpdateItemSpec item = new UpdateItemSpec();

		KeyAttribute task_group_id = new KeyAttribute(MediaTaskGroupField.TASK_GROUP_ID.alias(), entity.task_group_id);

		item.withPrimaryKey(new PrimaryKey(task_group_id));

		if (entity.task_group_status != null)
		{
			int code = entity.task_group_status.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.TASK_GROUP_STATUS.alias()).put(code));
		}

		if (entity.date_time != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.DATE_TIME.alias()).put(entity.date_time));
		}

		if (entity.timestamp != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.TIMESTAMP.alias()).put(entity.timestamp));
		}

		if (entity.output_mechanism != null)
		{
			int code = entity.output_mechanism.code();
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.OUTPUT_MECHANISM.alias()).put(code));
		}

		if (entity.log_stream != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.LOG_STREAM.alias()).put(entity.log_stream));
		}

		if (entity.user_id != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.USER_ID.alias()).put(entity.user_id));
		}

		if (entity.user_email != null)
		{
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.USER_EMAIL.alias()).put(entity.user_email));
		}

		if (entity.user_channel != null)
		{
			item.addAttributeUpdate(
					new AttributeUpdate(MediaTaskGroupField.USER_CHANNEL.alias()).put(entity.user_channel));
		}

		if (entity.created_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.created_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.CREATED_AT.alias()).put(value));
		}

		if (entity.started_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.started_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.STARTED_AT.alias()).put(value));
		}

		if (entity.finished_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.finished_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.FINISHED_AT.alias()).put(value));
		}

		if (entity.failed_at != null)
		{
			String value = DATE_TIME_FORMAT.print(entity.failed_at);
			item.addAttributeUpdate(new AttributeUpdate(MediaTaskGroupField.FAILED_AT.alias()).put(value));
		}

		return item;
	}

	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyyMMdd").withZoneUTC();
	private final static DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
			.withZoneUTC();
}
