/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

/**
 * @author Clearstream
 */
public enum MediaTaskField
{
	TASK_GROUP_ID,

	TASK_ID,
	TASK_CATEGORY,
	TASK_STATUS,

	DATE_TIME,
	TIMESTAMP,

	OUTPUT_MECHANISM,
	OUTPUT_FILE_PATH,
	OUTPUT_FILE_TYPE,

	LOG_STREAM,

	PARTITION_INDEX,

	CREATED_AT,
	STARTED_AT,
	RUNNING_AT,
	FINISHED_AT,
	FAILED_AT,;

	MediaTaskField()
	{
		alias_ = name().toLowerCase();
	}

	public String alias()
	{
		return alias_;
	}

	private final String alias_;
}