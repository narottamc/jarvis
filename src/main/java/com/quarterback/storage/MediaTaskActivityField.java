/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

/**
 * @author Clearstream
 */
public enum MediaTaskActivityField
{
	TASK_GROUP_ID,

	TASK_ID,
	TASK_COMMENT,
	TASK_STATUS,
	TASK_PAYLOADS,
	TASK_WORKLOAD,

	DATE_TIME,
	TIMESTAMP,

	OUTPUT_MECHANISM,

	LOG_STREAM,

	CREATED_AT,
	STARTED_AT,
	RUNNING_AT,
	FINISHED_AT,
	FAILED_AT,;

	MediaTaskActivityField()
	{
		alias_ = name().toLowerCase();
	}

	public String alias()
	{
		return alias_;
	}

	private final String alias_;
}
