/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.storage;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.google.common.primitives.Longs;
import com.quarterback.models.TaskCategory;
import com.quarterback.models.TaskStatus;
import com.quarterback.utils.ContextScope;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream
 */
public class MediaTaskStorage
{
	public MediaTaskStorage(DynamoDB dynamodb, ContextScope scope)
	{
		task_table_ = dynamodb.getTable("media_tasks_" + scope.alias());
		task_index_ = task_table_.getIndex("by_task_group");

		task_activity_table_ = dynamodb.getTable("media_task_activities_" + scope.alias());
		task_activity_index_ = task_activity_table_.getIndex("by_task_group");

		task_group_table_ = dynamodb.getTable("media_task_groups_" + scope.alias());
		task_group_index_ = task_group_table_.getIndex("by_date_time");
	}

	public MediaTaskEntity getFinishedTasks(String task_group_id, TaskCategory task_category)
	{
		// MediaTaskResultSet result_set = new MediaTaskResultSet();

		KeyAttribute hash_key = new KeyAttribute(MediaTaskField.TASK_GROUP_ID.alias(), task_group_id);

		QueryFilter task_category_filter = new QueryFilter(MediaTaskField.TASK_CATEGORY.alias())
				.eq(task_category.code());

		QueryFilter task_status_filter = new QueryFilter(MediaTaskField.TASK_STATUS.alias())
				.eq(TaskStatus.FINISHED.code());

		QuerySpec query = new QuerySpec();
		query = query.withHashKey(hash_key).withQueryFilters(task_category_filter, task_status_filter)
				.withConsistentRead(false);

		ItemCollection<QueryOutcome> outcomes = task_index_.query(query);
		MediaTaskEntity entity = null;
		for (Item item : outcomes)
		{
			entity = MediaTaskEntity.fromItem(item);
			return entity;
		}

		return entity;
	}

	@Nullable
	public MediaTaskGroupEntity getTaskGroups(DateTime date_time, String user_email)
	{
		Long date_time_value = Longs.tryParse(DATE_FORMAT.print(date_time));
		KeyAttribute hash_key = new KeyAttribute(MediaTaskGroupField.DATE_TIME.alias(), date_time_value);

		QueryFilter email_filter = new QueryFilter(MediaTaskGroupField.USER_EMAIL.alias()).eq(user_email);

		QuerySpec query = new QuerySpec();
		query = query.withHashKey(hash_key).withQueryFilters(email_filter).withConsistentRead(false)
				.withScanIndexForward(false).withMaxResultSize(1); // descending
																	// order

		List<MediaTaskGroupEntity> entities = new ArrayList<>();

		ItemCollection<QueryOutcome> outcomes = task_group_index_.query(query);

		for (Item item : outcomes)
		{
			MediaTaskGroupEntity entity = MediaTaskGroupEntity.fromItem(item);
			entities.add(entity);
		}

		return entities.isEmpty() ? null : entities.get(0);
	}

	private final Table task_table_;
	private final Index task_index_;
	private final Table task_activity_table_;
	private final Index task_activity_index_;
	private final Table task_group_table_;
	private final Index task_group_index_;

	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyyMMdd").withZoneUTC();
}