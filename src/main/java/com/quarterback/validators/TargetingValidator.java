/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.google.common.base.CharMatcher;
import com.google.common.collect.*;
import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.categoriesresponse.CategoriesJsonResponse;
import com.osapi.models.countriesresponse.CountiresJsonResponse;
import com.osapi.models.dmasresponse.DmasJsonResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.inventorytypesresponse.InventoryTypesJsonResponse;
import com.osapi.models.languageresponse.LanguageJsonResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.regionsresponse.RegionJsonResponse;
import com.osapi.models.targetingresponse.*;
import com.quarterback.comparatorfactory.TargetingComparator;
import com.quarterback.factory.*;
import com.quarterback.mappers.ConfigData;
import com.quarterback.utils.Constants;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.regexp.RE;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clearstream.
 */
public class TargetingValidator {
    private static final Logger LOG = Logger.getLogger(TargetingValidator.class);
    public static Comparator<List<String>> ComparatorForListOfString = new Comparator<List<String>>() {
        @Override
        public int compare(List<String> list1, List<String> list2) {
            if (list1.size() == list2.size()) {
                Collections.sort(list1);
                Collections.sort(list2);
                if (!(list1.equals(list2))) {
                    LOG.info("List #1 is " + list1);
                    LOG.info("List #2 is " + list2);
                    LOG.info("Difference is " + CollectionUtils.subtract(list1, list2));
                }
                return list1.equals(list2) ? 0 : 1;
            }
            return 1;
        }
    };

    public static Comparator<Set<String>> ComparatorForSetOfString = new Comparator<Set<String>>() {
        @Override
        public int compare(Set<String> set1, Set<String> set2) {
            return set1.containsAll(set2) ? 0 : 1;
        }
    };

    public static Comparator<Map<Boolean, TreeSet<String>>> ComparatorForMapOfBooleanAndStringTreeSet = new Comparator<Map<Boolean, TreeSet<String>>>() {
        @Override
        public int compare(Map<Boolean, TreeSet<String>> map1, Map<Boolean, TreeSet<String>> map2) {
            if (map1.keySet().containsAll(map2.keySet())) {
                for (Boolean key : map1.keySet()) {
                    TreeSet<String> set1 = map1.get(key);
                    TreeSet<String> set2 = map2.get(key);
                    if (!(set1.equals(set2))) {
                        LOG.info("List #1 is " + set1);
                        LOG.info("List #2 is " + set2);
                        LOG.info("Difference is " + CollectionUtils.subtract(set1, set2));
                        return set1.equals(set2) ? 0 : 1;
                    }
                }
                return 0;
            }
            return 1;
        }
    };

    public static Comparator<Map<Boolean, List<Integer>>> ComparatorForMapOfBooleanAndIntegerList = new Comparator<Map<Boolean, List<Integer>>>() {
        @Override
        public int compare(Map<Boolean, List<Integer>> map1, Map<Boolean, List<Integer>> map2) {
            if (map1.keySet().containsAll(map2.keySet())) {
                for (Boolean key : map1.keySet()) {
                    List<Integer> list1 = map1.get(key);
                    List<Integer> list2 = map2.get(key);
                    Collections.sort(list1);
                    Collections.sort(list2);
                    if (!(list1.equals(list2))) {
                        LOG.info("List #1 is " + list1);
                        LOG.info("List #2 is " + list2);
                        LOG.info("Difference is " + CollectionUtils.subtract(list1, list2));
                        return list1.equals(list2) ? 0 : 1;
                    }
                }
                return 0;
            }
            return 1;
        }
    };

    public static Comparator<Multimap<String, Map<String, Boolean>>> ComparatorForMultiMapOfStringAndMap = new Comparator<Multimap<String, Map<String, Boolean>>>() {
        @Override
        public int compare(Multimap<String, Map<String, Boolean>> multiMap1, Multimap<String, Map<String, Boolean>> multiMap2) {
            if (multiMap1.keySet().containsAll(multiMap2.keySet())) {
                Multimap<String, Map<String, Boolean>> firstSecondDifference =
                        Multimaps.filterEntries(multiMap1, e -> !multiMap2.containsEntry(e.getKey(), e.getValue()));
                Multimap<String, Map<String, Boolean>> secondFirstDifference =
                        Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                if (!(firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty())) {
                    LOG.info("Difference In XML's Countries Information and OS's Countries Information " + firstSecondDifference);
                    LOG.info("Difference In XML's Countries Information and OS's Countries Information " + secondFirstDifference);
                }
                return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty() ? 0 : 1;
            }
            return 1;
        }
    };

    public static Comparator<Set<PlayerSize>> PlayerSizeComparator = new Comparator<Set<PlayerSize>>() {

        @Override
        public int compare(Set<PlayerSize> set1, Set<PlayerSize> set2) {
            return set1.containsAll(set2) ? 0 : 1;
        }
    };

    public static Comparator<Multimap<String, Integer>> ComparatorForMultiMapOfStringAndInteger = new Comparator<Multimap<String, Integer>>() {

        @Override
        public int compare(Multimap<String, Integer> multiMap1, Multimap<String, Integer> multiMap2) {
            if (multiMap1.keySet().containsAll(multiMap2.keySet())) {
                Multimap<String, Integer> firstSecondDifference =
                        Multimaps.filterEntries(multiMap1, e -> !multiMap2.containsEntry(e.getKey(), e.getValue()));
                Multimap<String, Integer> secondFirstDifference =
                        Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                if (!(firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty())) {
                    LOG.info("Difference In OS's PageSite Domain Information and XML's MediaList Information " + firstSecondDifference);
                    LOG.info("Difference In XML's PageSite Domain Information and OS's MediaList Information " + secondFirstDifference);
                }
                return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty() ? 0 : 1;
            }
            return 1;
        }

    };

    public static Comparator<Map<String, Boolean>> ComparatorForMapOfStringAndBoolean = new Comparator<Map<String, Boolean>>() {
        @Override
        public int compare(Map<String, Boolean> map1, Map<String, Boolean> map2) {
            if (map1.keySet().containsAll(map2.keySet())) {
                List<Boolean> list1 = new ArrayList<>(map1.values());
                List<Boolean> list2 = new ArrayList<>(map2.values());
                Collections.sort(list1);
                Collections.sort(list2);
                return list1.equals(list2) ? 0 : 1;
            }
            return 1;
        }
    };

    public static Comparator<Map<Boolean, Multimap<Integer, Integer>>> ComparatorForMapOfBooleanAndMultiMap = new Comparator<Map<Boolean, Multimap<Integer, Integer>>>() {

        @Override
        public int compare(Map<Boolean, Multimap<Integer, Integer>> map1, Map<Boolean, Multimap<Integer, Integer>> map2) {
            if (map1.keySet().containsAll(map2.keySet())) {
                for (Boolean key : map1.keySet()) {
                    Multimap<Integer, Integer> multiMap1 = map1.get(key);
                    Multimap<Integer, Integer> multiMap2 = map2.get(key);
                    Multimap<Integer, Integer> firstSecondDifference =
                            Multimaps.filterEntries(multiMap1, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                    Multimap<Integer, Integer> secondFirstDifference =
                            Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                    if (!(firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty())) {
                        LOG.info("Difference In XML's SiteCategories Information and OS's SiteCategories Information " + firstSecondDifference);
                        LOG.info("Difference In XML's SiteCategories Information and OS's SiteCategories Information " + secondFirstDifference);
                        return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty() ? 0 : 1;
                    }
                }
                return 0;
            }
            return 1;
        }
    };

    public static Comparator<Map<String, Map<String, Boolean>>> ComparatorForMapOfStringAndMap = new Comparator<Map<String, Map<String, Boolean>>>() {
        @Override
        public int compare(Map<String, Map<String, Boolean>> map1, Map<String, Map<String, Boolean>> map2) {
            if (map1.keySet().containsAll(map2.keySet())) {
                for (String key : map1.keySet()) {
                    Map<String, Boolean> innerMap1 = map1.get(key).entrySet().stream()
                            .sorted(Map.Entry.comparingByKey())
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new));
                    Map<String, Boolean> innerMap2 = map2.get(key).entrySet().stream()
                            .sorted(Map.Entry.comparingByKey())
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new));
                    if(!(innerMap1.equals(innerMap2))){
                        MapDifference<String, Boolean> diff = Maps.difference(innerMap1, innerMap2);
                        LOG.info("Difference Between OSAPI & XML's dayPart Information is "+ diff.entriesDiffering());
                        return 1;
                    }
                }
                return 0;
            }
            return 1;
        }
    };


    public static class OSTargeting implements TargetingComparator {
        private static final Logger LOG = Logger.getLogger(TargetingValidator.class);
        private Integer placementNumber ;
        private String scenarioTestDataPath;
        private RootPlacementResponse rootPlacementResponse;
        private RootGeoResponse geoResponse;
        private RootGeoListResponse geoListResponse;
        private RootTechnologyResponse technologyResponse;
        private RootCategoryResponse categoryResponse;
        private RootDayPartResponse dayPartResponse;
        private RootAddtionalOptionResponse addtionalOptionResponse;
        private RootMediaInventoryResponse mediaInventoryResponseMediaList;
        private RootMediaInventoryResponse mediaInventoryResponseSmartList;

        public OSTargeting(RootPlacementResponse rootPlacementResponse, Integer placementNumber,String scenarioTestDataPath) {
            this.placementNumber =placementNumber;
            this.scenarioTestDataPath = scenarioTestDataPath;
            this.geoResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getGeoDetails(scenarioTestDataPath).get(placementNumber);
            this.technologyResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getTechnologiesDetails(scenarioTestDataPath).get(placementNumber);
            this.categoryResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getCategoriesDetails(scenarioTestDataPath).get(placementNumber);
            this.dayPartResponse = DependencyManager.getInjector().getInstance(SessionVariables.class).getDayPartDetails(scenarioTestDataPath).get(placementNumber);
            this.geoListResponse =  DependencyManager.getInjector().getInstance(SessionVariables.class).getGeoListDetails(scenarioTestDataPath).get(placementNumber);
            this.rootPlacementResponse = rootPlacementResponse;
            this.mediaInventoryResponseMediaList = DependencyManager.getInjector().getInstance(SessionVariables.class).
                    getMediaListDetailsForAPlacement(scenarioTestDataPath).get(rootPlacementResponse.getId());
            this.mediaInventoryResponseSmartList = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getSmartListDetailsForAPlacement(scenarioTestDataPath).get(rootPlacementResponse.getId());

        }

        @Override
        public String toString() {
            return "OSTargeting{" +
                    "targetingLanguages=" + getTargetingLanguages() +
                    ", targetingCountries=" + getTargetingCountries() +
                    ", targetingZipCodes=" + getTargetingZipCodes() +
                    ", targetingDmas=" + getTargetingDmas() +
                    ", dayParts=" + getDayParts() +
                    ", browserVersionDetails=" + getBrowserVersionDetails() +
                    ", OSVersionDetails=" + getOSVersionDetails() +
                    ", deviceTypeDetails=" + getDeviceTypeDetails() +
                    // ", advertiserCategories=" + getAdvertiserCategories() +
                    ", siteAdvertiserCategories=" + getSiteAdvertiserCategories() +
                    ", typeForSiteCategories='" + getTypeForSiteCategories() + '\'' +
                    ", visibilityDetails=" + getVisibilityDetails() +
                    ", SSPSellerNetwork=" + getSSPSellerNetwork() +
                    ", pageDomainList=" + getPageDomainList() +
                    ", siteDomainList=" + getSiteDomainList() +
                    ", domainOperator='" + getDomainOperator() + '\'' +
                    ", inventoryTypeDetails='" + getInventoryTypeDetails() + '\'' +
                    ", playerSizes=" + getPlayerSizes() +
                    '}';
        }

        @Override
        public List<String> getTargetingLanguages() {
            Map<Integer, LanguageJsonResponse> languagesMasterDataMap = MapBuilderfromJsonFile.buildFromHashMap(false, LanguageJsonResponse.class
                    , Constants.LANGUAGES_MASTERDATAJSON).getInstanceMap();
            List<String> languageCodeList = new ArrayList<>();
            for (int i = 0; i < geoResponse.getLanguages().size(); i++) {
                languageCodeList.add(languagesMasterDataMap.get(geoResponse.getLanguages().get(i).getId()).getCode());
            }
            return languageCodeList;
        }

        @Override
        public Multimap<String, Map<String, Boolean>> getTargetingCountries() {
            Map<Integer, CountiresJsonResponse> countriesMasterDataMap = MapBuilderfromJsonFile.buildFromHashMap(false, CountiresJsonResponse.class
                    , Constants.COUNTRIES_MASTERDATAJSON).getInstanceMap();
            Multimap<Integer, RegionJsonResponse> regionsMasterDataMap = MapBuilderfromJsonFile.buildFromHashMap(true, RegionJsonResponse.class
                    , Constants.REGIONS_MASTERDATAJSON).getInstanceMultiMap();
            Multimap<String, Map<String, Boolean>> countriesRegionsMapOS = ArrayListMultimap.create();
            for (int i = 0; i < geoResponse.getGeos().size(); i++) {
                if (geoResponse.getGeos().get(i).getCountryId() != null) {
                    String multimapCountryKey = countriesMasterDataMap.get(geoResponse.getGeos().get(i).getCountryId()).getCountryCode();
                    Collection<RegionJsonResponse> regionJsonResponses = regionsMasterDataMap.asMap().get(geoResponse.getGeos().get(i).getCountryId());
                    Map<String, Boolean> regionCodesMap = new HashMap<>();
                    for (RegionJsonResponse regionJsonResponse : regionJsonResponses) {
                        regionCodesMap.put(regionJsonResponse.getAbbr(), true);
                    }
                    countriesRegionsMapOS.put(multimapCountryKey, regionCodesMap);
                }
            }
            return countriesRegionsMapOS;
        }

        @Override
        public Map<Boolean, TreeSet<String>> getTargetingZipCodes() {
            Map<Boolean, TreeSet<String>> zipCodesMapOS = new HashMap<>();
            TreeSet<String> zipCodesSetOS = new TreeSet<>();
            CharMatcher matcherFinal = CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('A', 'Z')
                    .or(CharMatcher.inRange('0', '9').or(CharMatcher.anyOf("-&. "))));
            for (int i = 0; i < geoListResponse.getListName().getZipCodes().size(); i++) {
                zipCodesSetOS.add(matcherFinal.retainFrom(geoListResponse.getListName().getZipCodes().get(i).getCode()));
            }
            zipCodesMapOS.put(geoResponse.getZipAntiTargeting(), zipCodesSetOS);
            return zipCodesMapOS;
        }

        @Override
        public Map<Boolean, List<Integer>> getTargetingDmas() {
            Map<Integer, DmasJsonResponse> dmasJsonResponseMap = MapBuilderfromJsonFile.buildFromHashMap(false, DmasJsonResponse.class,
                    Constants.DMAS_MASTERDATAJSON).getInstanceMap();
            Map<Boolean, List<Integer>> dmasDetailsOS = new HashMap<>();
            List<Integer> dmasCodeList = new ArrayList<>();
            for (int i = 0; i < geoResponse.getGeos().size(); i++) {
                if (geoResponse.getGeos().get(i).getDmaId() != null) {
                    dmasCodeList.add(dmasJsonResponseMap.get(geoResponse.getGeos().get(i).getDmaId()).getCode());
                }
            }
            dmasDetailsOS.put(geoResponse.getDmaAntiTargeting(), dmasCodeList);
            return dmasDetailsOS;
        }

        @Override
        public Map<String, Map<String, Boolean>> getDayParts() {
            Map<String, Map<String, Boolean>> dayPartDetailsOS = new HashMap<>();
            for (int i = 0; i < dayPartResponse.getDayParts().size(); i++) {
                Map<String, Boolean> hourDetailsOS = new HashMap<>();
                for (int j = 0; j < dayPartResponse.getDayParts().get(i).getHours().size(); j++) {
                    hourDetailsOS.put(dayPartResponse.getDayParts().get(i).getHours().get(j), true);
                }
                hourDetailsOS.put(Constants.DAYPARTS_EXCLUDEDHOURS_KEY, false);
                dayPartDetailsOS.put(DayOfWeek.fromDayCode(dayPartResponse.getDayParts().get(i).getDayOfWeek()).getDayAbbreviation(), hourDetailsOS);
            }
            dayPartDetailsOS.put(Constants.DAYPARTS_EXCLUDEDHOURS_KEY, new HashMap<>());
            return dayPartDetailsOS;
        }

        @Override
        public Map<String, Boolean> getBrowserVersionDetails() {
            Map<String, Boolean> browserDetailsMapOS = new HashMap<>();
            for (int i = 0; i < technologyResponse.getTechnologies().size(); i++) {
                if (technologyResponse.getTechnologies().get(i).getTechnologyType() == 0) {
                    String browserRawText = BrowserTypes.valueOf(technologyResponse.getTechnologies().get(i).getDescription()
                            .replace(" ", "").split("\\(")[0].toUpperCase()).getRawBrowserText();
                    for (int j = 0; j < browserRawText.split(",").length; j++) {
                        browserDetailsMapOS.put(browserRawText.split(",")[j], true);
                    }
                }
            }
            return browserDetailsMapOS;
        }

        @Override
        public Map<String, Boolean> getOSVersionDetails() {
            Map<String, Boolean> operatingSystemDetailsMapOS = new HashMap<>();
            for (int i = 0; i < technologyResponse.getTechnologies().size(); i++) {
                if (technologyResponse.getTechnologies().get(i).getTechnologyType() == 1) {
                    String osRawText = OperatinSystemTypes.valueOf(technologyResponse.getTechnologies().get(i).getDescription()
                            .replace(" ", "").split("\\(")[0].replace("/", "").toUpperCase()).getRawOS();
                    for (int j = 0; j < osRawText.split(",").length; j++) {
                        operatingSystemDetailsMapOS.put(osRawText.split(",")[j], true);
                    }
                }
            }
            return operatingSystemDetailsMapOS;
        }

        @Override
        public Map<Boolean, TreeSet<String>> getDeviceTypeDetails() {
            Map<Boolean, TreeSet<String>> deviceTypeDetailsMapOS = new HashMap<>();
            TreeSet<String> deviceTypeSet = new TreeSet<>();
            for (int i = 0; i < technologyResponse.getTechnologies().size(); i++) {
                if (technologyResponse.getTechnologies().get(i).getTechnologyType() == 3) {
                    String deviceRawText = DeviceTypes.valueOf(technologyResponse.getTechnologies().get(i).getDescription()
                            .toUpperCase()).getRawDeviceType();
                    deviceTypeSet.add(deviceRawText);

                }
            }
            deviceTypeDetailsMapOS.put(true, deviceTypeSet);
            return deviceTypeDetailsMapOS;
        }

//        @Override
//        public Integer getAdvertiserCategories() {
//            return null;
//        }

        @Override
        public Map<Boolean, Multimap<Integer, Integer>> getSiteAdvertiserCategories() {
            Multimap<Integer, CategoriesJsonResponse> categoriesMasterDataMap = MapBuilderfromJsonFile.buildFromHashMap(true, CategoriesJsonResponse.class
                    , Constants.PCATEGORIES_MASTERDATAJSON).getInstanceMultiMap();
            Map<Boolean, Multimap<Integer, Integer>> siteCategoriesDetailsOS = new HashMap<>();
            Multimap<Integer, Integer> categoriesMultiMap = ArrayListMultimap.create();
            for (int i = 0; i < categoryResponse.getTargetCategories().size(); i++) {
                Integer categoryId = categoryResponse.getTargetCategories().get(i).getId();
                if (categoriesMasterDataMap.keySet().contains(categoryId)) {
                    Collection<CategoriesJsonResponse> subCategoriesList = categoriesMasterDataMap.asMap().get(categoryId);
                    if (subCategoriesList.size() > 1) {
                        for (CategoriesJsonResponse category : subCategoriesList) {
                            if (category.getParentId() != null) {
                                Integer subCategoryiabId = Integer.parseInt(CharMatcher.inRange('0', '9').retainFrom(category.getIabId().split("-")[1]));
                                Integer parentCatergoryiabId = Integer.parseInt(CharMatcher.inRange('0', '9')
                                        .retainFrom(category.getIabId().split("-")[0]));
                                categoriesMultiMap.put(parentCatergoryiabId, subCategoryiabId);
                            }
                        }
                    } else {
                        Integer parentCatergoryiabId = Integer.parseInt(CharMatcher.inRange('0', '9')
                                .retainFrom(subCategoriesList.stream().findFirst().get().getIabId().split("-")[0]));
                        categoriesMultiMap.put(parentCatergoryiabId, 0);
                    }
                }
            }
            siteCategoriesDetailsOS.put(categoryResponse.getHitOnUnknownResults(), categoriesMultiMap);
            return siteCategoriesDetailsOS;
        }

        @Override
        public String getTypeForSiteCategories() {
            return ConfigData.IncludeOrExclude.INCLUDE.name();
        }

        @Override
        public Map<String, Boolean> getSSPSellerNetwork() {
            Map<String, Boolean> sspSellerNetworkMapOS = new HashMap<>();
            for (int i = 0; i < rootPlacementResponse.getSsps().size(); i++) {
                sspSellerNetworkMapOS.put(rootPlacementResponse.getSsps().get(i).getName(), true);
            }
            sspSellerNetworkMapOS.put(Constants.SSP_EXCLUDEDSSP_KEY, false);
            return sspSellerNetworkMapOS;
        }

        @Override
        public Multimap<String, Integer> getPageDomainList() {
            Multimap<String, Integer> pageDomainListOS = ArrayListMultimap.create();
            if (mediaInventoryResponseMediaList != null) {
                for (int i = 0; i < mediaInventoryResponseMediaList.getLists().size(); i++) {
                    String type = mediaInventoryResponseMediaList.getType().toUpperCase().contentEquals("BLACKLIST") ?
                            ConfigData.IncludeOrExclude.EXCLUDE.name() : ConfigData.IncludeOrExclude.INCLUDE.name();
                    pageDomainListOS.put(type, mediaInventoryResponseMediaList.getLists().get(i).getId());
                }
            }

            return pageDomainListOS;
        }

        @Override
        public Multimap<String, Integer> getSiteDomainList() {
            Multimap<String, Integer> siteDomainListOS = ArrayListMultimap.create();
            if (mediaInventoryResponseMediaList != null) {
                for (int i = 0; i < mediaInventoryResponseMediaList.getLists().size(); i++) {
                    String type = mediaInventoryResponseMediaList.getType().toUpperCase().contentEquals("BLACKLIST") ?
                            ConfigData.IncludeOrExclude.EXCLUDE.name() : ConfigData.IncludeOrExclude.INCLUDE.name();
                    siteDomainListOS.put(type, mediaInventoryResponseMediaList.getLists().get(i).getId());
                }
            }
            return siteDomainListOS;
        }

        @Override
        public String getDomainOperator() {
            if (mediaInventoryResponseMediaList != null) {
                return mediaInventoryResponseMediaList.getType().toUpperCase().contentEquals("BLACKLIST") ?
                        ConfigData.Operator.AND.name() : ConfigData.Operator.OR.name();
            }
            return "";
        }

        @Override
        public String getInventoryTypeDetails() {
            Map<Integer, InventoryTypesJsonResponse> categoriesMasterDataMap = MapBuilderfromJsonFile.buildFromHashMap(false, InventoryTypesJsonResponse.class
                    , Constants.INVENTORYTYPES_MASTERDATAJSON).getInstanceMap();
            return categoriesMasterDataMap.get(technologyResponse.getInventoryTypeId()).getValue();
        }

        @Override
        public Set<String> getVisibilityDetails() {
            Set<String> visibilityListOS = new HashSet<>();
            if(ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()).getproducType().contentEquals(ProductType.VIDEO.getproducType())){
                RootAddtionalOptionResponse addtionalOptionResponse =
                        DependencyManager.getInjector().getInstance(SessionVariables.class).getAddtionalOptionsDetails(scenarioTestDataPath).get(placementNumber);
                for (int i = 0; i < addtionalOptionResponse.getVisibilities().size(); i++) {
                    visibilityListOS.add(addtionalOptionResponse.getVisibilities().get(i).getValue());
                }
                return visibilityListOS;
            }
            return visibilityListOS;
        }

        @Override
        public Set<PlayerSize> getPlayerSizes() {
            Set<PlayerSize> playerSizesOSList = new HashSet<>();
            if(ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()).getproducType().contentEquals(ProductType.VIDEO.getproducType())){
                RootAddtionalOptionResponse addtionalOptionResponse =
                        DependencyManager.getInjector().getInstance(SessionVariables.class).getAddtionalOptionsDetails(scenarioTestDataPath).get(placementNumber);
                for (int i = 0; i < addtionalOptionResponse.getPlayerSizeGroups().size(); i++) {
                    Integer playerMinWidth = addtionalOptionResponse.getPlayerSizeGroups().get(i).getMinWidth() == null
                            ? 0 : addtionalOptionResponse.getPlayerSizeGroups().get(i).getMinWidth();
                    Integer playerMaxWidth = addtionalOptionResponse.getPlayerSizeGroups().get(i).getMaxWidth() == null
                            ? 0 : addtionalOptionResponse.getPlayerSizeGroups().get(i).getMaxWidth();
                    PlayerSize playerSize = new PlayerSize(playerMinWidth, playerMaxWidth);
                    playerSizesOSList.add(playerSize);
                }
                return playerSizesOSList;
            }
            return playerSizesOSList;
        }
    }

    public static class XMLTargeting implements TargetingComparator {
        private ConfigData.Campaign xmlCampaign;
        private ConfigData.Targeting xmlTargeting;
        private static final String UTF8_BOM = "\uFEFF";

        public XMLTargeting(RootPlacementResponse placementResponseOS) {
            this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(placementResponseOS.getId());
            this.xmlTargeting = xmlCampaign.targeting;
        }

        @Override
        public String toString() {
            return "XMLTargeting{" +
                    "targetingLanguages=" + getTargetingLanguages() +
                    ", targetingCountries=" + getTargetingCountries() +
                    ", targetingZipCodes=" + getTargetingZipCodes() +
                    ", targetingDmas=" + getTargetingDmas() +
                    ", dayParts=" + getDayParts() +
                    ", browserVersionDetails=" + getBrowserVersionDetails() +
                    ", OSVersionDetails=" + getOSVersionDetails() +
                    ", deviceTypeDetails=" + getDeviceTypeDetails() +
                    //", advertiserCategories=" + getAdvertiserCategories() +
                    ", siteAdvertiserCategories=" + getSiteAdvertiserCategories() +
                    ", typeForSiteCategories='" + getTypeForSiteCategories() + '\'' +
                    ", visibilityDetails=" + getVisibilityDetails() +
                    ", SSPSellerNetwork=" + getSSPSellerNetwork() +
                    ", pageDomainList=" + getPageDomainList() +
                    ", siteDomainList=" + getSiteDomainList() +
                    ", domainOperator='" + getDomainOperator() + '\'' +
                    ", playerSizes=" + getPlayerSizes() +
                    ", inventoryTypeDetails='" + getInventoryTypeDetails() + '\'' +
                    '}';
        }


        private static String removeUTF8BOM(String s) {
            if (s.startsWith(UTF8_BOM)) {
                s = s.substring(1);
            }
            return s;
        }

        @Override
        public List<String> getTargetingLanguages() {
            List<String> languageCodeListXML = new ArrayList<>();
            languageCodeListXML.addAll(xmlTargeting.language.values);
            return languageCodeListXML;
        }

        @Override
        public Multimap<String, Map<String, Boolean>> getTargetingCountries() {
            Multimap<String, Map<String, Boolean>> countriesDetailsXML = ArrayListMultimap.create();
            Map<String, Boolean> regionsMapXML = new HashMap<>();
            for (int i = 0; i < xmlTargeting.geo.countries.size(); i++) {
                for (int j = 0; j < xmlTargeting.geo.countries.get(i).regions.size(); j++) {
                    regionsMapXML.put(xmlTargeting.geo.countries.get(i).regions.get(j).name, xmlTargeting.geo.countries.get(i).regions.get(j).value);
                }
                countriesDetailsXML.put(xmlTargeting.geo.countries.get(i).name, regionsMapXML);
            }
            return countriesDetailsXML;
        }

        @Override
        public Map<Boolean, TreeSet<String>> getTargetingZipCodes() {
            Map<Boolean, TreeSet<String>> zipCodesMapXML = new HashMap<>();
            TreeSet<String> zipCodesSetXML = new TreeSet<>();
            for (int i = 0; i < xmlTargeting.zip.values.size(); i++) {
                String s = xmlTargeting.zip.values.get(i);
                String bomRemovedString = XMLTargeting.removeUTF8BOM(s);
                zipCodesSetXML.add(bomRemovedString);
            }
            if (xmlTargeting.zip.type.name().toLowerCase().contentEquals(ConfigData.IncludeOrExclude.INCLUDE.name().toLowerCase())) {
                zipCodesMapXML.put(false, zipCodesSetXML);
            } else {
                zipCodesMapXML.put(true, zipCodesSetXML);
            }
            return zipCodesMapXML;
        }

        @Override
        public Map<Boolean, List<Integer>> getTargetingDmas() {
            Map<Boolean, List<Integer>> dmasCodesMapXML = new HashMap<>();
            List<Integer> dmasCodesListXML = new ArrayList<>();
            dmasCodesListXML.addAll(xmlTargeting.dma.values);
            if (xmlTargeting.dma.type.name().toLowerCase().contentEquals(ConfigData.IncludeOrExclude.INCLUDE.name().toLowerCase())) {
                dmasCodesMapXML.put(false, dmasCodesListXML);
            } else {
                dmasCodesMapXML.put(true, dmasCodesListXML);
            }
            return dmasCodesMapXML;
        }

        @Override
        public Map<String, Map<String, Boolean>> getDayParts() {
            Map<String, Map<String, Boolean>> dayPartsXML = new HashMap<>();
            for (int i = 0; i < xmlTargeting.days.size(); i++) {
                Map<String, Boolean> hoursMapXML = new HashMap<>();
                for (int j = 0; j < xmlTargeting.days.get(i).hours.size(); j++) {
                    hoursMapXML.put(xmlTargeting.days.get(i).hours.get(j).hour_id, xmlTargeting.days.get(i).hours.get(j).value);
                }
                dayPartsXML.put(xmlTargeting.days.get(i).id, hoursMapXML);
            }
            return dayPartsXML;
        }

        @Override
        public Map<String, Boolean> getBrowserVersionDetails() {
            Map<String, Boolean> browserDetailMap = new HashMap<>();
            for (int i = 0; i < xmlTargeting.user_agent.browsers.size(); i++) {
                browserDetailMap.put(xmlTargeting.user_agent.browsers.get(i).id, xmlTargeting.user_agent.browsers.get(i).value);
            }
            return browserDetailMap;
        }

        @Override
        public Map<String, Boolean> getOSVersionDetails() {
            Map<String, Boolean> operatingSystemDetailMap = new HashMap<>();
            for (int i = 0; i < xmlTargeting.user_agent.operating_systems.size(); i++) {
                operatingSystemDetailMap.put(xmlTargeting.user_agent.operating_systems.get(i).id, xmlTargeting.user_agent.operating_systems.get(i).value);
            }
            return operatingSystemDetailMap;
        }

        @Override
        public Map<Boolean, TreeSet<String>> getDeviceTypeDetails() {
            Map<Boolean, TreeSet<String>> deviceTypeDetailMapXML = new HashMap<>();
            TreeSet<String> deviceListXML = new TreeSet<>();
            for (int i = 0; i < xmlTargeting.user_agent.device_type.values.size(); i++) {
                deviceListXML.add(xmlTargeting.user_agent.device_type.values.get(i));
            }
            Boolean type = xmlTargeting.user_agent.device_type.type.name().toLowerCase().contentEquals(ConfigData.IncludeOrExclude.INCLUDE.name().toLowerCase());
            deviceTypeDetailMapXML.put(type, deviceListXML);
            return deviceTypeDetailMapXML;
        }

//        @Override
//        public Integer getAdvertiserCategories() {
//            return null;
//        }

        @Override
        public Map<Boolean, Multimap<Integer, Integer>> getSiteAdvertiserCategories() {
            Map<Boolean, Multimap<Integer, Integer>> siteAdvertiserCatergoreisXML = new HashMap<>();
            Multimap<Integer, Integer> categoriesDetailsXML = ArrayListMultimap.create();
            for (int i = 0; i < xmlTargeting.site_categories.categories.size(); i++) {
                Integer parentCategoryCode = xmlTargeting.site_categories.categories.get(i).code;
                if (xmlTargeting.site_categories.categories.get(i).sub_categories != null) {
                    for (int j = 0; j < xmlTargeting.site_categories.categories.get(i).sub_categories.size(); j++) {
                        categoriesDetailsXML.put(parentCategoryCode, xmlTargeting.site_categories.categories.get(i).sub_categories.get(j).code);
                    }
                } else {
                    categoriesDetailsXML.put(parentCategoryCode, 0);
                }
            }
            siteAdvertiserCatergoreisXML.put(xmlTargeting.site_categories.passes_on_unknown_data, categoriesDetailsXML);
            return siteAdvertiserCatergoreisXML;
        }

        @Override
        public String getTypeForSiteCategories() {
            return xmlTargeting.site_categories.type.name();
        }

        @Override
        public Set<String> getVisibilityDetails() {
            Set<String> visibilityDetailXML = new HashSet<>();
            if(xmlTargeting.visibilities!=null){
                for (int i = 0; i < xmlTargeting.visibilities.size(); i++) {
                    visibilityDetailXML.add(xmlTargeting.visibilities.get(i).value);
                }
                return visibilityDetailXML;
            }
            return visibilityDetailXML;
        }

        @Override
        public Map<String, Boolean> getSSPSellerNetwork() {
            Map<String, Boolean> sspSellerNetworkDetailsXML = new HashMap<>();
            for (int i = 0; i < xmlTargeting.ssp_seller_network.size(); i++) {
                sspSellerNetworkDetailsXML.put(String.valueOf(xmlTargeting.ssp_seller_network.get(i).name),
                        Boolean.valueOf(xmlTargeting.ssp_seller_network.get(i).value));
            }
            return sspSellerNetworkDetailsXML;
        }

        @Override
        public Multimap<String, Integer> getPageDomainList() {
            Multimap<String, Integer> pageDomainDetailsXML = ArrayListMultimap.create();
            if (xmlTargeting.domain_lists != null) {
                for (int i = 0; i < xmlTargeting.domain_lists.page_domain.domain_lists.size(); i++) {
                    pageDomainDetailsXML.put(xmlTargeting.domain_lists.page_domain.type.name(),
                            Integer.parseInt(xmlTargeting.domain_lists.page_domain.domain_lists.get(i)));
                }
            }
            return pageDomainDetailsXML;
        }

        @Override
        public Multimap<String, Integer> getSiteDomainList() {
            Multimap<String, Integer> siteDomainDetailsXML = ArrayListMultimap.create();
            if (xmlTargeting.domain_lists != null) {
                for (int i = 0; i < xmlTargeting.domain_lists.site_domain.domain_lists.size(); i++) {
                    siteDomainDetailsXML.put(xmlTargeting.domain_lists.site_domain.type.name(),
                            Integer.parseInt(xmlTargeting.domain_lists.site_domain.domain_lists.get(i)));
                }
            }
            return siteDomainDetailsXML;
        }

        @Override
        public String getDomainOperator() {
            if (xmlTargeting.domain_lists != null) {
                return xmlTargeting.domain_lists.operator.name().toUpperCase();
            }
            return "";
        }

        @Override
        public Set<PlayerSize> getPlayerSizes() {
            Set<PlayerSize> playerSizeSetXML = new HashSet<>();
            if(xmlTargeting.player_sizes!=null){
                for (int i = 0; i < xmlTargeting.player_sizes.size(); i++) {
                    Integer playerMinWidth = xmlTargeting.player_sizes.get(i).min == null ? 0 : xmlTargeting.player_sizes.get(i).min;
                    Integer playerMaxWidth = xmlTargeting.player_sizes.get(i).max == null ? 0 : xmlTargeting.player_sizes.get(i).max;
                    PlayerSize playerSize = new PlayerSize(playerMinWidth, playerMaxWidth);
                    playerSizeSetXML.add(playerSize);
                }
                return playerSizeSetXML;
            }
            return playerSizeSetXML;
        }

        @Override
        public String getInventoryTypeDetails() {
            return xmlTargeting.inventory_type.name().toLowerCase();
        }
    }
}

