/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.osapi.factory.DependencyManager;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.comparatorfactory.CampaignParameterComparator;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;

/**
 * @author Clearstream.
 *
 */
public class CampaignValidator 
{
	public static class PlacementDataOS implements CampaignParameterComparator
	{
		private RootCampaignResponse campaignResponseOS;
		private RootPlacementResponse placementResponseOS;
		private RootFraudsResponse fraudResponseOS;
		private RootMediaInventoryResponse rootMediaInventoryResponseSmartList;

		public PlacementDataOS(RootPlacementResponse placementResponseOS,String scenarioTestData,
							   Integer placementNumber,RootMediaInventoryResponse rootMediaInventoryResponseSmartList)
		{
			this.placementResponseOS = placementResponseOS;
			this.campaignResponseOS =  DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignDetails(scenarioTestData);
			this.fraudResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getFraudDetailsForPlacement(scenarioTestData, placementNumber);
			this.rootMediaInventoryResponseSmartList = rootMediaInventoryResponseSmartList;
		}

		@Override
		public String toString() {
			return "PlacementDataOS{" +
					"placementId=" + getPlacementId() +
					", placementName='" + getPlacementName() + '\'' +
					", campaignId=" + getCampaignId() +
					", campaignName='" + getCampaignName() + '\'' +
					", insertionOrder='" + getInsertionOrder() + '\'' +
					", doubleVerifyEnabled=" + getDoubleVerifyEnabled() +
					", smartListTrue=" + isSmartListEnabled() +
					'}';
		}

		@Override
		public Integer getPlacementId()
		{
			return placementResponseOS.getId();
		}

		@Override
		public String getPlacementName()
		{
			return placementResponseOS.getName();
		}
		
		@Override
		public Integer getCampaignId()
		{
			return campaignResponseOS.getId();
		}

		@Override
		public String getCampaignName()
		{
			return campaignResponseOS.getName();
		}

		@Override
		public String getInsertionOrder()
		{
			return campaignResponseOS.getInsertionOrderNumber().toString();
		}
		
		@Override
		public boolean getDoubleVerifyEnabled()
		{
			return fraudResponseOS.getEnableDoubleVerify();
		}

		@Override
		public boolean isSmartListEnabled() {
			return !(rootMediaInventoryResponseSmartList==null);
		}
	}
	
	public static class PlacementDataXML implements CampaignParameterComparator
	{
		private ConfigData.Campaign xmlCampaign;
		public PlacementDataXML(RootPlacementResponse placementResponseOS)
		{
			this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(placementResponseOS.getId());
		}

		@Override
		public String toString() {
			return "PlacementDataXML{" +
					"placementId=" + getPlacementId() +
					", placementName='" + getPlacementName() + '\'' +
					", campaignId=" + getCampaignId() +
					", campaignName='" + getCampaignName() + '\'' +
					", insertionOrder='" + getInsertionOrder() + '\'' +
					", doubleVerifyEnabled=" + getDoubleVerifyEnabled() +
					", smartListTrue=" + isSmartListEnabled() +
					'}';
		}

		@Override
		public Integer getPlacementId()
		{
			return  xmlCampaign.id;
		}

		@Override
		public String getPlacementName()
		{
			return xmlCampaign.name;
		}
		
		@Override
		public Integer getCampaignId()
		{
			return xmlCampaign.group_id;
		}

		@Override
		public String getCampaignName()
		{
			return xmlCampaign.group_name;
		}

		@Override
		public String getInsertionOrder()
		{
			return xmlCampaign.insertion_order;
		}
		
		@Override
		public boolean getDoubleVerifyEnabled()
		{
			return xmlCampaign.double_verify_enabled;
		}

		@Override
		public boolean isSmartListEnabled() {
			return !(xmlCampaign.smartlist==null);
		}
	}
}
