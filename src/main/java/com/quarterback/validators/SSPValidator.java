/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.osapi.factory.DependencyManager;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.comparatorfactory.SSPComparator;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clearstream.
 */
public class SSPValidator {
    private static final Logger LOG = Logger.getLogger(SSPValidator.class);


    public static Comparator<Map<Integer, String>> MapComparator = new Comparator<Map<Integer, String>>() {
        @Override
        public int compare(Map<Integer, String> map1, Map<Integer, String> map2) {
            Map<Integer, String> Map1 = map1.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            Map<Integer, String> Map2 = map2.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            return Map1.equals(Map2) ? 0 : 1;
        }
    };


    public static Comparator<Map<String, Multimap>> SSPComparator = new Comparator<Map<String, Multimap>>() {
        @Override
        public int compare(Map<String, Multimap> o1, Map<String, Multimap> o2) {
            if (o1.keySet().containsAll(o2.keySet())) {
                for (String key : o1.keySet()) {
                    Multimap<String, String> multiMap1 = o1.get(key);
                    Multimap<String, String> multiMap2 = o2.get(key);
                    Multimap<String, String> firstSecondDifference =
                            Multimaps.filterEntries(multiMap1, e -> !multiMap2.containsEntry(e.getKey(), e.getValue()));
                    Multimap<String, String> secondFirstDifference =
                            Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                    if (!(firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty())) {
                        LOG.info("Difference In XML's SSP and OS's SSP " + firstSecondDifference);
                        LOG.info("Difference In XML's SSP and OS's SSP " + secondFirstDifference);
                        return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty() ? 0 : 1;
                    }
                }
                return 0;
            }
            return 1;
        }
    };


    public static class SellerNetworksOS implements SSPComparator {

        private RootDealsAndSeatsResponse dealsAndSeatsResponseOS;
        private RootPlacementResponse placementResponseOS;

        public SellerNetworksOS(String scenarioTestDataPath, Integer placementNumber) {
            this.dealsAndSeatsResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getDealsAndSeatsDetails(scenarioTestDataPath).get(placementNumber);
            this.placementResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class)
                    .getPlacementDetails(scenarioTestDataPath).get(placementNumber);
        }


        @Override
        public String toString() {
            return "SellerNetworksOS [placementResponseOS=" + placementResponseOS
                    + ", getSellerNetworkDealsAndSeatsDetails()=" + getSellerNetworkDealsAndSeatsDetails()
                    + ", getSSPAttribute()=" + getSSPAttribute() + ", getTotalSSPCount()=" + getTotalSSPCount() + "]";
        }

        @Override
        public Map<String, Multimap> getSellerNetworkDealsAndSeatsDetails() {
            Map<String, Multimap> finaldealSeatDeatilMapOS = new HashMap<String, Multimap>();
            for (int i = 0; i < placementResponseOS.getSsps().size(); i++) {
                Multimap<String, String> dealSeatMultiMapOS = ArrayListMultimap.create();

                //For Deals
                for (int j = 0; j < dealsAndSeatsResponseOS.getSellerNetworkDeals().size(); j++) {
                    dealSeatMultiMapOS.put(dealsAndSeatsResponseOS.getSellerNetworkDeals().get(j).getSellerNetwork(),
                            dealsAndSeatsResponseOS.getSellerNetworkDeals().get(j).getValue());
                }
                //For Seats
                for (int k = 0; k < dealsAndSeatsResponseOS.getSellerNetworkSeats().size(); k++) {
                    dealSeatMultiMapOS.put(dealsAndSeatsResponseOS.getSellerNetworkSeats().get(k).getSellerNetwork(),
                            dealsAndSeatsResponseOS.getSellerNetworkSeats().get(k).getValue());
                }
                // System.out.println("MultiMap from OS "+ dealSeatMultiMapOS);
                finaldealSeatDeatilMapOS.put(placementResponseOS.getSsps().get(i).getName(), dealSeatMultiMapOS);
            }
            // System.out.println("SSP Details From OS "+ finaldealSeatDeatilMapOS);
            return finaldealSeatDeatilMapOS;
        }

        @Override
        public Map<Integer, String> getSSPAttribute() {
            Map<Integer, String> sspAttributeMapOS = new HashMap<Integer, String>();
            for (int i = 0; i < placementResponseOS.getSsps().size(); i++) {
                sspAttributeMapOS.put(i + 1, placementResponseOS.getSsps().get(i).getName());
            }
            return sspAttributeMapOS;
        }

        @Override
        public Integer getTotalSSPCount() {
            return placementResponseOS.getSsps().size();
        }
    }


    public static class SellerNetworkXML implements SSPComparator {

        private ConfigData.Campaign xmlCampaign;
        private List<ConfigData.SSO_SSP> xmlSSPList;

        public SellerNetworkXML(RootPlacementResponse placementResponseOS) {
            this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(placementResponseOS.getId());
            this.xmlSSPList = xmlCampaign.seller_side_options;
        }

        @Override
        public String toString() {
            return "SellerNetworkXML [getSellerNetworkDealsAndSeatsDetails()=" + getSellerNetworkDealsAndSeatsDetails()
                    + ", getSSPAttribute()=" + getSSPAttribute() + ", getTotalSSPCount()=" + getTotalSSPCount() + "]";
        }


        @Override
        public Map<String, Multimap> getSellerNetworkDealsAndSeatsDetails() {
            Map<String, Multimap> finaldealSeatDeatilMapXML = new HashMap<String, Multimap>();
            for (int i = 0; i < xmlSSPList.size(); i++) {
                Multimap<String, String> dealSeatMultiMapXML = ArrayListMultimap.create();
                for (int j = 0; j < xmlSSPList.get(i).seller_networks.size(); j++) {
                    //For Deals
                    if (xmlSSPList.get(i).seller_networks.get(j).deals != null) {
                        for (int k = 0; k < xmlSSPList.get(i).seller_networks.get(j).deals.size(); k++) {
                            dealSeatMultiMapXML.put(xmlSSPList.get(i).seller_networks.get(j).name,
                                    xmlSSPList.get(i).seller_networks.get(j).deals.get(k).id);
                        }
                    }
                    //For Seats
                    if (xmlSSPList.get(i).seller_networks.get(j).seats != null) {
                        for (int k = 0; k < xmlSSPList.get(i).seller_networks.get(j).seats.size(); k++) {
                            dealSeatMultiMapXML.put(xmlSSPList.get(i).seller_networks.get(j).name,
                                    xmlSSPList.get(i).seller_networks.get(j).seats.get(k).id);
                        }
                    }
                    //System.out.println("MultiMap From XML" + dealSeatMultiMapXML);
                }
                finaldealSeatDeatilMapXML.put(xmlSSPList.get(i).name, dealSeatMultiMapXML);
            }
            //System.out.println("SSP Details From XML "+ finaldealSeatDeatilMapXML);
            return finaldealSeatDeatilMapXML;
        }

        @Override
        public Map<Integer, String> getSSPAttribute() {
            Map<Integer, String> sspAttributeMapXML = new HashMap<Integer, String>();
            for (int i = 0; i < xmlSSPList.size(); i++) {
                ;
                sspAttributeMapXML.put(i + 1, xmlSSPList.get(i).name);
            }
            return sspAttributeMapXML;
        }

        @Override
        public Integer getTotalSSPCount() {
            return xmlSSPList.size();
        }
    }
}
