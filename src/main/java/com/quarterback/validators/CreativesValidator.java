/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.google.common.collect.Sets;
import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.comparatorfactory.CreativeComparator;
import com.quarterback.comparatorfactory.CreativeComparatorType;
import com.quarterback.factory.IponWebMacros;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clearstream.
 *
 */
public class CreativesValidator
{	
	public static Comparator<Map<String, String>> MapComparatorWithStringKey = new Comparator<Map<String, String>>()
	{
		@Override
		public int compare(Map<String, String>map1, Map<String, String> map2)
		{
			Map<String, String> Map1 = map1.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			Map<String, String> Map2 = map2.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			return Map1.equals(Map2) ? 0: 1;
		}
	};
	
	public static Comparator<Map<Integer, String>> MapComparatorWithIntegerKey = new Comparator<Map<Integer, String>>()
	{
		@Override
		public int compare(Map<Integer, String>map1, Map<Integer, String> map2)
		{
			Map<Integer, String> Map1 = map1.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			Map<Integer, String> Map2 = map2.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			return Map1.equals(Map2) ? 0: 1;
		}
	};
	
	public static Comparator<Set<String>> SetComparatorForString = new Comparator<Set<String>>()
	{

		@Override
		public int compare(Set<String> set1, Set<String> set2)
		{

			if(set1.size()==set2.size()) {
				return set1.containsAll(set2) ? 0:1;
			}
			return 1;
		}
	};
	
	public static Comparator<Set<Integer>> SetComparatorForInteger = new Comparator<Set<Integer>>()
	{

		@Override
		public int compare(Set<Integer> set1, Set<Integer> set2)
		{
			if(set1.size()==set2.size()) {
				return set1.containsAll(set2) ? 0:1;
			}
			return 1;
		}
	};
	
	public static class VideoCreativeOS extends CreativeComparator{
		private RootBrandResponse brandResponseOS;
		private FlightResponseAd flightAd;
		private List<RootAssetResponse> assetVideoResponseOS;
		private ProductType productType;
		private RootPlacementResponse placementResponse;
	
		
		public VideoCreativeOS(String scenarioTestDataPath, FlightResponseAd flightAd,RootFlightsResponse flightResponse
				, Integer productType,RootPlacementResponse placementResponse)
		{
			this.brandResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getBrandDetails(scenarioTestDataPath);
			this.flightAd = flightAd;
			this.assetVideoResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class)
					.getVideoAssetsForAFlight(scenarioTestDataPath, flightResponse);
			this.productType = ProductType.fromProductTypeNumber(productType);
			this.placementResponse = placementResponse;
		}
		
		@Override
		public String toString()
		{
			return "VideoCreativeOS [getCreativeAttributes()=" + getCreativeAttributes() + ", getCreativeDuration()="
					+ getCreativeDuration() + ", getMimeTypes()=" + getMimeTypes() + ", getLinear()=" + getLinear()
					+ ", getSkippable()=" + getSkippable() + ", getVpaid()=" + getVpaid() + ", getVastURL()="
					+ getVastURL() + ", getVastURLSecure()=" + getVastURLSecure() + ", getBidSwitchMacros()="
					+ getBidSwitchMacros() + ", getLandingPageDomain()=" + getLandingPageDomain() + "]";
		}
		
		@Override
		public Map<Integer,String> getCreativeAttributes()
		{
			Map<Integer,String> creativeMap = new HashMap<Integer,String>();
			creativeMap.put(flightAd.getId(), flightAd.getName());
			return creativeMap ;
		}
		
		@Override
		public Integer getCreativeDuration()
		{
			return 30;
		}
		
		@Override
		public Set<String> getMimeTypes()
		{
			Set<String> assetMimeTypeList = new HashSet<String>();
			for(int i=0;i<assetVideoResponseOS.size();i++)
			{
				for(int j=0;j<assetVideoResponseOS.get(i).getMimeTypes().size();j++) 
				{
					assetMimeTypeList.add(assetVideoResponseOS.get(i).getMimeTypes().get(j).getValue());
				}
			}
			return assetMimeTypeList;
		}
		
		@Override
		public String getLinear()
		{
			return assetVideoResponseOS.get(0).getIsLinear().toString();
		}
		
		@Override
		public String getSkippable()
		{
			return assetVideoResponseOS.get(0).getIsSkippable().toString();
		}
		
		@Override
		public String getVpaid()
		{
			return assetVideoResponseOS.get(0).getIsVPaid().toString();
		}
		
		@Override
		public String getVastURL()
		{
			return flightAd.getAdTag();
		}
		
		@Override
		public String getVastURLSecure()
		{
			return flightAd.getAdTagSecure();
		}
		
		@Override
		public Map<String,String> getBidSwitchMacros()
		{
			Map<String,String> bidSwitchMacrosMap = productType.getMacrosForProductType();
			bidSwitchMacrosMap.put(IponWebMacros.SSP.toString(),placementResponse.getSsps().get(0).getName());
			return bidSwitchMacrosMap;
		}
		
		@Override
		public String getLandingPageDomain()
		{
			return brandResponseOS.getLandingPageUrl();
		}
	}
	
	public static class HTMLCreativeOS extends CreativeComparator 
	{
		private RootBrandResponse brandResponseOS;
		private FlightResponseAd flightAd;
		private ProductType productType;
		private RootFlightsResponse flightsResponseOS;
		
		public HTMLCreativeOS(String scenarioTestDataPath, FlightResponseAd flightAd,RootFlightsResponse flightResponse, Integer productType)
		{
			this.brandResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getBrandDetails(scenarioTestDataPath);
			this.flightAd = flightAd;
			this.flightsResponseOS = flightResponse;
			this.productType = ProductType.fromProductTypeNumber(productType);
		}
			
		@Override
		public String toString()
		{
			return "HTMLCreativeOS [getCreativeAttributes()=" + getCreativeAttributes() + ", getCreativeDuration()="
					+ getCreativeDuration() + ", getLandingPageDomain()=" + getLandingPageDomain() + ", getAdType()="
					+ getAdType() + ", getCanTrackClicks()=" + getCanTrackClicks() + ", getSize()=" + getSize()
					+ ", getHTMLContent()=" + getHTMLContent() + "]";
		}

		@Override
		public Map<Integer,String> getCreativeAttributes()
		{
			Map<Integer,String> creativeMap = new HashMap<Integer,String>();
			creativeMap.put(flightAd.getId(), flightAd.getName());
			return creativeMap ;
		}
		
		@Override
		public Integer getCreativeDuration()
		{
			return 30;
		}
		
		@Override
		public String getLandingPageDomain()
		{
			return CreativeComparatorType.valueOf(productType.toString()).getLandingPageDomainForProductType(brandResponseOS, flightsResponseOS);
		}
		
		@Override
		public String getAdType()
		{
			return flightAd.getAdType().contentEquals("VIDEO") ? "VIDEO" : "HTML";
		}

		@Override
		public String getCanTrackClicks()
		{
			return productType.getCanTrackClick(flightsResponseOS.getBusinessModel().getStrategyId());
		}
		
		@Override
		public String getSize()
		{
			 return CreativeComparatorType.valueOf(productType.toString()).getAssetSizeForProductType(flightsResponseOS);
		}
				
		@Override
		public String getHTMLContent()
		{
			//String constant = "<![CDATA[&]]>";
			//String htmlContent = constant.replace("&", flightAd.getAdTag());
			return flightAd.getAdTag();
		}

	}

	public static class VideoCreativeXML extends CreativeComparator{
		private ConfigData.Campaign xmlCampaign;
		private ConfigData.Creative creative;
		
		public VideoCreativeXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd)
		{
			this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(flightResponseOS.getPlacementId());
			this.creative = DependencyManager.getInjector().getInstance(SessionVariables.class).getSingleCreativeFromXML(xmlCampaign, flightAd.getId());
		}
				
		@Override
		public String toString()
		{
			return "VideoCreativeXML [getCreativeAttributes()=" + getCreativeAttributes() + ", getCreativeDuration()="
					+ getCreativeDuration() + ", getMimeTypes()=" + getMimeTypes() + ", getLinear()=" + getLinear()
					+ ", getSkippable()=" + getSkippable() + ", getVpaid()=" + getVpaid() + ", getVastURL()="
					+ getVastURL() + ", getVastURLSecure()=" + getVastURLSecure() + ", getBidSwitchMacros()="
					+ getBidSwitchMacros() + ", getLandingPageDomain()=" + getLandingPageDomain() + "]";
		}

		@Override
		public Map<Integer, String> getCreativeAttributes()
		{
			Map<Integer,String> creativeAttributeMap = new HashMap<Integer,String>();
			creativeAttributeMap.put(Integer.parseInt(creative.id), creative.name);
			return creativeAttributeMap;
		}
		
		@Override
		public Integer getCreativeDuration()
		{
			return creative.duration;
		}
		
		@Override
		public Set<String> getMimeTypes()
		{
			Set<String> mimeTypesXML = new HashSet<String>();
			for(int i=0;i<creative.mime_type_values.size();i++) {
				mimeTypesXML.add(creative.mime_type_values.get(i));
			}
			return mimeTypesXML;
		}
		
		@Override
		public String getLinear()
		{
			return String.valueOf(creative.is_linear);
		}
		
		@Override
		public String getSkippable()
		{
			return String.valueOf(creative.is_skippable);
		}
		
		@Override
		public String getVpaid()
		{
			return String.valueOf(creative.is_vpaid);
		}
		
		@Override
		public String getVastURL()
		{
			return creative.vast_url;
		}
		
		@Override
		public String getVastURLSecure()
		{
			return creative.vast_url_secure;
		}
		
		@Override
		public Map<String, String> getBidSwitchMacros()
		{
			String resultString = creative.bidswitch_macros.replaceFirst("&", "");
			Map<String,String> resultXML = Arrays.stream(resultString.split("&")).map(next->next.split("="))
					.collect(Collectors.toMap(entry->entry[0], entry->entry[1]));
			return resultXML;
		}
		
		@Override
		public String getLandingPageDomain()
		{
			return creative.landing_page_domain;
		}
		
		
	}
	public static class HTMLCreativeXML extends CreativeComparator{
		private ConfigData.Campaign xmlCampaign;
		private ConfigData.Creative creative;
		
		public HTMLCreativeXML(RootFlightsResponse flightResponseOS,FlightResponseAd flightAd)
		{
			this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(flightResponseOS.getPlacementId());
			this.creative = DependencyManager.getInjector().getInstance(SessionVariables.class).getSingleCreativeFromXML(xmlCampaign, flightAd.getId());
		}
		
		@Override
		public String toString()
		{
			return "HTMLCreativeXML [getCreativeAttributes()=" + getCreativeAttributes() + ", getCreativeDuration()="
					+ getCreativeDuration() + ", getLandingPageDomain()=" + getLandingPageDomain() + ", getAdType()="
					+ getAdType() + ", getSize()=" + getSize() + ", getCanTrackClicks()=" + getCanTrackClicks()
					+ ", getHTMLContent()=" + getHTMLContent() + "]";
		}

		@Override
		public Map<Integer, String> getCreativeAttributes()
		{
			Map<Integer,String> creativeAttributeMap = new HashMap<Integer,String>();
			creativeAttributeMap.put(Integer.parseInt(creative.id), creative.name);
			return creativeAttributeMap;
		}
		
		@Override
		public Integer getCreativeDuration()
		{
			return creative.duration;
		}
		
		@Override
		public String getLandingPageDomain()
		{
			return creative.landing_page_domain;
		}
		
		@Override
		public String getAdType()
		{
			return creative.type;
		}
		
		@Override
		public String getSize()
		{
			return creative.size;
		}
		
		@Override
		public String getCanTrackClicks()
		{
			return creative.can_track_clicks;
		}

		@Override
		public String getHTMLContent()
		{
			return creative.html_content;
		}
	}
	
}
