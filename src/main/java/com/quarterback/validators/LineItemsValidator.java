/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.osapi.enumtypes.ProductType;
import com.osapi.factory.DependencyManager;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.flightsresponse.FlightResponseAd;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.quarterback.comparatorfactory.LineItemsComparator;
import com.quarterback.factory.BusinessModels;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clearstream.
 *
 */
public class LineItemsValidator
{
	public static Comparator<Map<Integer, String>> MapComparator = new Comparator<Map<Integer, String>>()
	{
		@Override
		public int compare(Map<Integer, String>map1, Map<Integer, String> map2)
		{
			Map<Integer, String> Map1 = map1.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			Map<Integer, String> Map2 = map2.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			return Map1.equals(Map2) ? 0: 1;
		}
	};
	
	public static Comparator<Set<Integer>> SetComparator = new Comparator<Set<Integer>>()
	{

		@Override
		public int compare(Set<Integer> set1, Set<Integer> set2)
		{
			if(set1.size()==set2.size()) {
				return set1.containsAll(set2) ? 0:1;
			}
			return 1;
		}
	};
	
	public static Comparator<List<Map<String,Double>>> ListComparator = new Comparator<List<Map<String,Double>>>()
	{

		@Override
		public int compare(List<Map<String, Double>> list1, List<Map<String, Double>> list2)
		{
			if(list1.size()==list2.size()) {
				return list1.containsAll(list2) ? 0:1;
			}
			return 1;
		}
	};

	public static class LineItemsOS implements LineItemsComparator
	{
		private RootPlacementResponse rootPlacementResponse;
		private RootFlightsResponse flightResponseOS;
		private FlightResponseAd flightAd;
		private BusinessModels businessModel;
		private List<RootFlightsResponse> flightResponseList;
		private RootConversionPixelResponse conversionPixelResponse;
		
		public LineItemsOS(RootFlightsResponse flightResponseOS, FlightResponseAd flightAd, RootPlacementResponse rootPlacementResponse,
						   RootConversionPixelResponse conversionPixelResponse)
		{
			this.flightResponseOS = flightResponseOS;
			this.flightAd = flightAd;
			this.businessModel = BusinessModels.fromCode(flightResponseOS.getBusinessModel().getStrategyId());
			this.rootPlacementResponse =rootPlacementResponse;
			this.conversionPixelResponse = conversionPixelResponse;
		}
		
		public LineItemsOS(List<RootFlightsResponse> flightResponseList) {
			this.flightResponseList = flightResponseList;
		}
		
		@Override
		public String toString()
		{
			return "LineItemsOS [getLineItemAttributeMap()=" + getLineItemAttributeMap()
					+ ", getStartDateForLineItem()=" + getStartDateForLineItem() + ", getEndDateForLineItem()="
					+ getEndDateForLineItem() + ", getCreativeForLineItem()=" + getCreativeForLineItem()
					+ ", getStrategyIdForLineItem()=" + getStrategyIdForLineItem()
					+ ", getStrategyParameterForLineItem()=" + getStrategyParameterForLineItem() +
					", getTrackingConversionForLineItem()=" + getTrackingConversionForLineItem() +
					", getIsOptimizedPixel()=" + getIsOptimizedPixel() +"]";
		}

		@Override
		public Map<Integer, String> getLineItemAttributeMap()
		{
			Map<Integer,String> lineItemAttributeMap = new HashMap<Integer,String>();
			lineItemAttributeMap.put(flightAd.getId(), flightAd.getName());
			return lineItemAttributeMap;
		}

		@Override
		public String getStartDateForLineItem()
		{
			return flightResponseOS.getStartDate().split("\\.")[0].replaceAll("T", " ");
		}

		
		@Override
		public String getEndDateForLineItem()
		{
			return flightResponseOS.getEndDate().split("\\.")[0].replaceAll("T", " ");
		}

		
		@Override
		public Set<Integer> getCreativeForLineItem()
		{
			Set<Integer> flightAdIds = new HashSet<Integer>();
			for(int i=0;i<flightResponseOS.getAds().size();i++)
			{
				flightAdIds.add(flightResponseOS.getAds().get(i).getId());
			}
			return flightAdIds;
		}

		@Override
		public Integer getStrategyIdForLineItem()
		{
			return flightResponseOS.getBusinessModel().getStrategyId();
		}

		@Override
		public List<Map<String, Double>> getStrategyParameterForLineItem()
		{
		    return ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType())
					.getBusinessModelMapForProductType(flightResponseOS.getBusinessModel().getStrategyId(), flightResponseOS);
		}

		@Override
		public Set<Integer> getTrackingConversionForLineItem() {
			return ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()).getConversionPixelsFromOS(
					rootPlacementResponse.getBusinessModel().getStrategyId(),conversionPixelResponse);
		}

		@Override
		public Integer getIsOptimizedPixel() {
			return ProductType.fromProductTypeNumber(rootPlacementResponse.getProductType()).getIsOptimizedPixelFromOS(
					rootPlacementResponse.getBusinessModel().getStrategyId(),conversionPixelResponse);
		}

		@Override
		public Set<Integer> getLineItemsId()
		{
			Set<Integer> flightAdIdsOS = new HashSet<Integer>();
			for(int i=0;i<flightResponseList.size();i++)
			{
				if(flightResponseList.get(i).getActive()) {
					for(FlightResponseAd flightad : flightResponseList.get(i).getAds())
					{
						flightAdIdsOS.add(flightad.getId());
					}
				}

			}
			return flightAdIdsOS;
		}

		@Override
		public Integer getTotalLineItemsCount()
		{
			Integer totalLineItemsOS=0;
			for(int i=0;i<flightResponseList.size();i++)
			{
				if(flightResponseList.get(i).getActive())
				{
					totalLineItemsOS = totalLineItemsOS + flightResponseList.get(i).getAds().size();
				}
			}
			return totalLineItemsOS;
		}
	}
	
	public static class LineItemsXML implements LineItemsComparator
	{
		
		private ConfigData.Campaign xmlCampaign;
		private ConfigData.LineItem lineItemXML;
		private RootPlacementResponse placementResponse;
		
		public LineItemsXML(RootPlacementResponse rootPlacementResponse ,RootFlightsResponse flightResponseOS,FlightResponseAd flightAd)
		{
			this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(flightResponseOS.getPlacementId());
			this.lineItemXML = DependencyManager.getInjector().getInstance(SessionVariables.class).getLineItemFromXML(xmlCampaign, flightAd.getId());
			this.placementResponse = rootPlacementResponse;
		}
		
		public LineItemsXML(Integer placementId)
		{
			this.xmlCampaign = DependencyManager.getInjector().getInstance(SessionVariables.class).getXmlCampaign(placementId);
		}

		@Override
		public String toString()
		{
			return "LineItemsOS [getLineItemAttributeMap()=" + getLineItemAttributeMap()
					+ ", getStartDateForLineItem()=" + getStartDateForLineItem() + ", getEndDateForLineItem()="
					+ getEndDateForLineItem() + ", getCreativeForLineItem()=" + getCreativeForLineItem()
					+ ", getStrategyIdForLineItem()=" + getStrategyIdForLineItem()
					+ ", getStrategyParameterForLineItem()=" + getStrategyParameterForLineItem() +
					", getTrackingConversionForLineItem()=" + getTrackingConversionForLineItem() +
					", getIsOptimizedPixel()=" + getIsOptimizedPixel() +"]";
		}

		@Override
		public Map<Integer, String> getLineItemAttributeMap()
		{
			Map<Integer,String> lineItemAttributeMap = new HashMap<Integer,String>();
			lineItemAttributeMap.put(lineItemXML.id, lineItemXML.name);
			return lineItemAttributeMap;
		}

		@Override
		public String getStartDateForLineItem()
		{
			return lineItemXML.start_date;
		}

		
		@Override
		public String getEndDateForLineItem()
		{
			return lineItemXML.end_date;
		}

		
		@Override
		public Set<Integer> getCreativeForLineItem()
		{
			return lineItemXML.creative_ids;
		}

		@Override
		public Integer getStrategyIdForLineItem()
		{
			return lineItemXML.strategy.id;
		}

		@Override
		public List<Map<String, Double>> getStrategyParameterForLineItem()
		{
			List<Map<String,Double>> strategyParametersXML = new LinkedList<Map<String,Double>>();
			for(int i=0;i<lineItemXML.strategy.parameters.size();i++) {
				Map<String,Double> parameterMap = new HashMap<String,Double>();
				parameterMap.put(lineItemXML.strategy.parameters.get(i).name.getXMLValue(), lineItemXML.strategy.parameters.get(i).value);
				strategyParametersXML.add(parameterMap);
			}
			return strategyParametersXML;
		}

		@Override
		public Set<Integer> getTrackingConversionForLineItem() {
			return  ProductType.fromProductTypeNumber(placementResponse.getProductType()).getConversionPixelsFromXML(lineItemXML.strategy.id,lineItemXML);
		}

		@Override
		public Integer getIsOptimizedPixel() {
			return  ProductType.fromProductTypeNumber(placementResponse.getProductType()).getIsOptimizedPixelFromXML(lineItemXML.strategy.id,lineItemXML);
		}

		@Override
		public Set<Integer> getLineItemsId()
		{
			Set<Integer> lineItemsIdXML = new HashSet<Integer>();
			for(int i=0;i<xmlCampaign.line_items.size();i++)
			{
				lineItemsIdXML.add(xmlCampaign.line_items.get(i).id);
			}
			return lineItemsIdXML;
		}

		@Override
		public Integer getTotalLineItemsCount()
		{
			return xmlCampaign.line_items.size();
		}
	}
}
