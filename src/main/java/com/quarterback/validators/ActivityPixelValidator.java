/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.osapi.factory.DependencyManager;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.quarterback.comparatorfactory.ActivityPixelComparator;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;

/**
 * @author Clearstream.
 */
public class ActivityPixelValidator {

    private static final Logger LOG = Logger.getLogger(ActivityPixelValidator.class);


    public static Comparator<Multimap<Integer, String>> MultiMapComparator = new Comparator<Multimap<Integer, String>>()
    {
        @Override
        public int compare(Multimap<Integer, String> multiMap1, Multimap<Integer, String> multiMap2) {
            if(multiMap1.keySet().containsAll(multiMap2.keySet())) {
                Multimap<Integer, String> firstSecondDifference =
                        Multimaps.filterEntries(multiMap1, e -> !multiMap2.containsEntry(e.getKey(), e.getValue()));
                Multimap<Integer, String> secondFirstDifference =
                        Multimaps.filterEntries(multiMap2, e -> !multiMap1.containsEntry(e.getKey(), e.getValue()));
                if(!(firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty())) {
                    LOG.info("Difference In XML's ActivityPixel Information and OS's ActivityPixel " + firstSecondDifference);
                    LOG.info("Difference In XML's ActivityPixel Information and OS's ActivityPixel " + secondFirstDifference);
                }
                return firstSecondDifference.isEmpty() && secondFirstDifference.isEmpty() ? 0 : 1;
            }
            return 1;
        }
    };

    public static class ActivityPixelOS implements ActivityPixelComparator{

        private RootActivityPixelResponse activityPixelResponse ;

        public ActivityPixelOS(RootActivityPixelResponse activityPixelResponse) {
            this.activityPixelResponse = activityPixelResponse;
        }

        @Override
        public String toString()
        {
            return "ActivityPixelOS [getActivityPixelDetails()=" + getActivityPixelDetails() + "]";
        }


        @Override
        public Multimap<Integer, String> getActivityPixelDetails() {
            Multimap<Integer,String> activityPixelMultiMap = ArrayListMultimap.create();
            activityPixelMultiMap.put(activityPixelResponse.getId(),activityPixelResponse.getName());
            activityPixelMultiMap.put(activityPixelResponse.getId(),String.valueOf(activityPixelResponse.getPostViewHours()));
            activityPixelMultiMap.put(activityPixelResponse.getId(),String.valueOf(activityPixelResponse.getPostClickHours()));
            activityPixelMultiMap.put(activityPixelResponse.getId(),String.valueOf(activityPixelResponse.getExpirationHours()));
            activityPixelMultiMap.put(activityPixelResponse.getId(),String.valueOf(activityPixelResponse.getSyncEnabled()));
            return activityPixelMultiMap;
        }
    }


    public static class ActivityPixelXML implements  ActivityPixelComparator{

        private RootActivityPixelResponse activityPixelResponse;
        private List<ConfigData.Pixel> activityPixelList;


        public ActivityPixelXML(RootActivityPixelResponse activityPixelResponse) {
            this.activityPixelResponse = activityPixelResponse;
            this.activityPixelList = DependencyManager.getInjector().getInstance(SessionVariables.class).getActivityPixelsListFromXML();
        }

        @Override
        public String toString()
        {
            return "ActivityPixelXML [getActivityPixelDetails()=" + getActivityPixelDetails() + "]";
        }

        @Override
        public Multimap<Integer, String> getActivityPixelDetails() {
            Multimap<Integer,String> activityPixelMultiMapXML = ArrayListMultimap.create();
             for(int i =0;i<activityPixelList.size();i++){
                    ConfigData.Pixel activityPixelXML = activityPixelList.get(i);
                    if(activityPixelResponse.getId()==activityPixelXML.id){
                        activityPixelMultiMapXML.put(activityPixelXML.id,activityPixelXML.name);
                        activityPixelMultiMapXML.put(activityPixelXML.id,String.valueOf(activityPixelXML.post_view_hours));
                        activityPixelMultiMapXML.put(activityPixelXML.id,String.valueOf(activityPixelXML.post_click_hours));
                        activityPixelMultiMapXML.put(activityPixelXML.id,String.valueOf(activityPixelXML.expiration_hours));
                        activityPixelMultiMapXML.put(activityPixelXML.id,String.valueOf(activityPixelXML.sync_enabled));
                        return activityPixelMultiMapXML;
                    }
             }
             return activityPixelMultiMapXML;
        }
    }

}
