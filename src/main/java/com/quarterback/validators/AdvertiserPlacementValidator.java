/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.validators;

import com.google.common.collect.HashBasedTable;
import com.osapi.factory.DependencyManager;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.quarterback.comparatorfactory.AdvertiserPlacementComparator;
import com.quarterback.factory.SessionVariables;
import com.quarterback.mappers.ConfigData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clearstream.
 *
 */
public class AdvertiserPlacementValidator
{
	public static Comparator<Map<Integer, Integer>> MapComparator = new Comparator<Map<Integer, Integer>>()
	{
		@Override
		public int compare(Map<Integer, Integer>map1, Map<Integer, Integer> map2)
		{
			Map<Integer, Integer> Map1 = map1.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			Map<Integer, Integer> Map2 = map2.entrySet().stream()
					.sorted(Map.Entry.comparingByKey())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
							(oldValue, newValue) -> oldValue, LinkedHashMap::new));
			return Map1.equals(Map2) ? 0: 1;
		}
	};
	
	public static class AdvertiserPlacementXML implements AdvertiserPlacementComparator
	{
		private RootCampaignResponse campaignResponseOS;
		private Integer placementId;
		
		public AdvertiserPlacementXML(Integer placementId,String scenarioTestData)
		{
			this.campaignResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignDetails(scenarioTestData);
			this.placementId = placementId;
		}
		
		@Override
		public String toString()
		{
			return "AdvertiserPlacementXML [getAdvertiserId()=" + getAdvertiserId() + ", getPlacementId()="
					+ getPlacementId() + ", getPlacementAdvertiserMap()=" + getPlacementAdvertiserMap() + "]";
		}
		
		@Override
		public Integer getAdvertiserId()
		{
			HashBasedTable<Integer,Integer,ConfigData.Campaign>campaignsByAdvertiserPlacementId = 
					DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignsAdvertiserTableFromXML();
			if(campaignsByAdvertiserPlacementId.containsRow(campaignResponseOS.getAgency().getId()))
			{
				return campaignResponseOS.getAgency().getId();
			}
			return 0;
		}

		
		@Override
		public Integer getPlacementId()
		{
			HashBasedTable<Integer,Integer,ConfigData.Campaign>campaignsByAdvertiserPlacementId = 
					DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignsAdvertiserTableFromXML();
			if(campaignsByAdvertiserPlacementId.containsColumn(placementId))
			{
				return placementId;
			}
			return 0;
		}


		@Override
		public Map<Integer, Integer> getPlacementAdvertiserMap()
		{
			Map<Integer,Integer> advertiserPlacementMap = new HashMap<>();
			HashBasedTable<Integer,Integer,ConfigData.Campaign>campaignsByAdvertiserPlacementId = 
					DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignsAdvertiserTableFromXML();
			Map<Integer,ConfigData.Campaign>xmlCampaignAdvertiserMap = campaignsByAdvertiserPlacementId.column(placementId);
			if(!xmlCampaignAdvertiserMap.keySet().isEmpty()) {
				for (Integer key : xmlCampaignAdvertiserMap.keySet()) {
					advertiserPlacementMap.put(key, placementId);
				}
			}
			return advertiserPlacementMap;
		}
	}
	
	public static class AdvertiserPlacementOS implements AdvertiserPlacementComparator
	{
		private RootCampaignResponse campaignResponseOS;
		private Integer placementId;
		private List<RootFlightsResponse> flightsResponseListOS;
		private List<RootFlightsResponse> flightResponsePlacementListOS;
		
		public AdvertiserPlacementOS(Integer placementId,String scenarioTestData)
		{
			this.campaignResponseOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getCampaignDetails(scenarioTestData);
			this.placementId = placementId;
			this.flightsResponseListOS = DependencyManager.getInjector().getInstance(SessionVariables.class).getFlightDetails(scenarioTestData);
			this.flightResponsePlacementListOS = DependencyManager.getInjector().getInstance(SessionVariables.class).
					getFlightsFromOSForAPlacement(scenarioTestData,placementId);
		}

		@Override
		public Integer getAdvertiserId()
		{
			for(int i =0;i<flightsResponseListOS.size();i++)
			{
				if(flightsResponseListOS.get(i).getActive()) {
					return campaignResponseOS.getAgency().getId();
				}
			}
			return 0;
		}

		@Override
		public Integer getPlacementId()
		{
			for(int i =0;i<flightResponsePlacementListOS.size();i++) {
				if (flightResponsePlacementListOS.get(i).getActive()) {
					return placementId;
				}
			}
			return 0;
		}
		
		@Override
		public Map<Integer, Integer> getPlacementAdvertiserMap() {
			Map<Integer, Integer> agencyPlacementMap = new HashMap<>();
			for (int i = 0; i < flightResponsePlacementListOS.size(); i++) {
				if (flightResponsePlacementListOS.get(i).getActive()) {
					agencyPlacementMap.put(campaignResponseOS.getAgency().getId(), placementId);
				}
			}
			return agencyPlacementMap;
		}

		@Override
		public String toString()
		{
			return "AdvertiserPlacementOS [getAdvertiserId()=" + getAdvertiserId() + ", getPlacementId()="
					+ getPlacementId() + ", getPlacementAdvertiserMap()=" + getPlacementAdvertiserMap() + "]";
		}
		
		
	}
}
