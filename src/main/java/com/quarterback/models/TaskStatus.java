/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.models;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum TaskStatus
{
	FAILED(-1),
	CREATED(1),
	STARTED(2),
	RUNNING(3),
	FINISHED(4),;

	TaskStatus(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static TaskStatus fromCode(int code)
	{
		for (TaskStatus value : TaskStatus.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}
