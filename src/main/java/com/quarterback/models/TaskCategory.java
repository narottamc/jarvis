/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.models;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum TaskCategory
{
	UNKNOWN(100),

	/**
	 * Assemble config (XML) file
	 */
	ASSEMBLE_CONFIG_FILE(101),

	/**
	 * Prepare direct domain based dataset
	 */
	PREPARE_DIRECT_DOMAIN(102),

	/**
	 * Prepare global domain based dataset
	 */
	PREPARE_GLOBAL_DOMAIN(103),

	/**
	 * Prepare smart domain based dataset
	 */
	PREPARE_SMARAT_DOMAIN(104),

	/**
	 * Prepare (Assemble) partial domain file
	 */
	PREPARE_DOMAIN_FILE_PART(105),

	/**
	 * Assemble domain (TSV) file from partial domain files
	 */
	ASSEMBLE_DOMAIN_FILE(106),

	/**
	 * Assemble artifact (tarball) file
	 */
	ASSEMBLE_ARTIFACT_FILE(107),

	/**
	 * Upload artifact file
	 */
	UPLOAD_ARTIFACT_FILE(108),

	/**
	 * Reset domain cache
	 */
	RESET_DOMAIN_CACHE(109),

	/**
	 * Export smart domains
	 */
	EXPORT_SMART_DOMAINS(110);

	TaskCategory(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static TaskCategory fromCode(int code)
	{
		for (TaskCategory value : TaskCategory.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}
