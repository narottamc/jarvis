/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.models;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum TaskGroupStatus
{
	FAILED(-1),
	CREATED(1),
	STARTED(2),
	// ASSEMBLED(3),
	FINISHED(4),
	INCOMPLETE_ARTIFACT(5);

	TaskGroupStatus(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static TaskGroupStatus fromCode(int code)
	{
		for (TaskGroupStatus value : TaskGroupStatus.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}