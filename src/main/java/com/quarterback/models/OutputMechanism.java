/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
//Source file: quarterback.proto at 43:1
package com.quarterback.models;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum OutputMechanism
{
	/**
	 * UNKNOWN
	 */
	UNKNOWN(0),

	/**
	 * Complete TSV/XML
	 */
	COMPLETE(1),

	/**
	 * Incremental On-Demand (Complete XML + Complete Direct Domain)
	 */
	INCREMENTAL_ON_DEMAND(2),

	/**
	 * Incremental TSV
	 */
	INCREMENTAL_SCHEDULED(3),

	/**
	 * Export Smart Domains
	 */
	EXPORT_SMART_DOMAINS(4);

	OutputMechanism(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static OutputMechanism fromCode(int code)
	{
		for (OutputMechanism value : OutputMechanism.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}