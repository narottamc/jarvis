/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.models;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Clearstream
 */
public class OutputFileTypes
{
	public static List<OutputFileType> get(OutputMechanism output_mechanism)
	{
		List<OutputFileType> output_file_types;

		if (output_mechanism == OutputMechanism.COMPLETE)
		{
			output_file_types = Lists.newArrayList(OutputFileType.TARBALL);
		}
		else if (output_mechanism == OutputMechanism.INCREMENTAL_SCHEDULED)
		{
			output_file_types = Lists.newArrayList(OutputFileType.TSV);
		}
		else if (output_mechanism == OutputMechanism.INCREMENTAL_ON_DEMAND)
		{
			output_file_types = Lists.newArrayList(OutputFileType.TSV, OutputFileType.XML);
		}
		else
		{
			output_file_types = Lists.newArrayList();
		}

		return output_file_types;
	}
}