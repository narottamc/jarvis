/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
//Source file: quarterback.proto at 52:1
package com.quarterback.models;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum OutputFileType
{
	/**
	 * UNKNOWN
	 */
	UNKNOWN(0),

	/**
	 * Incremental XML File
	 */
	XML(1),

	/**
	 * Intermediate TSV Partial File
	 */
	TSV_PART(2),

	/**
	 * Incremental TSV File
	 */
	TSV(3),

	/**
	 * Complete (TSV/XML) Artifact File
	 */
	TARBALL(4),

	/**
	 * Export smart domains CSV file
	 */
	CSV(5);

	OutputFileType(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static OutputFileType fromCode(int code)
	{
		for (OutputFileType value : OutputFileType.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}