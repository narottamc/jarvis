/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 */
public class PlayerSize {
    private int minWidth;
    private int maxWidth;

    public PlayerSize(int minWidth, int maxWidth) {
        this.minWidth = minWidth;
        this.maxWidth = maxWidth;
    }

    public Integer getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(Integer minWidth) {
        this.minWidth = minWidth;
    }

    public Integer getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(Integer maxWidth) {
        this.maxWidth = maxWidth;
    }

    @Override
    public String toString() {
        return "PlayerSize With Attributes{" +
                "minWidth=" + minWidth +
                ", maxWidth=" + maxWidth +
                '}';
    }

    @Override
    public int hashCode() {
        int a = this.minWidth;
        int b = this.maxWidth;
        return a + b;

    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PlayerSize){
            PlayerSize playerSize = (PlayerSize) obj;
            if((this.minWidth==playerSize.minWidth) && (this.maxWidth==playerSize.maxWidth)){
                return true;
            }
        }
        return false;
    }

}
