/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 *
 */
public enum XMLCampaignSection
{
	CAMPAIGNS,
	SSP,
	TARGETING,
	LINEITEMS,
	CREATIVES;
	
	public enum CampaignSection
	{
		
	}
	
}
