/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 */
public enum BrowserTypes {

    IE("MSIE"),
    CHROME("Chrome"),
    SAFARI("Safari"),
    FIREFOX("Firefox"),
    OPERA("Opera,OperaMini,OperaMobi"),
    OTHER("BlackBerry,Android,Outlook,Thunderbird,MSOE"),
    IOSAPP("iOS_App"),
    ANDROID("Android"),
    BLACKBERRY("BlackBerry");

    private String rawBrowserText;

    BrowserTypes(String rawBrowserText) {
        this.rawBrowserText = rawBrowserText;
    }

    public String getRawBrowserText() {
        return rawBrowserText;
    }
}
