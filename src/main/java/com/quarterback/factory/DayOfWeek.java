/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 */
public enum DayOfWeek {

    SUNDAY(0,"Sun"),
    MONDAY(1,"Mon"),
    TUESDAY(2,"Tue"),
    WEDNESDAY(3,"Wed"),
    THURSDAY(4,"Thu"),
    FRIDAY(5,"Fri"),
    SATURDAY(6,"Sat");

    private Integer dayCode;
    private String dayAbbreviation;

    DayOfWeek(Integer dayCode, String dayAbbreviation) {
        this.dayCode = dayCode;
        this.dayAbbreviation = dayAbbreviation;
    }

    public Integer getDayCode() {
        return dayCode;
    }

    public String getDayAbbreviation() {
        return dayAbbreviation;
    }

    public static DayOfWeek fromDayCode(Integer code)
    {
        for (DayOfWeek value : DayOfWeek.values())
        {
            if (value.dayCode == code)
            {
                return value;
            }
        }
        return null;
    }
}
