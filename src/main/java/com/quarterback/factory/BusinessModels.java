/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

import com.osapi.factory.DependencyManager;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.quarterback.mappers.ConfigData;

import java.util.*;

/**
 * @author Clearstream.
 *
 */
public enum BusinessModels
{
	SIMPLE_CPM_WITH_MARGIN_MAXIMIZATION(1){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getCpmBid());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			return businessModelParamsMapList;
		}
		@Override
		public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();

			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getOutstreamMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getOutstreamCpm());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}

	},
	CPM_WITH_COMPLETED_VIEW_RATE(2){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getCpmBid());
			Map<String,Double> cmvrMap = new HashMap<String,Double>();
			cmvrMap.put(BusinessModelParameters.CMVR.getParameterName(), (double) flightResponse.getBusinessModel().getCmvr());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(cmvrMap);
			return businessModelParamsMapList;
		}
		@Override
		public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getOutstreamMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getOutstreamCpm());
			Map<String,Double> cmvrMap = new HashMap<String,Double>();
			cmvrMap.put(BusinessModelParameters.CMVR.getParameterName(), (double) flightResponse.getBusinessModel().getCmvr());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(cmvrMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
		
	},
	CPM_WITH_CTR(3){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getCpmBid());
			Map<String,Double> ctrMap = new HashMap<String,Double>();
			ctrMap.put(BusinessModelParameters.CTR.getParameterName(), flightResponse.getBusinessModel().getCtr());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(ctrMap);
			return businessModelParamsMapList;
		}
		@Override
		public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();

			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getOutstreamMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getOutstreamCpm());
			Map<String,Double> ctrMap = new HashMap<String,Double>();
			ctrMap.put(BusinessModelParameters.CTR.getParameterName(), flightResponse.getBusinessModel().getCtr());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(ctrMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
		
	},
	CPM_WITH_CTR_AND_COMPLETED_VIEW_RATE(4){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(), flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getCpmBid());
			Map<String,Double> cmvrMap = new HashMap<String,Double>();
			cmvrMap.put(BusinessModelParameters.CMVR.getParameterName(),  flightResponse.getBusinessModel().getCmvr());
			Map<String,Double> ctrMap = new HashMap<String,Double>();
			ctrMap.put(BusinessModelParameters.CTR.getParameterName(),  flightResponse.getBusinessModel().getCtr());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(ctrMap);
			businessModelParamsMapList.add(cmvrMap);
			return businessModelParamsMapList;
		}
		@Override
		public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();

			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(), flightResponse.getBusinessModel().getOutstreamMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(), (double) flightResponse.getBusinessModel().getOutstreamCpm());
			Map<String,Double> cmvrMap = new HashMap<String,Double>();
			cmvrMap.put(BusinessModelParameters.CMVR.getParameterName(),  flightResponse.getBusinessModel().getCmvr());
			Map<String,Double> ctrMap = new HashMap<String,Double>();
			ctrMap.put(BusinessModelParameters.CTR.getParameterName(),  flightResponse.getBusinessModel().getCtr());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(ctrMap);
			businessModelParamsMapList.add(cmvrMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
		
	},
	CPCV_WITH_MARGIN_MAXIMIZATION(5){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(),flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(), flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpcvMap = new HashMap<String,Double>();
			cpcvMap.put(BusinessModelParameters.CPCV.getParameterName(), (double) flightResponse.getBusinessModel().getCpcv());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpcvMap);
			return businessModelParamsMapList;
		}
		@Override
		public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();

			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(),flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(), flightResponse.getBusinessModel().getOutstreamMargin());
			Map<String,Double> cpcvMap = new HashMap<String,Double>();
			cpcvMap.put(BusinessModelParameters.CPCV.getParameterName(), (double) flightResponse.getBusinessModel().getCpcv());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpcvMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
		
	},
	CPM_WITH_MARGIN_MAXIMIZATION(7){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(),  (double)flightResponse.getBusinessModel().getCpmBid());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			return businessModelParamsMapList;
		}

		@Override
		public List<Map<String, Double>> getBusinessModelMapForSurvey(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();

			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(),  (double)flightResponse.getBusinessModel().getCpmBid());

			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			return businessModelParamsMapList;
		}

		@Override
		public String getCanTrackClick() {
			return "false";
		}
	},
	CPM_WITH_CTR_MAXIMIZATION(8){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(),  (double)flightResponse.getBusinessModel().getCpmBid());
			Map<String,Double> ctrMap = new HashMap<String,Double>();
			ctrMap.put(BusinessModelParameters.CTR.getParameterName(),  flightResponse.getBusinessModel().getCtr());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(ctrMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
	},
	CPC_WITH_MARGIN_MAXIMIZATION(9){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpcBidMap = new HashMap<String,Double>();
			cpcBidMap.put(BusinessModelParameters.CPC.getParameterName(),  (double)flightResponse.getBusinessModel().getCpc());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpcBidMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}
	},
	CPM_WITH_ACTION_MAXIMIZATION(10){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getParameterName(),  (double)flightResponse.getBusinessModel().getCpmBid());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpmBidMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}

		@Override
		public Integer getIsOptimizedPixelFromOS(RootConversionPixelResponse conversionPixelResponse) {
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				if(conversionPixelResponse.getConversionPixels().get(i).getIsOptimized()!=null){
					return conversionPixelResponse.getConversionPixels().get(i).getId();
				}
			}
			return  0;
		}

		@Override
		public Integer getIsOptimizedPixelFromXML(ConfigData.LineItem lineItemXML) {
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				if(lineItemXML.conversionValues.get(i).is_optimizing){
					return Integer.parseInt(lineItemXML.conversionValues.get(i).value);
				}
			}
			return 0;
		}

		@Override
		public Set<Integer> getConversionPixelsFromOS(RootConversionPixelResponse conversionPixelResponse) {
			Set<Integer> conversionPixelSetOS = new HashSet<>();
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				conversionPixelSetOS.add(conversionPixelResponse.getConversionPixels().get(i).getId());
			}
			return  conversionPixelSetOS;
		}

		@Override
		public Set<Integer> getConversionPixelsFromXML(ConfigData.LineItem lineItemXML) {
			Set<Integer> conversionPixelSetXML = new HashSet<>();
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				conversionPixelSetXML.add(Integer.parseInt(lineItemXML.conversionValues.get(i).value));
			}
			return  conversionPixelSetXML;
		}
		@Override
		public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(String scenarioTestData,Integer size) {
			return DependencyManager.getInjector().getInstance(SessionVariables.class).getConversionPixelDetails(scenarioTestData);
		}
	},
	CPA_WITH_MARGIN_MAXIMIZATION(11){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> marginMap = new HashMap<String,Double>();
			marginMap.put(BusinessModelParameters.MARGIN.getParameterName(),  flightResponse.getBusinessModel().getMargin());
			Map<String,Double> cpaBidMap = new HashMap<String,Double>();
			cpaBidMap.put(BusinessModelParameters.CPA.getParameterName(), (double) flightResponse.getBusinessModel().getCpa());
			
			businessModelParamsMapList.add(budgetMap);
			businessModelParamsMapList.add(marginMap);
			businessModelParamsMapList.add(cpaBidMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}

		@Override
		public Integer getIsOptimizedPixelFromOS(RootConversionPixelResponse conversionPixelResponse) {
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				if(conversionPixelResponse.getConversionPixels().get(i).getIsOptimized()!=null){
					return conversionPixelResponse.getConversionPixels().get(i).getId();
				}
			}
			return  0;
		}

		@Override
		public Integer getIsOptimizedPixelFromXML(ConfigData.LineItem lineItemXML) {
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				if(lineItemXML.conversionValues.get(i).is_optimizing){
					return Integer.parseInt(lineItemXML.conversionValues.get(i).value);
				}
			}
			return 0;
		}

		@Override
		public Set<Integer> getConversionPixelsFromOS(RootConversionPixelResponse conversionPixelResponse) {
			Set<Integer> conversionPixelSetOS = new HashSet<>();
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				conversionPixelSetOS.add(conversionPixelResponse.getConversionPixels().get(i).getId());
			}
			return  conversionPixelSetOS;
		}

		@Override
		public Set<Integer> getConversionPixelsFromXML(ConfigData.LineItem lineItemXML) {
			Set<Integer> conversionPixelSetXML = new HashSet<>();
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				conversionPixelSetXML.add(Integer.parseInt(lineItemXML.conversionValues.get(i).value));
			}
			return  conversionPixelSetXML;
		}
		@Override
		public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(String scenarioTestData,Integer size) {
			return DependencyManager.getInjector().getInstance(SessionVariables.class).getConversionPixelDetails(scenarioTestData);
		}
	},
	CPA_LEARNING(12){
		@Override
		public List<Map<String, Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
			List<Map<String,Double>> businessModelParamsMapList = new LinkedList<Map<String,Double>>();
			
			Map<String,Double> budgetMap = new HashMap<String,Double>();
			budgetMap.put(BusinessModelParameters.BUDGET.getSecondaryParameterName(), flightResponse.getBusinessModel().getBudget().doubleValue());
			Map<String,Double> cpmBidMap = new HashMap<String,Double>();
			cpmBidMap.put(BusinessModelParameters.CPM.getSecondaryParameterName(),  (double)flightResponse.getBusinessModel().getCpmBid());
			
			businessModelParamsMapList.add(cpmBidMap);
			businessModelParamsMapList.add(budgetMap);
			return businessModelParamsMapList;
		}
		@Override
		public String getCanTrackClick() {
			return "true";
		}

		@Override
		public Integer getIsOptimizedPixelFromOS(RootConversionPixelResponse conversionPixelResponse) {
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				if(conversionPixelResponse.getConversionPixels().get(i).getIsOptimized()!=null){
					return conversionPixelResponse.getConversionPixels().get(i).getId();
				}
			}
			return  0;
		}

		@Override
		public Integer getIsOptimizedPixelFromXML(ConfigData.LineItem lineItemXML) {
			Integer isOpimizedPixelIdXML ;
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				if(lineItemXML.conversionValues.get(i).is_optimizing){
					return Integer.parseInt(lineItemXML.conversionValues.get(i).value);
				}
			}
			return 0;
		}

		@Override
		public Set<Integer> getConversionPixelsFromOS(RootConversionPixelResponse conversionPixelResponse) {
			Set<Integer> conversionPixelSetOS = new HashSet<>();
			for(int i =0 ;i<conversionPixelResponse.getConversionPixels().size();i++){
				conversionPixelSetOS.add(conversionPixelResponse.getConversionPixels().get(i).getId());
			}
			return  conversionPixelSetOS;
		}

		@Override
		public Set<Integer> getConversionPixelsFromXML(ConfigData.LineItem lineItemXML) {
			Set<Integer> conversionPixelSetXML = new HashSet<>();
			for(int i =0;i<lineItemXML.conversionValues.size();i++){
				conversionPixelSetXML.add(Integer.parseInt(lineItemXML.conversionValues.get(i).value));
			}
			return  conversionPixelSetXML;
		}

		@Override
		public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(String scenarioTestData,Integer size) {
			return DependencyManager.getInjector().getInstance(SessionVariables.class).getConversionPixelDetails(scenarioTestData);
		}
	};
	
	private final int code_;
	
	BusinessModels(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}
	
	public static BusinessModels fromCode(int code)
	{
		BusinessModels businessModels=null;
		for (BusinessModels value : BusinessModels.values())
		{
			if (value.code() == code)
			{
				businessModels=value;
			}
		}
		if(businessModels==null){
			throw new NullPointerException("Invalid Business Model Strategy Id "+ code);
		}
		return businessModels;
	}
	
	public List<Map<String,Double>> getBusinessModelMapForVideo(RootFlightsResponse flightResponse){
		return null;
	}
	public List<Map<String,Double>> getBusinessModelMapForAlchemy(RootFlightsResponse flightResponse){
		return null;
	}
	public List<Map<String,Double>> getBusinessModelMapForDisplay(RootFlightsResponse flightResponse){
		return null;
	}
	public List<Map<String,Double>> getBusinessModelMapForSurvey(RootFlightsResponse flightResponse){
		return null;
	}
	public String getCanTrackClick(){return null;}
	public Set<Integer>getConversionPixelsFromOS(RootConversionPixelResponse conversionPixelResponse)
	{
		Set<Integer> converisonPixelSet = new HashSet<>();
		converisonPixelSet.add(0);
		return converisonPixelSet;
	}
	public Integer getIsOptimizedPixelFromOS(RootConversionPixelResponse conversionPixelResponse){return 0;}

	public Set<Integer> getConversionPixelsFromXML(ConfigData.LineItem lineItemXML) {
		Set<Integer> converisonPixelSet = new HashSet<>();
		converisonPixelSet.add(0);
		return converisonPixelSet;
	}
	public Integer getIsOptimizedPixelFromXML(ConfigData.LineItem lineItemXML){return 0;}

	public List<RootConversionPixelResponse> getConversionPixelResponseListForProductType(String scenarioTestData,Integer size){
		List<RootConversionPixelResponse> rootConversionPixelResponses = new ArrayList<>();
		for(int i =0;i<size;i++){
			rootConversionPixelResponses.add(new RootConversionPixelResponse());
		}
		return rootConversionPixelResponses;
	}
}
