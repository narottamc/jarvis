/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.osapi.models.targetingresponse.*;

import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 *
 */
public interface SessionVariablesService
{
	RootAgencyResponse getAgencyDetails(String scenarioTestDataPath);
	RootBrandResponse getBrandDetails(String scenarioTestDataPath);
	RootCampaignResponse getCampaignDetails(String scenarioTestDataPath);
	List<RootAssetResponse> getVideoAssetDetails(String scenarioTestDataPath);
	List<RootSurveyResponse> getSurveyAssetDetails(String scenarioTestDataPath);
	List<RootDisplayResponse> getDisplayAssetDetails(String scenarioTestDataPath);
	List<RootPlacementResponse> getPlacementDetails(String scenarioTestDataPath);
	List<RootFlightsResponse> getFlightDetails(String scenarioTestDataPath);
	Map<Integer,RootMediaInventoryResponse> getMediaListDetailsForAPlacement(String scenarioTestDataPath);
	Map<Integer,RootMediaInventoryResponse> getSmartListDetailsForAPlacement(String scenarioTestDataPath);
	List<RootFraudsResponse>getFraudDetails(String scenarioTestDataPath);
	List<RootActivityPixelResponse>getActivityPixelDeatails(String scenarioTestDataPath);
	List<RootConversionPixelResponse>getConversionPixelDetails(String scenarioTestDataPath);
	List<RootDealsAndSeatsResponse>getDealsAndSeatsDetails(String scenarioTestDataPath);
	List<RootTechnologyResponse> getTechnologiesDetails(String scenarioTestDataPath);
	List<RootCategoryResponse> getCategoriesDetails(String scenarioTestDataPath);
	List<RootAddtionalOptionResponse> getAddtionalOptionsDetails(String scenarioTestDataPath);
	List<RootDayPartResponse> getDayPartDetails(String scenarioTestDataPath);
	List<RootGeoResponse> getGeoDetails(String scenarioTestDataPath);
	List<RootGeoListResponse>getGeoListDetails(String scenarioTestDataPath);
	
}
