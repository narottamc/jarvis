/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 */
public enum OperatinSystemTypes {
    IOS("iOS"),
    WINDOWSPHONE("WindowsPhone"),
    WINDOWS("Windows"),
    OSX("OSX"),
    UNIXLINUX("Linux"),
    ANDROID("Android"),
    OTHER("BlackBerry,Symbian,BlackBerryTablet");

    private String rawOS;

    OperatinSystemTypes(String rawOS) {
        this.rawOS = rawOS;
    }

    public String getRawOS() {
        return rawOS;
    }
}
