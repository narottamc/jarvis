/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

import com.google.common.collect.HashBasedTable;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.osapi.factory.MapBuilderfromJsonFile;
import com.osapi.models.activitypixelresponse.RootActivityPixelResponse;
import com.osapi.models.agencyresponse.RootAgencyResponse;
import com.osapi.models.assetresponse.RootAssetResponse;
import com.osapi.models.brandresponse.RootBrandResponse;
import com.osapi.models.campaignresponse.RootCampaignResponse;
import com.osapi.models.conversionpixelresponse.RootConversionPixelResponse;
import com.osapi.models.dealsandseatsresponse.RootDealsAndSeatsResponse;
import com.osapi.models.displayresponse.RootDisplayResponse;
import com.osapi.models.flightsresponse.RootFlightsResponse;
import com.osapi.models.fraudsresponse.RootFraudsResponse;
import com.osapi.models.geolistszipcoderesponse.RootGeoListResponse;
import com.osapi.models.mediainventory.response.RootMediaInventoryResponse;
import com.osapi.models.mediainventoryrequest.RootMediaListRequest;
import com.osapi.models.mediainventoryrequest.RootSmartListRequest;
import com.osapi.models.placementresponse.RootPlacementResponse;
import com.osapi.models.surveyresponse.RootSurveyResponse;
import com.osapi.models.targetingresponse.*;
import com.quarterback.mappers.ConfigData;
import com.quarterback.utils.Constants;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream.
 *
*/
@Singleton
public class SessionVariables implements SessionVariablesService
{
	private SessionVariablesService sessionVariableService;
	private String scenarioTestDataPath;
	private File xmlFile;
	private ConfigData configData;
	private HashBasedTable<Integer,String,Boolean>xmlCampaignVerifiactionStatus = HashBasedTable.create();
	
	
	@Inject
	public SessionVariables(SessionVariablesService sessionVariableService)
	{
		this.sessionVariableService= sessionVariableService;
	}

	public String getScenarioTestDataPath()
	{
		return scenarioTestDataPath;
	}

	public void setScenarioTestDataPath(String scenarioTestDataPath)
	{
		this.scenarioTestDataPath = scenarioTestDataPath;
	}

	public File getXmlFile()
	{
		return xmlFile;
	}

	public void setXmlFile(File xmlFile)
	{
		this.xmlFile = xmlFile;
	}
	
	public ConfigData getConfigData()
	{
		return configData;
	}

	public void setConfigData(ConfigData configData)
	{
		this.configData = configData;
	}
	
	public HashBasedTable<Integer, String, Boolean> getXmlCampaignVerifiactionStatus()
	{
		return xmlCampaignVerifiactionStatus;
	}

	public void setXmlCampaignVerifiactionStatus(Integer placementId,XMLCampaignSection xmlSection,Boolean validationStatus)
	{
		this.xmlCampaignVerifiactionStatus.put(placementId, xmlSection.toString().toUpperCase(), validationStatus);
	}

	public SessionVariablesService getSessionVariableService()
	{
		return sessionVariableService;
	}

	public void setSessionVariableService(SessionVariablesService sessionVariableService)
	{
		this.sessionVariableService = sessionVariableService;
	}


	public List<ConfigData.Pixel> getActivityPixelsListFromXML(){
		return this.configData.pixels;
	}
	
	public List<RootFlightsResponse>getFlightsFromOSForAPlacement(String scenarioTestDataPath,Integer placementId)
	{
		List<RootFlightsResponse> flightsPlacementList = new LinkedList<RootFlightsResponse>();
		List<RootFlightsResponse> flightResponseList = this.getFlightDetails(scenarioTestDataPath);
		for(int i=0;i<flightResponseList.size();i++)
		{
			if(flightResponseList.get(i).getPlacementId().intValue() == placementId.intValue())
			{
				flightsPlacementList.add(flightResponseList.get(i));
			}
		}
		return flightsPlacementList;
	}
	
	public List<RootAssetResponse>getVideoAssetsForAFlight(String scenarioTestDataPath,RootFlightsResponse flightResponse)
	{
		List<RootAssetResponse> assetResponseList = new LinkedList<RootAssetResponse>();
		for(int i=0;i<flightResponse.getFlightAssets().size();i++) {
			for(int j=0;j<this.getVideoAssetDetails(scenarioTestDataPath).size();j++) {
				if(flightResponse.getFlightAssets().get(i).getAssetId().intValue()==this.getVideoAssetDetails(scenarioTestDataPath).get(j).getId().intValue())
				{
					assetResponseList.add(this.getVideoAssetDetails(scenarioTestDataPath).get(j));
					break;
				}
			}
		}
		return assetResponseList;
	}
	
	public List<RootSurveyResponse>getSurveyAssetsForAFlight(String scenarioTestDataPath,RootFlightsResponse flightResponse)
	{
		List<RootSurveyResponse> surveyAssetResponseList = new LinkedList<RootSurveyResponse>();
		for(int i=0;i<flightResponse.getFlightAssets().size();i++) {
			for(int j=0;j<this.getSurveyAssetDetails(scenarioTestDataPath).size();j++) {
				if(flightResponse.getFlightAssets().get(i).getAssetId().intValue()==this.getSurveyAssetDetails(scenarioTestDataPath).get(j).getId().intValue())
				{
					surveyAssetResponseList.add(this.getSurveyAssetDetails(scenarioTestDataPath).get(j));
					break;
				}
			}
		}
		return surveyAssetResponseList;
	}
	
	public List<RootDisplayResponse>getDisplayAssetsForAFlight(String scenarioTestDataPath,RootFlightsResponse flightResponse)
	{
		List<RootDisplayResponse> displayAssetResponseList = new LinkedList<RootDisplayResponse>();
		for(int i=0;i<flightResponse.getFlightAssets().size();i++) {
			for(int j=0;j<this.getDisplayAssetDetails(scenarioTestDataPath).size();j++) {
				if(flightResponse.getFlightAssets().get(i).getAssetId().intValue()==this.getDisplayAssetDetails(scenarioTestDataPath).get(j).getId().intValue())
				{
					displayAssetResponseList.add(this.getDisplayAssetDetails(scenarioTestDataPath).get(j));
					break;
				}
			}
		}
		return displayAssetResponseList;
	}
	
	public RootFraudsResponse getFraudDetailsForPlacement(String scenarioTestDataPath,Integer placementNumber)
	{
		return this.getFraudDetails(scenarioTestDataPath).get(placementNumber);
	}
	
	public List<Integer>getPlacementIdsFromOS(List<RootPlacementResponse> rootPlacementResponseList){
		List<Integer> placementList = new LinkedList<>();
		for(int i =0;i<rootPlacementResponseList.size();i++) 
		{
			placementList.add(rootPlacementResponseList.get(i).getId());
		}
		return placementList;
	}
	
	public HashBasedTable<Integer, Integer, ConfigData.Campaign> getCampaignsAdvertiserTableFromXML()
	{
		HashBasedTable<Integer,Integer,ConfigData.Campaign>campaignsByAdvertiserPlacementId = HashBasedTable.create();
		List<ConfigData.Advertiser> advertisers = this.getConfigData().advertisers;
		for (ConfigData.Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				campaignsByAdvertiserPlacementId.put(advertiser.id, campaign.id, campaign);
			}
		}
		return campaignsByAdvertiserPlacementId;
	} 
	
	public ConfigData.Campaign getXmlCampaign(Integer placementId)
	{
		ConfigData.Campaign xmlCampaign = null;
		HashBasedTable<Integer,Integer,ConfigData.Campaign>campaignsByAdvertiserPlacementId = this.getCampaignsAdvertiserTableFromXML();
		Map<Integer,ConfigData.Campaign>xmlCampaignAdvertiserMap = campaignsByAdvertiserPlacementId.column(placementId);
		for(Integer key : xmlCampaignAdvertiserMap.keySet())
		{
			xmlCampaign= xmlCampaignAdvertiserMap.get(key);
		}
		return xmlCampaign;
	}
	
	public ConfigData.LineItem getLineItemFromXML(ConfigData.Campaign xmlCampaign,Integer flightAdId)
	{
		ConfigData.LineItem lineItem = null;
		for(int i=0;i<xmlCampaign.line_items.size();i++)
		{
			if(xmlCampaign.line_items.get(i).id==flightAdId.intValue()) {
				lineItem = xmlCampaign.line_items.get(i);
			}
		}
		return lineItem;
	}
	
	public ConfigData.Creative getSingleCreativeFromXML(ConfigData.Campaign xmlCampaign,Integer flightAdId)
	{
		ConfigData.Creative creative = null;
		for(int i=0;i<xmlCampaign.line_items.size();i++)
		{
			if(Integer.parseInt(xmlCampaign.creatives.get(i).id)==flightAdId.intValue()) {
				creative = xmlCampaign.creatives.get(i);
			}
		}
		return creative;
	}

	public boolean shouldAdvertiserBePresentInXML(String scenarioTestDataPath){
		List<RootFlightsResponse> flightsResponseList = this.getFlightDetails(scenarioTestDataPath);
		for(int i = 0;i<flightsResponseList.size();i++){
			if(flightsResponseList.get(i).getActive()){
				return true;
			}
		}
		return false;
	}

	public boolean shouldPlacementBePresentinXML(String scenarioTestDataPath,Integer placementId){
		List<RootFlightsResponse> flightsResponseList = this.getFlightsFromOSForAPlacement(scenarioTestDataPath,placementId);
		for(int i =0;i<flightsResponseList.size();i++){
			if(flightsResponseList.get(i).getActive()){
				return true;
			}
		}
		return false;
	}


	@Override
	public RootAgencyResponse getAgencyDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootAgencyResponse.class, Constants.AGENCY_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}


	@Override
	public RootBrandResponse getBrandDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootBrandResponse.class, Constants.BRAND_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}

	@Override
	public RootCampaignResponse getCampaignDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootCampaignResponse.class, Constants.CAMPAIGN_RESPONSE_DATA, scenarioTestDataPath, false).get();
	}

	@Override
	public List<RootAssetResponse> getVideoAssetDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootAssetResponse.class, Constants.ASSET_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}
	
	@Override
	public List<RootSurveyResponse> getSurveyAssetDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootSurveyResponse.class, Constants.SURVEY_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootDisplayResponse> getDisplayAssetDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootDisplayResponse.class, Constants.DISPLAY_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootPlacementResponse> getPlacementDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootPlacementResponse.class, Constants.PLACEMENT_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootFlightsResponse> getFlightDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootFlightsResponse.class, Constants.FLIGHTS_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	@Override
	public Map<Integer, RootMediaInventoryResponse> getMediaListDetailsForAPlacement(String scenarioTestDataPath) {
		Map<Integer,RootMediaInventoryResponse> mediaListMap = new HashMap<>();
		List<RootMediaListRequest> mediaListRequestsList = MapBuilderfromJsonFile.build(RootMediaListRequest.class, Constants.MEDIALISTS_REQUEST_DATA,
				scenarioTestDataPath, true).getAsCollection();
		List<RootMediaInventoryResponse> mediaListResponse =
				MapBuilderfromJsonFile.build(RootMediaInventoryResponse.class, Constants.MEDIALIST_RESPONSE_DATA,
						scenarioTestDataPath, true).getAsCollection();
		for(int i =0;i<mediaListRequestsList.size();i++){
			mediaListMap.put(mediaListRequestsList.get(i).getPlacementId(),mediaListResponse.get(i));
		}
		return mediaListMap;
	}

	@Override
	public Map<Integer, RootMediaInventoryResponse> getSmartListDetailsForAPlacement(String scenarioTestDataPath) {
		Map<Integer,RootMediaInventoryResponse> smartListMap = new HashMap<>();
		List<RootSmartListRequest> smartListRequestsList = MapBuilderfromJsonFile.build(RootSmartListRequest.class, Constants.SMARTLISTS_REQUEST_DATA,
				scenarioTestDataPath, true).getAsCollection();
		List<RootMediaInventoryResponse> smartListResponse =
				MapBuilderfromJsonFile.build(RootMediaInventoryResponse.class, Constants.SMARTLIST_RESPONSE_DATA,
						scenarioTestDataPath, true).getAsCollection();
		for(int i =0;i<smartListRequestsList.size();i++){
			smartListMap.put(smartListRequestsList.get(i).getPlacementId(),smartListResponse.get(i));
		}
		return smartListMap;
	}

	@Override
	public List<RootTechnologyResponse> getTechnologiesDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootTechnologyResponse.class, Constants.TECHNOLOGIES_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootCategoryResponse> getCategoriesDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootCategoryResponse.class, Constants.CATEGORIES_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootAddtionalOptionResponse> getAddtionalOptionsDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootAddtionalOptionResponse.class, Constants.ADDTIONALOPTION_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootDayPartResponse> getDayPartDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootDayPartResponse.class, Constants.DAYPARTS_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	
	@Override
	public List<RootGeoResponse> getGeoDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootGeoResponse.class, Constants.GEOS_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	@Override
	public List<RootFraudsResponse> getFraudDetails(String scenarioTestDataPath)
	{
		return MapBuilderfromJsonFile.build(RootFraudsResponse.class, Constants.FRAUDS_RESPONSE_DATA, scenarioTestDataPath, true).getAsCollection();
	}

	@Override
	public List<RootDealsAndSeatsResponse> getDealsAndSeatsDetails(String scenarioTestDataPath) {
		return MapBuilderfromJsonFile.build(RootDealsAndSeatsResponse.class,Constants.DEALSANDSEATS_RESPONSE_DATA,scenarioTestDataPath,true).getAsCollection();
	}

	@Override
	public List<RootActivityPixelResponse> getActivityPixelDeatails(String scenarioTestDataPath) {
		return MapBuilderfromJsonFile.build(RootActivityPixelResponse.class,Constants.PIXEL_RESPONSE_DATA,scenarioTestDataPath,true).getAsCollection();
	}

	@Override
	public List<RootConversionPixelResponse> getConversionPixelDetails(String scenarioTestDataPath) {
		return MapBuilderfromJsonFile.build(RootConversionPixelResponse.class,Constants.CONVERSIONPIXELS_RESPONSE_DATA,scenarioTestDataPath,true).getAsCollection();
	}

	@Override
	public List<RootGeoListResponse> getGeoListDetails(String scenarioTestDataPath) {
		return MapBuilderfromJsonFile.build(RootGeoListResponse.class,Constants.ZIPCODES_RESPONSE_DATA,scenarioTestDataPath,true).getAsCollection();
	}
}
