/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 *
 */
public enum IponWebMacros
{
	
	AAID("${AAID}"),
	API_FRAMEWORK("${API}"),
	APP_BUNDLE("${APP_BUNDLE}"),
	BID_CONTEXT("${BID_CONTEXT:B64}"),
	BID_CPM("${BID_CPM}"),
	BID_REQ_ID("${BID_REQ_ID}"),
	BROWSER("${BROWSER}"),
	BROWSER_VERSION("${BROWSER_VERSION}"),
	CAT("${IAB_CAT}"),
	COOKIE_ID("${UUID}"),
	DEVICE_IP("${DEVICE_IP}"),
	DEVICE_MAKER("${DEVICE_MAKER}"),
	DEVICE_MODEL("${DEVICE_MODEL}"),
	DEVICE_TYPE("${DEVICE_TYPE}"),
	DISPLAY_CLICK_URL("${DISPLAY_CLICK_URL_ENC}"),
	EVENT_URL("${EVENT_URL_ENC}"),
	GEO_COUNTRY("${GEO_COUNTRY}"),
	GEO_DMA("${GEO_DMA}"),
	GEO_REGION("${GEO_REGION}"),
	GEO_ZIP("${GEO_ZIP}"),
	HEIGHT("${HEIGHT}"),
	IDFA("${IDFA}"),
	INV_TYPE("${INVENTORY_TYPE}"),
	LANGUAGE_CODE("${LANG}"),
	OS_TYPE("${OS}"),
	OS_VERSION("${OS_VERSION}"),
	PAGE_DOMAIN("${PAGE_DOMAIN}"),
	PROTOCOL("${PROTOCOL}"),
	PUBLISHER_ID("${PUBLISHER_ID}"),
	REPORTING_IPON_IMPRESSION_ID("${BID_ID}"),
	SEG_IPON("${IPONWEB_SEGMENTS}"),
	SEG_KRUX("${KRUX_SEGMENTS}"),
	SEG_LOTA("${LOTAME_SEGMENTS}"),
	SEG_LRMP("${LIVERAMP_SEGMENTS}"),
	SELLER_NETWORK("${SELLER_NETWORK}"),
	SITE_DOMAIN("${SITE_DOMAIN}"),
	SSP("${SELLER_NETWORK}"),
	USER_AGENT("${USER_AGENT}"),
	VIDEO_CLICK_URL("${VIDEO_CLICK_URL_ENC}"),
	VIDEO_IMP("${VIDEO_IMP_ENC}"),
	WIDTH("${WIDTH}"),
	WINSEG("${WINSEG}"),
	TRAFFIC_GROUP("${TRAFFIC_GROUP}");
	
	private String macroValue;

	IponWebMacros(String macroValue)
	{
		this.macroValue = macroValue;
	}

	public String getMacroValue()
	{
		return macroValue;
	}
}
