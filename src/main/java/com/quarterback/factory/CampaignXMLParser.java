/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

import com.quarterback.mappers.ConfigData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;


/**
 * @author Clearstream.
 *
 */
public class CampaignXMLParser
{
	private ConfigData configdata;

	private CampaignXMLParser(File xmlFile) 
	{
		JAXBContext jaxbContext;
		try
		{
			jaxbContext = JAXBContext.newInstance(ConfigData.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			configdata = (ConfigData) jaxbUnmarshaller.unmarshal(xmlFile);
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}	
	}
	private static class Holder
	{
		private static File xmlFile;
		private static CampaignXMLParser INSTANCE;

		private static CampaignXMLParser getInstance() 
		{
			INSTANCE = new CampaignXMLParser(xmlFile);
			return INSTANCE;
		}
	}

	public static CampaignXMLParser getInstance(File xmlFile) 
	{
		Holder.xmlFile = xmlFile;
		return Holder.getInstance();
	}

	public ConfigData getCamapignParser()
	{
		return configdata;
	}

	public void setCamapignParser(ConfigData configData)
	{
		this.configdata = configData;
	}

}
