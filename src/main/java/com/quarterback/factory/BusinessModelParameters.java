/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 *
 */
public enum BusinessModelParameters
{
	BUDGET("aBudget","aLearningBudget"),
	MARGIN("tMargin",""),
	CPM("aCPM","aLearningCPM"),
	CTR("cCTR",""),
	CMVR("cCVR",""),
	CPC("aCPC",""),
	CPA("aCPA",""),
	CPCV("aCPCV","");
	
	private String parameter;
	private String secondaryParameter;
	
	BusinessModelParameters(String parameter,String secondaryParameter)
	{
		this.parameter = parameter;
		this.secondaryParameter = secondaryParameter;
	}

	public String getParameterName()
	{
		return parameter;
	}
	
	public String getSecondaryParameterName()
	{
		return secondaryParameter;
	}
}
