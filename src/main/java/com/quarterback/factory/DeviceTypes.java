/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.factory;

/**
 * @author Clearstream.
 */
public enum DeviceTypes {
    PC("PC"),
    PHONE("Phone"),
    TABLET("Tablet"),
    MEDIACENTER("MediaCenter");

    private String rawDeviceType;

    DeviceTypes(String rawDeviceType) {
        this.rawDeviceType = rawDeviceType;
    }

    public String getRawDeviceType() {
        return rawDeviceType;
    }
}
