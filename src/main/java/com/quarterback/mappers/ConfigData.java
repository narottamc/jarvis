/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.mappers;

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.xml.XmlEscapers;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.*;

/**
 * @author Clearstream
 */
@XmlRootElement(name = "config")
public class ConfigData
{
	@XmlElementWrapper(name = "vectorone")
	@XmlElement(name = "vectorone_share", required = true)
	public List<VectoroneShare> vectorone = new ArrayList<>();
	@XmlElement(name = "vectorone_share", required = true)
	public List<vectoroneControlShare> vectoroneControlShare = new ArrayList<>();

	@XmlElementWrapper(name = "advertisers", required = true)
	@XmlElement(name = "advertiser")
	public final List<Advertiser> advertisers = new ArrayList<>();

	@XmlElementWrapper(name = "pixels", required = true)
	@XmlElement(name = "pixel")
	public final List<Pixel> pixels = new ArrayList<>();

	@XmlElementWrapper(name = "seller_side")
	@XmlElements(
	{ @XmlElement(name = "ssp", type = SSP_1.class), @XmlElement(name = "ssp", type = SSP_2.class) })
	public final List<SSP> ssps = new ArrayList<>(); // optional

	@XmlElementWrapper(name = "domain_lists")
	@XmlElement(name = "domain_list")
	public List<DomainList> domain_lists; // optional (NOT BEING USED)

	@XmlElement
	public SiteDomains site_domains; // optional (NOT BEING USED)

	@XmlElement
	public PageDomains page_domains; // optional (NOT BEING USED)

	@XmlElement(name = "ids_for_domain_file")
	public final DomainListFile domain_list_file = new DomainListFile();

	@XmlElementWrapper(name = "deals")
	@XmlElement(name = "deal")
	public List<Deal> deals; // optional

	@XmlElementWrapper(name = "autoroute_dsps")
	@XmlElement(name = "dsp")
	public DSP[] dsps = new DSP[]
	{ new DSP(79, "DataXu"), new DSP(16, "DBM"), new DSP(93, "TradeDesk"), new DSP(80, "MediaMath"), };

	@XmlElementWrapper(name = "ip_black_list")
	@XmlElement(name = "value")
	public List<BlacklistIP> blocked_ips;

	@XmlEnum
	public enum IncludeOrExclude
	{
		@XmlEnumValue("include")
		INCLUDE,

		@XmlEnumValue("exclude")
		EXCLUDE,;
	}

	@XmlEnum
	public enum Api
	{
		@XmlEnumValue("1")
		API_1,

		@XmlEnumValue("2")
		API_2,

		@XmlEnumValue("3")
		API_3,

		@XmlEnumValue("5")
		API_5,;
	}

	@XmlEnum
	public enum DayOfWeek
	{
		@XmlEnumValue("Other")
		OTHER(-1),

		@XmlEnumValue("Mon")
		MON(1),

		@XmlEnumValue("Tue")
		TUE(2),

		@XmlEnumValue("Wed")
		WED(3),

		@XmlEnumValue("Thu")
		THU(4),

		@XmlEnumValue("Fri")
		FRI(5),

		@XmlEnumValue("Sat")
		SAT(6),

		@XmlEnumValue("Sun")
		SUN(0),;

		DayOfWeek(int id)
		{
			id_ = id;
		}

		public int id()
		{
			return id_;
		}

		public static DayOfWeek fromId(int id)
		{
			for (DayOfWeek value : DayOfWeek.values())
			{
				if (value.id() == id)
				{
					return value;
				}
			}
			return OTHER;
		}

		private final int id_;
	}

	@XmlEnum
	public enum InventoryType
	{
		@XmlEnumValue("web")
		WEB,

		@XmlEnumValue("application")
		APPLICATION,;

		public static InventoryType fromAlias(String alias)
		{
			if (alias.equalsIgnoreCase(WEB.name()))
			{
				return WEB;
			}
			return APPLICATION;
		}
	}

	@XmlEnum
	public enum Operator
	{
		@XmlEnumValue("AND")
		AND,

		@XmlEnumValue("OR")
		OR,;
	}

	public static class AcceptOrReject
	{
		@XmlAttribute
		public final boolean value;

		public AcceptOrReject(boolean value)
		{
			this.value = value;
		}
	}

	public static class Advertiser
	{
		@XmlAttribute
		public int id;

		@XmlAttribute
		public String name;

		@XmlElementWrapper(name = "campaigns")
		@XmlElement(name = "campaign")
		public final List<Campaign> campaigns = new ArrayList<>();
	}

	public static class Campaign
	{
		public transient int advertiser_id;

		public final transient Map<String, SSP> seller_side_ssps = new HashMap<>();
		public final transient Map<String, SellerNetwork> seller_side_seller_networks = new HashMap<>();
		public final transient Map<String, Publisher> seller_side_publishers = new HashMap<>();

		public final transient Map<Integer, Category> site_categories = new HashMap<>();
		public final transient Map<Integer, Category> advertiser_categories = new HashMap<>();
		public final transient Map<String, ConfigData.Country> countries = new HashMap<>();

		public final transient Set<Integer> line_item_ids = new HashSet<>();
		public final transient Set<Integer> creative_ids = new HashSet<>();

		public final transient List<HTMLCreative> html_creatives = new ArrayList<>();
		public final transient List<DynamicCreative> dynamic_creatives = new ArrayList<>();
		public final transient List<VideoCreative> video_creatives = new ArrayList<>();

		public transient boolean passes_on_unknown_data;

		public transient ListType smart_list_type;

		public final transient Set<String> ssps = new HashSet<>();

		public boolean has_bidswitch()
		{
			return ssps.contains("bidswitch");
		}

		public boolean has_liverail()
		{
			return ssps.contains("liverail");
		}

		@XmlAttribute(name = "id", required = true)
		public int id;

		@XmlAttribute(name = "name", required = true)
		public String name;

		@XmlAttribute(name = "group_id", required = true)
		public int group_id;

		@XmlAttribute(name = "group_name")
		public String group_name; // optional

		@XmlAttribute(name = "insertion_order")
		public String insertion_order; // optional

		@XmlAttribute(name = "double_verify_enabled")
		public Boolean double_verify_enabled; // optional

		@XmlAttribute(name = "salesforce_id")
		public String salesforce_id; // optional

		@XmlAttribute(name = "smartlist")
		public Boolean smartlist; // optional

		@XmlElement(name = "ssp")
		public final List<SSO_SSP> seller_side_options = new ArrayList<>();

		@XmlElement(name = "targeting")
		public Targeting targeting = new Targeting();

		@XmlElementWrapper(name = "line_items", required = true)
		@XmlElement(name = "line_item")
		public final List<LineItem> line_items = new ArrayList<>();

		@XmlElementWrapper(name = "creatives", required = true)
		@XmlElements(
		{ @XmlElement(name = "creative", type = VideoCreative.class),
				@XmlElement(name = "creative", type = DynamicCreative.class),
				@XmlElement(name = "creative", type = HTMLCreative.class) })
		public final List<Creative> creatives = new ArrayList<>();
	}

	public static class LineItem
	{
		public transient int campaign_id;

		@XmlAttribute
		public int id;

		@XmlAttribute
		public String name;

		@XmlAttribute(name = "bid_share")
		public Double bid_adjustment;

		@XmlElement(name = "start_date")
		public String start_date;

		@XmlElement(name = "end_date")
		public String end_date;

		@XmlElementWrapper(name = "creative")
		@XmlElement(name = "value")
		public final Set<Integer> creative_ids = new LinkedHashSet<>();

		@XmlElement(name = "strategy")
		public final Strategy strategy = new Strategy();

		@XmlElementWrapper(name = "tracking_conversions")
		@XmlElement(name = "value")
		public final List<ConversionValues> conversionValues = new ArrayList<>();
	}

	public static class Creative
	{
		@XmlAttribute(name = "id")
		public String id;

		@XmlAttribute(name = "name")
		public String name;

		@XmlAttribute(name = "replaceable")
		public Boolean replaceable;

		@XmlElement(name = "duration", required = true)
		public Integer duration; // optional

		@XmlElement(name = "landing_page_domain", required = true)
		public String landing_page_domain;

		@XmlElement(name = "linear", required = true)
		public boolean is_linear;

		@XmlElement(name = "skippable", required = true)
		public boolean is_skippable;

		@XmlElement(name = "vpaid", required = true)
		public boolean is_vpaid;

		@XmlElement(name = "vast_url")
		public String vast_url;

		@XmlElement(name = "vast_url_secure")
		public String vast_url_secure;

		@XmlElement(name = "bidswitch_macros")
		public String bidswitch_macros;

		@XmlElement(name = "size")
		public String size;

		@XmlElement(name = "type")
		public String type;

		@XmlElement(name = "can_track_clicks")
		public String can_track_clicks;

		@XmlElementWrapper(name = "mime_types")
		@XmlElement(name = "value")
		public final List<String> mime_type_values = new ArrayList<>();

		@XmlElement(name = "html_content")
		public String html_content;

		@XmlElement
		public Creative_SSP ssp;

		/*
		 * @XmlElementWrapper(name = "ssp")
		 * 
		 * @XmlElement(name = "bidswitch")
		 * 
		 * @XmlEnumValue("attribute") public String bidswitch;
		 */

	}

	public static class HTMLCreative extends Creative
	{
		public transient int campaign_id;
		public transient int line_item_id;
		public transient boolean is_valid;

		@XmlAttribute(required = true)
		public int id;

		@XmlAttribute(required = true)
		public String name;

		@XmlElement(name = "size", required = true)
		public String size;

		@XmlElement(name = "type", required = true)
		public String type;

		@XmlElement(name = "can_track_clicks", required = true)
		public boolean can_track_clicks = true;

		@XmlElement
		public Creative_SSP ssp; // optional

		@XmlElement(name = "html_content", required = true)
		@XmlJavaTypeAdapter(value = CdataAdapter.class)
		public String html_content; // enclose ad_tag with CDATA block

		@XmlElement
		public Creative_Companion companion; // optional (NOT BEING USED)
	}

	public static class DynamicCreative extends Creative
	{
		public transient int campaign_id;
		public transient int line_item_id;
		public transient boolean is_valid;

		public transient Multimap<String, String> tracking_urls = HashMultimap.create();

		@XmlAttribute(required = true)
		public int id;

		@XmlAttribute(required = true)
		public String name;

		@XmlElement
		public String size; // optional

		@XmlElement
		public Creative_SSP ssp; // optional (NOT BEING USED)

		@XmlElementWrapper(name = "mime_types", required = true)
		@XmlElement(name = "value")
		public final Set<String> mime_types = new LinkedHashSet<>();

		@XmlElementWrapper(name = "impression_tracking_urls")
		@XmlElement(name = "value")
		public List<String> impression_tracking_urls; // optional

		@XmlElementWrapper(name = "click_tracking_urls")
		@XmlElement(name = "value")
		public List<String> click_tracking_urls; // optional

		@XmlElementWrapper(name = "event_tracking_urls")
		@XmlElement(name = "event")
		public List<Event> event_tracking_urls; // optional

		@XmlElement(name = "linear", required = true)
		public boolean is_linear;

		@XmlElement(name = "skippable", required = true)
		public boolean is_skippable;

		@XmlElement(name = "vpaid", required = true)
		public boolean is_vpaid;

		@XmlElement(name = "api")
		public Api api; // optional (NOT BEING USED)

		@XmlElement(name = "vast_url", required = true)
		public String vast_url;

		@XmlElement(name = "vast_url_secure")
		public String vast_url_secure;

		@XmlElement(name = "bidswitch_macros")
		public Macros bidswitch_macros; // optional

		@XmlElement(name = "liverail_macros")
		public Macros liverail_macros; // optional

		@XmlElement(name = "landing_page_domain", required = true)
		public String landing_page_domain;

		@XmlElement
		public Creative_Companion companion; // optional (NOT BEING USED)
	}

	public static class VideoCreative extends Creative
	{
		public transient int campaign_id;
		public transient int line_item_id;
		public transient boolean is_valid;

		public transient Multimap<String, String> tracking_urls = HashMultimap.create();

		@XmlAttribute(required = true)
		public int id;

		@XmlAttribute(required = true)
		public String name;

		@XmlElement
		public String size; // optional

		@XmlElement
		public Creative_SSP ssp; // optional (NOT BEING USED)

		@XmlElementWrapper(name = "mime_types", required = true)
		@XmlElement(name = "value")
		public final Set<String> mime_types = new LinkedHashSet<>();

		@XmlElementWrapper(name = "impression_tracking_urls")
		@XmlElement(name = "value")
		public List<String> impression_tracking_urls; // optional

		@XmlElementWrapper(name = "click_tracking_urls")
		@XmlElement(name = "value")
		public List<String> click_tracking_urls; // optional

		@XmlElementWrapper(name = "event_tracking_urls")
		@XmlElement(name = "event")
		public List<Event> event_tracking_urls; // optional

		@XmlElement(name = "linear", required = true)
		public boolean is_linear;

		@XmlElement(name = "skippable", required = true)
		public boolean is_skippable;

		@XmlElement(name = "vpaid", required = true)
		public boolean is_vpaid;

		@XmlElement(name = "api")
		public Api api; // optional (NOT BEING USED)

		@XmlElement(name = "vast_url", required = true)
		public String vast_url;

		@XmlElement(name = "vast_url_secure")
		public String vast_url_secure;

		@XmlElement(name = "bidswitch_macros")
		public Macros bidswitch_macros; // optional

		@XmlElement(name = "liverail_macros")
		public Macros liverail_macros; // optional

		@XmlElement(name = "landing_page_domain", required = true)
		public String landing_page_domain;

		@XmlElement
		public Creative_Companion companion; // optional (NOT BEING USED)
	}

	public static class Targeting
	{
		/*
		 * @XmlElementWrapper(name = "day_of_week")
		 * 
		 * @XmlElements({
		 * 
		 * @XmlElement(name = "day", type = Day_1.class),
		 * 
		 * @XmlElement(name = "day", type = Day_2.class) }) public List<Day>
		 * days; // optional
		 */

		@XmlElementWrapper(name = "day_of_week")
		@XmlElement(name = "day")
		public List<Day> days;

		@XmlElement(name = "frequency_cap")
		public FrequencyCap frequency_cap; // optional

		@XmlElement(name = "language")
		public Language language; // optional

		@XmlElement(name = "geo")
		public Geo geo; // optional

		@XmlElement(name = "zip")
		public Zip zip; // optional

		@XmlElement(name = "dma")
		public Dma dma; // optional (NOT BEING USED)

		@XmlElement(name = "useragent")
		public UserAgent user_agent; // optional

		@XmlElementWrapper(name = "advertiser_categories")
		@XmlElement(name = "cat")
		public List<Category> advertiser_categories; // optional

		@XmlElement(name = "site_categories")
		public SiteCategories site_categories; // optional

		@XmlElement(name = "page_url")
		public PageUrl page_url; // optional (NOT BEING USED)

		@XmlElement(name = "inventory_type")
		public InventoryType inventory_type; // optional

		/*
		 * @XmlElementWrapper(name = "seller_side")
		 * 
		 * @XmlElements({
		 * 
		 * @XmlElement(name = "ssp", type = SSP_1.class),
		 * 
		 * @XmlElement(name = "ssp", type = SSP_2.class) }) public List<SSP>
		 * ssps; // optional
		 */

		@XmlElementWrapper(name = "seller_side") // Added a new block
		@XmlElement(name = "ssp")
		public List<SSP> ssps;

		@XmlElementWrapper(name = "ssp_seller_network")
		@XmlElement(name = "ssp")
		public List<SSP> ssp_seller_network;

		/*
		 * @XmlElementWrapper(name = "ssp_seller_network")
		 * 
		 * @XmlElements({
		 * 
		 * @XmlElement(name = "ssp", type = SSP_Reference_1.class),
		 * 
		 * @XmlElement(name = "ssp", type = SSP_Reference_2.class) }) public
		 * List<SSP_Reference> ssp_references; // optional
		 */

		@XmlElement(name = "segment")
		public Segment segment; // optional

		@XmlElement(name = "lotame_segment")
		public Segment lotame_segment; // optional

		@XmlElement(name = "liveramp_segment")
		public Segment liveramp_segment; // optional

		@XmlElement(name = "krux_segment")
		public Segment krux_segment; // optional

		@XmlElement(name = "adobe_segment")
		public Segment adobe_segment; // optional

		@XmlElement(name = "domain")
		public DomainLists domain_lists; // optional

		@XmlElementWrapper(name = "player_sizes")
		@XmlElement(name = "size")
		public List<PlayerSize> player_sizes; // optional

		@XmlElementWrapper(name = "visibility")
		@XmlElement(name = "value")
		public List<Visibility> visibilities; // optional

		@XmlElementWrapper(name = "linked_campaigns")
		@XmlElement(name = "value")
		public List<String> linked_campaigns; // optional (NOT BEING USED)
	}

	public static class Day
	{
		// public transient int campaign_id;

		@XmlAttribute(name = "id")
		public String id;

		@XmlElement(name = "hour")
		public List<Hour> hours = new ArrayList<>();

	}

	public static class Day_1 extends Day
	{
		@XmlAttribute
		public DayOfWeek id;

		@XmlElement(name = "hour")
		public final List<Hour> hours = new ArrayList<>();
	}

	public static class Day_2 extends Day
	{
		public final static Day_2 OTHER_FALSE = new Day_2(DayOfWeek.OTHER, false);

		@XmlAttribute
		public DayOfWeek id;

		@XmlAttribute
		public boolean value;

		public Day_2(DayOfWeek id, boolean value)
		{
			this.id = id;
			this.value = value;
		}

		public Day_2()
		{

		}
	}

	public static class Hour
	{
		public final static Hour OTHER_FALSE = new Hour("Other", false);

		@XmlAttribute(name = "id")
		public String hour_id;

		@XmlAttribute(name = "hour_value")
		public boolean hour_value;

		@XmlAttribute(required = true)
		public String id;

		@XmlAttribute(required = true)
		public boolean value;

		public Hour(String id, boolean value)
		{
			this.id = id;
			this.value = value;
		}

		public Hour()
		{
		}
	}

	public static class FrequencyCap
	{
		public transient int campaign_id;

		@XmlAttribute(name = "count")
		public int count;

		@XmlAttribute(name = "hours")
		public int hours;

		public void override(FrequencyCap another)
		{
			campaign_id = another.campaign_id;

			count = another.count;
			hours = another.hours;
		}
	}

	public static class Category
	{
		public transient int campaign_id;
		public transient int cat_id; // internal category id
		public transient Integer parent_cat_id; // internal parent category id

		@XmlAttribute(name = "id")
		public Integer code;

		@XmlElementWrapper(name = "subcats")
		@XmlElement(name = "cat")
		public List<Category> sub_categories; // optional

		public void add(Category sub_category)
		{
			if (sub_categories == null)
			{
				sub_categories = new ArrayList<>();
			}
			sub_categories.add(sub_category);
		}
	}

	public static class SiteCategories
	{
		@XmlAttribute(name = "passes_on_unknown_data")
		public Boolean passes_on_unknown_data; // optional

		@XmlAttribute
		public IncludeOrExclude type = IncludeOrExclude.INCLUDE; // default to
																	// 'include'

		@XmlElement(name = "cat")
		public final List<Category> categories = new ArrayList<>();

		public void add(Category category)
		{
			categories.add(category);
		}
	}

	public static class SiteDomains
	{
		@XmlAttribute
		public final IncludeOrExclude type = IncludeOrExclude.EXCLUDE; // for
																		// exclusion
																		// only

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();
	}

	public static class PageDomains
	{
		@XmlAttribute
		public final IncludeOrExclude type = IncludeOrExclude.EXCLUDE; // for
																		// exclusion
																		// only

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();
	}

	public static class SiteDomain
	{
		public transient int type_code;

		@XmlAttribute
		public IncludeOrExclude type;

		@XmlElementWrapper(name = "domain_lists")
		@XmlElement(name = "value")
		public final List<String> domain_lists = new ArrayList<>();
	}

	public static class PageDomain
	{
		public transient int type_code;

		@XmlAttribute
		public IncludeOrExclude type;

		@XmlElementWrapper(name = "domain_lists")
		@XmlElement(name = "value")
		public final List<String> domain_lists = new ArrayList<>();
	}

	public static class DomainLists
	{
		@XmlAttribute(name = "operator")
		public Operator operator;

		@XmlElement(name = "page_domain")
		public final PageDomain page_domain = new PageDomain();

		@XmlElement(name = "site_domain")
		public final SiteDomain site_domain = new SiteDomain();

	}

	public static class DomainListRecord
	{
		public transient int campaign_id;
		public transient int domain_list_id;
		public transient int domain_list_type;
	}

	public static class MimeType
	{
		public transient int line_item_id;
		public transient int creative_id;

		@XmlValue
		public String value;
	}

	public static class Macros
	{
		@XmlValue
		public String getText()
		{
			return AMP + JOINER.join(pairs);
		}

		public void add(String name, String value)
		{
			pairs.put(name, value);
		}

		public void override(Macros another)
		{
			pairs.clear();
			pairs.putAll(another.pairs);
		}

		private final Map<String, String> pairs = new LinkedHashMap<>();

		private final static String AMP = "&";
		private final static Joiner.MapJoiner JOINER = Joiner.on("&").withKeyValueSeparator("=");
	}

	public static class Segment
	{
		public transient int campaign_id;

		private transient Set<String> values = new HashSet<>();
		private transient Set<String> anti_values = new HashSet<>();

		// @XmlValue
		/*
		 * public String getText() { String targeting_segments =
		 * OR_JOINER.join(values); if (targeting_segments.isEmpty()) {
		 * targeting_segments = null; }
		 * 
		 * Iterable<String> antis = Iterables.transform(anti_values, value ->
		 * "NOT " + value);
		 * 
		 * String anti_segments = AND_JOINER.join(antis); if
		 * (anti_segments.isEmpty()) { anti_segments = null; }
		 * 
		 * if(targeting_segments != null && anti_segments != null) {
		 * targeting_segments = "(" + targeting_segments + ")"; anti_segments =
		 * "(" + anti_segments + ")"; }
		 * 
		 * return AND_JOINER.join(targeting_segments, anti_segments); }
		 */

		public void add(String value, boolean anti)
		{
			if (anti)
			{
				anti_values.add(value);
			}
			else
			{
				values.add(value);
			}
		}

		public void add(Segment segment, String dmp_provider)
		{
			for (String segment_value : segment.values)
			{
				String prefixed_value = DOT_JOINER.join(dmp_provider, segment_value);
				values.add(prefixed_value);
			}

			for (String segment_value : segment.anti_values)
			{
				String prefixed_value = DOT_JOINER.join(dmp_provider, segment_value);
				anti_values.add(prefixed_value);
			}
		}

		public void override(Segment another)
		{
			values.clear();
			values.addAll(another.values);

			anti_values.clear();
			anti_values.addAll(another.anti_values);
		}

		private final static Joiner AND_JOINER = Joiner.on(" AND ").skipNulls();
		private final static Joiner OR_JOINER = Joiner.on(" OR ").skipNulls();
		private final static Joiner DOT_JOINER = Joiner.on('.').skipNulls();
	}

	public static class Language
	{
		public transient int campaign_id;

		@XmlAttribute
		public String type = "include";

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();

		public void override(Language another)
		{
			campaign_id = another.campaign_id;

			values.clear();
			values.addAll(another.values);
		}
	}

	public static class Geo
	{
		public transient int campaign_id;

		@XmlElement(name = "country")
		public final List<Country> countries = new ArrayList<>();

		public void add(Country country)
		{
			countries.add(country);
		}
	}

	public static class Country
	{
		public transient int campaign_id;

		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "region")
		public List<Region> regions; // optional

		@XmlAttribute
		public Boolean value; // optional

		public void add(Region region)
		{
			if (regions == null)
			{
				regions = new ArrayList<>();
			}
			regions.add(region);
		}
	}

	public static class Region
	{
		public transient int campaign_id;
		public transient String country_code;

		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "city")
		public List<City> citys; // optional (NOT BEING USED)

		@XmlAttribute
		public Boolean value; // optional
	}

	public static class City
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;
	}

	public static class Zip
	{
		public transient int campaign_id;

		@XmlAttribute
		public IncludeOrExclude type = IncludeOrExclude.INCLUDE; // include only

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();

		public void override(Zip another)
		{
			campaign_id = another.campaign_id;

			type = another.type;

			values.clear();
			values.addAll(another.values);
		}
	}

	public static class Dma
	{
		public transient int campaign_id;

		@XmlAttribute
		public IncludeOrExclude type = IncludeOrExclude.INCLUDE; // include only

		@XmlElement(name = "value")
		public final List<Integer> values = new ArrayList<>();

		public void override(Dma another)
		{
			campaign_id = another.campaign_id;

			type = another.type;

			values.clear();
			values.addAll(another.values);
		}
	}

	public static class PageUrl
	{
		public transient int campaign_id;

		@XmlAttribute
		public IncludeOrExclude type;

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();

		public void override(PageUrl another)
		{
			campaign_id = another.campaign_id;

			type = another.type;

			values.clear();
			values.addAll(another.values);
		}
	}

	public static class UserAgent
	{
		private final transient Set<String> browser_names = new HashSet<>();
		private final transient Set<String> operating_system_names = new HashSet<>();
		private final transient Set<String> device_model_names = new HashSet<>();

		@XmlElementWrapper(name = "browser_version_dt")
		@XmlElement(name = "browser")
		public List<Browser> browsers;

		@XmlElementWrapper(name = "os_version_dt")
		@XmlElement(name = "os")
		public List<OperatingSystem> operating_systems;

		@XmlElementWrapper(name = "device_model_dt")
		@XmlElement(name = "device_maker")
		public List<DeviceModel> device_models;

		@XmlElement(name = "device_type")
		public DeviceType device_type;

		public void add(Browser browser)
		{
			if (browser_names.contains(browser.id))
			{
				return;
			}
			browser_names.add(browser.id);

			if (this.browsers == null)
			{
				this.browsers = new ArrayList<>();
			}
			this.browsers.add(browser);
		}

		public void add(OperatingSystem operating_system)
		{
			if (operating_system_names.contains(operating_system.id))
			{
				return;
			}
			operating_system_names.add(operating_system.id);

			if (this.operating_systems == null)
			{
				this.operating_systems = new ArrayList<>();
			}
			this.operating_systems.add(operating_system);
		}

		public void add(DeviceModel device_model)
		{
			if (device_model_names.contains(device_model.id))
			{
				return;
			}
			device_model_names.add(device_model.id);

			if (this.device_models == null)
			{
				this.device_models = new ArrayList<>();
			}
			this.device_models.add(device_model);
		}

		public void add(DeviceType device_type)
		{
			if (this.device_type == null)
			{
				this.device_type = new DeviceType();
			}
			this.device_type.values.clear();
			this.device_type.values.addAll(device_type.values);
		}
	}

	public static class Browser
	{
		public transient int campaign_id;

		@XmlAttribute
		public String id;

		@XmlAttribute
		public boolean value;
	}

	public static class OperatingSystem
	{
		public transient int campaign_id;

		@XmlAttribute
		public String id;

		@XmlAttribute
		public boolean value;
	}

	public static class DeviceModel
	{
		public transient int campaign_id;

		@XmlAttribute
		public String id;

		@XmlAttribute
		public boolean value;
	}

	public static class DeviceType
	{
		public transient int campaign_id;

		@XmlAttribute
		public IncludeOrExclude type = IncludeOrExclude.INCLUDE; // defaults to
																	// include

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();
	}

	public static class PlayerSize
	{
		public transient int campaign_id;

		@XmlAttribute(name = "min")
		public Integer min; // optional

		@XmlAttribute(name = "max")
		public Integer max; // optional
	}

	public static class Visibility
	{
		public transient int campaign_id;

		@XmlValue
		public String value;
	}

	public static class Strategy
	{
		public transient int campaign_id;
		public transient int line_item_id;

		public transient Double budget;
		public transient Double margin;
		public transient Double ctr;
		public transient Double cpm;
		public transient Double cmvr;
		public transient Double cpcv;

		@XmlAttribute
		public int id;

		@XmlElement(name = "parameter")
		public final List<Parameter> parameters = new ArrayList<>();

		public void initialize()
		{
			StrategyType strategy_type = StrategyType.fromCode(id);
			if (strategy_type == null)
			{
				return;
			}
			strategy_type.set(this);
		}

		public void override(Strategy another)
		{
			campaign_id = another.campaign_id;
			line_item_id = another.line_item_id;

			budget = another.budget;
			margin = another.margin;
			ctr = another.ctr;
			cpm = another.cpm;
			cmvr = another.cmvr;
			cpcv = another.cpcv;

			id = another.id;

			parameters.clear();
			parameters.addAll(another.parameters);
		}
	}

	public enum StrategyType
	{
		CPM(1)
		{
			@Override
			public void set(Strategy strategy)
			{
				if (strategy.budget != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.BUDGET;
					param.value = strategy.budget;
					strategy.parameters.add(param);
				}
				if (strategy.margin != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.MARGIN;
					param.value = strategy.margin;
					strategy.parameters.add(param);
				}
				if (strategy.cpm != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CPM;
					param.value = strategy.cpm;
					strategy.parameters.add(param);
				}
			}
		},
		CPM_WITH_VCR(2)
		{
			@Override
			public void set(Strategy strategy)
			{
				if (strategy.budget != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.BUDGET;
					param.value = strategy.budget;
					strategy.parameters.add(param);
				}
				if (strategy.margin != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.MARGIN;
					param.value = strategy.margin;
					strategy.parameters.add(param);
				}
				if (strategy.cpm != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CPM;
					param.value = strategy.cpm;
					strategy.parameters.add(param);
				}
				if (strategy.cmvr != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CMVR;
					param.value = strategy.cmvr;
					strategy.parameters.add(param);
				}
			}
		},
		CPM_WITH_CTR(3)
		{
			@Override
			public void set(Strategy strategy)
			{
				if (strategy.budget != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.BUDGET;
					param.value = strategy.budget;
					strategy.parameters.add(param);
				}
				if (strategy.margin != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.MARGIN;
					param.value = strategy.margin;
					strategy.parameters.add(param);
				}
				if (strategy.cpm != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CPM;
					param.value = strategy.cpm;
					strategy.parameters.add(param);
				}
				if (strategy.ctr != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CTR;
					param.value = strategy.ctr;
					strategy.parameters.add(param);
				}
			}
		},
		CPM_WITH_CTR_AND_VCR(4)
		{
			@Override
			public void set(Strategy strategy)
			{
				if (strategy.budget != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.BUDGET;
					param.value = strategy.budget;
					strategy.parameters.add(param);
				}
				if (strategy.margin != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.MARGIN;
					param.value = strategy.margin;
					strategy.parameters.add(param);
				}
				if (strategy.cpm != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CPM;
					param.value = strategy.cpm;
					strategy.parameters.add(param);
				}
				if (strategy.ctr != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CTR;
					param.value = strategy.ctr;
					strategy.parameters.add(param);
				}
				if (strategy.cmvr != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CMVR;
					param.value = strategy.cmvr;
					strategy.parameters.add(param);
				}
			}
		},
		CPCV(5)
		{
			@Override
			public void set(Strategy strategy)
			{
				if (strategy.budget != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.BUDGET;
					param.value = strategy.budget;
					strategy.parameters.add(param);
				}
				if (strategy.margin != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.MARGIN;
					param.value = strategy.margin;
					strategy.parameters.add(param);
				}
				if (strategy.cpcv != null)
				{
					Parameter param = new Parameter();
					param.name = Parameter.Name.CPCV;
					param.value = strategy.cpcv;
					strategy.parameters.add(param);
				}
			}
		},;

		StrategyType(int code)
		{
			code_ = code;
		}

		public int code()
		{
			return code_;
		}

		public abstract void set(Strategy strategy);

		public static StrategyType fromCode(int code)
		{
			for (StrategyType value : StrategyType.values())
			{
				if (value.code() == code)
				{
					return value;
				}
			}
			return null;
		}

		private final int code_;
	}

	public static class Parameter
	{
		@XmlAttribute
		public Name name;

		@XmlValue
		public double value;

		public enum Name
		{
			@XmlEnumValue("aBudget")
			BUDGET("aBudget"),
			
			@XmlEnumValue("aLearningBudget")
			BUDGET1("aLearningBudget"),

			@XmlEnumValue("tMargin")
			MARGIN("tMargin"),

			@XmlEnumValue("aLearningCPM")
			CPM1("aLearningCPM"),
			
			@XmlEnumValue("aCPM")
			CPM("aCPM"),

			@XmlEnumValue("cCTR")
			CTR("cCTR"),

			@XmlEnumValue("cCVR")
			CMVR("cCVR"),

			@XmlEnumValue("aCPCV")
			CPCV("aCPCV"),
			
			@XmlEnumValue("aCPA")
			CPA("aCPA"),
			
			@XmlEnumValue("aCPC")
			CPC("aCPC");
			
			private String xmlValue;
			
			Name(String xmlValue){
				this.xmlValue = xmlValue;
			}
			
			public String getXMLValue()
			{
				return xmlValue;
			}
		}
	}

	public static class ConversionValues
	{

		@XmlAttribute(name = "is_optimizing")
		public boolean is_optimizing;

		@XmlValue
		public String value;
	}

	/**
	 * Seller Side Options - SSP
	 */
	public static class SSO_SSP
	{
		public transient int campaign_id;

		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "seat")
		public List<SSO_Seat> seats; // optional

		@XmlElement(name = "deal")
		public List<SSO_Deal> deals; // optional

		@XmlElement(name = "seller_network")
		public final List<SSO_SellerNetwork> seller_networks = new ArrayList<>();

	}

	/**
	 * Seller Side Options - Seller Network
	 */
	public static class SSO_SellerNetwork
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "seat")
		public List<SSO_Seat> seats; // optional

		@XmlElement(name = "deal")
		public List<SSO_Deal> deals; // optional

	}

	/**
	 * Seller Side Options - Seat
	 */
	public static class SSO_Seat
	{
		@XmlAttribute
		public String id;

	}

	/**
	 * Seller Side Options - Deal
	 */
	public static class SSO_Deal
	{
		@XmlAttribute
		public String id;

		@XmlElement(name = "seat")
		public List<SSO_Seat> seats; // Optional

	}

	public static class Creative_SSP
	{
		@XmlElement
		public final Bidswitch bidswitch = new Bidswitch();

		public static class Bidswitch
		{
			@XmlElementWrapper(name = "attribute")
			@XmlElement(name = "value")
			public List<Integer> values = new ArrayList<>();
		}
	}

	// (NOT BEING USED)
	public static class Creative_Companion
	{
		@XmlAttribute
		public int id;

		@XmlElement(name = "impression_tracking_url")
		public String impression_tracking_url; // optional

		@XmlElement(name = "click_tracking_url")
		public String click_tracking_url; // optional
	}

	public static class SSP
	{
		public transient int ssp_id;

		/*
		 * public abstract String name();
		 * 
		 * public abstract void name(String name);
		 * 
		 * public abstract boolean value();
		 * 
		 * public abstract List<SellerNetwork> seller_networks();
		 * 
		 * public abstract void add(SellerNetwork seller_network);
		 */

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute(name = "value")
		public String value;

		@XmlElement(name = "seller_network")
		public List<SellerNetwork> sellerNetworks = new ArrayList<>();

	}

	public static class SSP_1 extends SSP
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlElements(
		{ @XmlElement(name = "seller_network", type = SellerNetwork_1.class),
				@XmlElement(name = "seller_network", type = SellerNetwork_2.class) })
		public final List<SellerNetwork> seller_networks = new ArrayList<>();

		/*
		 * @Override public String name() { return name; }
		 * 
		 * @Override public boolean value() { return true; }
		 * 
		 * @Override public void name(String name) { String value =
		 * URLs.decode(name); if (value == null) { value = name; } this.name =
		 * XmlEscapers.xmlAttributeEscaper().escape(value); }
		 * 
		 * @Override public List<SellerNetwork> seller_networks() { return
		 * seller_networks; }
		 * 
		 * private final transient Set<String> seller_network_names = new
		 * HashSet<>();
		 * 
		 * @Override public void add(SellerNetwork seller_network) {
		 *//*
			 * if (seller_network_names.contains(seller_network.name())) {
			 * return; } seller_network_names.add(seller_network.name());
			 * seller_networks.add(seller_network);
			 *//*
				 * }
				 */
	}

	public static class SSP_2 extends SSP
	{
		// public final static SSP_2 OTHER_TRUE = new SSP_2("Other", true);
		// public final static SSP_2 OTHER_FALSE = new SSP_2("Other", false);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		/*
		 * @Override public String name() { return name; }
		 * 
		 * @Override public boolean value() { return value; }
		 * 
		 * @Override public void name(String name) { String value =
		 * URLs.decode(name); if (value == null) { value = name; } this.name =
		 * XmlEscapers.xmlAttributeEscaper().escape(value); }
		 * 
		 * @Override public List<SellerNetwork> seller_networks() { return
		 * Collections.emptyList(); }
		 * 
		 * public SSP_2(String name, boolean value) { this.name = name;
		 * this.value = value; }
		 * 
		 * public SSP_2(){
		 * 
		 * }
		 * 
		 * @Override public void add(SellerNetwork seller_network) {}
		 */
	}

	public static class SellerNetwork
	{
		public transient int ssp_id;
		public transient int seller_network_id;
		public transient int campaign_id;

		public transient String ssp_name;

		// public String name();

		// public boolean value();

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute(name = "value")
		public String value;

		public List<Publisher> publishers = new ArrayList<>();

		/*
		 * public abstract void name(String name);
		 * 
		 * public abstract List<Publisher> publishers();
		 * 
		 * public abstract void add(Publisher publisher);
		 */

	}

	public static class SellerNetwork_1 extends SellerNetwork
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "accept_empty_page_domain")
		public AcceptOrReject accept_empty_page_domain; // optional

		@XmlElement(name = "accept_empty_domain")
		public AcceptOrReject accept_empty_domain; // optional

		@XmlElement(name = "accept_empty_language")
		public AcceptOrReject accept_empty_language; // optional

		@XmlElements(
		{ @XmlElement(name = "publisher", type = Publisher_1.class),
				@XmlElement(name = "publisher", type = Publisher_2.class) })
		public final List<Publisher> publishers = new ArrayList<>();

		// @Override
		// public String name()
		// {
		// return name;
		// }
		//
		// @Override
		// public void name(String name)
		// {
		// String value = URLs.decode(name);
		// if (value == null) {
		// value = name;
		// }
		// this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		// }
		//
		// @Override
		// public boolean value()
		// {
		// return true;
		// }
		//
		// @Override
		// public List<Publisher> publishers()
		// {
		// return publishers;
		// }
		//
		// private final transient Set<String> publisher_names = new
		// HashSet<>();
		//
		// @Override
		// public void add(Publisher publisher)
		// {
		// if (publisher_names.contains(publisher.name())) {
		// return;
		// }
		// publisher_names.add(publisher.name());
		// publishers.add(publisher);
		// }
	}

	public static class SellerNetwork_2 extends SellerNetwork
	{
		public final static SellerNetwork_2 OTHER_TRUE = new SellerNetwork_2("Other", true);
		public final static SellerNetwork_2 OTHER_FALSE = new SellerNetwork_2("Other", false);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		public SellerNetwork_2()
		{
		}

		public SellerNetwork_2(String name, boolean value)
		{
			this.name = name;
			this.value = value;
		}

		// @Override
		// public String name()
		// {
		// return name;
		// }
		//
		// @Override
		// public void name(String name)
		// {
		// String value = URLs.decode(name);
		// if (value == null) {
		// value = name;
		// }
		// this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		// }
		//
		// @Override
		// public boolean value()
		// {
		// return value;
		// }
		//
		// @Override
		// public List<Publisher> publishers()
		// {
		// return Collections.emptyList();
		// }
		//
		// @Override
		// public void add(Publisher publisher) {}
	}

	public static abstract class Publisher
	{
		public transient int seller_network_id;
		public transient int publisher_id;
		public transient int campaign_id;

		public transient String ssp_name;
		public transient String seller_network_name;

		public abstract String name();

		public abstract void name(String name);

		public abstract boolean value();

		public abstract List<Domain> domains();

		public abstract void add(Domain domain);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute(name = "value")
		public String value;

	}

	public static class Publisher_1 extends Publisher
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlElements(
		{ @XmlElement(name = "domain", type = Domain.class) })
		public final List<Domain> domains = new ArrayList<>();

		@Override
		public String name()
		{
			return name;
		}

		@Override
		public void name(String name)
		{
			String value = URLs.decode(name);
			if (value == null)
			{
				value = name;
			}
			this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		}

		@Override
		public boolean value()
		{
			return true;
		}

		@Override
		public void add(Domain domain)
		{
			domains.add(domain);
		}

		@Override
		public List<Domain> domains()
		{
			return domains;
		}
	}

	public static class Publisher_2 extends Publisher
	{
		public final static Publisher_2 OTHER_TRUE = new Publisher_2("Other", true);
		public final static Publisher_2 OTHER_FALSE = new Publisher_2("Other", false);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		public Publisher_2()
		{
		}

		public Publisher_2(String name, boolean value)
		{
			this.name = name;
			this.value = value;
		}

		@Override
		public String name()
		{
			return name;
		}

		@Override
		public void name(String name)
		{
			String value = URLs.decode(name);
			if (value == null)
			{
				value = name;
			}
			this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		}

		@Override
		public boolean value()
		{
			return value;
		}

		@Override
		public void add(Domain domain)
		{
		}

		@Override
		public List<Domain> domains()
		{
			return Collections.emptyList();
		}
	}

	public static class Domain
	{
		public final static Domain OTHER_TRUE = new Domain("Other", true);
		public final static Domain OTHER_FALSE = new Domain("Other", false);

		public transient int publisher_id;

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		public Domain()
		{
		}

		public Domain(String name, boolean value)
		{
			this.name = name;
			this.value = value;
		}

		public void name(String name)
		{
			String value = URLs.decode(name);
			if (value == null)
			{
				value = name;
			}
			this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		}
	}

	public static abstract class SSP_Reference
	{
		public transient int campaign_id;

		public abstract String name();

		public abstract void add(SellerNetwork_Reference seller_network);
	}

	public static class SSP_Reference_1 extends SSP_Reference
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlElement(name = "seller_network")
		public final List<SellerNetwork_Reference> seller_networks = new ArrayList<>();

		@Override
		public String name()
		{
			return name;
		}

		@Override
		public void add(SellerNetwork_Reference seller_network)
		{
			seller_networks.add(seller_network);
		}
	}

	public static class SSP_Reference_2 extends SSP_Reference
	{
		public final static SSP_Reference_2 OTHER_TRUE = new SSP_Reference_2("Other", true);
		public final static SSP_Reference_2 OTHER_FALSE = new SSP_Reference_2("Other", false);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		public SSP_Reference_2()
		{
		}

		public SSP_Reference_2(String name, boolean value)
		{
			this.name = name;
			this.value = value;
		}

		@Override
		public String name()
		{
			return name;
		}

		@Override
		public void add(SellerNetwork_Reference seller_network)
		{
		}
	}

	public static class SellerNetwork_Reference
	{
		public final static SellerNetwork_Reference OTHER_TRUE = new SellerNetwork_Reference("Other", true);

		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public boolean value;

		public SellerNetwork_Reference(String name, boolean value)
		{
			this.name = name;
			this.value = value;
		}
	}

	public static class Pixel
	{
		@XmlAttribute(required = true)
		public int id;

		@XmlAttribute(required = true)
		public String name;

		@XmlAttribute(name = "post_view_hours")
		public Integer post_view_hours;

		@XmlAttribute(name = "post_click_hours")
		public Integer post_click_hours;

		@XmlAttribute(name = "expiration_hours")
		public Integer expiration_hours;

		@XmlAttribute(name = "sync_enabled")
		public Boolean sync_enabled;

		public void override(Pixel another)
		{
			id = another.id;
			name = another.name;
			post_view_hours = another.post_view_hours;
			post_click_hours = another.post_click_hours;
			expiration_hours = another.expiration_hours;
			sync_enabled = another.sync_enabled;
		}
	}

	public static class DomainList
	{
		@XmlAttribute
		public String id;

		@XmlElement(name = "value")
		public final List<String> values = new ArrayList<>();
	}

	public static class DomainListFile
	{
		public final transient Set<Integer> seller_network_ids = new HashSet<>();
		public final transient Set<Integer> publisher_ids = new HashSet<>();

		@XmlElementWrapper(name = "codes")
		@XmlElement(name = "code")
		public final List<Code> block_codes = new ArrayList<>();

		@XmlElementWrapper(name = "ssps")
		@XmlElement(name = "ssp")
		public final List<Code> ssp_codes = new ArrayList<>();

		@XmlElementWrapper(name = "seller_networks")
		@XmlElement(name = "seller_network")
		public final List<Code> seller_network_codes = new ArrayList<>();

		@XmlElementWrapper(name = "publishers")
		@XmlElement(name = "publisher")
		public final List<Code> publisher_codes = new ArrayList<>();
	}

	public static class Code
	{
		@XmlAttribute(name = "id")
		public String name;

		@XmlAttribute
		public int value;

		public void name(String name)
		{
			String value = URLs.decode(name);
			if (value == null)
			{
				value = name;
			}
			this.name = XmlEscapers.xmlAttributeEscaper().escape(value);
		}
	}

	public static class Event
	{
		@XmlAttribute
		public String name;

		@XmlElement(name = "value")
		public final List<String> urls = new ArrayList<>();
	}

	public static class EventRecord
	{
		public transient int creative_id;

		public transient String event_type;
		public transient String tracking_url;
	}

	public static class Deal
	{
		public final transient Map<String, SSP> seller_side_ssps = new HashMap<>();
		public final transient Map<String, SellerNetwork> seller_side_seller_networks = new HashMap<>();
		public final transient Map<String, Publisher> seller_side_publishers = new HashMap<>();

		public final transient Map<Integer, Category> site_categories = new HashMap<>();
		public final transient Map<Integer, Category> advertiser_categories = new HashMap<>();
		public final transient Map<String, ConfigData.Country> countries = new HashMap<>();

		public transient boolean passes_on_unknown_data;

		public transient ListType smart_list_type;

		public final transient Set<String> ssps = new HashSet<>();

		@XmlAttribute(name = "id", required = true)
		public int id;

		@XmlAttribute(name = "name", required = true)
		public String name;

		@XmlAttribute(name = "share", required = true)
		public double share;

		@XmlAttribute(name = "margin", required = true)
		public double margin;

		@XmlAttribute(name = "double_verify_enabled")
		public Boolean double_verify_enabled; // optional

		@XmlAttribute(name = "dsp_name", required = true)
		public String dsp_name;

		@XmlAttribute(name = "smartlist")
		public Boolean smartlist; // optional

		@XmlElementWrapper(name = "ssp_deals")
		@XmlElement(name = "ssp_deal")
		public List<SSP_Deal> ssp_deals; // optional

		@XmlElement(name = "creative_url", required = true)
		@XmlJavaTypeAdapter(value = CdataAdapter.class)
		public String creative_url;

		@XmlElement(name = "creative_url_secure", required = true)
		@XmlJavaTypeAdapter(value = CdataAdapter.class)
		public String creative_url_secure;

		// @XmlElement(name = "url")
		// @XmlJavaTypeAdapter(value = CdataAdapter.class)
		// public String url; // enclose url with CDATA block

		@XmlElementWrapper(name = "url")
		@XmlElement(name = "dc")
		public List<DC> data_centers;

		@XmlElement(name = "start_date", required = true)
		public String start_date;

		@XmlElement(name = "end_date", required = true)
		public String end_date;

		@XmlElement(name = "strategy")
		public final Strategy strategy = new Strategy();

		@XmlElement(name = "targeting")
		public Targeting targeting = new Targeting();

		@XmlElement(name = "ssp")
		public final List<SSO_SSP> seller_side_options = new ArrayList<>();

		public void bidswitch()
		{
			dsp_name = "bidswitch";

			data_centers = Lists.newArrayList(new DC("sc", "http://us-east.bidswitch.net/clearstream_bid"),
					new DC("or", "http://us-west.bidswitch.net/clearstream_bid"),
					new DC("be", "http://eu.bidswitch.net/clearstream_bid"));
		}

		public void brealtime()
		{
			dsp_name = "brealtime";

			data_centers = Lists.newArrayList(new DC("va", "http://clearstream-east.brealtime.com/"),
					new DC("ca", "http://clearstream-west.brealtime.com/"),
					new DC("fr", "http://clearstream-euro.brealtime.com/"));
		}
	}

	public static class SSP_Deal
	{
		public transient int deal_id;

		@XmlAttribute(name = "id", required = true)
		public String id;

		@XmlAttribute(name = "bidfloor")
		public Double floor_price;
	}

	public static class DC
	{
		@XmlAttribute(name = "id", required = true)
		public String id;

		@XmlValue
		public String value;

		DC(String id, String value)
		{
			this.id = id;
			this.value = value;
		}

		DC()
		{

		}
	}

	public static class DSP
	{
		@XmlAttribute(name = "id", required = true)
		public int id;

		@XmlAttribute(name = "name", required = true)
		public String name;

		DSP(int id, String name)
		{
			this.id = id;
			this.name = name;
		}

		DSP()
		{
		}
	}

	public static class BlacklistIP
	{
		public transient String start_ip;
		public transient String end_ip;

		@XmlValue
		public String getValue()
		{
			if (start_ip.equals(end_ip))
			{
				return start_ip;
			}
			else
			{
				return DASH_JOINER.join(start_ip, end_ip);
			}
		}

		private final static Joiner DASH_JOINER = Joiner.on('-').skipNulls();
	}

	public static class VectoroneShare
	{
		@XmlValue
		public int id;

	}

	public static class vectoroneControlShare
	{
		@XmlValue
		public int id;

	}
}
