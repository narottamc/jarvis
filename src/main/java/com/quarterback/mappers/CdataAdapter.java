/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.mappers;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author Clearstream
 */
public class CdataAdapter extends XmlAdapter<String, String>
{
	@Override
	public String marshal(String v) throws Exception
	{
		return "<![CDATA[" + v + "]]>";
	}

	@Override
	public String unmarshal(String v) throws Exception
	{
		return v;
	}
}
