/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.mappers;

import com.google.common.base.Charsets;
import com.google.common.net.InternetDomainName;
import com.google.common.primitives.Ints;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.URLDecoder;

/**
 * @author Clearstream
 */
public final class URLs
{
	@Nullable
	public static String getTopLevelDomain(String url)
	{
		if (url.equals(NULL) || url.equals(UNDEFINED) || url.startsWith(ABOUT_BLANK))
		{
			return null;
		}

		// check for weird domains
		if (Ints.tryParse(url) != null)
		{
			return null;
		}

		url = sanitize(url);

		HttpUrl http_url = HttpUrl.parse(url);
		if (http_url == null)
		{
			String decoded = decode(url); // in case the url was been
											// double-encoded
			if (decoded != null)
			{
				http_url = HttpUrl.parse(decoded);
			}
		}
		if (http_url == null)
		{
			LOG.warn("Unable to parse the given {} into a valid HTTP URL.", url);
			return null;
		}

		InternetDomainName domain;
		try
		{
			InternetDomainName domain_name = InternetDomainName.from(http_url.host());
			domain = domain_name.topPrivateDomain();
		}
		catch (Exception e)
		{
			LOG.warn("Unable to interpret the parsed URL host {} to a legal domain.", http_url.host());
			return null;
		}

		return domain.toString();
	}

	@Nullable
	public static String getFullDomain(String url)
	{
		if (url.equals(NULL) || url.equals(UNDEFINED) || url.startsWith(ABOUT_BLANK))
		{
			return null;
		}

		// check for weird domains
		if (Ints.tryParse(url) != null)
		{
			return null;
		}

		url = sanitize(url);

		HttpUrl http_url = HttpUrl.parse(url);
		if (http_url == null)
		{
			String decoded = decode(url); // in case the url was been
											// double-encoded
			if (decoded != null)
			{
				http_url = HttpUrl.parse(decoded);
			}
		}
		if (http_url == null)
		{
			LOG.warn("Unable to parse the given {} into a valid HTTP URL.", url);
			return null;
		}

		return http_url.host();
	}

	@Nullable
	public static String decode(@Nullable String url)
	{
		if (url == null || url.equals(NULL) || url.equals(UNDEFINED))
		{
			return null;
		}

		// handle incomplete trailing escape (%) pattern issue
		if (url.length() > 0 && url.charAt(url.length() - 1) == '%')
		{
			url = url.substring(0, url.length() - 1);
		}
		if (url.length() > 1 && url.charAt(url.length() - 2) == '%')
		{
			url = url.substring(0, url.length() - 2);
		}

		try
		{
			return URLDecoder.decode(url, Charsets.UTF_8.name());
		}
		catch (Exception e)
		{
			LOG.warn("Unable to decoded the given URL {}: {}", url, e.getMessage());
			return null;
		}
	}

	static String sanitize(String url)
	{
		return url.startsWith("http") ? url : "http://" + url;
	}

	private final static String NULL = "null";
	private final static String UNDEFINED = "undefined";
	private final static String ABOUT_BLANK = "about:blank";

	private final static Logger LOG = LoggerFactory.getLogger(URLs.class);
}
