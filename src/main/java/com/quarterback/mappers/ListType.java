/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.mappers;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum ListType
{
	UNKNOWN(-1),
	WHITELIST(0), // code of whitelist in openstream database is 0
	BLACKLIST(1), // code of blacklist in openstream database is 1
	;

	ListType(int code)
	{
		code_ = code;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static ListType fromCode(@Nullable Integer code)
	{
		if (code == null)
		{
			return null;
		}
		for (ListType value : ListType.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	private final int code_;
}