/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;

/**
 * @author Clearstream
 */
public enum ContextScope
{
	LOCALHOST(0),
	DEVELOPMENT(1),
	SANDBOX(2),
	PRODUCTION(3);

	ContextScope(int code)
	{
		alias_ = name().toLowerCase();
		description_ = StringUtils.capitalize(alias_);
		code_ = code;
	}

	public String alias()
	{
		return alias_;
	}

	public String description()
	{
		return description_;
	}

	public int code()
	{
		return code_;
	}

	@Nullable
	public static ContextScope fromCode(int code)
	{
		for (ContextScope value : ContextScope.values())
		{
			if (value.code() == code)
			{
				return value;
			}
		}
		return null;
	}

	@Nullable
	public static ContextScope fromAlias(String alias)
	{
		for (ContextScope value : values())
		{
			if (value.alias().equalsIgnoreCase(alias))
			{
				return value;
			}
		}
		return null;
	}

	private final String alias_;
	private final String description_;
	private final int code_;
}
