/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;
import com.osapi.awsutils.AWSAuthenticate;
import com.osapi.awsutils.DynamodbModule;
import com.osapi.enumtypes.TestEnvironment;
import com.quarterback.models.TaskCategory;
import com.quarterback.models.TaskGroupStatus;
import com.quarterback.storage.MediaTaskEntity;
import com.quarterback.storage.MediaTaskGroupEntity;
import com.quarterback.storage.MediaTaskStorage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.*;
import java.util.Random;
import java.util.zip.GZIPInputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;

/**
 * @author Clearstream
 */
public class CommonUtils
{
	private static Logger LOG = LogManager.getLogger(CommonUtils.class);
	private static ContextScope contextScope = System.getProperty("testEnvironment") != null ? 
			TestEnvironment.valueOf(System.getProperty("testEnvironment").toUpperCase()).getAWSEnvironmentExecution() : ContextScope.DEVELOPMENT ;
	public static void decompressGzipFile(String gzipFile, String newFile)
	{
		try
		{
			FileInputStream fis = new FileInputStream(gzipFile);
			GZIPInputStream gis = new GZIPInputStream(fis);
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[3000000];
			int len;
			while ((len = gis.read(buffer)) != -1)
			{
				fos.write(buffer, 0, len);
			}
			// close resources
			fos.close();
			gis.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public static MediaTaskStorage getMediaTaskStorageObj(ContextScope scope)
	{
		AWSAuthenticate aWSAuthenticate = new AWSAuthenticate();
		aWSAuthenticate.getS3Client();

		Injector injector = Guice.createInjector(Stage.PRODUCTION, new Module[]
		{ new DynamodbModule() });

		DynamoDB dynamoDB = injector.getInstance(DynamoDB.class);

		MediaTaskStorage taskStorage = new MediaTaskStorage(dynamoDB, scope);
		return taskStorage;
	}

	public static MediaTaskGroupEntity getTakGroupEntity(String emailId, ContextScope scope)
	{
		MediaTaskStorage taskStorage = CommonUtils.getMediaTaskStorageObj(scope);

		new DateTime();
		// LOG.info(new DateTime().minusDays(1));
		MediaTaskGroupEntity taskGrpEntity = taskStorage.getTaskGroups(DateTime.now(), emailId);
		return taskGrpEntity;
	}

	public static MediaTaskGroupEntity waitForTaskGroupToFinish(String emailId, ContextScope scope)
	{
		int currentPoolingTimeInMin = 0;
		MediaTaskGroupEntity taskGroupEnity = CommonUtils.getTakGroupEntity(emailId, scope);
		LOG.info("Current TaskGroup Status:" + taskGroupEnity.task_group_status);

		while (taskGroupEnity.task_group_status.code() != TaskGroupStatus.FINISHED.code()
				&& currentPoolingTimeInMin < Constants.TASKGROUP_POOLING_TIMEOUT_IN_MINUTE)
		{
			LOG.info("Current pooling Time in minute: " + currentPoolingTimeInMin);
			WaitUtils.waitForMinutes(Constants.TASKGROUP_POOLING_INTERVAL_IN_MINUTE);
			currentPoolingTimeInMin = currentPoolingTimeInMin + Constants.TASKGROUP_POOLING_INTERVAL_IN_MINUTE;
			taskGroupEnity = getTakGroupEntity(emailId, scope);
			LOG.info("New TaskStatus: " + taskGroupEnity.task_group_status);
			assertThat("XML Generation FAILED!! ; TaskGroup Status Is "+TaskGroupStatus.fromCode(taskGroupEnity.task_group_status.code()).toString(),
					TaskGroupStatus.fromCode(taskGroupEnity.task_group_status.code()).toString(),
					allOf(is(not(TaskGroupStatus.INCOMPLETE_ARTIFACT.toString())),is(not(TaskGroupStatus.FAILED.toString()))));		
		}

		if (taskGroupEnity.task_group_status == TaskGroupStatus.FINISHED)
		{
			return taskGroupEnity;
		}
		else
		{
			LOG.info("Current TaskGroup Status when Max timeout reached:" + taskGroupEnity.task_group_status);
			return null;
		}
	}

	public static String getS3FilePathByUserId(MediaTaskGroupEntity taskGroupEnity, ContextScope scope)
	{
		AWSAuthenticate aWSAuthenticate = new AWSAuthenticate();
		aWSAuthenticate.getS3Client();

		Injector injector = Guice.createInjector(Stage.PRODUCTION, new Module[]
		{ new DynamodbModule() });

		DynamoDB dynamoDB = injector.getInstance(DynamoDB.class);

		MediaTaskStorage taskStorage = new MediaTaskStorage(dynamoDB, scope);
		MediaTaskEntity taskEntityList = taskStorage.getFinishedTasks(taskGroupEnity.task_group_id,
				TaskCategory.UPLOAD_ARTIFACT_FILE);

		return taskEntityList.output_file_path;
	}

	public synchronized static String getDateTimeStampAsString()
	{
		Random randomNum = new Random();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss").withZoneUTC();
		String date = formatter.print(DateTime.now());
		return date+randomNum.nextInt(50)+1;
	}

	public static boolean isFileExistInDir(String filePathString)
	{
		File f = new File(filePathString);
		if (f.exists() && !f.isDirectory())
		{
			return true;
		}
		return false;
	}

	public static String getCampaignXMLFileName()
	{
		File file = new File(Constants.CAMPAIGN_XML_PATH);
		File[] files = file.listFiles(new FilenameFilter()
		{
			MediaTaskGroupEntity taskGroupEntity = CommonUtils.getTakGroupEntity(Constants.LAUNCHCAMPAIGN_USERID,
					contextScope);

			@Override
			public boolean accept(File dir, String name)
			{
				if (name.toLowerCase().endsWith(".xml") && name.startsWith(taskGroupEntity.task_group_id))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		});
		for (File f : files)
		{
			return Constants.CAMPAIGN_XML_PATH + f.getName();
		}
		return Constants.CAMPAIGN_XML_PATH + file.getName();
	}
}
