/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Clearstream
 */
public class PropertiesUtils
{
	private static final Logger LOG = Logger.getLogger(PropertiesUtils.class);

	public static void loadAWSProperties()
	{
		FileInputStream propFile = null;
		try
		{
			propFile = new FileInputStream(Constants.AWS_RESOURCEPATH);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		Properties p = new Properties(System.getProperties());
		try
		{
			p.load(propFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		System.setProperties(p);
		LOG.info("Loading AWS Credentials From Properties");
	}

}
