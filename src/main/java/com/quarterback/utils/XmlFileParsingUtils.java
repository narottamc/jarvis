/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import com.google.common.collect.HashBasedTable;
import com.quarterback.mappers.ConfigData;
import com.quarterback.mappers.ConfigData.Deal;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Clearstream
 */
public class XmlFileParsingUtils
{
	private static final Logger LOG = Logger.getLogger(XmlFileParsingUtils.class);
	private static HashBasedTable<Integer, Integer, ConfigData.Campaign> campaignsByAdvertiserPlacementId = HashBasedTable
			.create();
	private static HashMap<Integer, ConfigData.Campaign> placementByCampaignId = new HashMap<>();
	private static Map<Integer, Deal> marketplacePlacementByPlacementId = new HashMap<>();

	public static ConfigData parseXML(File file)
	{
		ConfigData configdata = null;

		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(ConfigData.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			configdata = (ConfigData) jaxbUnmarshaller.unmarshal(file);
		}

		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		return configdata;

	}

	public static HashBasedTable<Integer, Integer, ConfigData.Campaign> getCampaignTable(ConfigData configData)
	{
		List<ConfigData.Advertiser> advertisers = configData.advertisers;
		for (ConfigData.Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				campaignsByAdvertiserPlacementId.put(advertiser.id, campaign.id, campaign);
			}
		}
		return campaignsByAdvertiserPlacementId;
	}

	public static HashMap<Integer, ConfigData.Campaign> getCampaignMap(ConfigData configData)
	{
		List<ConfigData.Advertiser> advertisers = configData.advertisers;
		for (ConfigData.Advertiser advertiser : advertisers)
		{
			List<ConfigData.Campaign> campaigns = advertiser.campaigns;
			for (ConfigData.Campaign campaign : campaigns)
			{
				placementByCampaignId.put(campaign.id, campaign);
			}
		}
		return placementByCampaignId;
	}

	public static Map<Integer, Deal> getDealMap(ConfigData configData)
	{
		List<Deal> deals = configData.deals;
		for (Deal deal : deals)
		{
			marketplacePlacementByPlacementId.put(deal.id, deal);
		}
		return marketplacePlacementByPlacementId;
	}

}
