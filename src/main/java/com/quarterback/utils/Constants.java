/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import com.osapi.enumtypes.FileResourcesType;
import com.osapi.enumtypes.ObjectType;
import com.osapi.enumtypes.QueryMethod;
import com.osapi.factory.BaseEndPointURLFactory;
import com.osapi.factory.BaseFilePathFactory;

import java.io.File;

/**
 * @author Clearstream
 */
public class Constants
{
	// User Details for OS
	public static final String USER_ID = "jaym@cybage.com";
	public static final String LAUNCHCAMPAIGN_USERID = "launch@cybage.com";

	//Constansts Strings
	public static final String DAYPARTS_EXCLUDEDHOURS_KEY ="Other";
	public static final String SSP_EXCLUDEDSSP_KEY ="Other";

	// Execution Environment
	public static final String EXECUION_ENVIRONMENT_DEFAULT = "development";
	public static final String API_VERSION_DEFAULT = "v2";
	public static final String EXECUTION_ENVIRONMENT = System.getProperty("testEnvironment") != null
			? System.getProperty("testEnvironment")
			: EXECUION_ENVIRONMENT_DEFAULT;


	// BASE URLs path
	public static final String USERS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.USERS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String SIGNIN_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.SIGN_IN, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String AGENCY_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.AGENCIES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String BRAND_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.BRANDS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String CAMPAIGN_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.CAMPAIGNS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String PLACEMENT_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.PLACEMENTS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String FLIGHT_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.FLIGHTS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String PIXELS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.ACTIVITYPIXELS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String SELLERNETWORKMASTERS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.SSP, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String LANGUAGES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.LANGUAGES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String REGIONS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.REGIONS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String VISIBILITIES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.VISIBILITIES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String PLAYERSIZEGROUP_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.PLAYERSIZEGROUP, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String COUNTRIES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.COUNTRIES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String PCATEGORIES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.PCATEGORIES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String DMAS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.DMAS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String INVENTORYTYPES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.INVENTORYTYPES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String ZIPCODES_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.ZIPCODES, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String GEOLISTS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.GEOLISTS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String DMPPROVIDERS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.DMPPROVIDERS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();
	public static final String DATASEGEMENTS_BASEENDPOINT_URL = BaseEndPointURLFactory
			.getBaseEndPointURLBuilderWith(ObjectType.DATASEGMENTS, EXECUTION_ENVIRONMENT).buildBaseEndPointURL();


	// AWS Constants
	public static final String S3_BASEURL_ENDPOINT = "https://s3.amazonaws.com/";
	public static final String AWS_RESOURCEPATH = "src\\test\\resources\\config\\awscredentials";
	public static final String S3_BUCKET_DEVELOPMENT = "clearstream-media-tasks-development";
	public static final String S3_BUCKET_STAGING = "clearstream-media-tasks-sandbox";
	public static final int TASKGROUP_POOLING_TIMEOUT_IN_MINUTE = 60;
	public static final int TASKGROUP_POOLING_INTERVAL_IN_MINUTE = 1;
	public static final String S3_BUCKET_MEDIAFILEUPLOADS = "clearstream-media-files-upload";
	public static final String S3_BUCKET_MEDIAFILEPROCESSING = "clearstream-media-files";
	public static final String S3_BUCKET_MEDIAFILEUPLOADS_ENVIRONMENT = EXECUTION_ENVIRONMENT;

	// XML Local File Paths
	public static final String CAMPAIGN_XML_PATH = ".//TestDataQuarterback//";
	public static final String CAMPAIGN_XML_NAME = "campaign";
	public static final String CAMPAIGN_XML_ZIP_FILE_PATH = ".//TestDataQuarterback//campaign_xml";
	public static final String XML_LOCAL_PATH = "src/test/resources/" + "local_config.properties";
	public static final String RNG_SCHEMA_FILEPATH = "src\\test\\resources\\config\\validate.rng";

	// Database Config File
	public static final String DATABASE_CONFIG = "src\\test\\resources\\config\\databaseconfig.ini";
	public static final String DB_ENV_DEV = EXECUTION_ENVIRONMENT;
	public static final String HIBERNATE_CONFIG_DEV = "src\\main\\resources\\HibernateConfigs\\devlopment\\hibernate.cfg.xml";
	public static final String HIBERNATE_CONFIG_STAGING = "src\\main\\resources\\HibernateConfigs\\staging\\hibernate.cfg.xml";

	// Base Path for Test Data
	public static final String CAMPAIGNDATA_BASEPATH = ".//TestDataOS";

	public static final File TESTDATACONFIG = new File("src\\test\\resources\\config\\testdataconfig.ini");

	// FILE RESOURCES PATH
	public static final String DISPLAYASSET_ZIP_UPLOAD = BaseFilePathFactory
			.getFilePathBuilderWith(FileResourcesType.DISPLAY_ZIP, QueryMethod.FILERESOURCES).getUploadFilePath();
	public static final String DISPLAYASSET_IMAGE_UPLOAD = BaseFilePathFactory
			.getFilePathBuilderWith(FileResourcesType.DISPLAY_IMAGE, QueryMethod.FILERESOURCES).getUploadFilePath();
	public static final String VIDEOASSET_CLEARSTREAM_UPLOAD = BaseFilePathFactory
			.getFilePathBuilderWith(FileResourcesType.VIDEOASSET_CLEARSTREAM, QueryMethod.FILERESOURCES)
			.getUploadFilePath();

	//TARGETINGDATASOURCES
	public static final String GEOLIST_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.GEOLISTS, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String LANGUAGES_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.LANGUAGES, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String REGIONS_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.REGIONS, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String VISIBILITIES_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.VISIBILITIES, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String PLAYERSIZEGROUP_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PLAYERSIZEGROUP, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String COUNTRIES_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.COUNTRIES, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String PCATEGORIES_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PCATEGORIES, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String DMAS_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DMAS, QueryMethod.TARGETINGDATA).getFilePath();
	public static final String INVENTORYTYPES_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.INVENTORYTYPES, QueryMethod.TARGETINGDATA).getFilePath();

	//SEGMENTS DATA SOURCES
	public static final String DMPPROVIDERS_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DMPPROVIDERS, QueryMethod.SEGMENTSDATA).getFilePath();
	public static final String DATASEGMENTS_MASTERDATAJSON = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DATASEGMENTS, QueryMethod.SEGMENTSDATA).getFilePath();


	// NEWREQUESTFILEPATH
	public static final String NEWPIXEL_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ACTIVITYPIXELS, QueryMethod.NEWENTITYREQUEST).getFilePath();
	public static final String NEWPLACEMENT_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PLACEMENTS, QueryMethod.NEWENTITYREQUEST).getFilePath();
	public static final String NEWFLIGHT_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FLIGHTS, QueryMethod.NEWENTITYREQUEST).getFilePath();
	public static final String NEWCAMPAIGN_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CAMPAIGNS, QueryMethod.NEWENTITYREQUEST).getFilePath();
	public static final String NEWVIDEOASSET_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.VIDEOASSETS, QueryMethod.NEWENTITYREQUEST).getFilePath();
	
	// TEMPLATES FILE PATH
	public static final String LOGINREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SIGN_IN, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String AGENCYREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.AGENCIES, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String BRANDREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.BRANDS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String PIXELREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ACTIVITYPIXELS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String CAMPAIGNREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CAMPAIGNS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String LAUNCHCAMPAIGNREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.LAUNCHCAMPAIGN, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String ASSETREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.VIDEOASSETS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String SURVEYREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SURVEY, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String DISPLAYREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DISPLAY, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String PLACEMENTREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PLACEMENTS, QueryMethod.REQUESTTEMPLATES).getFilePath();
	
	public static final String FRAUDSREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FRAUDS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String CONVERSIONPIXELREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CONVERSIONPIXELS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String DEALSANDSSEATSPLACEMENTREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DEALSANDSEATS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String SSPREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SSP, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String FLIGHTREQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FLIGHTS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String MEDIALISTS_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.MEDIALIST, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String SMARTLISTS_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SMARTLIST, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String ADDTIONALOPTION_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ADDITIONALOPTIONS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String TECHNOLOGIES_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.TECHNOLOGY, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String GEOS_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.GEOS, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String DAYPARTS_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DAYPART, QueryMethod.REQUESTTEMPLATES).getFilePath();

	public static final String CATEGORIES_REQUEST_TEMPLATE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CATEGORIES, QueryMethod.REQUESTTEMPLATES).getFilePath();

	// RequestData File Paths
	public static final String LOGIN_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SIGN_IN, QueryMethod.REQUEST).getFilePath();
	
	public static final String AGENCY_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.AGENCIES, QueryMethod.REQUEST).getFilePath();

	public static final String BRAND_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.BRANDS, QueryMethod.REQUEST).getFilePath();

	public static final String PIXEL_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ACTIVITYPIXELS, QueryMethod.REQUEST).getFilePath();

	public static final String CAMPAIGN_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CAMPAIGNS, QueryMethod.REQUEST).getFilePath();

	public static final String ASSET_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.VIDEOASSETS, QueryMethod.REQUEST).getFilePath();

	public static final String DISPLAY_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DISPLAY, QueryMethod.REQUEST).getFilePath();

	public static final String SURVEY_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SURVEY, QueryMethod.REQUEST).getFilePath();

	public static final String PLACEMENT_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PLACEMENTS, QueryMethod.REQUEST).getFilePath();

	public static final String CONVERSIONPIXELS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CONVERSIONPIXELS, QueryMethod.REQUEST).getFilePath();
	
	public static final String FRAUDS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FRAUDS, QueryMethod.REQUEST).getFilePath();
	
	public static final String SSP_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SSP, QueryMethod.REQUEST).getFilePath();

	public static final String DEALSANDSEATS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DEALSANDSEATS, QueryMethod.REQUEST).getFilePath();

	public static final String FLIGHTS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FLIGHTS, QueryMethod.REQUEST).getFilePath();;

	public static final String MEDIALISTS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.MEDIALIST, QueryMethod.REQUEST).getFilePath();

	public static final String SMARTLISTS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SMARTLIST, QueryMethod.REQUEST).getFilePath();

	public static final String TECHNOLOGIES_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.TECHNOLOGY, QueryMethod.REQUEST).getFilePath();

	public static final String GEOS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.GEOS, QueryMethod.REQUEST).getFilePath();

	public static final String DAYPARTS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DAYPART, QueryMethod.REQUEST).getFilePath();

	public static final String CATEGORIES_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CATEGORIES, QueryMethod.REQUEST).getFilePath();

	public static final String ADDITIONALOPTIONS_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ADDITIONALOPTIONS, QueryMethod.REQUEST).getFilePath();

	public static final String LAUNCHCAMPAIGN_REQUEST_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.LAUNCHCAMPAIGN, QueryMethod.REQUEST).getFilePath();

	// ResponseData File Paths
	public static final String AGENCY_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.AGENCIES, QueryMethod.RESPONSE).getFilePath();

	public static final String BRAND_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.BRANDS, QueryMethod.RESPONSE).getFilePath();

	public static final String PIXEL_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ACTIVITYPIXELS, QueryMethod.RESPONSE).getFilePath();

	public static final String CAMPAIGN_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CAMPAIGNS, QueryMethod.RESPONSE).getFilePath();

	public static final String LAUNCHEDCAMPAIGN_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ACTIVECAMPAIGN, QueryMethod.RESPONSE).getFilePath();

	public static final String ASSET_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.VIDEOASSETS, QueryMethod.RESPONSE).getFilePath();

	public static final String SURVEY_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SURVEY, QueryMethod.RESPONSE).getFilePath();

	public static final String DISPLAY_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DISPLAY, QueryMethod.RESPONSE).getFilePath();

	public static final String PLACEMENT_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.PLACEMENTS, QueryMethod.RESPONSE).getFilePath();

	public static final String CONVERSIONPIXELS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CONVERSIONPIXELS, QueryMethod.RESPONSE).getFilePath();
	
	public static final String FRAUDS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FRAUDS, QueryMethod.RESPONSE).getFilePath();
	
	public static final String SSP_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SSP, QueryMethod.RESPONSE).getFilePath();

	public static final String DEALSANDSEATS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DEALSANDSEATS, QueryMethod.RESPONSE).getFilePath();

	public static final String FLIGHTS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.FLIGHTS, QueryMethod.RESPONSE).getFilePath();

	public static final String MEDIALIST_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.MEDIALIST, QueryMethod.RESPONSE).getFilePath();

	public static final String SMARTLIST_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.SMARTLIST, QueryMethod.RESPONSE).getFilePath();

	public static final String ADDTIONALOPTION_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ADDITIONALOPTIONS, QueryMethod.RESPONSE).getFilePath();

	public static final String TECHNOLOGIES_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.TECHNOLOGY, QueryMethod.RESPONSE).getFilePath();

	public static final String GEOS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.GEOS, QueryMethod.RESPONSE).getFilePath();

	public static final String ZIPCODES_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.ZIPCODES, QueryMethod.RESPONSE).getFilePath();

	public static final String DAYPARTS_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.DAYPART, QueryMethod.RESPONSE).getFilePath();

	public static final String CATEGORIES_RESPONSE_DATA = BaseFilePathFactory
			.getFilePathBuilderWith(ObjectType.CATEGORIES, QueryMethod.RESPONSE).getFilePath();

}
