/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Clearstream
 */
public class WaitUtils
{

	private static final Logger LOG = LogManager.getLogger(WaitUtils.class);

	/**
	 * waiting for seconds
	 *
	 * @param timeoutInSeconds
	 *            timeout in seconds for wait
	 */
	public static void waitForMinutes(int waitInMins)
	{
		try
		{
			Thread.sleep(waitInMins * 1000 * 60);
		}
		catch (InterruptedException e)
		{
			LOG.error("Thread interrupted error while timeout: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
