/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quarterback.utils;

import com.thaiopensource.validate.ValidationDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

/**
 * @author Clearstream
 */
public class RNGValidator
{
	private static final ValidationDriver validationDriver = new ValidationDriver();
	private static final Logger LOG = LoggerFactory.getLogger(RNGValidator.class);

	public static boolean validate(File rng_schema, File source_file)
	{
		boolean valid = Boolean.FALSE;
		InputSource rng_source = ValidationDriver.fileInputSource(rng_schema);
		InputSource file_source = ValidationDriver.fileInputSource(source_file);
		try
		{
			validationDriver.loadSchema(rng_source);
			valid = validationDriver.validate(file_source);
		}
		catch (SAXException | IOException e)
		{
			LOG.error("An error occurred while validating xml - {}", e.getMessage());
		}
		return valid;
	}

}
