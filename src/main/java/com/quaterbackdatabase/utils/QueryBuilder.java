/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quaterbackdatabase.utils;

import org.apache.log4j.Logger;

/**
 * @author Clearstream
 */
public class QueryBuilder
{

	private static final Logger LOG = Logger.getLogger(QueryBuilder.class);

	public String getPlacementsQuery()
	{

		String query = null;

		LOG.info("Executing Placements Query");
		query = "select distinct p.id as placementId "
				+ "				   from campaigns c, placement_groups pg, placements p, flights f, ads a "
				+ "				   where c.id = pg.campaign_id and pg.id = p.placement_group_id and p.id = f.placement_id "
				+ "				   and p.state_id = 1 and c.state_id = 2 and c.campaign_type_id in (1, 2) "
				+ "				   and f.id = a.flight_id and f.end_date >= now() and f.active = true"
				+ "				   and ((p.product_type_id = 1 and a.ad_type = 'VIDEO' and (a.budget_percentage > 0 or budget_percentage is null)) "
				+ "				   or (p.product_type_id = 2 and a.ad_type = 'HTML' and (a.budget_percentage > 0 or budget_percentage is null)) "
				+ "				   or (p.product_type_id = 5 and a.budget_percentage > 0) "
				+ "				   or (p.product_type_id = 3 and a.ad_type = 'HTML') "
				+ "				   or (p.product_type_id = 4 and a.ad_type = 'HTML')) and a.ad_tag is not null";

		return query;

	}

	public String getCampaignsMetadataQuery()
	{

		String query = null;
		LOG.info("Executing CampaignsMetadata Query");

		query = " select c.id as campaignId, c.agency_id as agencyId, a.id as adId "
				+ "				   from campaigns c, placement_groups pg, placements p, flights f, ads a "
				+ "				   where c.id = pg.campaign_id and pg.id = p.placement_group_id and p.id = f.placement_id "
				+ "				   and p.state_id = 1 and c.state_id = 2 and c.campaign_type_id in (1) "
				+ "				   and f.id = a.flight_id and f.end_date >= now() and f.active = true"
				+ "				   and ((p.product_type_id = 1 and a.ad_type = 'VIDEO' and (a.budget_percentage > 0 or budget_percentage is null)) "
				+ "				   or (p.product_type_id = 2 and a.ad_type = 'HTML' and (a.budget_percentage > 0 or budget_percentage is null)) "
				+ "				   or (p.product_type_id = 5 and a.budget_percentage > 0) "
				+ "				   or (p.product_type_id = 3 and a.ad_type = 'HTML') "
				+ "				   or (p.product_type_id = 4 and a.ad_type = 'HTML')) and a.ad_tag is not null ";

		return query;

	}

}
