package com.quaterbackdatabase.utils;

import javax.annotation.Nullable;

public enum MarketplaceUrl
{

	SC("http://us-east.bidswitch.net/clearstream_bid"),
	OR("http://us-west.bidswitch.net/clearstream_bid"),
	BE("http://eu.bidswitch.net/clearstream_bid"),
	VA("http://clearstream-east.brealtime.com/"),
	CA("http://clearstream-west.brealtime.com/"),
	FR("http://clearstream-euro.brealtime.com/");

	private String url;

	MarketplaceUrl(String url)
	{
		this.url = url;
	}

	public String url()
	{
		return url;
	}

	@Nullable
	public static MarketplaceUrl fromId(String Id)
	{
		for (MarketplaceUrl url : values())
		{
			if (url.name().toLowerCase().equals(Id))
			{
				return url;
			}
		}
		return null;
	}
}
