/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quaterbackdatabase.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Clearstream
 */
public class ActiveCampaignMetadata

{
	public static ResultSet getActiveCampaignMetadataFromDB(Connection connection)
	{
		QueryBuilder query = new QueryBuilder();
		ResultSet result = null;
		try
		{
			String activeCampaignMetadataQuery = query.getCampaignsMetadataQuery();
			PreparedStatement statement = connection.prepareStatement(activeCampaignMetadataQuery,
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			result = statement.executeQuery();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public static List<Integer> getActivePlacementsFromDB(Connection connection)
	{
		QueryBuilder query = new QueryBuilder();
		String activePlacementsQuery = query.getPlacementsQuery();
		List<Integer> activePlacementsList = new ArrayList<>();
		try
		{
			PreparedStatement statement = connection.prepareStatement(activePlacementsQuery);
			ResultSet result = statement.executeQuery();

			while (result.next())
			{

				activePlacementsList.add(result.getInt("placementId"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return activePlacementsList;
	}

}
