/*
 * Copyright (C) 2018 Clearstream.TV, Inc. All Rights Reserved.
 * Proprietary and confidential.
 */
package com.quaterbackdatabase.utils;

import com.quarterback.utils.Constants;
import org.apache.log4j.Logger;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Clearstream
 */
public class CreatePostgreDBConnection
{
	private static final Logger log = Logger.getLogger(CreatePostgreDBConnection.class);

	private Connection conn;

	public CreatePostgreDBConnection()
	{
	}

	/**
	 * Description: Set POSTGRES database connection
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws InvalidFileFormatException
	 */

	public void setConnection() throws InvalidFileFormatException, FileNotFoundException, IOException
	{
		Ini configIni = new Ini();
		configIni.load(new FileReader(Constants.DATABASE_CONFIG));
		String executionEnvironment = System.getProperty("databaseEnv") != null ? System.getProperty("databaseEnv") : Constants.DB_ENV_DEV;
		Ini.Section envValue = configIni.get(executionEnvironment);

		String connString = envValue.fetch("HOST_ADDRESS") + envValue.fetch("DATABASE_NAME");
		String uname = envValue.fetch("UNAME");
		String password = envValue.fetch("PASSWORD");

		try
		{
			this.conn = DriverManager.getConnection(connString, uname, password);
			this.conn.setAutoCommit(true);
			log.info("Postgre Connection : Connection Successfull");
		}
		catch (SQLException e)
		{
			log.info(e.getMessage());
		}
	}

	/**
	 * Description: Get POSTGRES database Connection
	 * 
	 * @return
	 */
	public Connection getConnection()
	{
		return this.conn;
	}

	/**
	 * Description: close POSTGRES database
	 */
	public void closeConnection(Connection conn)
	{
		try
		{
			conn.close();
			log.info("Postgre Connection : Connection Closed");
		}
		catch (SQLException e)
		{
			log.info(e.getMessage());
		}
	}

}
